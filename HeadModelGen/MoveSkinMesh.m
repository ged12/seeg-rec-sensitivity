function MoveSkinMesh(ID)
info=niftiinfo(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/work/MRI.nii']);
load(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/ConductivityTensors.mat'])


TR=stlread(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/MeshLayers/SkinMeshFinal2.stl']);
points=TR.Points.*info.PixelDimensions;
offsetx=min(tetmesh.node(1,:))-min(points(:,1));%match right
offsety=min(tetmesh.node(2,:))-min(points(:,2));%match posterior
offsetz=max(tetmesh.node(3,:))-max(points(:,3));%match superior
offset=[offsetx offsety offsetz];
disp(offset)
TR2=triangulation(TR.ConnectivityList,TR.Points.*info.PixelDimensions+offset);
TR=stlread(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/MeshLayers/MRI_brain_outskin_mesh.stl']);
points=TR.Points.*info.PixelDimensions;
offsetx=min(tetmesh.node(1,:))-min(points(:,1));%match right
offsety=min(tetmesh.node(2,:))-min(points(:,2));%match posterior
offsetz=max(tetmesh.node(3,:))-max(points(:,3));%match superior
offset=[offsetx offsety offsetz];
% offset=(max(tetmesh.node')+min(tetmesh.node'))./2-(max(TR.Points.*info.PixelDimensions)+min(TR.Points.*info.PixelDimensions))./2;
disp(offset)

stlwrite(TR2,['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/MeshLayers/SkinMeshFinal.stl']);

TR=stlread(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/MeshLayers/MRI_brain_mesh.stl']);
TR3=triangulation(TR.ConnectivityList,TR.Points.*info.PixelDimensions+offset);

stlwrite(TR3,['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/MeshLayers/BrainMesh.stl']);

TR=stlread(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/MeshLayers/MRI_brain_outskull_mesh.stl']);
TR3=triangulation(TR.ConnectivityList,TR.Points.*info.PixelDimensions+offset);

stlwrite(TR3,['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/MeshLayers/OutSkullMesh.stl']);

[v,f]=read_vtk(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/MeshLayers/MRI_brain_outskin_mesh.vtk']);

vtkwrite(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/MeshLayers/SkinMeshFinal.vtk'],...
    'polydata','triangle',v(1,:).*info.PixelDimensions(1)+offset(1),v(2,:).*info.PixelDimensions(2)+offset(2)...
    ,v(3,:).*info.PixelDimensions(3)+offset(3),f')

TRL=stlread(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/work/lh_brain_final2.stl']);
TRR=stlread(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/work/rh_brain_final2.stl']);

f=[TRL.ConnectivityList;TRR.ConnectivityList+size(TRL.Points,1)];
v=[TRL.Points;TRR.Points];

scirunfield.node=v';
scirunfield.face=f';
save(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/work/SmoothedBrainMesh2.mat'],'scirunfield');

TRL=stlread(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/work/lh_brain_final.stl']);
TRR=stlread(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/work/rh_brain_final.stl']);

f=[TRL.ConnectivityList;TRR.ConnectivityList+size(TRL.Points,1)];
v=[TRL.Points;TRR.Points];

scirunfield.node=v';
scirunfield.face=f';
save(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/work/SmoothedBrainMesh.mat'],'scirunfield');


homeDirectory=['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/'];
fid = fopen([homeDirectory '/work/MapVectorsOntoSmoothedBrain2.py'],'w');
fprintf(fid,'import os\n');
fprintf(fid,['scirun_load_network("' homeDirectory '../MapNormalsOntoRemeshedBrain.srn5")\n']);
fprintf(fid,['scirun_set_module_state("ReadField:56","Filename","' homeDirectory 'work/LinkVectors.mat")\n']);
fprintf(fid,['scirun_set_module_state("ReadField:57","Filename","' homeDirectory 'work/SmoothedBrainMesh2.mat")\n']);
fprintf(fid,['scirun_set_module_state("WriteField:1","Filename","' homeDirectory 'work/SmoothedBrainWithVectors2.mat")\n']);
fprintf(fid,['scirun_save_network("' homeDirectory 'work/MapVectorsPatient2.srn5")\n']);
fclose(fid);

homeDirectory=['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID) '/'];
fid = fopen([homeDirectory '/work/MapVectorsOntoSmoothedBrain.py'],'w');
fprintf(fid,'import os\n');
fprintf(fid,['scirun_load_network("' homeDirectory '../MapNormalsOntoRemeshedBrain.srn5")\n']);
fprintf(fid,['scirun_set_module_state("ReadField:56","Filename","' homeDirectory 'work/LinkVectors.mat")\n']);
fprintf(fid,['scirun_set_module_state("ReadField:57","Filename","' homeDirectory 'work/SmoothedBrainMesh.mat")\n']);
fprintf(fid,['scirun_set_module_state("WriteField:1","Filename","' homeDirectory 'work/SmoothedBrainWithVectors.mat")\n']);
fprintf(fid,['scirun_save_network("' homeDirectory 'work/MapVectorsPatient.srn5")\n']);
fclose(fid);

