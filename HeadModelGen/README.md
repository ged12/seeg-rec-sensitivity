# Head Model Generation

With this code, we semiautomated the generation of patient specific head models. 

Running the full version requires a SLURM-based computing cluster (Duke Computing Cluster) and integration with the main simulation code pipeline in other sections. 

## Software and hardware requirements:
#### Full Version
- SLURM cluster (with ≥ 20 CPUs total, ≥ 20 CPUs/node and ≥ 120 GB / node). 
- matlabR2019a
- SCIRun-5.0-beta.Y 
- Python version 3.9.13 with packages: 
    * nilearn, nibabel, numpy, skimage
- freesurfer v6.0.0
- fsl 6.1
- dcm2niix

## Running the full version
It is easiest to run the semiautomated pipeline in an interactive slurm session.

1.	Activate the conda environment with all the necessary python packages and make sure all the required executables are in the current folder.

2.  Run MakePatient.m.

3.  Place the patient specific neuroimaging into the corresponding folders (MRI, CT, DTI)
    Inputs: 
    - MRI, CT, and DTI in Dicom format - example in InputImaging/Patient25
    - result{patientNum}.csv -> labels electrodes, provides entry and target locations, and provides electrode types - example in InputImaging/Patient25

4.  Run each line of Master.slurm in the command line.
	
5.  The manual tasks are commented in the place that they need to be conducted in Master.slurm.

6.  This script will create the mesh objects, electrode contact locations, and tissue properties that can be used in subsequent analyses.
