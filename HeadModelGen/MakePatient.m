function MakePatient(ids)
for i=1:length(ids)
    mkdir(['Patient' num2str(ids(i))])
    mkdir(['Patient' num2str(ids(i)) '/neuroimaging'])
    mkdir(['Patient' num2str(ids(i)) '/neuroimaging/MRI'])
    mkdir(['Patient' num2str(ids(i)) '/neuroimaging/CT'])
    mkdir(['Patient' num2str(ids(i)) '/neuroimaging/DTI'])
    mkdir(['Patient' num2str(ids(i)) '/Ephys'])
end
