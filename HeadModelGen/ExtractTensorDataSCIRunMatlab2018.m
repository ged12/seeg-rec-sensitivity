function ExtractTensorDataSCIRunMatlab2018(PatientID)
patientID=['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(PatientID)];
%% Extract CSF
%This line would create a structure named V with all the information about the NIfTI file
CSF = niftiread([patientID '/MeshLayers/TissueLayers_pve_0.nii']);
% CSF=cat(3,CSF,zeros(512,512));

%% Extract Gray
Gray = niftiread([patientID '/MeshLayers/TissueLayers_pve_1.nii']);

%% Extract White
White = niftiread([patientID '/MeshLayers/TissueLayers_pve_2.nii']);

%% Extract Skin
Skin = niftiread([patientID '/MeshLayers/MRI_brain_outskin_mask.nii']);

%% Extract Skull
Skull = niftiread([patientID '/MeshLayers/MRI_brain_outskull_mask.nii']);

%% Eigen Vectors
WhiteV1 = niftiread([patientID '/work/dti_V1_Reg.nii']);
WhiteV2 = niftiread([patientID '/work/dti_V2_Reg.nii']);
WhiteV3 = niftiread([patientID '/work/dti_V3_Reg.nii']);

%% Eigen Values
WhiteE1 = niftiread([patientID '/work/DTI_L1_Reg.nii']);
WhiteE2 = niftiread([patientID '/work/DTI_L2_Reg.nii']);
WhiteE3 = niftiread([patientID '/work/DTI_L3_Reg.nii']);

%% Initialize SCIRUN structure
tetmesh.node=[];
tetmesh.field=[];
scale = 1000;%changes from S/m to S/mm
[xDim,yDim,zDim]=size(White);

header=niftiinfo([patientID '/MeshLayers/TissueLayers_pve_0.nii']);
transform = header.Transform.T;
coords=find(Skin>0);

tetmesh.node=zeros(3,length(coords));
tetmesh.field=zeros(6,length(coords));
%% Define Conductivities of Tissues
white = 0.15;
gray = 0.23;
csf=1.79;
skull=0.01;
skin=0.2;
%% Make Conductivity Tensor Field
tic
[x,y,z]=ind2sub(size(CSF),coords);
disp(max(x))
disp(min(x))
disp(max(y))
disp(min(y))
disp(max(z))
disp(min(z))
for i=1:length(coords)
    [xct,yct,zct]=ind2sub(size(Skin),coords(i));
%     index=sub2ind(size(White),xct,yct,zct);
    loc=[];
    data=[];
    %% White Matter
    if White(xct,yct,zct)>.5
        if(WhiteE1(xct,yct,zct)>0 & WhiteE2(xct,yct,zct)>0 & WhiteE3(xct,yct,zct)>0)
            w12=power(abs(WhiteE1(xct,yct,zct)./WhiteE2(xct,yct,zct)),0.8);
            w13=power(abs(WhiteE1(xct,yct,zct)./WhiteE3(xct,yct,zct)),0.8);
            theta=2.15*w12*1.85*w13./((power(1.21,0.8)+w12).*(power(1.12,0.8)+w13));
            EL1=white*theta./scale;
            EL2=white*theta*power(WhiteE1(xct,yct,zct)./WhiteE2(xct,yct,zct),-1)./scale;
            EL3=white*theta*power(WhiteE1(xct,yct,zct)./WhiteE3(xct,yct,zct),-1)./scale;
            D=[EL1 0 0;0 EL2 0;0 0 EL3];
            EV1=WhiteV1(floor(xct),floor(yct),floor(zct),:);
            EV2=WhiteV2(floor(xct),floor(yct),floor(zct),:);
            EV3=WhiteV3(floor(xct),floor(yct),floor(zct),:);
            VMat =[EV1(1) EV2(1) EV3(1);EV1(2) EV2(2) EV3(2);EV1(3) EV2(3) EV3(3)];
            M=VMat*D;
            M=M*VMat';
%             if sum(sum(M))~=0
%                 %                         loc=XYZ(:,index);
%                 data=[M(1,1);M(1,2);M(1,3);M(2,2);M(2,3);M(3,3)];
%                
%             end
            data=[white;0;0;white;0;white]./scale;
        end
        %% Gray Matter
    elseif Gray(xct,yct,zct)>.5
        if(WhiteE1(xct,yct,zct)>0 & WhiteE2(xct,yct,zct)>0 & WhiteE3(xct,yct,zct)>0)
            w12=power(abs(WhiteE1(xct,yct,zct)./WhiteE2(xct,yct,zct)),0.8);
            w13=power(abs(WhiteE1(xct,yct,zct)./WhiteE3(xct,yct,zct)),0.8);
            theta=2.15*w12*1.85*w13./((power(1.21,0.8)+w12).*(power(1.12,0.8)+w13));
            EL1=gray*theta./scale;
            EL2=gray*theta*power(WhiteE1(xct,yct,zct)./WhiteE2(xct,yct,zct),-1)./scale;
            EL3=gray*theta*power(WhiteE1(xct,yct,zct)./WhiteE3(xct,yct,zct),-1)./scale;
            D=[EL1 0 0;0 EL2 0;0 0 EL3];
            EV1=WhiteV1(floor(xct),floor(yct),floor(zct),:);
            EV2=WhiteV2(floor(xct),floor(yct),floor(zct),:);
            EV3=WhiteV3(floor(xct),floor(yct),floor(zct),:);
            VMat =[EV1(1) EV2(1) EV3(1);EV1(2) EV2(2) EV3(2);EV1(3) EV2(3) EV3(3)];
            M=VMat*D;
            M=M*VMat';
%             if sum(sum(M))~=0
%                 %                         loc=XYZ(:,index);
%                 data=[M(1,1);M(1,2);M(1,3);M(2,2);M(2,3);M(3,3)];
%             end
            data=[gray;0;0;gray;0;gray]./scale;
        end
        %% CSF
    elseif CSF(xct,yct,zct)>.5
        if(WhiteE1(xct,yct,zct)>0 & WhiteE2(xct,yct,zct)>0 & WhiteE3(xct,yct,zct)>0)
            w12=power(abs(WhiteE1(xct,yct,zct)./WhiteE2(xct,yct,zct)),0.8);
            w13=power(abs(WhiteE1(xct,yct,zct)./WhiteE3(xct,yct,zct)),0.8);
            theta=2.15*w12*1.85*w13./((power(1.21,0.8)+w12).*(power(1.12,0.8)+w13));
            EL1=csf*theta./scale;
            EL2=csf*theta*power(WhiteE1(xct,yct,zct)./WhiteE2(xct,yct,zct),-1)./scale;
            EL3=csf*theta*power(WhiteE1(xct,yct,zct)./WhiteE3(xct,yct,zct),-1)./scale;
            D=[EL1 0 0;0 EL2 0;0 0 EL3];
            EV1=WhiteV1(floor(xct),floor(yct),floor(zct),:);
            EV2=WhiteV2(floor(xct),floor(yct),floor(zct),:);
            EV3=WhiteV3(floor(xct),floor(yct),floor(zct),:);
            VMat =[EV1(1) EV2(1) EV3(1);EV1(2) EV2(2) EV3(2);EV1(3) EV2(3) EV3(3)];
            M=VMat*D;
            M=M*VMat';
%             if sum(sum(M))~=0
%                 %                         loc=XYZ(:,index);
%                 data=[M(1,1);M(1,2);M(1,3);M(2,2);M(2,3);M(3,3)];
%             end
            data=[csf;0;0;csf;0;csf]./scale;
        end
        %% Skull
    elseif Skull(xct,yct,zct)>0
        %                 loc=XYZ(:,index);
        data=[skull;0;0;skull;0;skull]./scale;
        %% Skin
    elseif Skin(xct,yct,zct)>0
        %                 loc=XYZ(:,index);
        data=[skin;0;0;skin;0;skin]./scale;
    end
    if ~isempty(data)
        loc=[xct yct zct 1]*transform;
        loc=loc(1:3)';
        tetmesh.node(:,i)=loc;
        tetmesh.field(:,i)=data;
    end
end
omit=find(sum(tetmesh.node==0));
tetmesh.node(:,omit)=[];
tetmesh.field(:,omit)=[];
toc
save([patientID '/ConductivityTensors_ISO.mat'],'tetmesh')%make sure to parameterize the file naming, so you can batch it on the cluster

