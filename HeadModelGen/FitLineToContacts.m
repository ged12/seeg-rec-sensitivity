    function FitLineToContacts(ID)
patientID=['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(ID)];

Elecs=readtable([patientID '/result' num2str(ID) '.csv']);
numElecs=length(Elecs.ElectrodeType);
load('/hpc/group/wmglab/bjt20/HeadModelAutomation/ElectrodeLibrary/ElectrodeSpacingMap.mat')
spacings=[];
contactLength=[];
target=[];
for i=1:numElecs
    spacings=[spacings ElectrodeSpacingMap(Elecs.ElectrodeType{i})];
    target=[target;[Elecs.targetx Elecs.targety Elecs.targetz]];
    if all(Elecs.ElectrodeType{i}(1:3)=='PMT')
        contactLength=2;
    else
        contactLength=2.29;
    end
end

data = dlmread([patientID '/work/ContactsInMRI.txt'], ' ', 1, 0);
data=data(:,1:2:5);

contactLabels = dlmread([patientID '/work/electrodes.csv'], ',', 3, 11);
contactLabels=contactLabels(:,1);
numcontacts=[];
prev=0;
for i=1:length(contactLabels)
    if contactLabels(i)<prev
        numcontacts=[numcontacts prev+1];
    end
    prev=contactLabels(i);
end
numcontacts=[numcontacts contactLabels(end)+1];
% spacings=[1.5 1.5 2.43 2.43 1.5 1.5 1.5 1.5 1.5 1.5 1.5 1.5 1.5];
% contactLength=2;
start=1;
startLocs=[];
vectors=[];
figure
hold on
contacts=[];
for i=1:length(numcontacts)
    spacing=spacings(i);
    total=spacing+contactLength;
    r0=mean(data(start:(start+numcontacts(i)-1),:));
    data2=data(start:(start+numcontacts(i)-1),:)-r0;
    [~,~,V]=svd(data2,0);
    vector=V(:,1);
    
    ends=[r0+vector'.*(-(numcontacts(i)/2-1)*total-total/2);r0+vector'.*((numcontacts(i)/2-1)*total+total/2)];
    dists=sqrt(sum((data(start,:)-ends).^2,2));
    startLoc=ends(dists==min(dists),:);
    shift=[0 0 0];
    if sum(sqrt(sum((data(start,:)-data).^2,2))<1.5)>1
        if min(sqrt(sum((target-data(start,:)).^2,2)))>2
            shift=(vector.*(contactLength+spacing))';
            startLoc=startLoc+(vector.*(contactLength+spacing))';
        end
    end
    vector=ends(sum(ends==(startLoc-shift),2)==0,:)-ends(sum(ends==(startLoc-shift),2)==3,:);
    vector=vector./norm(vector);
    start=start+numcontacts(i);
    startLocs=[startLocs;startLoc];
    contacts=[contacts;(startLoc+vector.*(0:(contactLength+spacing):((contactLength+spacing)*(numcontacts(i)-1)))')];
    vectors=[vectors;vector];
    plot3([startLoc(1) startLoc(1)+vector(1).*sqrt(sum((ends(1,:)-ends(2,:)).^2))],[startLoc(2) startLoc(2)+vector(2).*sqrt(sum((ends(1,:)-ends(2,:)).^2))],[startLoc(3) startLoc(3)+vector(3).*sqrt(sum((ends(1,:)-ends(2,:)).^2))])
end
plot3(data(:,1),data(:,2),data(:,3),'k*')
faces=[];
vertices=[];
shiftamt=zeros(length(numcontacts),1);
for i=1:length(numcontacts)
    if spacings(i)==1.5
        if numcontacts(i)<10
            [f,v]=stlread([patientID '/../ElectrodeLibrary/PMT-2102-0' num2str(numcontacts(i)) '-091_Contacts.stl']);
        else
            [f,v]=stlread([patientID '/../ElectrodeLibrary/PMT-2102-' num2str(numcontacts(i)) '-091_Contacts.stl']);
        end
    elseif spacings(i) == 1.97
        [f,v]=stlread([patientID '/../ElectrodeLibrary/PMT-2102-' num2str(numcontacts(i)) '-092_Contacts.stl']);
    elseif spacings(i)== 2.43
        [f,v]=stlread([patientID '/../ElectrodeLibrary/PMT-2102-' num2str(numcontacts(i)) '-093_Contacts.stl']);
    elseif spacings(i)== 0.71
        if numcontacts(i)<10
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD0' num2str(numcontacts(i)) 'R-SP03_Contacts.stl']);
        else
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD' num2str(numcontacts(i)) 'R-SP03_Contacts.stl']);
        end
    elseif spacings(i)== 1.71
        if numcontacts(i)<10
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD0' num2str(numcontacts(i)) 'R-SP04_Contacts.stl']);
        else
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD' num2str(numcontacts(i)) 'R-SP04_Contacts.stl']);
        end
    elseif spacings(i)== 2.71
        if numcontacts(i)<10
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD0' num2str(numcontacts(i)) 'R-SP05_Contacts.stl']);
        else
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD' num2str(numcontacts(i)) 'R-SP05_Contacts.stl']);
        end
    elseif spacings(i)== 3.71
        if numcontacts(i)<10
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD0' num2str(numcontacts(i)) 'R-SP06_Contacts.stl']);
        else
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD' num2str(numcontacts(i)) 'R-SP06_Contacts.stl']);
        end
    elseif spacings(i)== 4.71
        if numcontacts(i)<10
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD0' num2str(numcontacts(i)) 'R-SP07_Contacts.stl']);
        else
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD' num2str(numcontacts(i)) 'R-SP07_Contacts.stl']);
        end
    elseif spacings(i)== 5.71
        if numcontacts(i)<10
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD0' num2str(numcontacts(i)) 'R-SP08_Contacts.stl']);
        else
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD' num2str(numcontacts(i)) 'R-SP08_Contacts.stl']);
        end
    end
    V1=[0 0 1];
    
    V2=vectors(i,:);
    V3 = cross(V1,V2)./norm(cross([0 0 1],V2)); 
    V4 = cross(V3, V1);
    M1=[V1;V4;V3];
    cos = dot(V2, V1);
    sin = dot(V2, V4);
    M2=[cos sin 0;-sin cos 0; 0 0 1];
    R=inv(M1)*M2*M1;
    rotated=f.Points*R;
    translated=rotated+startLocs(i,:);
    faces=[faces;f.ConnectivityList+length(vertices)];
    if i>1
        dists=sqrt((vertices(:,1)-translated(:,1)').^2+(vertices(:,2)-translated(:,2)').^2+(vertices(:,3)-translated(:,3)').^2);
        disp(min(dists(:)))
        while min(dists(:))<0.1
            translated=translated+0.01.*V2;
            shiftamt(i)=shiftamt(i)+0.01;
            dists=sqrt((vertices(:,1)-translated(:,1)').^2+(vertices(:,2)-translated(:,2)').^2+(vertices(:,3)-translated(:,3)').^2);
        end
    end
    vertices=[vertices;translated];
end
start=1;
for i=1:length(numcontacts)
    contacts(start:(start+numcontacts(i)-1),:)=contacts(start:(start+numcontacts(i)-1),:)+shiftamt(i).*vectors(i,:);
    start=start+numcontacts(i);
end
TR=triangulation(faces,vertices);
stlwrite(TR,[patientID '/work/ContactGeoms.stl'])
scirunfield.node=contacts;
scirunfield.field=zeros(length(contacts),1);
save([patientID '/work/ContactLocations.mat'],'scirunfield')

faces=[];
vertices=[];
for i=1:length(numcontacts)
    if spacings(i)==1.5
        if numcontacts(i)<10
            [f,v]=stlread([patientID '/../ElectrodeLibrary/PMT-2102-0' num2str(numcontacts(i)) '-091_Insulation.stl']);
        else
            [f,v]=stlread([patientID '/../ElectrodeLibrary/PMT-2102-' num2str(numcontacts(i)) '-091_Insulation.stl']);
        end
    elseif spacings(i) == 1.97
        [f,v]=stlread([patientID '/../ElectrodeLibrary/PMT-2102-' num2str(numcontacts(i)) '-092_Insulation.stl']);
    elseif spacings(i)== 2.43
        [f,v]=stlread([patientID '/../ElectrodeLibrary/PMT-2102-' num2str(numcontacts(i)) '-093_Insulation.stl']);
    elseif spacings(i)== 0.71
        if numcontacts(i)<10
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD0' num2str(numcontacts(i)) 'R-SP03_Insulation.stl']);
        else
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD' num2str(numcontacts(i)) 'R-SP03_Insulation.stl']);
        end
    elseif spacings(i)== 1.71
        if numcontacts(i)<10
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD0' num2str(numcontacts(i)) 'R-SP04_Insulation.stl']);
        else
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD' num2str(numcontacts(i)) 'R-SP04_Insulation.stl']);
        end
    elseif spacings(i)== 2.71
        if numcontacts(i)<10
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD0' num2str(numcontacts(i)) 'R-SP05_Insulation.stl']);
        else
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD' num2str(numcontacts(i)) 'R-SP05_Insulation.stl']);
        end
    elseif spacings(i)== 3.71
        if numcontacts(i)<10
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD0' num2str(numcontacts(i)) 'R-SP06_Insulation.stl']);
        else
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD' num2str(numcontacts(i)) 'R-SP06_Insulation.stl']);
        end
    elseif spacings(i)== 4.71
        if numcontacts(i)<10
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD0' num2str(numcontacts(i)) 'R-SP07_Insulation.stl']);
        else
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD' num2str(numcontacts(i)) 'R-SP07_Insulation.stl']);
        end
    elseif spacings(i)== 5.71
        if numcontacts(i)<10
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD0' num2str(numcontacts(i)) 'R-SP08_Insulation.stl']);
        else
            [f,v]=stlread([patientID '/../ElectrodeLibrary/RD' num2str(numcontacts(i)) 'R-SP08_Insulation.stl']);
        end
    end
    V1=[0 0 1];
    V2=vectors(i,:);
    V3 = cross(V1,V2)./norm(cross([0 0 1],V2)); % WithAxon
    V4 = cross(V3, V1);
    M1=[V1;V4;V3];
    cos = dot(V2, V1);
    sin = dot(V2, V4);
    M2=[cos sin 0;-sin cos 0; 0 0 1];
    R=inv(M1)*M2*M1;
    rotated=f.Points*R;
    translated=rotated+startLocs(i,:);
    faces=[faces;f.ConnectivityList+length(vertices)];
    vertices=[vertices;translated+shiftamt(i).*V2];
end
TR=triangulation(faces,vertices);
stlwrite(TR,[patientID '/work/InsulationGeoms.stl'])

%faces=[];
%vertices=[];
%for i=1:length(numcontacts)
%    if spacings(i)==1.5
%        if numcontacts(i)<10
%            [f,v]=stlread([patientID '/../ElectrodeLibrary/PMT-2102-0' num2str(numcontacts(i)) '-091_HigherMesh.stl']);
%        else
%            [f,v]=stlread([patientID '/../ElectrodeLibrary/PMT-2102-' num2str(numcontacts(i)) '-091_HigherMesh.stl']);
%        end
%    elseif spacings(i) == 1.97
%        [f,v]=stlread([patientID '/../ElectrodeLibrary/PMT-2102-' num2str(numcontacts(i)) '-092_HigherMesh.stl']);
%    elseif spacings(i)== 2.43
%        [f,v]=stlread([patientID '/../ElectrodeLibrary/PMT-2102-' num2str(numcontacts(i)) '-093_HigherMesh.stl']);
%    end
%    V1=[0 0 1];
%    V2=vectors(i,:);
%    V3 = cross(V1,V2)./norm(cross([0 0 1],V2)); % WithAxon
%    V4 = cross(V3, V1);
%    M1=[V1;V4;V3];
%    cos = dot(V2, V1);
%    sin = dot(V2, V4);
%    M2=[cos sin 0;-sin cos 0; 0 0 1];
%    R=inv(M1)*M2*M1;
%    rotated=f.Points*R;
%    translated=rotated+startLocs(i,:);
%    faces=[faces;f.ConnectivityList+length(vertices)];
%    vertices=[vertices;translated];
%end
%TR=triangulation(faces,vertices);
%stlwrite(TR,[patientID '/work/HigherMeshGeoms.stl'])
