function CombineLRHead(PatientID)
[verticesL,facesL]=freesurfer_read_surf(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(PatientID) '/work/brainMeshes/' num2str(PatientID) '/surf/lh.white']);
[verticesR,facesR]=freesurfer_read_surf(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(PatientID) '/work/brainMeshes/' num2str(PatientID) '/surf/rh.white']);
% [verticesL,facesL]=freesurfer_read_surf('PatientD48/work/lh.pial');
% [verticesR,facesR]=freesurfer_read_surf('PatientD48/work/rh.pial');


[verticesLP,facesLP]=freesurfer_read_surf(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(PatientID) '/work/brainMeshes/' num2str(PatientID) '/surf/lh.pial']);
[verticesRP,facesRP]=freesurfer_read_surf(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(PatientID) '/work/brainMeshes/' num2str(PatientID) '/surf/rh.pial']);


f=[facesL;facesR+length(verticesL)];

info=niftiinfo(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(PatientID) '/work/MRI.nii']);
offset=info.Transform.T(4,1:3)+info.ImageSize.*info.PixelDimensions./2;
v=[verticesL+offset;verticesR+offset];

TR=triangulation(facesL,verticesL+offset);

stlwrite(TR,['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(PatientID) '/work/lh_brain.stl'])

TR=triangulation(facesR,verticesR+offset);

stlwrite(TR,['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(PatientID) '/work/rh_brain.stl'])

TR=triangulation(f,v+offset);
stlwrite(TR,['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(PatientID) '/work/Brain_native.stl'])

vectorsL=verticesLP-verticesL;
vectorsR=verticesRP-verticesR;


field=[vectorsL;vectorsR]';
mags=sqrt(sum(field.^2));
scirunfield.node=v(mags>0,:)';
scirunfield.field=field(:,mags>0)./mags(mags>0);

save(['/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient' num2str(PatientID) '/work/LinkVectors.mat'],'scirunfield');