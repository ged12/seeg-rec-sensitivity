from subprocess import run
import os 
from nilearn import plotting
import nilearn
import nibabel as nib
import numpy as np
import sys

if(len(sys.argv)!=2):
	print("Wrong number of inputs!!!")
	exit()

patientID = sys.argv[1]
dir_dcm2nii = "/hpc/group/wmglab/bjt20/HeadModelAutomation/dcm2niix"
dir_Patient = f"/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient{patientID}"

dir_MRI = f'{dir_Patient}/neuroimaging/MRI'
dir_work = f'{dir_Patient}/work'
dir_CT = f'{dir_Patient}/neuroimaging/CT'
dir_DTI = f'{dir_Patient}/neuroimaging/DTI'
dir_MeshLayers = f'{dir_Patient}/MeshLayers'
dir_brainMeshes = f'{dir_work}/brainMeshes'

if (os.path.isdir(dir_work)!=True):  
    os.mkdir(dir_work)

result = run(f'{dir_dcm2nii} -m y -f "MRI_{patientID}" "{dir_MRI}"', capture_output=True,shell=True).stdout

result = run(f'{dir_dcm2nii} -m y -f "CT_{patientID}" "{dir_CT}"', capture_output=True,shell=True).stdout


result = run(f'{dir_dcm2nii} -m y -f "DTI_{patientID}" "{dir_DTI}"', capture_output=True,shell=True).stdout

nifti_PostOpCt = f'{dir_work}/PostOpCT.nii'
nifti_MRI = f'{dir_work}/MRI.nii'
nifti_DTI = f'{dir_work}/DTI.nii'

from shutil import copyfile
def selectNifti(niftiFolder, output, filetype):
    files = []
    maxval = 0
    maxfile = ""
    secondmax = 0
    targetfile = f"{filetype}_{patientID}_Tilt_1.nii"
    if(os.path.isfile(niftiFolder+targetfile)!=True):
        #Sometimes the CT images is not correctly aligned.
        targetfile = f"{filetype}_{patientID}.nii"
    copyfile(niftiFolder+targetfile, output)


selectNifti(f'{dir_Patient}/neuroimaging/MRI/',nifti_MRI,"MRI")
selectNifti(f'{dir_Patient}/neuroimaging/CT/',nifti_PostOpCt,"CT")
selectNifti(f'{dir_Patient}/neuroimaging/DTI/',nifti_DTI,"DTI")

import os
path = dir_work
PostOpCt = path+"/PostOpCT.nii"
MRI = path+"/MRI.nii"


dir_map_PostOpCtMRI = f'{dir_work}/map_PostOpCtMRI'


#PostOpCt to MRI
cmd = f"flirt -in {PostOpCt} -ref {MRI} -out {dir_map_PostOpCtMRI}.nii -omat {dir_map_PostOpCtMRI}.mat -bins 256 -cost mutualinfo -searchrx -90 90 -searchry -90 90 -searchrz -90 90 -dof 6  -interp trilinear"
print(cmd)
result = run(cmd, capture_output=True,shell=True).stdout


#unzip nii files
cmd = f'gzip -d {dir_map_PreOpCtPostOpCt}.nii.gz'
result = run(cmd, capture_output=True,shell=True).stdout

processed_PostOpCt = nifti_PostOpCt

# #### post image

postimage = nib.load(processed_PostOpCt)
header = postimage.header

postArr = postimage.get_fdata()
postArr = np.where(postArr < 346,0,postArr)

ni_postimg = nib.Nifti1Image(postArr, postimage.affine,header)
nib.save(ni_postimg, f"{dir_work}/threshold_postCT.nii")

# skull stripping
resultImg = postArr

resultImg = np.where(resultImg < 2500,0,resultImg)
ni_resultimg = nib.Nifti1Image(resultImg, postimage.affine, header)

nib.save(ni_resultimg, f"{dir_work}/result{patientID}.nii")

#Run FSL
if (os.path.isdir(dir_MeshLayers)!=True):  
    os.mkdir(dir_MeshLayers)
# run bet with skull and skin extraction
cmd=f"bet {dir_work}/MRI.nii {dir_Patient}/MeshLayers/MRI_brain -A -f 0.5 -g 0 -o -m -t -s"
print(cmd)
result = run(cmd, capture_output=True,shell=True).stdout

# run fast
cmd=f"fast -o {dir_Patient}/MeshLayers/TissueLayers {dir_Patient}/MeshLayers/MRI_brain.nii.gz"
print(cmd)
result = run(cmd, capture_output=True,shell=True).stdout

#unzip tissue nii files
cmd = f'gzip -d {dir_Patient}/MeshLayers/TissueLayers_pve_0.nii.gz'
result = run(cmd, capture_output=True,shell=True).stdout
cmd = f'gzip -d {dir_Patient}/MeshLayers/TissueLayers_pve_1.nii.gz'
result = run(cmd, capture_output=True,shell=True).stdout
cmd = f'gzip -d {dir_Patient}/MeshLayers/TissueLayers_pve_2.nii.gz'
result = run(cmd, capture_output=True,shell=True).stdout
cmd = f'gzip -d {dir_Patient}/MeshLayers/MRI_brain_outskin_mask.nii.gz'
result = run(cmd, capture_output=True,shell=True).stdout
cmd = f'gzip -d {dir_Patient}/MeshLayers/MRI_brain_outskull_mask.nii.gz'
result = run(cmd, capture_output=True,shell=True).stdout

# run bet on DTI
cmd=f"bet {dir_work}/DTI.nii {dir_Patient}/MeshLayers/DTI_brain -A -f 0.1 -g 0 -o -m -t -s"
print(cmd)
result = run(cmd, capture_output=True,shell=True).stdout

# Run DTIfit
cmd=f"dtifit --data={dir_work}/DTI.nii --out={dir_work}/dti --mask={dir_Patient}/MeshLayers/DTI_brain_mask.nii.gz --bvecs={dir_DTI}/DTI_{patientID}.bvec --bvals={dir_DTI}/DTI_{patientID}.bval --save_tensor"
print(cmd)
result = run(cmd, capture_output=True,shell=True).stdout

# Flirt to get transform from DTI to T1 space
cmd = f"flirt -in {dir_Patient}/MeshLayers/DTI_brain_mask.nii.gz -ref {MRI} -out {dir_work}/DTI_Brain_Mask_Reg.nii.gz -omat {dir_work}/DTI2T1.mat -bins 256 -cost mutualinfo -searchrx -180 180 -searchry -180 180 -searchrz -180 180 -dof 6  -interp trilinear"
result = run(cmd, capture_output=True,shell=True).stdout

#VecReg and Flirt
cmd = f"flirt -in {dir_work}/dti_L1.nii.gz -applyxfm -init {dir_work}/DTI2T1.mat -out {dir_work}/DTI_L1_Reg.nii.gz -paddingsize 0.0 -interp trilinear -ref {dir_work}/MRI.nii"
print(cmd)
result = run(cmd, capture_output=True,shell=True).stdout
cmd = f"flirt -in {dir_work}/dti_L2.nii.gz -applyxfm -init {dir_work}/DTI2T1.mat -out {dir_work}/DTI_L2_Reg.nii.gz -paddingsize 0.0 -interp trilinear -ref {dir_work}/MRI.nii"
print(cmd)
result = run(cmd, capture_output=True,shell=True).stdout
cmd = f"flirt -in {dir_work}/dti_L3.nii.gz -applyxfm -init {dir_work}/DTI2T1.mat -out {dir_work}/DTI_L3_Reg.nii.gz -paddingsize 0.0 -interp trilinear -ref {dir_work}/MRI.nii"
print(cmd)
result = run(cmd, capture_output=True,shell=True).stdout
cmd = f"vecreg -i {dir_work}/dti_V1.nii.gz -o {dir_work}/dti_V1_Reg.nii.gz -r {dir_work}/MRI.nii -t {dir_work}/DTI2T1.mat"
print(cmd)
result = run(cmd, capture_output=True,shell=True).stdout
cmd = f"vecreg -i {dir_work}/dti_V2.nii.gz -o {dir_work}/dti_V2_Reg.nii.gz -r {dir_work}/MRI.nii -t {dir_work}/DTI2T1.mat"
print(cmd)
result = run(cmd, capture_output=True,shell=True).stdout
cmd = f"vecreg -i {dir_work}/dti_V3.nii.gz -o {dir_work}/dti_V3_Reg.nii.gz -r {dir_work}/MRI.nii -t {dir_work}/DTI2T1.mat"
print(cmd)
result = run(cmd, capture_output=True,shell=True).stdout

# unzip DTI files
cmd = f'gzip -d {dir_work}/DTI_L1_Reg.nii.gz'
result = run(cmd, capture_output=True,shell=True).stdout
cmd = f'gzip -d {dir_work}/DTI_L2_Reg.nii.gz'
result = run(cmd, capture_output=True,shell=True).stdout
cmd = f'gzip -d {dir_work}/DTI_L3_Reg.nii.gz'
result = run(cmd, capture_output=True,shell=True).stdout
cmd = f'gzip -d {dir_work}/dti_V1_Reg.nii.gz'
result = run(cmd, capture_output=True,shell=True).stdout
cmd = f'gzip -d {dir_work}/dti_V2_Reg.nii.gz'
result = run(cmd, capture_output=True,shell=True).stdout
cmd = f'gzip -d {dir_work}/dti_V3_Reg.nii.gz'
result = run(cmd, capture_output=True,shell=True).stdout
print("Finished FSL\n")

# Run freesurfer
if (os.path.isdir(dir_brainMeshes)!=True):  
    os.mkdir(dir_brainMeshes)
import multiprocessing
cpuCount=len(os.sched_getaffinity(0))
strcpuCount=str(cpuCount)
cmd = f"recon-all -i {dir_work}/MRI.nii -sd {dir_brainMeshes} -subjid {patientID} -all -openmp {strcpuCount}"
print(cmd)
result = run(cmd, capture_output=True,shell=True).stdout
filename=f"{dir_brainMeshes}/{patientID}/surf/lh.white"
if(os.path.exists(filename)!=True):
	cmd = f"mris_make_surfaces -SDIR {dir_brainMeshes} -aseg aseg.presurf -noaparc -mgz -T1 brain.finalsurfs {patientID} rh"
	print(cmd)
	result = run(cmd, capture_output=True,shell=True).stdout
	cmd = f"mris_make_surfaces -SDIR {dir_brainMeshes} -aseg aseg.presurf -noaparc -mgz -T1 brain.finalsurfs {patientID} lh"
	print(cmd)
	result = run(cmd, capture_output=True,shell=True).stdout
print("Finished Freesurfer\n")
