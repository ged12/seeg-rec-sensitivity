"""
MIT License

Copyright (c) 2022 Mokhtari Mohammed El Amine

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import nibabel as nib
import numpy as np
from stl import mesh
from skimage import measure
import sys

def nii_2_mesh (filename_nii, filename_stl):

    """
    From https://github.com/amine0110/nifti-to-stl
    Adapted by Brandon Thio on 13 January 2023
    Read a nifti file including a binary map of a segmented organ with label id = label. 
    Convert it to a smoothed mesh of type stl.
    filename_nii     : Input nifti binary map 
    filename_stl     : Output mesh name in stl format
    """

    # Extract the numpy array
    nifti_file = nib.load(filename_nii)
    np_array = nifti_file.get_fdata()

    verts, faces, normals, values = measure.marching_cubes(np_array)
    obj_3d = mesh.Mesh(np.zeros(faces.shape[0], dtype=mesh.Mesh.dtype))

    for i, f in enumerate(faces):
        obj_3d.vectors[i] = verts[f]

    # Save the STL file with the name and the path
    obj_3d.save(filename_stl)

if(len(sys.argv)!=5):
    print("Wrong number of inputs!!!")
    exit()

patientID = sys.argv[1]
directory = sys.argv[2]
inputFile = sys.argv[3]
outputFile = sys.argv[4]

filename_nii =  f'/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient{patientID}/{directory}/{inputFile}'
filename_stl = f'/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient{patientID}/{directory}/{outputFile}'

nii_2_mesh (filename_nii, filename_stl)