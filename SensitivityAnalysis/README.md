# Sensitivity Analysis
__Supplemental Figure 4__

With this code, we determined the error in optimized configuration recording sensitivity due to source type and threshold-priority parameters in our optimization. 

This code is designed to run on a SLURM-based computing cluster (Duke Computing Cluster) and cannot be run locally due to large memory requirements.

## Software and dependencies:
- SLURM cluster (with ≥ 400 CPUs total, ≥ 22 CPUs/node and ≥ 200 GB / node). Data storage location with at least 3 TB availability. 
- matlabR2019a
- SCIRun-5.0-beta.Y 

## Running process:

1. Configure all scripts for your cluster storage system, making sure to update all paths of software, data, and scripts. 
    * The system is designed for a main /home directory to store main scripts and main data, and secondary /work directory to store intermediate scripts and intermediate high-memory data. 

2. Run HeadModelGeneration section

3. Run MainPipeline section

4. Run `crossAn.slurm`  (est. runtime < 1 hour) on a SLURM-based cluster
    * this script submits `sourceSearchSens.m` for all cases of source type parameters

5. Run `costCross.slurm` (est. runtime < 1 hour) on a SLURM-based cluster
    * this script submits `costFnSens.m` for all cases of threshold-priority orders

5. Plot and analyze data locally
    * Scripts: 
        * FigureS4abcd_plot.m
        * FigureS4efgh_plot.m
    * Outputs:
        * Figures/FigureS4[a,b,c,d,e,f,g,h].fig & .pdf
		* SourceData/Figure4[a,b,c,d,e,f,g,h].xls











