% quantify diff in recordability by cost function
% Grace Dessert

function costFnSens(trialSelect)
    costSens_run(trialSelect)
    if trialSelect == 4
        medCostSensVis(trialSelect)
    end
 end
 
function medCostSensVis(trialSelect)
    % for this source type, get rec sens (RS) data across electrodes
    % using a single source type for analysis, how much do RS values for
    % configurations optimized with other cost functions?
    % look at mean across all patients
    % answers the question: what is expected RS if we optimize with some
    % cost function and then analyze at one threshoold
    % or, if we have off-target estimation of what thr we care about, how
    % much worse do we do on RS.
    
    thrI = 2; % 500 µV
    
    % get cases of interest and make full case IDs
    % get parameters
    patientIDs = [2,3,7,19,20,24,25,26,27,30,31,33];
    cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
    patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
    ROIs = ["clinicianROI","LTL","LH"];
    dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';
    thrs = [200, 500, 1000];

    tys_cA = strcat(num2str(patchAreas(trialSelect)),"cm2_",num2str(cdmds(trialSelect)),"cdmd");
    
    % overall dat storage
    dataAll = cell(3,3,12,9); % 3 ROIs, 3 thresholds, 12 patients, 6 cost functions for analysis
    
    % for all 12 patients
    for patI = 1:12
        patientID = patientIDs(patI); 
        patientDir = ['Patient_' num2str(patientID)];
        
        % go through all source types for optimization, and analyze all with same main soure type
        % open the visDataConfigs file for this source type and patient
        load(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_crossAnalyze',tys_cA,'.mat'),'visDataConfigs')
        
        % get full case IDs for all cases, to look up in config data
        fullCaseIDs = cell(3*3*6,1); % 3 ROIs, 3 thrs, and 6 cost fns
        
        % for all cost fns for optimization
        count = 1;
        for thrPriorityI = 1:6
            
            % for all 3 ROIs 
            for ROI_i = 1:3 
                
                % for all three thresholds
                for thrI = 1:3
                    thrPrioritys = ["200_500_1000_thrPriority","500_200_1000_thrPriority","1000_500_200_thrPriority","1000_200_500_thrPriority","500_1000_200_thrPriority","200_1000_500_thrPriority"];
                    thrPriority = thrPrioritys(thrPriorityI);

                    mapStrStr = strcat("single mapping at ",num2str(thrs(thrI))," µV threshold");
                    full_case_id = strcat(tys_cA,'_ROI_',ROIs(ROI_i),"_singleMap_",thrPriority,mapStrStr);
                    fullCaseIDs{count} = full_case_id;
                    count = count + 1;

                    % find caseID 
                    for k = 1:length(visDataConfigs)
                        if isempty(visDataConfigs{k})
                            continue
                        end
                        ch = split(visDataConfigs{k}.full_case_id,"_crossAnalyze");
                        ch = ch(1);
                        if ch == full_case_id
                            visInd = k;
                            break
                        end
                    end
                    if ch ~= full_case_id
                        disp(strcat(num2str(count),": We didn't find this full_case_id: "))
                        disp(strcat("      ",fullCaseIDs{count}))
                    end
                    
                    % get and store data
                    dat = zeros(31,1);
                    if ~isempty(visDataConfigs{visInd})
                        numEsI = length(visDataConfigs{visInd}.percMapROI);
                        dat(1:numEsI) = visDataConfigs{visInd}.percMapROI;
                        dat(numEsI:31) = visDataConfigs{visInd}.percMapROI(end);
                    end
                    
                    dataAll{ROI_i,thrI,patI,thrPriorityI} = dat;
                end
        
            end
        end
        
        % save data so far
        save(strcat(dataWorkPath,'medSourceCostSensConverg_source',num2str(trialSelect),'.mat'),'dataAll')
    
    end
    
    % save full uncompressed data
    save(strcat(dataWorkPath,'medSourceCostSensConverg_source',num2str(trialSelect),'.mat'),'dataAll')
    
    % average across patients to get mean
    dataAllMean = cell(3,3,9); % 3 ROIs, 3 thrs, 6 costs
    for ROI_i = 1:3
        for thrI = 1:3
            for thrPriorityI = 1:6
                for patI = 1:12
                    dat = dataAll{ROI_i,thrI,patI,thrPriorityI};
                    disp(strcat("dat is of size: ",num2str(size(dat))))
                    if isempty(dataAllMean{ROI_i,thrI,thrPriorityI})
                        dataAllMean{ROI_i,thrI,thrPriorityI} = zeros(31,1);
                    end
                    dataAllMean{ROI_i,thrI,thrPriorityI} = dataAllMean{ROI_i,thrI,thrPriorityI} + dat;
                end
                dataAllMean{ROI_i,thrI,thrPriorityI} = dataAllMean{ROI_i,thrI,thrPriorityI}/12;
            end
        end
    end
    save(strcat(dataWorkPath,'medSourceMeanCostSensConverg_source',num2str(trialSelect),'.mat'),'dataAllMean')
    
    
end

function costSens_run(trialSelect) 
% for all this source type 'trialSelect'
% open visConfig data (analyzed solution configurations)
% and, for all patients and ROIs, calcualte avg RS in all cost fn cases
% using the same # electrodes for each set of 6 configurations such that
% the best cost fn has the least # electrodes for ≥ 75% RS

% get parameters
patientIDs = [2,3,7,19,20,24,25,26,27,30,31,33];
cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
ROIs = ["clinicianROI","LTL","LH"]; % array of length 8
dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';

patchArea = patchAreas(trialSelect);
cdmd = cdmds(trialSelect);
tys = strcat(num2str(patchArea),"cm2_",num2str(cdmd),"cdmd");

% for all 3 ROIs 
for ROI_i = 1:3  %ceil(runCaseID/9); % 

diffRS_allPats = cell(12,1);

% for all 12 patients
for patI = 1:12
    
patientID = patientIDs(patI); 
patientDir = ['Patient_' num2str(patientID)];

tys_cA = strcat(num2str(patchAreas(trialSelect)),"cm2_",num2str(cdmds(trialSelect)),"cdmd");

% open the visDataConfigs file for this source type and patient, if not
% already open
fileNameThis = strcat(dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_crossAnalyze',tys_cA,'.mat');
patDatOpen = [];
patDats = cell(1,1);
if length(patDatOpen)>0 && max(find(patDatOpen==fileNameThis))==1
    disp(strcat("We already have: ",dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_crossAnalyze',tys_cA,'.mat'))
    visDataConfigs = patDats{patDatOpen==fileNameThis};
else
    disp(strcat("Opening: ",dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_crossAnalyze',tys_cA,'.mat'))
    load(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_crossAnalyze',tys_cA,'.mat'),'visDataConfigs')
    patDatOpen = [patDatOpen,fileNameThis];
    patDats{length(patDats)+1} = visDataConfigs;
    %   load(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataImplantConfig_crossAnalyze',tys_cA,'.mat'),'visDataImplantConfig')
end


diffRS = zeros(6,3); % 6 cost fns, 3 thrs

% for all three thresholds
for thrI = 1:3
    
% get full case IDs for all source types, to look up in config data
fullCaseIDs = cell(6,1);

for thrPriorityI = 1:6

    thrs = [200, 500, 1000];
    
    thrPrioritys = ["200_500_1000_thrPriority","500_200_1000_thrPriority","1000_500_200_thrPriority","1000_200_500_thrPriority","500_1000_200_thrPriority","200_1000_500_thrPriority"];
    thrPriority = thrPrioritys(thrPriorityI);

    mapStrStr = strcat("single mapping at ",num2str(thrs(thrI))," µV threshold");
    
    full_case_id = strcat(tys,'_ROI_',ROIs(ROI_i),"_singleMap_",thrPriority,mapStrStr);
    
    fullCaseIDs{thrPriorityI} = full_case_id;
end

visInds = zeros(6,1);
% find indices of these case ids in data visDataConfigs
for j = 1:6
    for k = 1:length(visDataConfigs)
        if isempty(visDataConfigs{k})
            continue
        end
        ch = split(visDataConfigs{k}.full_case_id,"_crossAnalyze");
        ch = ch(1);
        if ch == fullCaseIDs{j}
            visInds(j) = k;
            break
        end
    end
    if ch == fullCaseIDs{j}
        continue
    end
    disp(strcat(num2str(j),": We didn't find this full_case_id: "))
    disp(strcat("      ",fullCaseIDs{j}))
end

% get number of electrodes to analyze at
% find smallest # electrodes that ANY of 6 cases needs for ≥ 75% RS

dat = zeros(31,length(visInds));
case_IDs = cell(1,length(visInds));
numEsT = 32;
selBestCostI = 0;
for i = 1:length(visInds)
    if ~isempty(visDataConfigs{visInds(i)})
        % get # E's at first with ≥ 75% RS in matched case
        thisNumE = find(visDataConfigs{visInds(i)}.percMapROI>=0.75,1);
        if isempty(thisNumE)
            thisNumE = length(visDataConfigs{visInds(i)}.percMapROI);
        end
        if thisNumE < numEsT
            numEsT = thisNumE;
            selBestCostI = i;
        end
    end
end
disp(strcat("We are comparing mapping percs at ",num2str(numEsT)," electrodes."))
disp(strcat("The best cost fn has ",num2str(visDataConfigs{visInds(selBestCostI)}.percMapROI(numEsT)*100)," % mapping of this ROI at this # of electrodes"))

% look up all cases using indices
% and calcualte difference in recording sensitivity
for i = 1:length(visInds)
    if ~isempty(visDataConfigs{visInds(i)})
        numEsI = length(visDataConfigs{visInds(i)}.percMapROI);
        dat(1:numEsI,i) = visDataConfigs{visInds(i)}.percMapROI;
        dat(numEsI:31,i) = visDataConfigs{visInds(i)}.percMapROI(end);
        case_IDs{i} = visDataConfigs{visInds(i)}.full_case_id;

        % save RS at relevant number of electrodes
        diffRS(i,thrI) = dat(numEsT,i);
    end
end


end

diffRS_allPats{patI} = diffRS;

save(strcat(dataWorkPath,'diffRSCost_allPats_singleMap_ROI',ROIs(ROI_i),'_source',num2str(trialSelect),'.mat'),'diffRS_allPats')

end

save(strcat(dataWorkPath,'diffRSCost_allPats_singleMap_ROI',ROIs(ROI_i),'_source',num2str(trialSelect),'.mat'),'diffRS_allPats')

end

end


