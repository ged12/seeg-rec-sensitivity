%% A+B
load SpikeTimeCourse_13_1000.mat

figure
plot(linspace(0,2,4096),filteredData,'k-')

figure
histogram(filteredData(1:2048,:))
axis([-200 200 0 10000])
%% C
load SpikeAmplitudes.mat

figure
histogram(log10(maxes))
hold on
plot([log10(200),log10(200)],[0 30],'r-')
plot([log10(500),log10(500)],[0 30],'r-')
plot([log10(1000),log10(1000)],[0 30],'r-')