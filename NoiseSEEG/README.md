# Noise Analysis
__Supp Figure 1__

With this code, we provide code to generate the plots for our noise analysis. 

## Software and hardware requirements:
#### Full Version
- matlabR2019a
 
## Running the full version

1.	Run VisualizeSpikeAmplitude.m. It will generate each panel of supplemental figure 1 from the data in SpikeAmplitudes.mat and SpikeTimeCourse_13_1000.mat