% fig3_analyzeSolnConfis
% Grace Dessert
% 10/24/2022


% global vars
cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
dataWorkPath = '';
dataHomePath = '';
patientIDs = [2,3,7,19,20,24,25,26,27,30,31,33];
costFnStrs = ["200_1000_500","500_1000_200","1000_500_200"];
degMapStrs = ["single mapping at 200 µV threshold", "single mapping at 500 µV threshold", "single mapping at 1000 µV threshold"];
ROIs = ["clinicianROI","LTL","LH"]; % array 

% select case
patientID = 25;
sourceInd = 4;

%% load data for this patient and source type
% note that only Patient 25 data is given in the repository
% for other patients, you must run the full pipeline to generate data

patientDir = ['Patient_' num2str(patientID)];
tys_cA = strcat(num2str(patchAreas(sourceInd)),"cm2_",num2str(cdmds(sourceInd)),"cdmd");
load(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_cis',tys_cA,'.mat'),'visDataConfigs')
load(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataImplantConfig_singleMap_crossAnalyze',tys_cA,'.mat'),'visDataImplantConfig')
load(strcat(dataHomePath,patientDir,'/Inputs/ContactLocations.mat'),'scirunfield')
contLocs = scirunfield;

LH_fileName = strcat(dataHomePath,patientDir,'/Inputs/lh_white_20k.stl');
LH_white = stlread(LH_fileName);
RH_fileName = strcat(dataHomePath,patientDir,'/Inputs/rh_white_20k.stl');
RH_white = stlread(RH_fileName);
load(strcat(dataWorkPath,patientDir,'/OptSolutions/ROIs.mat'),'patches')


%% Supp Figure 3
% make sure you run the previous sections in this script
% Figure 3 is saved automatically in Figures/ directory
% You need to use 
addpath('MainPipeline')
thrI = 2;
ROI_str = "LTL"; 
fileName = "Figure3c";
visThisConfig(fileName,patientID,visDataConfigs,thrI,ROI_str,contLocs,tys_cA,LH_white,RH_white,patches,visDataImplantConfig)

% Figure 3a and 3b must be generated with the SCIRun network 'InputGeneral/visConfigRec.srn5',
%   after running this section.
%   This section creates custom files in the 'RawData/VisCurrent/' directory for
%   the SCIRun network to load.
%   Make sure to update the filepaths of all vis files in the SCIRun
%   network.
% InputGeneral/visConfigRec.srn5
