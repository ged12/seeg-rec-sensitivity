% fig3_analyzeSolnConfis
% Grace Dessert
% 10/24/2022


% global vars
cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
dataWorkPath = '';
dataHomePath = '';
patientIDs = [2,3,7,19,20,24,25,26,27,30,31,33];
costFnStrs = ["200_1000_500","500_1000_200","1000_500_200"];
degMapStrs = ["single mapping at 200 µV threshold", "single mapping at 500 µV threshold", "single mapping at 1000 µV threshold"];
ROIs = ["clinicianROI","LTL","LH"]; % array 


%% Supp Figure 2
for ROI_i = 1:2
    ROIs(2) = "TL";
    saveFigTag = "compOpt";
    f = boxDiffAllCases(ROI_i, patientIDs,patchAreas,cdmds,dataWorkPath,degMapStrs,costFnStrs,ROIs,saveFigTag)
end



function f = boxDiffAllCases(ROI_i, patientIDs,patchAreas,cdmds,dataWorkPath,degMapStrs,costFnStrs,ROIs,saveFigTag)
    
    diffs = zeros(length(patientIDs),9,3);
    
    calcByDips = false;
    weightByArea = false;
    
    for sourceInd = 1:9
        
        dat_mapPerc = cell(3,1);
        dat_mapPerc_imp = cell(3,1);

        for t = 1:3
            dat_mapPerc{t} = zeros(length(patientIDs),1);
            dat_mapPerc_imp{t} = zeros(length(patientIDs),1);
        end

        redoCalc = false;
        [data_imp,~] = saveOrOpenCisImplant(redoCalc, ROI_i,sourceInd,patchAreas,cdmds,dataWorkPath,patientIDs,costFnStrs);
        redoCalc = false;
        
        dat = saveOrOpenCisSearch(ROI_i, sourceInd,redoCalc, patientIDs,degMapStrs,costFnStrs,dataWorkPath,patchAreas,cdmds,ROIs,saveFigTag);
        
        for t = 1:3
            for patI = 1:12
                if isempty(data_imp{patI,t})
                    continue
                end
                dat_mapPerc_imp{t}(patI) = data_imp{patI,t}(end);
                numEs = length(data_imp{patI,t});
                dat_mapPerc{t}(patI) = dat{patI,t}(min(numEs,length(dat{patI,t})));
            end
            diffs(:,sourceInd,t) = (dat_mapPerc{t} - dat_mapPerc_imp{t})*100;
        end
    end
    % reorder sources
    cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
    patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
    order = [3,6,9,1,4,7,2,5,8];
    diffs = diffs(:,order,:) ;
    
    % make table
    % save raw data in table
    varNames = ["6cm2_0.16nAmmm2","10cm2_0.16nAmmm2","20cm2_0.16nAmmm2","6cm2_0.465nAmmm2","10cm2_0.465nAmmm2","20cm2_0.465nAmmm2","6cm2_0.77nAmmm2","10cm2_0.77nAmmm2","20cm2_0.77nAmmm2,"];
    rowNames = string(patientIDs);
    T = table(diffs(:,1,1),diffs(:,2,1),diffs(:,3,1),diffs(:,4,1),diffs(:,5,1),diffs(:,6,1),diffs(:,7,1),diffs(:,8,1),diffs(:,9,1),'VariableNames',varNames,'RowNames',rowNames);
    if ROI_i == 1  % clinician-ROI
        writetable(T,'SourceData/SuppFigure2b.xls')
    elseif ROI_i==2  % LTL
        writetable(T,'SourceData/SuppFigure2a.xls')
    else
        disp("WHY COMPARE AT LH ROI?? CHECK")
    end
    disp(T)
    
    % remove patients with no data (for LTL)
    noDatPats = find(sum(sum(diffs,3),2)==0);
    diffs(noDatPats,:,:)=[];
    
    thrs = [200,500,1000];
    positions = [1,2,3,4.25,5.25,6.25,7.5,8.5,9.5];
    
    colors = parula(4);

    f = figure(4)
    clf
    hold on
    positionsScat = (rand(size(diffs,1),1)-0.5)/2;
    set(f, 'Position', [100, 100, 460, 500]);   
    
    tiledlayout(3,1)
    for t = 1:3
        nexttile
        hold on
        for s = 1:9
            scatter(positions(s)+positionsScat,diffs(:,s,t),10,[0.1,0.1,0.1],'HandleVisibility','off')
        end
        boxplot(diffs(:,:,t),'Positions',positions,'ColorGroup',[1,2,3,1,2,3,1,2,3],'Colors',colors(1:3,:),'Symbol','') 

        xticks([2,5.25,8.5])
        xticklabels(["0.16","0.465","0.77"])
        set(findobj(gca,'type','line'),'linew',1.5)
        ax = gca; 
        ax.FontSize = 25; 
        grid on
        ylim([0,100])
        yticks([0,25,50,75,100])
        
        if t == 1
            plot([-1,0],[0,0],'color',colors(1,:),'LineWidth',2.5,'DisplayName','6 cm2')
            plot([-1,0],[0,0],'color',colors(2,:),'LineWidth',2.5,'DisplayName','10 cm2')
            plot([-1,0],[0,0],'color',colors(3,:),'LineWidth',2.5,'DisplayName','20 cm2')
            legend('location','northeast')
            xlim([min(positions)-1,max(positions)+1])
            %hsp1 = get(gca, 'Position');
        end
        if t == 2
            ylabel(strcat("Percent recordability increase for ",ROIs(ROI_i)))
            %hsp2 = get(gca, 'Position') ;
            %set(gca, 'Position', [hsp2(1:3)  hsp1(4)])    % Use 2*(2,1,1) Height for (2,1,2)
        end
        if t == 3
            xlabel("Dipole moment density (nAm/mm2)")
            %hsp3 = get(gca, 'Position') ;
            %set(gca, 'Position', [hsp3(1:3)  hsp1(4)])    % Use 2*(2,1,1) Height for (2,1,2)
        end
    end


    if ROI_i==1
        fileName = strcat('Figures/SuppFigure2b.pdf');
    elseif ROI_i==2
        fileName = strcat('Figures/SuppFigure2a.pdf');
    end
    print(f,fileName,'-bestfit','-dpdf');

    
end



function [data_imp,data_imp_ROIAreas] = saveOrOpenCisImplant(redoCalc, ROI_i,sourceInd,patchAreas,cdmds,dataWorkPath,patientIDs,costFnStrs)
ROIs_full = ["clinicianROI","LTL","LH"];
ROI_string = ROIs_full(ROI_i);
data_imp = cell(length(patientIDs),3);
data_imp_ROIAreas = cell(length(patientIDs),3);

fileName = strcat('RawData/OptSolutions/Imp_ROI',num2str(ROI_i),'_source',num2str(sourceInd),".mat");

clinBroad = true;

if redoCalc || exist(fileName,'file')==0
patIDi = 1;
for patientID = patientIDs
    patientDir = ['Patient_' num2str(patientID)]
    
    load(strcat(dataWorkPath,patientDir,'/OptSolutions/ROIs.mat'),'patches')
    load(strcat(dataWorkPath,patientDir,'/HeadModel/cortex_areas.mat'),'cortex_areas')
    
    % four patients have no implanted TL ROI
    if ROI_i == 2 && (patientID == 3 || patientID == 7 || patientID == 19 || patientID == 33)
        patIDi = patIDi + 1;
        continue
    end
        
    tys_cA = strcat(num2str(patchAreas(sourceInd)),"cm2_",num2str(cdmds(sourceInd)),"cdmd");
    load(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataImplantConfig_singleMap_crossAnalyze',tys_cA,'.mat'),'visDataImplantConfig')
    
    % two patients have RTL ROIs, so choose clinicianROI that corresponds
    if ROI_i == 2 && ( patientID == 24 || patientID == 31  )
        ROI_string = "clinicianROI";
        dipolesROI = patches.dipoles{1};
    else
        ROI_string = ROIs_full(ROI_i);
        dipolesROI = patches.dipoles{ROI_i};
    end
    clinBI = 0;
    for roii = 1:length(patches.name)
        if patches.name{roii} == "clinicianROI_broad"
            clinBI = roii;
            break
        end
    end
    if ROI_i == 1 && clinBroad && clinBI>0
        ROI_string = "clinicianROI_broad";
        dipolesROI = patches.dipoles{clinBI};
    end
    
    for i = 1:length(visDataImplantConfig)
        if isempty(visDataImplantConfig{i})
            continue
        end
        caseID = visDataImplantConfig{i}.imp_case_id;
        for thrI = 1:3
            if isempty(data_imp{patIDi,thrI}) &&  contains(caseID,tys_cA) && contains(caseID,costFnStrs(thrI)) && contains(caseID,ROI_string)
                data_imp{patIDi,thrI} = visDataImplantConfig{i}.percMapROI_imp_allThr{5};
                data_imp_ROIAreas{patIDi,thrI} = sum(cortex_areas(dipolesROI));
            end
        end
    end
    patIDi = patIDi + 1
end
    save(strcat('RawData/OptSolutions/Imp_ROI',num2str(ROI_i),'_source',num2str(sourceInd),".mat"),'data_imp')
    save(strcat('RawData/OptSolutions/ImpArea_ROI',num2str(ROI_i),'_source',num2str(sourceInd),".mat"),'data_imp_ROIAreas')
else
    load(strcat('RawData/OptSolutions/Imp_ROI',num2str(ROI_i),'_source',num2str(sourceInd),".mat"),'data_imp')
    load(strcat('RawData/OptSolutions/ImpArea_ROI',num2str(ROI_i),'_source',num2str(sourceInd),".mat"),'data_imp_ROIAreas')
end

end


function dat = saveOrOpenCisSearch(ROI_i,sourceInd, redoCalc, patientIDs,degMapStrs,costFnStrs,dataWorkPath,patchAreas,cdmds,ROIs,saveTag)

fileName = strcat('RawData/OptSolutions/Search_ROI',num2str(ROI_i),'_source',num2str(sourceInd),"_",saveTag,".mat");

if redoCalc || ~isfile(fileName)
dat = cell(length(patientIDs),length(degMapStrs)); % patients, thrs, LTL mapping percs
patIDi = 1;


if contains(saveTag,"compOpt")
    useImpTLROIs = true;
else
    useImpTLROIs = false;
end

for patientID = patientIDs
    patientDir = ['Patient_' num2str(patientID)]
    
    ROINameSearch = ROIs(ROI_i);
    ROI_s = ROI_i;
    if useImpTLROIs && ROINameSearch=="LTL" && (patientID == 24 || patientID == 31)
        ROINameSearch = "clinicianROI";
        ROI_s = find(ROIs=="clinicianROI");
    end
    
    tys_cA = strcat(num2str(patchAreas(sourceInd)),"cm2_",num2str(cdmds(sourceInd)),"cdmd");
    load(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_cis',tys_cA,'.mat'),'visDataConfigs')   
    
    count = 1;
    for d = 1:length(degMapStrs)
        degMapStr = degMapStrs(d);
        for i = 1:length(visDataConfigs)
            caseID = visDataConfigs{i}.full_case_id;
            if contains(caseID,costFnStrs(d)) && contains(caseID,degMapStr)
                if contains(caseID,ROINameSearch)
                    % if were analyzing TLs and pat is 24 or 31, need to use normal clinROI (not clinROI_broad)
                    if useImpTLROIs && ROIs(ROI_i)=="LTL" && ~isempty(dat{patIDi,count}) && ( patientID == 24 || patientID == 31  )
                        % for pats whos TL ROIs are 'clinROI' surface, dont
                        % pick the broad clinROI!
                        break
                    end
                    datT = visDataConfigs{i}.percMapROI;
                    dat{patIDi,count} = datT';
                    caseID
                end
            end
        end
        count = count + 1;
    end
    patIDi = patIDi + 1;
    
end

save(fileName,'dat')

else
    load(fileName,'dat')
end

end