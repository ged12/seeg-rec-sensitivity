% Valid Electrode Tree Search
% to find the electrode configuration that maps the whole ROI 
% with the fewest electrodes
% DEMO VERSION

% given all valid electrode locations and their recordable dipoles,
% and given an ROI, find the electrode configurations with the fewest 
% number of electrodes
% that maps the whole ROI (at least two electrodes can record from every
% dipole in the ROI).


% $$ UPDATE PARAMETERS $$
patientID = 25;
dataWorkPath = '';
dataHomePath = '';
cdmds = [0.465]; %nAm/mm^2
patchAreas = [10]; % cm^2
ROIs = ["clinicianROI","LH","LTL"]; 
    
% DEMO is only designed for ONE source type!
sourceNum = 1;

patientDir = ['Patient_' num2str(patientID)];

% get source case string identifier
cdmd = cdmds(sourceNum) % nAm/mm^2
patchArea = patchAreas(sourceNum) % cm^2
tys = strcat(num2str(patchArea),"cm2_",num2str(cdmd),"cdmd")

% open electrode-dipole recordability matrix
% this is a logical matrix that is size ExD (~95k x ~40k)

% Use integrated cost function that uses recordable dipoles @ 3 thresholds
% open all three recordable dipole matrices
cNumStrings = ["C1","C2","C3"];
% thresholds = [200, 500, 1000]; % µV
for c = 1:3
    cNumStr = cNumStrings(c);
    load(strcat(dataWorkPath,'RawData/RecArea/recDipolesbyE_recLocs',cNumStr,'_',tys,'.mat'),'recDipolesbyE')
    size(recDipolesbyE)
    info = whos('recDipolesbyE');
    sizeGB = info.bytes/(1024^3);
    disp(strcat('Electrode-dipole recordability matrix is ',num2str(sizeGB),' GB'))
    if c==1 
        recDipolesbyE_C1 = recDipolesbyE;
    elseif c==2
        recDipolesbyE_C2 = recDipolesbyE;
    else
        recDipolesbyE_C3 = recDipolesbyE;
    end
end

for ROI_i = 1:3
    
    disp(strcat("Running optimization for ROI ",num2str(ROI_i),": ",ROIs(ROI_i)))
    
    % clip these matrices to only dipoles in ROI and only electrodes that can
    % record from at least one dipole in ROI
    load(strcat(dataWorkPath,patientDir,'/DemoOut/ROIs.mat'),'patches')
    ROI_dips = patches.dipoles{ROI_i};
    
    % Get ROI string identifier
    case_id = strcat(tys,'_ROI_',ROIs{ROI_i},'_singleMap_');
    
    % Run Next-Best Search algorithm
    runDefaultSearchTrials(case_id,recDipolesbyE_C1, recDipolesbyE_C2, recDipolesbyE_C3, ROI_dips, dataWorkPath, patientDir);
    
end



function runDefaultSearchTrials(case_id,recDipolesbyE_C1, recDipolesbyE_C2, recDipolesbyE_C3, ROI_dips, dataWorkPath, patientDir)
    
    % the order of cost function priority is standard in the naming scheme:
    %   1000_500_200_thrPriority means 1000 µV is first, 500 µV is 2nd, 200
    %   µV is 3rd. 
    % For the inputs to this function, runDefaultSearchTrials,
    %   recDipolesbyE_C1 correspodns to 200 µV.
    %   recDipolesbyE_C2 corresponds to 500 µV.
    %   recDipolesbyE_C3 corresponds to 1000 µV. 
    
    % For the inputs to nextBest_search, the three recDipolesbyE matrices
    % should be placed in OPPOSITE order to their priority order. So for
    % the above example, (1000_500_200_thrPriority), we place inputs 200, then 500, then 1000, meaning
    % recDipolesbyE_C1, then recDipolesbyE_C2, then recDipolesbyE_C3.
    
    seed = 1;
    tic
    nextBest_search_demo(strcat(case_id,"1000_500_200_thrPriority"), recDipolesbyE_C1, recDipolesbyE_C2, recDipolesbyE_C3, ROI_dips, dataWorkPath, seed, patientDir);
    toc

    % With 500 µV as priority threshold, then 1000, then 200
    seed = 1;
    tic
    nextBest_search_demo(strcat(case_id,"500_1000_200_thrPriority"), recDipolesbyE_C1, recDipolesbyE_C3, recDipolesbyE_C2, ROI_dips, dataWorkPath, seed, patientDir);
    toc

    % With 200 µV as priority threshold, then 1000, then 500
    seed = 1;
    tic
    nextBest_search_demo(strcat(case_id,"200_1000_500_thrPriority"), recDipolesbyE_C2, recDipolesbyE_C3, recDipolesbyE_C1, ROI_dips, dataWorkPath, seed, patientDir);
    toc

    % With 200 µV as priority threshold, then 500, then 1000
    seed = 1;
    tic
    nextBest_search_demo(strcat(case_id,"200_500_1000_thrPriority"), recDipolesbyE_C3, recDipolesbyE_C2, recDipolesbyE_C1, ROI_dips, dataWorkPath, seed, patientDir);
    toc

    % With 1000 µV, 200, 500 threshold priorities
    seed = 1;
    tic
    nextBest_search_demo(strcat(case_id,"1000_200_500_thrPriority"), recDipolesbyE_C2, recDipolesbyE_C1, recDipolesbyE_C3, ROI_dips, dataWorkPath, seed, patientDir);
    toc

    % With 500 µV as priority threshold, then 200, then 1000
    seed = 1;
    tic
    nextBest_search_demo(strcat(case_id,"500_200_1000_thrPriority"), recDipolesbyE_C3, recDipolesbyE_C1, recDipolesbyE_C2, ROI_dips, dataWorkPath, seed, patientDir);
    toc
        
end

