% 9/2/21 % Grace Dessert % master script for all pipeline precomputations for generating lead field %

% 1) read Patient_#.config file and send data to writeParameters.m to write pipeline-specific scripts with custom parameters for whole pipeline
% 1.5) open central out and err files, and make all directories needed
% 2) validate inputs (cortex (LH & RH), skin, skull, brain, recordingLocs, conductivityTensors) 
% 3) calculate cortex surface normals and face areas
% 4) make Python scripts to make SCIRun networks for lead field
% 5) calculate midline -> call fn getMidline
% 6) get sulci surface -> call fn getSulciSurface

% the following steps are not a part of this script, but are run afterwards in sequence by the bash monitoring scripts:
% 7) Precompute Valid Electrode Locations
%   a) get valid insertion locs
%   b) add in angle variability
%   c) add in depth variability
%   d) remove electrodes that intersect sulci surface
%   e) calculate intersections between all electrodes (in parallel to steps 8-11)
% 8) Run scripts to make networks
% 9) Run networks to make forward solutions
% 10) compress lead field
% 11) precompute recordable dipoles for all valid electrodes 
% 12) optimization process

% inputs to pipeline (outputs of DecimateMesh.py) with specifications
%   fileNames = ["lh_white_20k.stl","rh_white_20k.stl","SkinFinal.stl","SkullFinal.stl","brainDown.stl","ConductivityTensors.mat","RecordingLocations.mat"]
%   minAndMaxRange = [("faces",19950,20050),("faces",19950,20050),("points",1500,2000),("points",340000,380000),("points",2000,3000)]

% $$ UPDATE PARAMETERS $$
patientID = '';
dataWorkPath = '';
dataHomePath = '';
cdmds = '';
patchAreas = '';
ROIs = '';


patientDir = strcat('Patient_',num2str(patientID));

%% check validity of inputs!!
% do all files exist in PatientData_$PATID$/Inputs directory?
% are the surfaces/fields the right resolution and size and position?

LH_fileName = strcat(dataHomePath,patientDir,'/Inputs/lh_white_20k.stl');
LH_white = stlread(LH_fileName);
RH_fileName = strcat(dataHomePath,patientDir,'/Inputs/rh_white_20k.stl');
RH_white = stlread(RH_fileName);
skinFN = strcat(dataHomePath,patientDir,'/Inputs/SkinFinal.stl');
skinSurf = stlread(skinFN);
cortexSurf_Points = [LH_white.Points;RH_white.Points];
skullFN = strcat(dataHomePath,patientDir,'/Inputs/SkullFinal.stl');
trisurf = stlread(skullFN);
brainFN = strcat(dataHomePath,patientDir,"/Inputs/brainDown.stl");
brainDown = stlread(brainFN);
load(strcat(dataHomePath,patientDir,"/Inputs/ConductivityTensors.mat"),'tetmesh');
load(strcat(dataHomePath,patientDir,"/Inputs/ContactLocations.mat"),'scirunfield');

%%
% check cortex surfaces and combine to make full cortex surface
cortexSurf_ConnectivityList = [LH_white.ConnectivityList;RH_white.ConnectivityList+length(LH_white.Points)];
cortexSurf = triangulation(cortexSurf_ConnectivityList,cortexSurf_Points);
stlwrite(cortexSurf,strcat(dataHomePath,patientDir,'/Inputs/cortexSurf.stl'));
numDipoles = size(cortexSurf.ConnectivityList,1);
disp(strcat("There are ",num2str(numDipoles)," dipoles in the cortex surface"));
if numDipoles < 39900 || numDipoles > 40100
    errMessage = strcat("ERROR: numDipoles  is NOT between ",num2str(39900)," and ",num2str(40100),".");
    disp(errMessage);
else 
    disp(strcat("Cortex surface validation passed. There are between 39900 and 40100 dipoles (faces)"));
end
%%
% check if skin surface
skinSurf = checkOutsideCortexandNumPoints(cortexSurf_Points,skinSurf,1500,2000,"Skin");
%%
% check skull surface
trisurf = checkOutsideCortexandNumPoints(cortexSurf_Points,trisurf,340000,380000,"Skull");
%%
% check brainDown surface
brainDown = checkOutsideCortexandNumPoints(cortexSurf_Points,brainDown,2000,3000,"brainDown");
%%
% check Conductivity Tensors
mess = "The bounds of the ConductivityTensor field are: ";
disp(mess);
if size(tetmesh.node,1)<size(tetmesh.node,2)
    tetmesh.node = tetmesh.node';
end
disp(num2str(max(tetmesh.node)));
disp(num2str(min(tetmesh.node)));
mess = strcat("There are ",num2str(length(tetmesh.node))," points in the ConductivityTensor field");
disp(mess);
tetmesh = []; % clear the conductivity tensors field cuz its so large and not needed
%%
% check RecordingLocations
mess = strcat("There are",num2str(length(scirunfield.node))," contacts in the clinician-generated electrode configuration (recording locs)");
disp(mess);

disp("The bounds of the RecordingLocations field are: ");
if size(scirunfield.node,1)<size(scirunfield.node,2)
    scirunfield.node = scirunfield.node';
end
disp(num2str(max(scirunfield.node)));
disp(num2str(min(scirunfield.node)));

%%
% plot skin, cortex, and recLocs in one plot and save
inputSurfaces = figure('visible','off');
clf
hold on
trimesh(skinSurf,'FaceAlpha',0.1,'FaceColor',[0 0 0.4],'EdgeColor',[0 0 0.4],'EdgeAlpha',0,'DisplayName','Skin')
trimesh(brainDown,'FaceAlpha',0.1,'FaceColor',[0.4 0.4 0],'EdgeColor',[0.4 0.4 0],'EdgeAlpha',0,'DisplayName','brainDown')
trimesh(trisurf,'FaceAlpha',0.1,'FaceColor',[0.4 0 0.4],'EdgeColor',[0.4 0 0.4],'EdgeAlpha',0,'DisplayName','Skull')
trimesh(cortexSurf,'FaceAlpha',0.1,'FaceColor',[0 0.4 0.4],'EdgeColor',[0 0.4 0.4],'EdgeAlpha',0.1,'DisplayName','Cortex')
scatter3(scirunfield.node(:,1),scirunfield.node(:,2),scirunfield.node(:,3),'filled','DisplayName','Recording Locs Implanted')
legend()
title(strcat("Input Surfaces & Recording Locations for Patient ",num2str(patientID)),"FontSize",14)
saveas(inputSurfaces,strcat(dataHomePath,patientDir,'/DemoOut/inputSurfaces'),'jpg');
saveas(inputSurfaces,strcat(dataHomePath,patientDir,'/DemoOut/inputSurfaces'),'fig');
% openfig('Patient_7/Test/inputSurfaces.fig','new','visible')

%%
% create python script to make SCIRun network to make head mesh

disp('Writing Python Script for Lead Field Generation SciRun Network');

% create Python file to update prepareLFComp.srn5 with correct file paths
fid = fopen(strcat(dataWorkPath,patientDir,'/DemoOut/prepareLFCompNetwork.py'),'w');
fprintf(fid,'import os\n');
fprintf(fid,strcat('scirun_load_network("' ,dataHomePath, 'InputGeneral/prepareLFComp.srn5")\n'));
fprintf(fid,strcat('scirun_set_module_state("ReadField:2","Filename","',dataHomePath,patientDir,'/Inputs/brainDown.stl")\n'));
fprintf(fid,strcat('scirun_set_module_state("ReadField:4","Filename","' ,dataHomePath,patientDir,'/Inputs/SkinFinal.stl")\n'));
fprintf(fid,strcat('scirun_set_module_state("ReadField:165","Filename","',dataWorkPath,patientDir,'/DemoOut/cortex_normals.mat")\n'));
fprintf(fid,strcat('scirun_set_module_state("ReadField:169","Filename","',dataWorkPath,patientDir,'/DemoOut/contactLocs.mat")\n'));
% set tetgen settings
% NOTE: BUG IN SETTING TETGEN SETTINGS, SO DON'T DO IT! JUST CHANGE TEMPLATE NETWORK
% MinRad of 2 for both, and Max VOl of 1 and 0.45 respectively gives mesh ~20M elements !!
% fprintf(fid,strcat('scirun_set_module_state("InterfaceWithTetGen:1","QualityFlag", "1")\n'));
% fprintf(fid,strcat('scirun_set_module_state("InterfaceWithTetGen:1","SetMaxVolConstraintFlag", "1")\n'));
% fprintf(fid,strcat('scirun_set_module_state("InterfaceWithTetGen:30","QualityFlag", "1")\n'));
% fprintf(fid,strcat('scirun_set_module_state("InterfaceWithTetGen:30","SetMaxVolConstraintFlag", "1")\n'));
% fprintf(fid,strcat('scirun_set_module_state("InterfaceWithTetGen:1","SetRatioFlag", "1")\n'));
% fprintf(fid,strcat('scirun_set_module_state("InterfaceWithTetGen:1","VolConstraintFlag", "1")\n'));
% fprintf(fid,strcat('scirun_set_module_state("InterfaceWithTetGen:30","SetRatioFlag", "1")\n'));
% fprintf(fid,strcat('scirun_set_module_state("InterfaceWithTetGen:30","VolConstraintFlag", "1")\n'));
% 
% fprintf(fid,strcat('scirun_set_module_state("InterfaceWithTetGen:1","MaxVolConstraint", "1")\n'));
% fprintf(fid,strcat('scirun_set_module_state("InterfaceWithTetGen:1","MinRadius", "2")\n'));
% fprintf(fid,strcat('scirun_set_module_state("InterfaceWithTetGen:30","MaxVolConstraint", "0.8")\n')); % change this to change mesh density
% fprintf(fid,strcat('scirun_set_module_state("InterfaceWithTetGen:30","MinRadius", "2")\n'));
% write field and matrix
fprintf(fid,strcat('scirun_set_module_state("WriteField:2","FileTypeName", "SCIRun Field Binary")\n'));
fprintf(fid,strcat('scirun_set_module_state("WriteField:2","Filename", "',dataWorkPath,patientDir,'/DemoOut/HeadTetMesh.fld")\n'));
fprintf(fid,strcat('scirun_set_module_state("WriteMatrix:0","FileTypeName", "Matlab Matrix")\n'));
fprintf(fid,strcat('scirun_set_module_state("WriteMatrix:0","Filename","',dataWorkPath,patientDir,'/DemoOut/HeadTetMesh_numElements.mat")\n'));
fprintf(fid,strcat('scirun_save_network("',dataWorkPath,patientDir,'/DemoOut/prepareLFCompNetwork.srn5")\n'));
fclose(fid);

%%
% calculate cortex normals and face areas
disp('Calculating cortex normals and face areas');

% if these already exist, just open them
if isfile(strcat(dataWorkPath,patientDir, '/DemoOut/lh_areas.mat'))
    load(strcat(dataWorkPath,patientDir, '/DemoOut/lh_areas.mat'), 'lh_areas')
    load(strcat(dataWorkPath,patientDir, '/DemoOut/rh_areas.mat'), 'rh_areas')
    load(strcat(dataWorkPath,patientDir, '/DemoOut/lh_normals.mat'), 'lh_normals')
    load(strcat(dataWorkPath,patientDir, '/DemoOut/rh_normals.mat'), 'rh_normals')
    load(strcat(dataWorkPath,patientDir, '/DemoOut/cortex_areas.mat'), 'cortex_areas')
    load(strcat(dataWorkPath,patientDir, '/DemoOut/cortex_normals.mat'), 'cortex_normals')
else
    lh_areas = get_cortex_areas(LH_white);
    rh_areas = get_cortex_areas(RH_white);
    lh_normals = get_cortex_normals(LH_white);
    rh_normals = get_cortex_normals(RH_white);
    cortex_areas = cat(find(size(lh_areas)==max(size(lh_areas))),lh_areas,rh_areas);
    cortex_normals.node = cat(find(size(lh_normals.node)==max(size(lh_normals.node))),lh_normals.node, rh_normals.node);
    cortex_normals.field = cat(find(size(lh_normals.field)==max(size(lh_normals.field))),lh_normals.field, rh_normals.field);
    save(strcat(dataWorkPath,patientDir, '/DemoOut/lh_areas.mat'), 'lh_areas')
    save(strcat(dataWorkPath,patientDir, '/DemoOut/rh_areas.mat'), 'rh_areas')
    save(strcat(dataWorkPath,patientDir, '/DemoOut/lh_normals.mat'), 'lh_normals')
    save(strcat(dataWorkPath,patientDir, '/DemoOut/rh_normals.mat'), 'rh_normals')
    save(strcat(dataWorkPath,patientDir, '/DemoOut/cortex_areas.mat'), 'cortex_areas')
    save(strcat(dataWorkPath,patientDir, '/DemoOut/cortex_normals.mat'), 'cortex_normals')
end

% 
% lh_areas = get_cortex_areas(lh);
% rh_areas = get_cortex_areas(rh);
% lh_normals = get_cortex_normals(lh);
% rh_normals = get_cortex_normals(rh);
% 
% cortex_areas = cat(find(size(lh_areas)==max(size(lh_areas))),lh_areas,rh_areas);
% cortex_normals.node = cat(find(size(lh_normals.node)==max(size(lh_normals.node))),lh_normals.node, rh_normals.node);
% cortex_normals.field = cat(find(size(lh_normals.field)==max(size(lh_normals.field))),lh_normals.field, rh_normals.field);

    
%%
% get midline
disp('Getting Midline');

if isfile(strcat(dataWorkPath,patientDir, '/DemoOut/midlinePlane.mat'))
    load(strcat(dataWorkPath,patientDir, '/DemoOut/midlinePlane.mat'), 'midlinePlane')
    disp("Found midline files, loading and moving on...");
else
    [n, p, midlinePointField, midlinePlaneField, midlinePlot] = getMidline_demo(lh_normals, rh_normals, lh_areas, rh_areas, LH_white, RH_white);
    midlinePlane = [n, p];
    save(strcat(dataWorkPath,patientDir, '/DemoOut/midlinePlane.mat'), 'midlinePlane')
    save(strcat(dataWorkPath,patientDir, '/DemoOut/midlinePointField.mat'), 'midlinePointField')
    save(strcat(dataWorkPath,patientDir, '/DemoOut/midlinePlaneField.mat'), 'midlinePlaneField')
    % visualize midline to check
    saveas(midlinePlot,strcat(dataHomePath,patientDir, '/DemoOut/midlinePlane'),'jpg');
    saveas(midlinePlot,strcat(dataHomePath,patientDir, '/DemoOut/midlinePlane'),'fig');
    % remember when opening, you must turn visibility to 'on'
    % ie: openfig('midlinePlot.fig','new','visible')

    % print info about midline to check
    disp("Midline has a normal of: ");
    disp(num2str(n'));
end


%%
% get insertion locs
mess = 'Getting insertion locs';
disp(mess);
% open MNI brain and MNI ins locs template
if ~isfile(strcat(dataWorkPath,patientDir,'/DemoOut/SkinValidForElectrodeImplant_insPoints.mat'))
    scirunfield = getSkinInsertionLocs_demo(skinSurf, dataHomePath, dataWorkPath, midlinePlane, patientID, LH_white, RH_white);
else
    mess = "Found SkinValidForElectrodeImplant_insPoints.mat, opening and skipping new generation...";
    disp(mess);
    load(strcat(dataWorkPath,patientDir,'/DemoOut/SkinValidForElectrodeImplant.mat'),'scirunfield');
end

%%
% make Python scripts
mess = 'Making Python Scripts for Networks for Lead Field';
disp(mess);

scriptsDir=dir(strcat(dataWorkPath,patientDir,'/DipoleAutoScripts/*.py'));
numScripts=size(scriptsDir,1);
if numScripts ~= numDipoles+1
    mess = "Making 10 example Python Scripts for Networks for Lead Field";
    disp(mess);
    WriteAutoPythonScripts_Singular_demo(dataWorkPath, dataHomePath, patientDir, 10)
else
    disp("Found Python scripts already made for networks for Lead Field");
end


%%
% get sulci surface
disp('Getting sulci surface');
disp("Attempting to load sulci files...");
sulcSurfLHFN = strcat(dataWorkPath,patientDir,'/DemoOut/LH_smoothSurfSulciTri.stl');
sulcSurfRHFN = strcat(dataWorkPath,patientDir,'/DemoOut/RH_smoothSurfSulciTri.stl');

try
    load(strcat(dataWorkPath,patientDir,'/DemoOut/SulciSurface_refined_field.mat'), 'sulciPointsVis')
    load(strcat(dataWorkPath,patientDir,'/DemoOut/SulciSurface_refined.mat'),'sulciPoints');
    LH_smoothSurfSulciTri = stlread(sulcSurfLHFN);
    RH_smoothSurfSulciTri = stlread(sulcSurfRHFN);
catch
    disp("We didn't find all the sulci files, getting sulci surface now...");
    lhSmooth = stlread(strcat(dataHomePath,patientDir,'/Inputs/lh_white_20k_SUPERSMOOTH.stl'));
    rhSmooth = stlread(strcat(dataHomePath,patientDir,'/Inputs/rh_white_20k_SUPERSMOOTH.stl'));
    [sulciPoints, LH_smoothSurfSulciTri, RH_smoothSurfSulciTri] = getSulciSurface_demo(LH_white, RH_white, lhSmooth, rhSmooth, midlinePlane, dataHomePath, dataWorkPath, patientDir);
end

sulciPointsVis.node = sulciPoints;
sulciPointsVis.field = zeros(length(sulciPoints),1);
sulcPointsVisFN = strcat(dataWorkPath,patientDir,'/DemoOut/SulciSurface_refined_field.mat');
save(sulcPointsVisFN, 'sulciPointsVis')
sulcPointsFN = strcat(dataWorkPath,patientDir,'/DemoOut/SulciSurface_refined.mat');
save(sulcPointsFN,'sulciPoints');

% check # sulci elements to validate
% we want around 47k points (compared to original head model)
% really, we should just check accuracy of collision tests
disp(strcat("There are ",num2str(length(LH_smoothSurfSulciTri.Points))," points and ",num2str(length(LH_smoothSurfSulciTri.ConnectivityList))," faces in the LH sulci surface"));
disp(strcat("There are ",num2str(length(RH_smoothSurfSulciTri.Points))," points and ",num2str(length(RH_smoothSurfSulciTri.ConnectivityList))," faces in the RH sulci surface"));
disp(strcat("This brings us to",num2str(length(LH_smoothSurfSulciTri.Points)+length(RH_smoothSurfSulciTri.Points))," points total and ",num2str(length(LH_smoothSurfSulciTri.ConnectivityList)+length(RH_smoothSurfSulciTri.ConnectivityList))," faces total. \n\tIdeally, we want to have around 47k points and 86k faces"));

disp("Testing sulci surface resolution now using electrode intersection tests");


%%
% test required resolution for accurate collision tests for sulci

[LH_smoothSurfSulciTri, ~, incrSurfRes] = testECollisionAccuracySuface(LH_smoothSurfSulciTri, scirunfield, "LH sulci");

if incrSurfRes
    stlwrite(LH_smoothSurfSulciTri, sulcSurfLHFN)
    disp(strcat("Writing new LH Sulci surface to original ",sulcSurfLHFN," file!"));

    [RH_smoothSurfSulciTri, ~, ~] = testECollisionAccuracySuface(RH_smoothSurfSulciTri, scirunfield, "RH sulci");

    stlwrite(RH_smoothSurfSulciTri, sulcSurfRHFN)
    disp(strcat("Writing new RH Sulci surface to original ",sulcSurfRHFN," file!"));
    
    sulciPoints = [LH_smoothSurfSulciTri.Points;RH_smoothSurfSulciTri.Points];
    sulciPointsVis.node = sulciPoints;
    sulciPointsVis.field = zeros(length(sulciPoints),1);
    save(sulcPointsVisFN, 'sulciPointsVis')
    sulcPointsFN = strcat(dataWorkPath,patientDir,'/DemoOut/SulciSurface_refined.mat');
    save(sulcPointsFN,'sulciPoints');
    
end


%%
[trisurf, ~, incrSurfRes] = testECollisionAccuracySuface(trisurf, scirunfield, "Skull");
if incrSurfRes
    stlwrite(trisurf, skullFN);
    disp(strcat("Writing new Skull surface to original ",skullFN," file!"));
end

%% 
% closing up
disp("LeadFieldMain.m is finished!");


function [surfRet, errPercent, incrSurfRes] = testECollisionAccuracySuface(surface, skinValidEImplant, nameSurf)

    if exist('refinepatch_version2b','dir')
        addpath('refinepatch_version2b')
    end
    if exist('DemoScripts','dir')
        addpath('DemoScripts')
        addpath('DemoScripts/refinepatch_version2b')
    end
    
    % refine surface until > 100k points
    surf_refined.faces = surface.ConnectivityList;
    surf_refined.vertices = surface.Points;
    
    while length(surf_refined.vertices) < 100000
        surf_refined = refinepatch(surf_refined);
    end
    disp(strcat("Testing electrode collision accuracy for ",nameSurf))
    surf_refined_input.ConnectivityList = surf_refined.faces;
    surf_refined_input.Points = surf_refined.vertices;

    % get starting locs and normals for test electrodes using skin valid for electrode implant
    [start_locs, normals_start] = getSLocs_and_normals(skinValidEImplant);

    % calc intersections w refined and test surfaces
    [BVH_surface_pointIndsREF,BVH_boundingBoxesREF,childrenIndsREF] = boundingVolumeHierarchyConstruction_demo(surf_refined_input);
    
    % using a 2.5 mm threshold of intersection (max distance from a surface point to an electrode points that we would consider an intersection)
    threshold = 2.5;
    
    errPercent = 10;  % arbitrary number larger than 0.5
    
    errGoal = 1;
    count = 1;
    while errPercent > errGoal 
        disp(strcat("Attempt #",num2str(count),": Comparing current ",nameSurf," surface with ",num2str(length(surface.Points))," points to a refined surface with ",num2str(length(surf_refined.vertices))))
        if length(surface.Points) >= length(surf_refined.vertices)
            disp("These are the same surface, or the current one is more refined than the test one... moving on")
            break
        end
        % make BVH of current surface
        [BVH_surface_pointInds,BVH_boundingBoxes,childrenInds] = boundingVolumeHierarchyConstruction_demo(surface);
        
        if count == 1
            intersectionTruth = zeros(length(start_locs),20);
        end
        intersectionTest = zeros(length(start_locs),20);

        % for all starting (insertion) locations, sample 10 insertion angles, and calculate occurrences of intersection with both surfaces
        for i=1:length(start_locs)
            sloc = start_locs{i};
            nS = normals_start{i};

            for j=1:20  % (20 total angles)
                x_rot = j; % x angle will be 1:20
                z_angles = linspace(0,359,20); % z angle will be spaced evenly 0 to 359
                z_rot = z_angles(j);

                % perform rotation, first transforming to the origin
                phi = atand(sqrt((nS(1)^2)+(nS(2)^2))/nS(3));
                if (((nS(1)^2)+(nS(2)^2))>0 && nS(3)<0 || ((nS(1)^2)+(nS(2)^2))<0 && nS(3)<0) % if in 3rd or 4th quadrant
                    phi = phi + 180;
                end
                theta = atand(nS(2)/nS(1));
                if (nS(2)>0 && nS(1)<0 || nS(2)<0 && nS(1)<0) % if in 3rd or 4th quadrant
                    theta = theta + 180;
                end
                n1 = rotz(-theta)*nS;
                nT = roty(-phi)*n1; 
                % now that we have ~rotated~ nS to nT, we can apply rotations around the x y and z axes
                %   n rotated by x_rot degrees around x axis = rotx(x_rot)*n
                %   where rotx(x_rot) = [1 0 0;0 cos(x_rot) -sin(x_rot);0 sin(x_rot) cos(x_rot)];
                n1 = rotx(x_rot)*nT; % rotate around x axis by x_rot degrees 
                n1 = n1./norm(n1);
                nR = rotz(z_rot)*n1; % rotate around z axis by z_rot degrees 
                nR = nR ./ norm(nR); 
                n1 = roty(phi)*nR; 
                normal = rotz(theta)*n1; 

                % use a depth index of 5 to 10 (how deep electrode is along
                % "electrode line" determined by insertion loc and angle)
                k=5+floor(j/2);
                if count == 1
                    [intersectionREF,~] = boundingVolumeHierarchy_demo(sloc+3*k*normal,normal,threshold,[],0,BVH_surface_pointIndsREF,BVH_boundingBoxesREF,childrenIndsREF,128);
                    intersectionTruth(i,j) = intersectionREF;
                end
                [intersection,~] = boundingVolumeHierarchy_demo(sloc+3*k*normal,normal,threshold,[],0,BVH_surface_pointInds,BVH_boundingBoxes,childrenInds,128);
                intersectionTest(i,j) = intersection;
            end
        end
        % count error 
        diffInt = intersectionTest-intersectionTruth;
        disp("Clipping collision tests of interest to only those that result in an intersection for at least one of the surfaces")
        diffInt = diffInt((intersectionTest+intersectionTruth)>0);
        errRatio = sum(sum(abs(diffInt)))/numel(diffInt);
        errPercent = errRatio*100;
        disp(strcat("After calculating ",num2str(numel(intersectionTruth))," intersection tests, we found ",num2str(sum(sum(intersectionTruth)))," intersections with the refined ",nameSurf," surface"));
        disp(strcat("And ",num2str(sum(sum(intersectionTest)))," intersections with the test sulci surface"));
        disp(strcat("There are",num2str(sum(sum(abs(diffInt))))," differences in these intersections"));
        disp(strcat("This gives us an error percentage of ",num2str(errPercent),"%"));
        if errPercent > errGoal
            errMessage = strcat("ERROR: We have a significant intersection test error(> ",num2str(errGoal),") for the ",nameSurf," surface. It may need to be refined more! Attempting to refine surface and run again!");
            disp(errMessage);
            surface1.faces = surface.ConnectivityList;
            surface1.vertices = surface.Points;
            surface1 = refinepatch(surface1);
            surface = [];
            surface.ConnectivityList = surface1.faces;
            surface.Points = surface1.vertices;
            count = count + 1;
        else
            m = strcat("We have < ",num2str(errGoal)," intersection test error between the ",nameSurf," surface and a more refined one. Nice!");
            disp(m);
            if count > 1
                disp(strcat("ERROR RESOLVED MESSAGE from LeadFieldMain: ",m));
            end
        end
    
    end
    incrSurfRes = false;
    if count > 1
        incrSurfRes = true;
    end
    
    surfRet = triangulation(surface.ConnectivityList, surface.Points);

end


function [areas] = get_cortex_areas(FV)
    areas = zeros(length(FV.ConnectivityList),1);
    for i = 1:length(areas) % for each face
        face_points = [FV.ConnectivityList(i,1),FV.ConnectivityList(i,2),FV.ConnectivityList(i,3)];
        a = face_points(1);
        pa = FV.Points(a,1:3);
        b = face_points(2);
        pb = FV.Points(b,1:3);
        b = face_points(3);
        pc = FV.Points(b,1:3);
        %calc area from points
        areas(i) = 0.5 * norm(cross(pb-pa,pc-pa));
        %format = "face: %.0f \n    face_points: %.0f %.0f %.0f \n    area: %f \n";
        %fprintf(format, x, face_points, area)
    end
end

function [normals] = get_cortex_normals(FV)

    FVP = FV.Points;
    FVC = FV.ConnectivityList;
    %
    for k = 1:length(FVC)
        %get three points of each triangle
        v1 = FVC(k,1);
        v2 = FVC(k,2);
        v3 = FVC(k,3);
    
        %get coordinates of each point
        a = FVP(v1,:);
        b = FVP(v2,:);
        c = FVP(v3,:);
    
        %calc cross product of two sides
        m = cross(b-a,c-a);
        
        %calc normal vector
        n = m/norm(m);
    
        %calc position by components
        x = (a(1) + b(1) + c(1)) / 3;
        y = (a(2) + b(2) + c(2)) / 3;
        z = (a(3) + b(3) + c(3)) / 3;
    
    %assemble position into vector
        p = [x,y,z];
    
    %put normal vector and position into matrix of row k
        if k == 1
            CDN_n = zeros([length(FVC),3]);
            CDN_p = zeros([length(FVC),3]);
        end
        CDN_n(k,:) = n;
        CDN_p(k,:) = p;
        
    end
  
    normals.node = transpose(CDN_p);
    normals.field = transpose(CDN_n);
    
    %figure(1)
    %clf
    %quiver3(CDN_p(:,1),CDN_p(:,2),CDN_p(:,3),CDN_n(:,1),CDN_n(:,2),CDN_n(:,3),'LineWidth',1);
    %hold on
    %trimesh(FV)
    % how to make sure normals are pointing correct way??
    
end

function lines = readlines(fileName)
    % Opens the file
    fid = fopen(fileName,'rt');

    maxl = -1; % read whole file
    lines = textscan(fid,'%s',maxl,'delimiter', '\n', 'whitespace','');
    if ~isempty(lines)
        lines = lines{1};
    end
    % Close file
    fclose(fid);
end


function [n,V,p] = affine_fit(X)

% Copyright (c) 2016, Fangdi Sun
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
% 
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in
%       the documentation and/or other materials provided with the distribution
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.


    %Computes the plane that fits best (lest square of the normal distance
    %to the plane) a set of sample points.
    %INPUTS:
    %
    %X: a N by 3 matrix where each line is a sample point
    %
    %OUTPUTS:
    %
    %n : a unit (column) vector normal to the plane
    %V : a 3 by 2 matrix. The columns of V form an orthonormal basis of the
    %plane
    %p : a point belonging to the plane
    %
    %NB: this code actually works in any dimension (2,3,4,...)
    %Author: Adrien Leygue
    %Date: August 30 2013
    
    %the mean of the samples belongs to the plane
    p = mean(X,1);
    
    %The samples are reduced:
    R = bsxfun(@minus,X,p);
    %Computation of the principal directions if the samples cloud
    [V,D] = eig(R'*R);
    %Extract the output from the eigenvectors
    n = V(:,1);
    V = V(:,2:end);
end

function [start_locs, normals_start] = getSLocs_and_normals(scirunfield)
    numSLocs = length(scirunfield.node);
    start_locs = cell(1,numSLocs);
    if size(scirunfield.node,2) == 3
        scirunfield.node = scirunfield.node';
        scirunfield.field = scirunfield.field';
    end
    for i = 1:numSLocs
        start_locs{i} = scirunfield.node(:,i);
    end
    % for every starting loc, get normal into brain
    normals_start = cell(1,numSLocs);
    for i = 1:numSLocs
        % get normal vector to skin for direction of implant
        % get closest points in skin to starting loc
        distances = sqrt(sum((scirunfield.node-start_locs{i}).^2,1));
        [d,di] = sort(distances);
        closest_pts = scirunfield.node(:,di(1:6));
        one = scirunfield.node(:,di(1)); % same as start_locs(:,i). check
        two = scirunfield.node(:,di(2));
        three = scirunfield.node(:,di(3));
        % fit closest points to a plane and get normal of the plane
        [n,~,~] = affine_fit(closest_pts');
        normals_start{i} = n;

        % make sure normal is facing correct direction.
        % first get farthest point away from starting loc in skin 
        % then get distance between that and starting loc + 120 mm along normal
        % and that and starting loc - 120 mm along normal
        % if first distance is larger, then flip normal 
        farthest_pt = scirunfield.node(:,di(end));
        posNormDist = sqrt(sum((farthest_pt-(start_locs{i}+120*normals_start{i})).^2,1));
        negNormDist = sqrt(sum((farthest_pt-(start_locs{i}-120*normals_start{i})).^2,1));
        if posNormDist>negNormDist
        normals_start{i} = -1*normals_start{i};
        end
    end
end

function surf = checkOutsideCortexandNumPoints(cortexSurf_Points,surf,minPoints,maxPoints,nameSurf)
    testPoints = surf.Points;
    disp(strcat("The bounds of the ",nameSurf," surface are: "));
    if size(testPoints,1)<size(testPoints,2)
        testPoints = testPoints';
    end
    disp(num2str(max(testPoints)));
    disp(num2str(min(testPoints)));
    if sum(max(cortexSurf_Points) > max(testPoints)) > 0 || sum(min(cortexSurf_Points) < min(testPoints)) > 0
        errMessage = strcat("ERROR: ",nameSurf," surface is not outside cortex surface!!");
        disp(errMessage);
        disp(strcat("ERROR MESSAGE from LeadFieldMain: ",errMessage));
    else
       disp(strcat(nameSurf," surface validation 1 of 2 passed. The cortex is fully inside of it"));
    end
    disp(strcat("There are ",num2str(length(testPoints))," points in the ",nameSurf," surface"));
    if length(testPoints) < minPoints || length(testPoints) > maxPoints
        errMessage = strcat("ERROR: ",nameSurf," surface with ",num2str(length(testPoints))," is NOT between ",num2str(minPoints)," and ",num2str(maxPoints)," points. Check DecimateMesh.py script!!");
        disp(errMessage);
    else 
        disp(strcat(nameSurf," surface validation 2 of 2 passed. There are between ",num2str(minPoints)," and ",num2str(maxPoints)," points."));
    end
    
end


function rot = rotz(angle)
    rot = [cos(angle) -sin(angle) 0; sin(angle) cos(angle) 0; 0 0 1];
end

function rot = rotx(angle)
    rot = [1 0 0; 0 cos(angle) -sin(angle); 0 sin(angle) cos(angle)];
end

function rot = roty(angle)
    rot = [cos(angle) 0 sin(angle); 0 1 0; -sin(angle) 0 cos(angle)];
end


