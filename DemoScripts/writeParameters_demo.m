% 2/28/22
% This function writes all scripts used for sEEG Opt pipeline
%   to change patient-specific and instance-specific filepaths and
%   parameters.
% Also changes slurm scripts for script paths and array values

% function writeParameters(patientID, cdmd, patchArea, dataWorkPath, dataHomePath, numDipoles)
% inputs:
%   patientID - integer of patientID
%   cdmds - array of floats of constant current dipole moment density in nAm/mm^2
%   patchAreas - array of areas in cm^2 of patches for extended dipole sources
%       should be between 0.5 and 9.5 cm^2
%   dataWorkPath, dataHomePath - filepaths of locations of output and input
%       files, respectively
%       Input files must be in a directory named: strcat('Patient_',num2str(patientID)) 
%       inside the dataHomePath
%       All scripts must be located in dataHomePath

% set parameters!
% params = ["patientID", "cdmds", "patchAreas", "dataWorkPath", "dataHomePath","ROIs","numDipoles"];
% patientID = 1; 
% cdmds = [10, 1, 0.2, 0.2]; %nAm/mm^2
% patchAreas = [5, 9.5, 5, 9.5]; % cm^2
% dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';
% dataHomePath = '/hpc/group/wmglab/ged12/cluster/Optimize_sEEG_implant/';
% numDipoles = 39954;
% ROIs = ["L10","L10","R10","R10","LTL","LH","WC"]; % array of length 7
% % future: thresholds parameter (right now hard coded in 100, 500 & 1000 µV);

function writeParameters_demo(patientID, cdmds, patchAreas, dataWorkPath, dataHomePath, ROIs)
params = ["patientID", "cdmds", "patchAreas", "dataWorkPath", "dataHomePath","ROIs","numDipoles"];

% get str versions
patientID_s = strcat(num2str(patientID),";"); %"1;"; 
cdmds_s = strcat("[",strjoin(arrayfun(@(a)num2str(a),cdmds,'uni',0), ", "),"];"); % "[1, 1, 0.2, 0.2];"; %nAm/mm^2
patchAreas_s = strcat("[",strjoin(arrayfun(@(a)num2str(a),patchAreas,'uni',0), ", "),"];"); % "[5, 9.5, 5, 9.5];"; % cm^2
dataWorkPath_s = strcat("'",dataWorkPath,"';"); %"'/work/ged12/Optimize_sEEG_implant/';"
dataHomePath_s = strcat("'",dataHomePath,"';"); %"'/hpc/group/wmglab/ged12/cluster/Optimize_sEEG_implant/';";
ROIs_s = strcat('["',strjoin(ROIs, '", "'),'"];');
numDipoles = 5; 
numDipoles_s = strcat(num2str(numDipoles),";"); %"1;";
paramValues = [patientID_s, cdmds_s, patchAreas_s, dataWorkPath_s, dataHomePath_s, ROIs_s, numDipoles_s];

% open files that contain parameters
test = 0;
scriptLoadPath = 'DemoScripts/';
if ~test
    scriptLoadPath = strcat(dataHomePath,scriptLoadPath);
end

patientDir = ['Patient_' num2str(patientID)];
% filesToEdit = {strcat("exampleChangeParams.m")};
filesToEdit = [strcat(scriptLoadPath, "precompRecAreaAllElectrodes_demo.m"), ...
               strcat(scriptLoadPath, "precomputeValidElectrodeLocs_demo.m") ...
               strcat(scriptLoadPath, "makePatchAllMatrices_demo.m") ...
               strcat(scriptLoadPath, "getSkinInsertionLocs_demo.m") ...
               strcat(scriptLoadPath, "eCollCompute_lines_demo.m") ...
               strcat(scriptLoadPath, "TreeSearch_demo.m") ...
               strcat(scriptLoadPath, "getROIs_demo.m") ...
               strcat(scriptLoadPath, "boundingVolumeHierarchy_demo.m") ...
               strcat(scriptLoadPath, "nextBest_search_demo.m") ...
               strcat(scriptLoadPath, "boundingVolumeHierarchyConstruction_demo.m") ...
               strcat(scriptLoadPath, "LeadFieldMain_demo.m") ...
               strcat(scriptLoadPath, "WriteAutoPythonScripts_Singular_demo.m") ... 
               strcat(scriptLoadPath, "getSulciSurface_demo.m") ... 
               strcat(scriptLoadPath, "getMidline_demo.m") ... 
               strcat(scriptLoadPath, "getSkinInsertionLocs_demo.m") ... 
               strcat(scriptLoadPath, "visLoadForSCIRun_demo.m") ... 
               strcat(scriptLoadPath, "getStContactLocs_demo.m") ... 
               ];
           
% search for query string 
queryTag = '% $$ UPDATE PARAMETERS $$';

% parse following lines until empty (or just whitespace) line
for fInd = 1:length(filesToEdit)
    fileName = char(filesToEdit{fInd});
    
    if isfile(fileName)
        lines = readlines(fileName);
    else
        disp(strcat("Error! ",fileName, " not found!"));
        continue;
    end
    
    for i = 1:length(lines)
        if contains(lines{i},queryTag)
            disp(lines{i})
            nextLine = lines{i};
            while (~isempty(nextLine))
                for p = 1:length(params)
                    nextLineC = char(nextLine);
                    if (length(nextLineC)<length(char(params(p))))
                        continue;
                    end
                    if (nextLineC(1:length(char(params(p)))) == params(p))
                        newParamLine = strcat(params(p), " = ", paramValues(p));
                        lines{i} = char(newParamLine);
                        break;
                    end
                end
                i = i + 1;
                nextLine = lines{i};
            end
            break;
        end
    end
    
    spFN = split(fileName,"/");
%     if (length(spFN)==1)
%         newDirFN = patientDir;
%         fileNameE = strcat(patientDir,"/",spFN);
%     else
%         newDirFN = strcat(join(spFN(1:end-1),"/"),"/",patientDir);
%         fileNameE = strcat(newDirFN,"/",spFN(end));
%     end
    newDirFN = strcat(dataHomePath,patientDir,"/DemoScripts");
    fileNameE = strcat(newDirFN,"/",spFN(end));
    
    if ~exist(newDirFN, 'dir')
       mkdir(newDirFN)
    end
    fid = fopen(fileNameE,'w');
    for i = 1:length(lines)
        fprintf(fid,'%s\n',lines{i});
    end
    fclose(fid);
    
end

end

function lines = readlines(fileName)
    % Opens the file
    fid = fopen(fileName,'rt');

    maxl = -1; % read whole file
    lines = textscan(fid,'%s',maxl,'delimiter', '\n', 'whitespace','');
    if ~isempty(lines)
        lines = lines{1};
    end
    % Close file
    fclose(fid);
end


%{
% exampleChangeParams.m
% Grace Dessert
% March 4, 2022

% $$ UPDATE PARAMETERS $$
patientID = 1;
cdmd = 1; % nAm/mm^2 %%%%%%%%%%% need a way to automate changing this %%%%
dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';

disp(strcat("Current patientID is: ",num2str(patientID)))
disp(strcat("Current cdmd is: ",num2str(cdmd)))
disp(strcat("Current dataWorkPath is: ",dataWorkPath))
%}
