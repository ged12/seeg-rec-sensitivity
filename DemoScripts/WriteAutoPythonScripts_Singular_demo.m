%% Create LH and RH python scripts to create ~40,000 SCIRun networks to create basis of ~40,000 forward solutions

% 9/2/21 - generalized to any head model

% Single-Ended Recording only (not differential recording)

% Create python scripts make SCIRun networks for each dipole.
% Each script, to be run on the Duke Computing Cluster, creates a SCIRun 
% network to run a unit dipole current source on our FEM. Each dipole current source is located at the 
% centroid of one face on the cortex surface, oriented orthogonally. The SCIRun networks output 
% a forward solution, a field of voltages at all points of a 2mm-spaced grid of the head.


function WriteAutoPythonScripts_Singular_demo(dataWorkPath, dataHomePath, patientDir, numDipoles)

for i=1:numDipoles
    fid = fopen(strcat(dataWorkPath,patientDir,'/DemoOut/DipoleScript',num2str(i),'.py'),'w');

    fprintf(fid,'import os\n');
    fprintf(fid,strcat('scirun_load_network("',dataHomePath,'InputGeneral/PatientForwardDipoleAllAutomation.srn5")\n'));
    fprintf(fid,strcat('scirun_set_module_state("GetMatrixSlice:0","SliceIndex",',num2str(i-1),')\n'));
    fprintf(fid,strcat('scirun_set_module_state("ReadField:94","Filename", "',dataWorkPath,patientDir, '/HeadModel/HeadTetMesh.fld")\n'));
    fprintf(fid,strcat('scirun_set_module_state("ReadField:3","Filename","',dataHomePath, patientDir,'/Inputs/ConductivityTensors.mat")\n'));
    fprintf(fid,strcat('scirun_set_module_state("ReadField:4","Filename","',dataHomePath,patientDir, '/Inputs/SkinFinal.stl")\n'));
    fprintf(fid,strcat('scirun_set_module_state("ReadField:128","Filename","',dataWorkPath,patientDir, '/HeadModel/cortex_normals.mat")\n'));
    % set target error 1x10^(-10)
    fprintf(fid,strcat('scirun_set_module_state("SolveLinearSystem:13","TargetError","1e-10")\n'));
    % auto-gen electrode contact locs + clinically implanted set
    fprintf(fid,strcat('scirun_set_module_state("ReadField:93","Filename","',dataWorkPath,patientDir,'/ElectrodeLocsData/contactLocs.mat")\n'));

    fprintf(fid,strcat('scirun_set_module_state("WriteMatrix:0","FileTypeName", "Matlab Matrix")\n'));
    fprintf(fid,strcat('scirun_set_module_state("WriteMatrix:0","Filename","',dataWorkPath,patientDir, '/LeadField/FSs/HeadDipoleVoltageSampledAtRecLoc',num2str(i),'.mat")\n'));

    fprintf(fid,strcat('scirun_save_network("',dataWorkPath,patientDir,'/DemoOut/PatientForwardDipoleAllAutomation',num2str(i),'.srn5")\n'));
    fclose(fid);
end

end

