% Bounding Volume Hierarchy 
% new method 12/7/2020
% inspired by Sparks 2017 BVH method


% precompute tree to traverse
% store indices of points in each node
% as well as store bounding box of each cube
% and matrix of indices of children nodes so don't have to store null nodes
% for in-order traversal linear indexing

% each node represents one box
% that has a number of surface points inside it



% purpose: to get actual distance of electrode to surface
%           to remove intrinsic variability in previous method
%

% now, we calculate the intersection of points with boxes, rather than 
% boxes with boxes.
% now, we will traverse the tree all the way down to a leaf node every
% time in order to find the smallest distance of electrode to surface
% rather than stopping when a certain level was reached (side length < a
% threshold value, say 4mm) and we had only a range of distances that the
% electrode could be away from the surface. 

% inputs:
%   electrode starting location and normal vector
%       all electrodes are 54.5mm long.
%   surface triangulation 
%   threshold distance

%   three parameters that store precomputed BVH tree
%       for skull:
%       load('BVH_surface_pointInds.mat','BVH_surface_pointInds')
%       load('BVH_boundingBoxes.mat','BVH_boundingBoxes')
%       load('childrenInds.mat','childrenInds')
%       for sulci:
%


% outputs:
%   boolean of intersection
%   minimum distance from electrode to surface or electrode

% this method, adapted from Sparks2017, finds whether the distance is > threshold,
% and the specific distance if that distance is < threshold). 
% the method in Sparks2017, in contrast, can find the actual minimum distance
% from the electrode to the surface whether it is above or below the
% threshold. 

function [intersect,minDist] = boundingVolumeHierarchy_demo(sloc1,normal1,threshold,trisurf,visual,BVH_surface_pointInds,BVH_boundingBoxes,childrenInds,n)

intersect = 0;
endpoint1  = sloc1 + 54.5*normal1;
%n = 100;
E1_points = sloc1' + [0:1:n-1]'*((endpoint1-sloc1)/(n-1))';

%threshold = 4; %mm  
% min dist from electrode to surface to be called "no intersect"  
%add this distance to ~both sides~ of each dimension of bounding box
% adding this distance has the same effect as finding the closest distance from the
% point to the bounding box and thesholding that to decide if we should add
% the corresponding node to the queue or not

minDist = threshold+0.01; % distance from any electrode point to any point on surface
    % set to the threshold distance because if no intersect is called, we
    % have no idea what the true distance from the point to the surface is,
    % only that it is larger than 4
    
% save bounding_boxes to plot
bounding_boxIs_plot =  [];

% for  each point on E1 
for p=1:n %  n=100
    Epoint = E1_points(p,:);
% breadth first tree traversal
queue_nodeInds = [1];

while ~isempty(queue_nodeInds) % while nodes in queue

    % remove node from  queue
    node = queue_nodeInds(1);

    % shift queue
    queue_nodeInds = queue_nodeInds(2:end);

    % get bounding box of node
    bounding_box = BVH_boundingBoxes{node};
    bounding_boxIs_plot = [bounding_boxIs_plot;bounding_box];

    leafnode=0;
    % add buffer to all sides of box
    if size(bounding_box,1)==1 % node is leaf; there is only one point in this node
        % create box around single point with each side length of threshold
        leafnode = 1;
        leafPoint = bounding_box;
        bounding_box  = [bounding_box;bounding_box];
        bounding_box(1,:) = bounding_box(1,:) - threshold;
        bounding_box(2,:) = bounding_box(2,:) + threshold;
    else % node is not leaf, add buffer to all coords
        bounding_box(1,:) = bounding_box(1,:) - threshold*sign(diff(bounding_box));
        bounding_box(2,:) = bounding_box(2,:) + threshold*sign(diff(bounding_box));
    end

    % is point inside box?
    inside = pointInBox(Epoint,bounding_box);

    % if point is inside box, 
    if inside==true 
        % and if node is not  a leaf node
        if leafnode==0 
            % add both childen nodes to  queue
            queue_nodeInds = [queue_nodeInds,childrenInds(node,1)];
            queue_nodeInds = [queue_nodeInds,childrenInds(node,2)];
        else % and if node IS a leaf node
            % calculate distance from point to point inside leaf node
            dist = norm(leafPoint-Epoint);
            minDist = min(dist,minDist);
        end
    % else. point is not inside box, don't do anything.
    end

end
% if we don't want to find exact distance, only if under threshold:
if minDist <= threshold
    intersect = 1;
    break
end
% now queue is empty
% go to next electrode point. do not reset minDist
% in future, break once minDist for any point is < threshold
end
if minDist <= threshold
    %disp(['min distace from electrode to surface is: ' num2str(minDist) ' mm'])
    intersect = 1;
    %disp('intersect')
else
    %disp('min distace from electrode to surface is: > 4 mm')
end

if visual==1
visualizeBoundaryHierarchySurface(sloc1,sloc1+54.5*normal1,trisurf,bounding_boxIs_plot,[])
end

end


function [boolean] = pointInBox(point,bounding_box)
% if, for ALL of the x, y, and z directions, the length of the projection 
% of the direction vector (vector from point to the center of the box) onto each direction is
% smaller than or equal to half of the length of that corresponding side of
% the box, the point is in the box

% since the box is in the standard basis (x, y, and z, directions),
% the length of the projection of the direction vector onto each direction
% is just the specific coordinate of the vector

box_point1 = bounding_box(1,:);
box_point2 = bounding_box(2,:);

% get direction vector from point to center
ctr = (box_point1 + box_point2)/2;
dir_v = abs(ctr-point);

x_len = abs(box_point2(1)-box_point1(1));
y_len = abs(box_point2(2)-box_point1(2));
z_len = abs(box_point2(3)-box_point1(3));

% for every direction, if the length from ctr to point is less or equal to
% half of the length of that side of the box, the point is in the box
if ((dir_v(1)<=x_len/2) && (dir_v(2)<=y_len/2) && (dir_v(3)<=z_len/2))
    boolean = true; % point is in box
else
    boolean = false; % point is not in box
end
% given intersection of two boxes, the max distance that electrodes can be
% away from each other (at their closest point) is the max side length of
% a box. the minimum is of course 0.
%error = max([x_len,y_len,z_len]); % upper limit of distance between two electrodes given intersection
end



function []=visualizeBoundaryHierarchySurface(sloc,endpoint,trisurf,box_points_1,box_points_2)
figure(1)
clf
hold on
% plot electrode

plot3([sloc(1),endpoint(1)],[sloc(2),endpoint(2)],[sloc(3),endpoint(3)],'LineWidth',5)


% plot trisurf OR point field
if isfield(trisurf,'ConnectivityList')
    stlPlot(trisurf.Points,trisurf.ConnectivityList)
else
    scatter3(trisurf(:,1),trisurf(:,2),trisurf(:,3),'.')
end
%STLPLOT is an easy way to plot an STL object
%V is the Nx3 array of vertices
%F is the Mx3 array of faces

if ~isempty(box_points_1) || ~isempty(box_points_2)
% plot bounding box(es)
for m=1:ceil(size(box_points_1,1)/2)-1
    plotcube([box_points_1(m*2-1,1)-box_points_1(m*2,1),box_points_1(m*2-1,2)-box_points_1(m*2,2),box_points_1(m*2-1,3)-box_points_1(m*2,3)],box_points_1(m*2,:),0.05,[0.9 0.9 0.9])
%     if (box_points_2(m*2-1,:)==box_points_2(m*2-1,:))  % is not Nan
%     plotcube([box_points_2(m*2-1,1)-box_points_2(m*2,1),box_points_2(m*2-1,2)-box_points_2(m*2,2),box_points_2(m*2-1,3)-box_points_2(m*2,3)],box_points_2(m*2,:),0.2,[0.9 0.9 0.9])
%     end
end
end
end


function plotcube(varargin)
% PLOTCUBE - Display a 3D-cube in the current axes
%
%   PLOTCUBE(EDGES,ORIGIN,ALPHA,COLOR) displays a 3D-cube in the current axes
%   with the following properties:
%   * EDGES : 3-elements vector that defines the length of cube edges
%   * ORIGIN: 3-elements vector that defines the start point of the cube
%   * ALPHA : scalar that defines the transparency of the cube faces (from 0
%             to 1)
%   * COLOR : 3-elements vector that defines the faces color of the cube
%
% Example:
%   >> plotcube([5 5 5],[ 2  2  2],.8,[1 0 0]);
%   >> plotcube([5 5 5],[10 10 10],.8,[0 1 0]);
%   >> plotcube([5 5 5],[20 20 20],.8,[0 0 1]);

% Default input arguments
inArgs = { ...
  [10 56 100] , ... % Default edge sizes (x,y and z)
  [10 10  10] , ... % Default coordinates of the origin point of the cube
  .7          , ... % Default alpha value for the cube's faces
  [0.3 0 0]       ... % Default Color for the cube
  };

% Replace default input arguments by input values
inArgs(1:nargin) = varargin;

% Create all variables
[edges,origin,alpha,clr] = deal(inArgs{:});

XYZ = { ...
  [0 0 0 0]  [0 0 1 1]  [0 1 1 0] ; ...
  [1 1 1 1]  [0 0 1 1]  [0 1 1 0] ; ...
  [0 1 1 0]  [0 0 0 0]  [0 0 1 1] ; ...
  [0 1 1 0]  [1 1 1 1]  [0 0 1 1] ; ...
  [0 1 1 0]  [0 0 1 1]  [0 0 0 0] ; ...
  [0 1 1 0]  [0 0 1 1]  [1 1 1 1]   ...
  };

XYZ = mat2cell(...
  cellfun( @(x,y,z) x*y+z , ...
    XYZ , ...
    repmat(mat2cell(edges,1,[1 1 1]),6,1) , ...
    repmat(mat2cell(origin,1,[1 1 1]),6,1) , ...
    'UniformOutput',false), ...
  6,[1 1 1]);


cellfun(@patch,XYZ{1},XYZ{2},XYZ{3},...
  repmat({clr},6,1),...
  repmat({'FaceAlpha'},6,1),...
  repmat({alpha},6,1)...
  );

view(3);

end


function stlPlot(v, f)
%STLPLOT is an easy way to plot an STL object
%V is the Nx3 array of vertices
%F is the Mx3 array of faces
%NAME is the name of the object, that will be displayed as a title
object.vertices = v;
object.faces = f;
c = [0.8 0.8 1.0];
color = repmat(c,length(f),1);

patch(object,'FaceVertexCData',       color, ...
         'FaceColor', 'flat',                ...
         'EdgeColor',       'none',        ...
         'FaceLighting',    'gouraud',     ...
         'AmbientStrength', 0.15);
% Add a camera light, and tone down the specular highlighting
camlight('headlight');
material('dull');
% Fix the axes scaling, and set a nice view angle
axis('image');
view([-135 35]);
grid on;
end

