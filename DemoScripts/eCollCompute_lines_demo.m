% ************************************************************************
% eCollCompute_lines.m
%
% inputs:
% Given a set of valid electrodes stored in depth storage,
%       load('normals_reInd_lines.mat','normals_reInd_lines')
%       load('slocs_reInd_lines.mat','slocs_reInd_lines')
% a matrix of bounding boxes and their overlapping occurrence,
%       load('BBOverlap.mat','BBOverlap')
% an array that defines the electrode lines that are in each bounding box
%       load('electrodes_insertionLocCorr_depth.mat','track_insertionLocCorr_depth')
% and a cell array that gives the linear indices of the electrode lines in
%   each bounding box
%       load('insertionLocs_Einds_depth.mat','insertionLocs_Einds_depth')
%
% outputs:
% We calculate the intersection of every electrode with every electrode
%   The collision matrix, of size NxN, is stored in chunks of 10000 rows
%   each 
% 
% methods:
% Calculate intersections between lines, as points on electrodes overlap
% exactly. 
% New electrode line discretization method: 9/14/21
% Use 3.5mm separation between electrodes so contacts line up exactly
%   use 3.5/8 mm spacing for points between electrodes with 126 points
%   making up each electrode (0.4375 * 125 = 54.6875mm length of electrode)
%   To keep full electrode line < 10cm, there can at most 14 electrodes
%   per line (13 depth iterations)
%       (3.5/8 * 125 + (13*3.5) = 100.1875)
% Thus, each full electrode line has 230 points. This method assumes that
% all lines are full for initial computation, and then maps the
% intersection occurrences onto only the electrodes in the lines that are
% valid.

% Previous method: 
% Electrodes are 54.5mm long, 128 points, 3.0039mm between depth
% iterations. This corresponds to iterating down 7 points for a single 
% depth iteration. ( 54.5/(128-1) * 7 = 3.0039 mm ). So we can describe
% each electrode line as 233 points. (15 depth iterations possible, meaning
% 16 possible electrodes. 128 + 7*15 = 233 points total) 
% Then, use electrode association matrices to go from point intersections
% on lines to intersections of electrodes within each line. Intersections
% must be mappeed down lines. 
% 
% ************************************************************************


% electrodeCollision_lines matrix is LxLx14x14 where L is the number of valid
% electrode lines for any depth, and each element (i,j,d1,d2) corresponds
% to the occurence of intersection between the d1'th depth electrode of the
% ith line and d2'th depth electrode of the jth line (1==intersect, 0=none)

% after calc electrodeCollision_lines, restructure it to get
% only valid electrodes in a linear format. 
% -> NxN ones and zeros


% for each job, parallelize computation by rows 
% with 22 workers
% for each row, split into desired width
% for each width matrix (1 line by <3000 lines)
%   make 3 230 x <3000x230 matrices of x, y and z points along whole lines
%       this represents 1 line by < 3000 lines. So <3000 line intersection
%       tests
%   get x_diff, y_diff, z_diff, add together and threshold by thres^2  (16mm)
%   multiply 230 x <3000x230 matrix by 2D association matrices that represent
%       the points along each line that correspond to the 14 electrodes
%       to reshape matrix to size 14x<3000x<14
%           threshold this again so for all points corresponding to one
%           electrode-electrode test if there is any point-point distance
%           less than threshold, call it intersect
%       ensure that association matrices encorporate ~trajectories~, so for
%       electrode #10, we care about all points before this electrode and
%       within this electrode. this is to ensure "mapping down" of
%       intersectoins along depth
%   reshape 14x<3000x<14 matrix to 4-D matrix 1x<3000x14x14 so it can be
%   concatenated with other width matrices and other rows
% place width matrices together
% place all rows for one job together 
% place all job pieces together
% reshape to make linear, removing electrodes that are invalid
% eColl = N x N sparse matrix of 1s and 0s


for i = 1:18
    
thicknessRow = 3000; % 3000 tests per matrix subdivision
tic

% $$ UPDATE PARAMETERS $$
patientID = 1;
cdmd = 1; % nAm/mm^2 
dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';
dataHomePath = '/hpc/group/wmglab/ged12/cluster/Optimize_sEEG_implant/';

patientDir = ['Patient_' num2str(patientID)];

% load electrodes stored by depth
load(strcat(dataWorkPath,patientDir,'/DemoOut/normals_reInd_lines.mat'),'normals_reInd_lines')
load(strcat(dataWorkPath,patientDir,'/DemoOut/slocs_reInd_lines.mat'),'slocs_reInd_lines')
load(strcat(dataWorkPath,patientDir,'/DemoOut/electrodes_insertionLocCorr_depth.mat'),'track_insertionLocCorr_depth')

% BB Overlap Eind data storage to work with depth storage
load(strcat(dataWorkPath,patientDir,'/DemoOut/BBOverlap.mat'),'BBOverlap')

% load lists of all indices of Lines, not electrodes, that are members of each bounding box
load(strcat(dataWorkPath,patientDir,'/DemoOut/insertionLocs_Einds_depth.mat'),'insertionLocs_Einds_depth')
load(strcat(dataWorkPath,patientDir,'/DemoOut/electrodes_insertionLocCorr.mat'),'track_insertionLocCorrespondence')

% load valid electrodes
load(strcat(dataWorkPath,patientDir,'/DemoOut/valid_electrodes_in_each_line.mat'),'valid_electrodes_in_each_line') 
load(strcat(dataWorkPath,patientDir,'/DemoOut/valid_electrodes_in_all_lines.mat'),'valid_electrodes_in_all_lines') 
% end

% for each row (1 line) in line-organized collision matrix, calc all intersections

% get N and L from opened files!
N = length(track_insertionLocCorrespondence); 
disp('N: ')
disp(N)
L = length(valid_electrodes_in_each_line(:,1));
disp('L: ')
disp(L)


% DEMO DOES NOT USE PARALLELIZATION, but keeps same data structure

% parallelization scheme:
% we are allowed to use <= 400 workers at once
% with 22 workers per job, we can run 18 jobs at a time
% split into 18 jobs of roughly equal ~running time~ not number rows
% assume roughly equal BB overlap exclusion (a simplification) 
total_num_tests = L^2;
target_tests = total_num_tests / 18;
row = 0;
rows_each_job = zeros(18,2);
tests_each_job = zeros(18,1);
target_tests_each_job = target_tests * [1:18];
for j = 1:18
    row2 = ceil(sqrt(target_tests_each_job(j)));
    rows_each_job(j,:) = [row+1,min(max(row2,row+1),L)];
    tests_each_job(j) = ((row2)^2 - row^2)/2 / 1e7;
    row = min(max(row2,row+1),L-1);
end
rows_each_job(18,2) = L; % make sure last job ends at last row
%
% each job runs rows of this eColl_depth matrix
% use 18 jobs so < 400 CPUs so can run at same time
% i = 1:18
% each row is 1xLx14x14 electrodes
% use 22 workers, ~10 GB each 
%   (bc x/y/z_diff matrix are 230*230*<3000 ~= 0.16 GB each)
    % need total GB per job to be < 240GB which is min of node on cluster
% using max matrix width of 3000 tests

% get points along each line for all electrodes
% new 10/21/21
line_points_inds = [[0:13]'*8+1,[0:13]'*8+126];
line_points_all = [0:(230-1)]'*(3.5/8);
e_pos_shiftPoints_locs = line_points_all(line_points_inds);

% rows and BBs for this job (i=1:18)
% if test local, run all rows in same job (i==1)
rowsI = 1:length(L);
total_num_rows=length(rowsI);
disp(strcat(num2str(total_num_rows),' total rows to compute for this job: ',num2str(i)))
BBs = track_insertionLocCorr_depth(rowsI); % BB that each line is a member of 
BBOverlap_slice = BBOverlap(unique(BBs),:);

% create linear collision matrix storage
% for each line, store linearized rows in cell
% after parfor, combine them to size sum(sum(valid_electrodes_in_each_line(rowsI,:))),N
coll_mat_cell = cell(length(rowsI),1);

% set up parallelization details
% request as many CPUs as requested in slurm script (22)
% NO PARALLEL FOR DEMO
numCPUs = 2;
disp(['numCpus= ' num2str(numCPUs)])

% store total tests completed in each row
numTestsTotal = zeros(total_num_rows,1);

% store total number of E1 valid electrodes that we have
total_electoes = 0;

% go through all rows of eColl_depth for this job
%parfor (p = 1:total_num_rows,numCPUs-1) % total_num_rows varies for each job. 10663 to 1056
for p = 1:total_num_rows

    valid_electrodes_in_each_line = load(strcat(dataWorkPath,patientDir,'/DemoOut/valid_electrodes_in_each_line.mat'));
    valid_electrodes_in_each_line = valid_electrodes_in_each_line.valid_electrodes_in_each_line;
    valid_electrodes_in_all_lines = reshape(valid_electrodes_in_each_line(:,1:14)',[],1);
    % disp('Size of valid_electrodes_in_each_line(:,1:14) and valid_electrodes_in_all_lines:')
    size(valid_electrodes_in_each_line(:,1:14))
    size(valid_electrodes_in_all_lines)
    % valid_electrodes_in_all_lines IS WRONG!!
    % valid_electrodes_in_all_lines = load([patch_precomp 'ElectrodeLocsData/valid_electrodes_in_all_lines.mat']);
    % valid_electrodes_in_all_lines = valid_electrodes_in_all_lines.valid_electrodes_in_all_lines;
    track_insertionLocCorrespondence = load(strcat(dataWorkPath,patientDir,'/DemoOut/electrodes_insertionLocCorr.mat'));
    track_insertionLocCorrespondence = track_insertionLocCorrespondence.track_insertionLocCorrespondence;
    
    rowI = rowsI(p); % row index in whole matrix

    % for this line, find all electrode "lines" to test intersection with (indices in L-basis)
    % get BB that this electrode belongs to
    BB = BBs(p);
    % for each row, get all inds of lines need to test from finding all BBs that intersect
    BBsToTest = find(BBOverlap_slice(unique(BBs)==BB,:)==1);
    if isempty(BBsToTest)
        disp(['0 electrode line intersections to compute for line row ' num2str(p)])
        valid_electrodes_L1 = find(valid_electrodes_in_each_line(rowI,:)==1); % 1 to 14
        thisRowCollisions_matrix = zeros(length(valid_electrodes_L1),sum(valid_electrodes_in_all_lines));
        
        disp(['valid_electrodes_L1 size: ' num2str(size(valid_electrodes_L1))])

        % place 1's for all electrodes in same bounding box as E1
        % get BB that this electrode belongs to
        BB_that_this_line_is_in = BBs(p);
        % get indices of all valid electrodes that are also in this BB
        validEs_Indices_in_this_BB = track_insertionLocCorrespondence==BB_that_this_line_is_in;
        % add collisions to all electrodes in this line with all other
        % electrodes in this bounding box. technically, we will be filling in
        % numbers above the diagonal. 
        thisRowCollisions_matrix(:,validEs_Indices_in_this_BB) = 1;
%         disp(['thisRowCollisions_matrix size: ' num2str(size(thisRowCollisions_matrix))])
        coll_mat_cell{p} = thisRowCollisions_matrix; 
%         disp(['coll_mat_cell{p} size: ' num2str(size(coll_mat_cell{p}))])
        total_electoes = total_electoes + length(valid_electrodes_L1);
        continue
    end
    testLineIs = zeros(1,L);
    indAt = 1;
    for j=1:length(BBsToTest)
        BBtest = BBsToTest(j);
        currentInds = insertionLocs_Einds_depth{BBtest};
        indAtNew = indAt + length(currentInds);
        testLineIs(indAt:indAtNew-1) = currentInds;
        indAt = indAtNew;
    end
    testLineIs(testLineIs==0)=[]; % indices of electrode "lines" that need to test
    
    disp([num2str(length(testLineIs)) ' electrode line intersections to compute for line row ' num2str(p)])
    disp(['Max row is ' num2str(max(testLineIs)) ])
    
    % split this row into pieces along length, each of length thicknessRow
    numTestsTotal(p) = length(testLineIs);
    numIterations = 1;
    numTests = length(testLineIs);
    while numTests > thicknessRow % ************ important number ********
        numIterations = numIterations+1;
        numTests = ceil(length(testLineIs)/numIterations);
    end
%     disp(['There are ' num2str(numIterations) ' pieces of this row'])
    tic

    % store collisions results as 1xlength(testLineIs)x14x14 matrix
        % so total eColl_lines matrix is LxLx14x14
    % whole row of eColl_lines matrix. BUT this is in second basis,
    % excluding lines that we eliminated from BB overlap. must map back to
    % full indices (original basis) after compute
    collisions = zeros(1,length(testLineIs),14,14); % collision results (0s and 1s) for this electrode line with all other electrode lines that could intersect (elimianted some based on BB overlaps)
    
    % for each piece along length of this row (piece length < 3000)
    for iPiece=1:numIterations % for each set of electrodes < 3000 to get through whole row
        
        theseIndsInTestElectrodes = numTests*(iPiece-1)+1:min(numTests*iPiece,length(testLineIs)); % indices of this width matrix in whole row of electrodes to test
        electrodeInds_splitwidth = testLineIs(theseIndsInTestElectrodes); % electrodes for this width chunk in standard indices (FULL l-basis)

        % create 6 matrices of x y and z coordinates for all 126 points on all electrodes
        
        % create 230 points along this line (L1)
        % start from thisSloc, where d = 1. get normal and sLoc
        thisLine = rowI;
        thisNormal = normals_reInd_lines(rowI,:,1);
        thisSloc = slocs_reInd_lines(rowI,:,1); 
        
%         disp(['This electrode line number is ' num2str(thisLine)])
%         disp('Normal: ')
%         disp(thisNormal) 
%         disp('This sloc: ')
%         disp(thisSloc)
        
        line_points_all = [0:(230-1)]'*(3.5/8);
        points_L1 = thisSloc + line_points_all*thisNormal; % 230 x 3
%         disp(['points_L1 size: ' num2str(size(points_L1))])
        
        % create 230 points along all lines we are testing intersection with (L2s)
        otherSlocs = slocs_reInd_lines(electrodeInds_splitwidth,:,1);
        otherNormals = normals_reInd_lines(electrodeInds_splitwidth,:,1);
%         disp(['otherSlocs size: ' num2str(size(otherSlocs))])
%         disp(['otherNormals size: ' num2str(size(otherNormals))])
        
        E2_points_x = otherSlocs(:,1)' + line_points_all*otherNormals(:,1)'; % 230 x width ( < 3000)
        E2_points_y = otherSlocs(:,2)' + line_points_all*otherNormals(:,2)'; % 230 x width
        E2_points_z = otherSlocs(:,3)' + line_points_all*otherNormals(:,3)'; % 230 x width
%         disp(['E2_points_z size: ' num2str(size(E2_points_z))])

        E2_points_x = permute(E2_points_x,[3,2,1]); % 1 x width x 230
        E2_points_y = permute(E2_points_y,[3,2,1]); % 1 x width x 230
        E2_points_z = permute(E2_points_z,[3,2,1]); % 1 x width x 230
%         disp(['E2_points_z size: ' num2str(size(E2_points_z))])

        % simple distance calc method
            % create diff matrix : row-repeat matrix - col-repeat matrix

        % put into 3D
        % diff distance matrixes [230,w,230]
        y_diff = repmat(E2_points_y,[230,1,1])-repmat(points_L1(:,2),[1,length(electrodeInds_splitwidth),230]);
        x_diff = repmat(E2_points_x,[230,1,1])-repmat(points_L1(:,1),[1,length(electrodeInds_splitwidth),230]);
        z_diff = repmat(E2_points_z,[230,1,1])-repmat(points_L1(:,3),[1,length(electrodeInds_splitwidth),230]);
%         disp(['z_diff size: ' num2str(size(z_diff))])
        
        E2_points_x =[];
        E2_points_y =[];
        E2_points_z =[];

        % square 
        % now all in 3rd basis, considerably smaller 
        x_diff = x_diff.*x_diff;
        y_diff = y_diff.*y_diff;
        z_diff = z_diff.*z_diff;
%         disp(['z_diff size: ' num2str(size(z_diff))])

        % add and dont sqrt (compare to threshold squared instead)
        dist = (x_diff+y_diff+z_diff);
%         disp(['dist size: ' num2str(size(dist))])

        x_diff=[];
        y_diff=[];
        z_diff=[];

        % threshold
        dist = double(dist<=16); % 230 x w x 230 

        % multiply by association matrices to move to 14 x w x 14 matrix
        
        % make electrode association matrix
        line_points_inds = [[0:13]'*8+1,[0:13]'*8+126];
%         disp(['line_points_inds size: ' num2str(size(line_points_inds))])
        
        electrodes_inLine_assoc = zeros(14,230);
        % encorporate electrode trajectories, not just electrodes
        for electrodeI = 1:14
            electrodes_inLine_assoc(electrodeI,1:line_points_inds(electrodeI,2)) = 1;
        end
%         disp(['electrodes_inLine_assoc size: ' num2str(size(electrodes_inLine_assoc))])
        
        % multiply line structure by electrode assoc matrix 
        % to go from line-line intersection to electrode-electrode
        %   intersection 
        % need to loop through all widths so have 2D matrices
        dist_electrodes_p1 = zeros(14,size(dist,2),230);
        for widthI = 1:size(dist,2)
            dist_electrodes_p1(:,widthI,:) = electrodes_inLine_assoc * squeeze(dist(:,widthI,:)); % 14 x w x 230
        end
        dist_electrodes_p2 = zeros(14,size(dist,2),14);
        for widthI = 1:size(dist,2)
            dist_electrodes_p2(:,widthI,:) = squeeze(dist_electrodes_p1(:,widthI,:)) * electrodes_inLine_assoc'; % 14 x w x 230
        end
%         disp(['dist_electrodes_p2 size: ' num2str(size(dist_electrodes_p2))])
        % dist_electrodes_p2 is 14xwx14
        
        % threshold again >= 1  [14 x w x 14 matrix of 1s and 0s]
        dist_electrodes_p2 = double(dist_electrodes_p2>=1);
        % dist_electrodes_p2 is 14xwx14 matrix of 1s and 0s

        % reshape to 4D storage (1xwx14x14)
        % must reshape correctly!!!
        dist_electrodes_p2 = reshape(permute(dist_electrodes_p2,[2,1,3]),1,size(dist,2),14,14);
%         disp(['dist_electrodes_p2 size: ' num2str(size(dist_electrodes_p2))])
        % place in collisions matrix, mapping back to
        % electrode lines to test basis, not full L basis (this_depth_eTests)
        collisions(1,theseIndsInTestElectrodes,:,:) = dist_electrodes_p2;
%         disp(['collisions size: ' num2str(size(collisions))])
        
        % visualize E1 with all E2s
        collisionE2s = permute(dist_electrodes_p2,[2,3,4,1]);
        
        % visEs(thisNormal, thisSloc, otherSlocs, otherNormals, collisionE2s, trisurf);
        
    end
%     disp('Done computing collisions in 4D for this line. Now reshaping.. ')
    
    % remap collisions to full indeces 
    % currently in testLineIs basis. need to go back to full.
    rowOfCollMatrix = zeros(1,L,14,14);
    rowOfCollMatrix(1,testLineIs,:,:) = collisions; 
%     disp(['rowOfCollMatrix size: ' num2str(size(rowOfCollMatrix))])
    %idx = find(rowOfCollMatrix==1);
    %[~,collInds_eLine2I,collInds_eDepL1I,collInds_eDepL2I] = ind2sub(size(rowOfCollMatrix),idx);
    
    % linearize these collisions (here rather than in concat script)
    % rowOfCollMatrix is 1,L,14,14
    % get valid electrodes in line 1
    valid_electrodes_L1 = find(valid_electrodes_in_each_line(rowI,:)==1); % 1 to 14
%     disp(['valid_electrodes_L1 size: ' num2str(size(valid_electrodes_L1))])
    % update total num valid E1 electrodes (length downward of this piece
    % of the linear collision matrix)
    total_electoes = total_electoes + length(valid_electrodes_L1);
    %disp(['total_electoes size: ' num2str(size(total_electoes))])
    % reshape thisRowIndsCollisions so only keep electrodes on L1 that are
    % valid (remove slices on third dimension that correspond to invalid
    % electrodes)
    rowOfCollMatrix = permute(rowOfCollMatrix,[2,3,4,1]);
    thisRowCollisions_matrix = rowOfCollMatrix(:,valid_electrodes_L1,:); % (L,<=14,14);
%     disp(['thisRowCollisions_matrix size: ' num2str(size(thisRowCollisions_matrix))])
    
    % reshape so valid L1 electrodes along first dimension (<=14,L,14)
    thisRowCollisions_matrix = reshape(permute(thisRowCollisions_matrix,[2,1,3]),length(valid_electrodes_L1),L,14);
%     disp(['thisRowCollisions_matrix size: ' num2str(size(thisRowCollisions_matrix))])
    
    % reshape so 2D (<=14,L*14)
    thisRowCollisions_matrix = reshape(permute(thisRowCollisions_matrix,[1,3,2]),length(valid_electrodes_L1),L*14);
%     disp(['thisRowCollisions_matrix size: ' num2str(size(thisRowCollisions_matrix))])
    
    % remove L2 electrodes that are invalid. <=14 x N collision matrix
    thisRowCollisions_matrix = thisRowCollisions_matrix(:,logical(valid_electrodes_in_all_lines));
%     disp(['thisRowCollisions_matrix size: ' num2str(size(thisRowCollisions_matrix))])
    % place 1's for all electrodes in same bounding box as E1
    % get BB that this electrode belongs to
    BB_that_this_line_is_in = BBs(p);
    % get indices of all valid electrodes that are also in this BB
    validEs_Indices_in_this_BB = track_insertionLocCorrespondence==BB_that_this_line_is_in;
    % add collisions to all electrodes in this line with all other
    % electrodes in this bounding box. technically, we will be filling in
    % numbers above the diagonal. 
    thisRowCollisions_matrix(:,validEs_Indices_in_this_BB) = 1;
%     disp(['thisRowCollisions_matrix size: ' num2str(size(thisRowCollisions_matrix))])
    
    % store this linearized piece of electrode collision matrix (for 1
    %   electrode line) in cell array. once all lines computed, concat and
    %   store
    coll_mat_cell{p} = thisRowCollisions_matrix; 
%     disp(['coll_mat_cell{p} size: ' num2str(size(coll_mat_cell{p}))])
    
    %disp([num2str(numel(thisRowCollisions_matrix)*8*1e-9) ' GB in inds matrix'])
end

%{
if local_or_cluster == 1
    % for testing, if we have not finished all computations, fill in with empty
    % data to test restructuring
    if p ~= total_num_rows
        for q = p:total_num_rows
            rowI = rowsI(q); % row index in whole matrix
            valid_electrodes_L1 = find(valid_electrodes_in_each_line(rowI,:)==1);
            total_electoes = total_electoes + length(valid_electrodes_L1);
            coll_mat_cell{q} = zeros(length(valid_electrodes_L1),N);
        end
    else
end
%}

% concatenate linearized electrode collision matrix rows
% save matrix for each job separately
thisPieceCollisions = zeros(total_electoes,N);
rowI = 1; % 1 to total_electoes
indE_this_cell = 1;
% while we still have more cells to concatenate
while rowI <= length(rowsI)
    thisRowCollisions_matrix = coll_mat_cell{rowI};
    indE_this_cell_next = indE_this_cell + size(thisRowCollisions_matrix,1);
    thisPieceCollisions(indE_this_cell:indE_this_cell_next-1,:) = thisRowCollisions_matrix; 
    indE_this_cell = indE_this_cell_next;
    rowI = rowI + 1;
end

% make this sparse slice from this completed collision matrix
sparse_slice = sparse(thisPieceCollisions);
filename = strcat(dataWorkPath,patientDir,'/DemoOut/electrodeCollisions_sparseSlice_job',num2str(i),'.mat');
save(filename,'sparse_slice','-v7.3')

time = toc

end


