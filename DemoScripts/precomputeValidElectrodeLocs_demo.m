% precompute Valid Electrode Locs
% 9/15/21
% Grace Dessert

% DEMO VERSION:
%   only create ~1000 electrode locations.
%   reduce number of angles per insertion location to 10
%   reduce number of insertion locations to ~ 100 
%       (in getSkinInsertionLocs_demo.m)


% Given a cortex surface, skin surface, skull surface, midline plane
%   and a set of about 800 valid insertion locs on the skin surface
%   get a set of about 80-100k valid electrode locations in the head

% this version is generalized to any head model and cleaned up
%   it does not contain visualization steps

% NOTES:
% one type of electrode:
% 16 contacts on each
% 3.5mm from start to start of 2mm contacts
% -> 54.5 mm long

% $$ UPDATE PARAMETERS $$
patientID = 7;
cdmd = 1; % nAm/mm^2 
dataWorkPath = '';
dataHomePath = '';

patientDir = ['Patient_' num2str(patientID)];

% add path for scripts
if exist('DemoScripts','dir')
    addpath('DemoScripts')
    addpath('DemoScripts/refinepatch_version2b')
end


% open input files
trisurf = stlread(strcat(dataHomePath,patientDir,'/Inputs/SkullFinal.stl'));
load(strcat(dataWorkPath,patientDir,'/DemoOut/midlinePlane.mat'), 'midlinePlane');
load(strcat(dataWorkPath,patientDir,'/DemoOut/SulciSurface_refined.mat'),'sulciPoints');
cortexSurf = stlread(strcat(dataHomePath,patientDir,'/Inputs/cortexSurf.stl'));
implantedELocations = load(strcat(dataHomePath,patientDir,'/Inputs/ContactLocations.mat'),'scirunfield');
implantedELocations = implantedELocations.scirunfield.node;
load(strcat(dataWorkPath,patientDir,'/DemoOut/SkinValidForElectrodeImplant.mat'),'scirunfield');
load(strcat(dataWorkPath,patientDir,'/DemoOut/recRadiusContactLocs.mat'),'allSelContacts'); % contact locs for rec radius study


%
% get normal vectors for all starting locs and init. storage
numSLocs = length(scirunfield.node);
start_locs = cell(1,numSLocs);
if size(scirunfield.node,2) == 3
    scirunfield.node = scirunfield.node';
    scirunfield.field = scirunfield.field';
end
for i = 1:numSLocs
    start_locs{i} = scirunfield.node(:,i);
end
% for every starting loc, get normal into brain
normals_start = cell(1,numSLocs);
for i = 1:numSLocs
    % get normal vector to skin for direction of implant
    % get closest points in skin to starting loc
    distances = sqrt(sum((scirunfield.node-start_locs{i}).^2,1));
    [d,di] = sort(distances);
    closest_pts = scirunfield.node(:,di(1:6));
    one = scirunfield.node(:,di(1)); % same as start_locs(:,i). check
    two = scirunfield.node(:,di(2));
    three = scirunfield.node(:,di(3));
    % fit closest points to a plane and get normal of the plane
    [n,~,~] = affine_fit(closest_pts');
    normals_start{i} = n;

    % make sure normal is facing correct direction.
    % first get farthest point away from starting loc in skin 
    % then get distance between that and starting loc + 120 mm along normal
    % and that and starting loc - 120 mm along normal
    % if first distance is larger, then flip normal 
    farthest_pt = scirunfield.node(:,di(end));
    posNormDist = sqrt(sum((farthest_pt-(start_locs{i}+120*normals_start{i})).^2,1));
    negNormDist = sqrt(sum((farthest_pt-(start_locs{i}-120*normals_start{i})).^2,1));
    if posNormDist>negNormDist
    normals_start{i} = -1*normals_start{i};
    end
end
% now we have length(scirunfield.node) starting locs with normals
%   808 for base head model

disp('Adding in angle variability')
tic
% add in angle variability
% 	365 angles per electrode + 1 for original direction
% 1) use change of basis to move electrode to +z axis
% 2) apply rotation matrix to rotate around x axis (1:10 degrees)
% 3) apply rotation matrix to rotate around z axis (0:(24-2*x_rot):359 degrees)
% 4) transform back to original basis

% DEMO VERSION: only use 10 angles per insertion location
x_angles = [5]; 
starting_locs = cell(numSLocs,10,2);
normals = cell(numSLocs,10,2);

% full version values:
% x_angles = 1:10;
% starting_locs = cell(numSLocs,366,2);
% normals = cell(numSLocs,366,2);

for j = 1:numSLocs 
    nS = normals_start{j};

    % first normal is the original one
    normals{j,1,1} = nS;
    normals{j,1,2} = nan;
    starting_locs{j,1,1} = start_locs{j};
    starting_locs{j,1,2} = nan;
    
    % i is index 1:365
    i = 1;
    for xInd = 1:length(x_angles)
        x_rot = x_angles(xInd);
        % demo version:
        z_angles = 0:(48-2*x_rot):359; 
        % full version:
        %z_angles = 0:(24-2*x_rot):359; % this spacing gives us 365 angles total! plus original gives 366
        for zInd = 1:length(z_angles)
            z_rot = z_angles(zInd);
            % rotate nS to nT to apply uniform rotations
            phi = atan(sqrt((nS(1)^2)+(nS(2)^2))/nS(3));
            if (((nS(1)^2)+(nS(2)^2))>0 && nS(3)<0 || ((nS(1)^2)+(nS(2)^2))<0 && nS(3)<0) % if in 3rd or 4th quadrant
                phi = phi + pi;
            end
            theta = atan(nS(2)/nS(1));
            if (nS(2)>0 && nS(1)<0 || nS(2)<0 && nS(1)<0) % if in 3rd or 4th quadrant
                theta = theta + pi;
            end
            n1 = rotz(-theta)*nS;
            nT = roty(-phi)*n1; 
            % now that we have ~rotated~ nS to nT, we can apply rotations around the x y and z axes
            %   n rotated by x_rot degrees around x axis = rotx(x_rot)*n
            %   where rotx(x_rot) = [1 0 0;0 cos(x_rot) -sin(x_rot);0 sin(x_rot) cos(x_rot)];
            n1 = rotx(deg2rad(x_rot))*nT; % rotate around x axis by x_rot degrees 
            n1 = n1./norm(n1);
            nR = rotz(deg2rad(z_rot))*n1; % rotate around z axis by z_rot degrees 
            nR = nR ./ norm(nR); 
            n1 = roty(phi)*nR; 
            nSR = rotz(theta)*n1; 
            % visualize rotation
            
%             if j == 2
%                 figure(1)
%                 clf
%                 hold
%                 stlPlot(cortexSurf.Points,cortexSurf.ConnectivityList,[],"")
%                 plot3(s1(1)+[0,50*nS(1)],s1(2)+[0,nS(2)*50],s1(3)+[0,nS(3)*50],'LineWidth',5,'DisplayName','OriginalNormal')
%                 plot3(s1(1)+[0,50*nT(1)],s1(2)+[0,50*nT(2)],s1(3)+[0,50*nT(3)],'LineWidth',5,'DisplayName','+Z Axis')
%                 n1 = rotx(deg2rad(5))*nT;
%                 plot3(s1(1)+[0,50*n1(1)],s1(2)+[0,50*n1(2)],s1(3)+[0,50*n1(3)],'LineWidth',5,'DisplayName','+Z Axis w X Rot')
%                 n2 = rotz(deg2rad(10))*n1;
%                 plot3(s1(1)+[0,50*n2(1)],s1(2)+[0,50*n2(2)],s1(3)+[0,50*n2(3)],'LineWidth',5,'DisplayName','+Z Axis w X & Z Rot')
%                 plot3(s1(1)+[0,50*nSR(1)],s1(2)+[0,50*nSR(2)],s1(3)+[0,50*nSR(3)],'LineWidth',5,'DisplayName','Original w X & Z Rot')
%                 legend()
%             end
            
            normals{j,i+1,1} = nSR;
            normals{j,i+1,2} = nan;
            starting_locs{j,i+1,1} = start_locs{j};
            starting_locs{j,i+1,2} = nan;
            i= i+1;
        end
    end
end
%save('normals_ImplantLocsandAngles.mat','normals')
%save('starting_locs_ImplantLocsandAngles.mat','starting_locs')
% plot angle variability to confirm

f = figure('visible','off');
clf
hold on
for j = [1:5:numSLocs]
    for i = 1:10
    s1 = starting_locs{j,i,1};
    endpoint = normals{j,i,1}*55 + s1;
    plot3([s1(1),endpoint(1)],[s1(2),endpoint(2)],[s1(3),endpoint(3)],'LineWidth',5)
    end
end
stlPlot(cortexSurf.Points,cortexSurf.ConnectivityList,[],"")
saveas(f,strcat(dataHomePath,patientDir,'/DemoOut/electrodeAngles'),'fig')
saveas(f,strcat(dataHomePath,patientDir,'/DemoOut/electrodeAngles'),'png')


tAngleVar = toc;
disp(['Done adding in angle variability. This took' num2str(tAngleVar/60) 'minutes'])


%% Remove electrodes that are far underneath cortex

% get min distance to cortex
% check distance to bottom of cortex 
cortexMinZ = min(cortexSurf.Points(:,3));

% store validity of electrode lines
norms2D = cell(numSLocs, 1);
startLocs2D = cell(numSLocs, 1);

for i=1:numSLocs
    startLocs2D_this = zeros(1,10);
    norms2D_this = zeros(1,10);
    
    for j=1:10
    
        sloc = starting_locs{i,j,1};
        normal = normals{i,j,1};
        
        % get start and end points of full line
        eloc = sloc + normal*100;
        
        % check distance to bottom of cortex 
        % Z-distance is good enough!
        if length(sloc) == 1 || length(eloc) == 1
            continue
        end
        minDistZSq = max(sloc(3)-cortexMinZ,eloc(3)-cortexMinZ);
        
        if minDistZSq < -10  % mm % if FULL electrode is more than 1 cm below the lowest point on the cortex, remove it
            % remove whole electrode
            startLocs2D_this(j) = nan;
            %normals{i,j,2} = nan;
            norms2D_this(j) = nan;
            starting_locs{i,j,1} = 0;
            normals{i,j,1} = 0;
        end
    end
    norms2D{i} = norms2D_this;
    startLocs2D{i} = startLocs2D_this;
end
% put sliced second dimension arrays back into cell storage
for i = 1:numSLocs
    for j = 1:10
        normals{i,j,2} = norms2D{i}(j);
        starting_locs{i,j,2} = startLocs2D{i}(j);
    end
end


%% build in depth variability
% new electrode line discretization method: 9/14/21
% use 3.5mm separation between electrodes so contacts line up exactly
%   use 3.5/8 mm spacing for points between electrodes with 125 points
%   making up each electrode (0.4375 * 125 = 54.6875mm length of electrode)
%   To keep full electrode line < 10cm, there can at most 13 electrodes
%   per line (12 depth iterations)
%       (3.5/8 * 125 + (12*3.5) = 96.6875)

% a) build bounding volume hierarchy for skull
disp(['Adding in depth variability'])
tic

[BVH_surface_pointInds,BVH_boundingBoxes,childrenInds] = boundingVolumeHierarchyConstruction_demo(trisurf);

% b) for each electrode, move iteratively down normal until one stopping
%   criteria is reached: any part of electrode in midline, total electrode
%   trajectory > 10cm, or any part of electrode intersects the skull
% track stopping criteria
track_LengthLimitElectrodes = cell(numSLocs, 1);
track_IntersectAgainElectrodes = cell(numSLocs, 1);
track_midlineElectrodes = cell(numSLocs, 1);

trackMaxK = zeros(1,numSLocs);

% get cooordinates of midline
% NOTE this is specific to example head model
% ml_xlim = [-12,8];
% if point is within 10mm of midline, it counts as being within midline
% load midline info
% load([dataWorkPath patientDir '/HeadModel/midlinePlane.mat'], 'midlinePlane')

norms2D = cell(numSLocs, 1);
startLocs2D = cell(numSLocs, 1);

for i=1:numSLocs

    maxK = 0;
    side_of_plane = 0;
    
    track_midlineElectrodes_this = [];
    track_IntersectAgainElectrodes_this = [];
    track_LengthLimitElectrodes_this = [];
    
    
    startLocs2D_this = zeros(1,10);
    norms2D_this = zeros(1,10);
    
    for j=1:10
        sloc = starting_locs{i,j,1};
        normal = normals{i,j,1};
        
        % check if this line exists (if it was removed for being too far away)
        % if was already removed, continue
        if normals{i,j,1}==0
            continue
        end
        
        % get last point on electrode
        % 16 contacts on each, 3.5mm from start to start of 2mm contacts
        % 54.5mm long
        endloc = sloc+54.5*normal;

        % get side of midline that electrode is on (or if inside midline)
        midLoc = checkMidline(sloc, endloc, midlinePlane);
        % if sloc or endloc is in the midline, reject this electrode line
        if midLoc == 0 % part of electrode is inside midline
            normals{i,j,1}=0;
            starting_locs{i,j,1}=0;
            %normals{i,j,2}=nan;
            norms2D_this(j) = nan;
            %starting_locs{i,j,2}=nan;
            startLocs2D_this(j) = nan;
            track_midlineElectrodes_this = [track_midlineElectrodes_this;i j 0];
            continue; % go to next electrode line
        else
            side_of_plane = midLoc; % -1 is left side, 1 is right side
        end

        % first electrode is valid by x positioning,
        % so iterate it deeper!
        %normals{i,j,2}=0;
        norms2D_this(j) = 0;
        %starting_locs{i,j,2}=0;
        startLocs2D_this(j) = 0;
        k = 1;

        while true % while end loc is on correct side of midline, iteratively move deeper
            % shift electrode deeper by 3.5 mm along normal
            endloc = endloc + 3.5*normal;
            sloc = sloc + 3.5*normal;

            
            if k > 5
                % check insersection of electrode with skull using 4mm threshold
                % if any electrode point is less than 4mm from
                % any point in the skull surface, call this an intersection            
                [intersection,minDist] = boundingVolumeHierarchy_demo(sloc,normal,4,[],0,BVH_surface_pointInds,BVH_boundingBoxes,childrenInds,128);

                if intersection==1
                    % if electrode intersects the skull after 5x 3.5 mm iterations
                    %  then stop iteration and don't include this electrode as
                    %  valid. This buffer is necessary because the skin points
                    %  are outside of the skull
                    track_IntersectAgainElectrodes_this = [track_IntersectAgainElectrodes_this;i j];
                    %disp('electrode hits skull again')
                    break
                end
            end

            % if new end loc is still on correct side of midline ~range~
            %   (if end loc is on correct side, sloc must also be, so we don't
            %   need to check sloc)
            %   store this k number and iterate k += 1, continue!
            % if not, stop depth iteration and don't store this last valid k number
            % k is 0-indexed; number of depth movements we have made
            midLoc = checkMidline(sloc, endloc, midlinePlane);
            if midLoc == side_of_plane
                %normals{i,j,2}=k;
                norms2D_this(j) = k;
                %starting_locs{i,j,2}=k;
                startLocs2D_this(j) = k;
                k = k+1;
            else % electrode has entered or crossed midline
                track_midlineElectrodes_this = [track_midlineElectrodes_this;i j k];
                %disp('cross midline')
                break
            end

            % max electrode length is 10cm
            % if k>ceil((100mm-54.5mm)/3.5 mm)=13. we have moved k*3.5mm, and the
            % electrode started at 54.5mm. so if k is > 13, the length is >10cm
            % this means that we can have at most k+1, or 14 valid electrodes per line
            % (because k is 0-indexed)
            if k>13
                track_LengthLimitElectrodes_this = [track_LengthLimitElectrodes_this;i j];
                %disp('hit electrode length limit')
                break
            end
        end

        if maxK<k
            maxK=k;
        end
    end
    
    norms2D{i} = norms2D_this;
    startLocs2D{i} = startLocs2D_this;
    
    track_midlineElectrodes{i} = track_midlineElectrodes_this;
    track_IntersectAgainElectrodes{i} = track_IntersectAgainElectrodes_this;
    track_LengthLimitElectrodes{i} = track_LengthLimitElectrodes_this;
    
    trackMaxK(i)=maxK;
end 

% restructure after parallelization
track_midlineElectrodes = cell2mat(track_midlineElectrodes);
track_IntersectAgainElectrodes = cell2mat(track_IntersectAgainElectrodes);
track_LengthLimitElectrodes = cell2mat(track_LengthLimitElectrodes);

for i = 1:numSLocs
    for j = 1:10
        normals{i,j,2} = norms2D{i}(j);
        starting_locs{i,j,2} = startLocs2D{i}(j);
    end
end
%%
tDepthVar = toc;
disp(['Done adding in depth variability. This took' num2str(tDepthVar/60) 'minutes'])

save(strcat(dataWorkPath,patientDir,'/DemoOut/track_IntersectAgainElectrodes.mat'),'track_IntersectAgainElectrodes')
save(strcat(dataWorkPath,patientDir,'/DemoOut/trackMaxK.mat'),'trackMaxK')
save(strcat(dataWorkPath,patientDir,'/DemoOut/track_LengthLimitElectrodes.mat'),'track_LengthLimitElectrodes')
save(strcat(dataWorkPath,patientDir,'/DemoOut/track_midlineElectrodes.mat'),'track_midlineElectrodes')

save(strcat(dataWorkPath,patientDir,'/DemoOut/starting_locs_withDepth_beforeRmSulci.mat'),'starting_locs')
save(strcat(dataWorkPath,patientDir,'/DemoOut/normals_withDepth_beforeRmSulci.mat'),'normals')

tic

%% count all electrodes
N = 0;
% count all "lines" where any depth is valid
L = 0;
% get max depth of an electrode
max_depth = 0;
% get average depth of lines
avg_depth = 0;
for i=1:numSLocs
    for j=1:10
        % if this is a valid electrode line, add every valid electrode to
        % our linear electrodes matrix
        line_exists = 0;
        if normals{i,j,1} ~= 0
            line_exists = 1;
            kmax = normals{i,j,2};
            max_depth = max(max_depth,kmax);
            for k=0:kmax
                N=N+1;
            end
            avg_depth = avg_depth + kmax;
        end
        L = line_exists + L;
    end
end 
avg_depth = avg_depth / L;

disp(strcat('There are ',num2str(N),' valid electrodes and',num2str(L),' valid lines'))
disp(strcat('The max depth of a line is ',num2str(max_depth),' and the average depth is ',num2str(avg_depth)))
% disp(num2str(N))
% disp(num2str(L))
% disp(num2str(avg_depth))
% disp(num2str(max_depth))
%%
disp('Removing electrodes that intersect sulci')

%% remove electrodes that intersect sulci
% threshold of intersection: 2.5mm. 

% for all valid electrodes found from depth iteration,
% test if they intersect sulci
% if an electrode intersects, stop iteration down that line
% and set k value to last valid index in that line
% we are not computing intersects of electrodes deeper than invalid
% electrodes

% precompute BVH tree
sulci.Points = sulciPoints;
[BVH_surface_pointInds,BVH_boundingBoxes,childrenInds] = boundingVolumeHierarchyConstruction_demo(sulci);

% save plot of sulci bounding boxes... this will not be nice to look at
% visBBs(BVH_boundingBoxes, sulciPoints, 0, strcat(dataHomePath,patientDir,'/Test/BoundingBoxVis'))  % BBcell, fieldPoints, plotBool, pathAndPlotName

%
norms2D = cell(numSLocs, 1);
startLocs2D = cell(numSLocs, 1);
for i = 1:numSLocs
    for j = 1:10
        norms2D{i}(j) = normals{i,j,2};
        startLocs2D{i}(j) = starting_locs{i,j,2};
    end
end

for i=1:numSLocs
%parfor (i=1:numSLocs, numCPUs-1)
    % disp(['s_loc: ' num2str(i)])
    
    startLocs2D_this = zeros(1,10);
    norms2D_this = zeros(1,10);
    
    for j=1:10
        
        sloc = starting_locs{i,j,1};
        normal = normals{i,j,1};
        %ks = starting_locs{i,j,2};
        ks = startLocs2D{i}(j);
        
        if ks ~= ks  % if isnan, this line is not valid. move on
            continue
        end
        
        for k = 0:ks

            % using 2.5mm threshold distance of electrode to sulci
            sloc_d = sloc + k*3.5*normal;
            [intersection,minDist] = boundingVolumeHierarchy_demo(sloc_d,normal,2.5,sulci.Points,0,BVH_surface_pointInds,BVH_boundingBoxes,childrenInds,128);

            % if does not intersect, continue going down line
            % if it does intersect, stop going down line
            %   save last valid k index in normals and starting_locs variables        
            % we stop going down each line because if one electrode intersects,
            % the trajectory of every deeper iteration will also intersect. 

            if intersection==1 % does  intersect
                % must remove this k value from consideration for this line
                % if k== 0, there are no valid electrodes in this line
                % remove this line from consideration, move on to next line
                %  disp(strcat("Intersect. i: ",num2str(i)," j: ",num2str(j)," k: ",num2str(k)))
                if k == 0 
                    %starting_locs{i,j,2} = nan;
                    startLocs2D_this(j) = nan;
                    %normals{i,j,2} = nan;
                    norms2D_this(j) = nan;
                    starting_locs{i,j,1} = 0;
                    normals{i,j,1} = 0;
                else % update valid k index, move on to next line
                    %starting_locs{i,j,2} = k;
                    startLocs2D_this(j) = k;
                    %normals{i,j,2} = k;
                    norms2D_this(j) = k;
                end
                break
            end
        end
        if k == ks
            % if we get through all current valid ks with no intersect, all are still valid
            norms2D_this(j) = ks;
            startLocs2D_this(j) = ks;
        end
    end
    norms2D{i} = norms2D_this;
    startLocs2D{i} = startLocs2D_this;
    
end

% restructure after parallelization
for i = 1:numSLocs
    for j = 1:10
        normals{i,j,2} = norms2D{i}(j);
        starting_locs{i,j,2} = startLocs2D{i}(j);
    end
end
%

% update normals and starting_locs with new valid electrodes
% save([patch_precomp 'ElectrodeLocsData/starting_locs_withDepth.mat'],'starting_locs')
% save([patch_precomp 'ElectrodeLocsData/normals_withDepth.mat'],'normals')

% update normals and starting_locs with new valid electrodes
save(strcat(dataWorkPath,patientDir,'/DemoOut/starting_locs_withDepth.mat'),'starting_locs')
save(strcat(dataWorkPath,patientDir,'/DemoOut/normals_withDepth.mat'),'normals')

tSint = toc;
disp(['Done removing electrodes that intersect sulci. This took' num2str(tSint/60) 'minutes'])

%% reindex electrodes for linear and depth/lines storage 
%   3D storage structure where x and y dimensions represent
%   "lines" of electrodes, and z direction is depth iterations
% 
% load([dataWorkPath patientDir '/ElectrodeLocsData/starting_locs_withDepth.mat'],'starting_locs')
% load([dataWorkPath patientDir '/ElectrodeLocsData/normals_withDepth.mat'],'normals')

disp('reindex electrodes for linear and depth/lines storage ')
tic

%
% count all electrodes
N = 0;
% count all "lines" where any depth is valid
L = 0;
% get max depth of an electrode
max_depth = 0;
avg_depth = 0;
for i=1:numSLocs
    for j=1:10
        % if this is a valid electrode line, add every valid electrode to
        % our linear electrodes matrix
        line_exists = 0;
        if normals{i,j,1} ~= 0
            line_exists = 1;
            kmax = normals{i,j,2};
            max_depth = max(max_depth,kmax);
            for k=0:kmax
                N=N+1;
            end
            avg_depth = avg_depth + kmax;
        end
        L = line_exists + L;
    end
end 
avg_depth = avg_depth / L;
% disp(num2str(N))
% disp(num2str(L))
% disp(num2str(max_depth))
disp(strcat('There are ',num2str(N),' valid electrodes and',num2str(L),' valid lines'))
disp(strcat('The max depth of a line is ',num2str(max_depth),' and the average depth is ',num2str(avg_depth)))
%

% reduce to 95k electrodes! 
disp(['There are ' num2str(N) ' valid electrodes'])

% remove full lines (don't remove electrodes with greatest depth because
% then we can bias electrode set - this can cause unexpected performance
% changes)
if (N > 95000) 
    while (N > 95000) 
        % get random electrode i and j with large k to remove, then remove max k
        i = randi(numSLocs);
        j = randi(10);
        % remove electrode (as long as not last one in its line and valid electrode)
        if (min(normals{i,j,1} ~= 0))
            k = normals{i,j,2};
            starting_locs{i,j,2} = nan;
            starting_locs{i,j,1} = 0;
            normals{i,j,2} = nan;
            normals{i,j,1} = 0;
            N = N-(k+1);
            L = L-1;
%             disp(strcat("N is now ", num2str(N)))
%             disp(strcat("L is now ", num2str(L)))
%             disp(strcat("Removed full line i=",num2str(i),", j=",num2str(j),", k is now ",num2str(normals{i,j,2})))
        end
    end
end

disp(num2str(N))
disp(num2str(L))
disp(num2str(max_depth))
disp(strcat('There are ',num2str(N),' valid electrodes and',num2str(L),' valid lines'))
%%
% check that N is still the same
% count all electrodes
N = 0;
% count all "lines" where any depth is valid
L = 0;
% get max depth of an electrode
max_depth = 0;
avg_depth = 0;
for i=1:numSLocs
    for j=1:10
        % if this is a valid electrode line, add every valid electrode to
        % our linear electrodes matrix
        line_exists = 0;
        if normals{i,j,1} ~= 0
            line_exists = 1;
            kmax = normals{i,j,2};
            max_depth = max(max_depth,kmax);
            for k=0:kmax
                N=N+1;
            end
            avg_depth = avg_depth + kmax;
        end
        L = line_exists + L;
    end
end 
avg_depth = avg_depth / L;
% disp(num2str(N))
% disp(num2str(L))
% disp(num2str(max_depth))
disp(strcat('There are ',num2str(N),' valid electrodes and',num2str(L),' valid lines'))
disp(strcat('The max depth of a line is ',num2str(max_depth),' and the average depth is ',num2str(avg_depth)))

%%
% store all electrodes
% linear and depth storage

% depth storage
normals_reInd_lines = nan(L,3,max_depth);
slocs_reInd_lines = nan(L,3,max_depth);
track_insertionLocCorr_depth = zeros(L,1);

% linear storage
normals_reInd = zeros(N,3);
slocs_reInd = zeros(N,3);
track_insertionLocCorrespondence = zeros(N,1);
track_depthICorrespondence = zeros(N,1);
track_angleICorrespondence = zeros(N,1);
track_jkIndsForMappingSTGeo = cell(1,numSLocs);

% contacts storage. there are 16xN contacts where N is the number of valid
% electrodes
contactLocs = scirunfield;
contactLocs.node = cell(1, L); % locations. each cell is 3xB matrix where B is the number of unique contacts from valid electrodes in that line. this will subsequently be concatenated together as 3xC matrix where C is total number of valid contacts
contactLocsIndsforLinElectrodes = zeros(2, N, 'int32'); % stores start and end index of contacts in contactLocs.node matrix that belong to each electrode

N=0;
L=1;
curNumContacts = 0;
for i=1:numSLocs
    % disp(['s_loc: ' num2str(i)])
    track_jkIndsForMappingSTGeo{i} = zeros(10,14); % max 14 electrodes in one line (k=0:13)
    for j=1:10
        line_exists = 0;
        % if this is a valid electrode line, add every valid electrode to
        % our linear electrodes matrix
        if normals{i,j,1} ~= 0
            line_exists = 1;
            kmax = normals{i,j,2};
            
            % get contact locations. scirunfield nodes are 3xN
            % there are 16, 3.5 mm apart, starting 1 mm down from sloc for
            % each electrode. but 15 contacts overlap on adjacent
            % electrodes in a line
            contactLocs.node{L} = starting_locs{i,j,1} + [1:3.5:(3.5*(15+kmax)+1)] .* normals{i,j,1};
            thisLineNumContacts = length([1:3.5:(3.5*(15+kmax)+1)]);
            n_temp = N;
            % store indices of overlapping contacts for each electrode in line
            for k=0:kmax  % kmax is at most 13 (so max 14 electodes in line)
                n_temp = n_temp + 1;
                contactLocsIndsforLinElectrodes(:,n_temp) = [int32(curNumContacts+k+1); int32(curNumContacts+k+15+1)];
            end
            curNumContacts = curNumContacts + thisLineNumContacts;
            
            for k=0:kmax
                sloc_d = starting_locs{i,j,1} + k*3.5*normals{i,j,1};
                N=N+1;
                normals_reInd(N,:) = normals{i,j,1}';
                slocs_reInd(N,:) = sloc_d'; 
                track_insertionLocCorrespondence(N) = i;
                track_depthICorrespondence(N) = k;
                track_angleICorrespondence(N) = j;
                track_jkIndsForMappingSTGeo{i}(j,k+1)=1;
                
                % depth storage
                normals_reInd_lines(L,:,k+1)= normals{i,j,1}';
                slocs_reInd_lines(L,:,k+1)= sloc_d';
                track_insertionLocCorr_depth(L) = i;
            end
        end
        L = line_exists + L;
    end
end
L = L-1;

% add implanted contacts to contactLocs structure
contactLocs.node = [contactLocs.node{:}]; % convert cell to matrix
if find(size(implantedELocations)==3) ~= find(size(contactLocs.node)==3)
    implantedELocations = implantedELocations';
end
contactLocs.node = cat(find(size(contactLocs.node)~=3),contactLocs.node,implantedELocations);

% also add recordable radius study contact locs!
if find(size(allSelContacts.node)==3) ~= find(size(allSelContacts.node)==3)
    allSelContacts.node = allSelContacts.node';
end
contactLocs.node = cat(find(size(contactLocs.node)~=3),contactLocs.node,allSelContacts.node);
contactLocs.field = zeros(1, length(contactLocs.node)); % data. always zeros

disp(strcat('There are ',num2str(length(contactLocs.node)),' contact locs, meaning nodes in our lead field'))
disp(strcat(num2str(length(implantedELocations))," of these contact locs are from the implanted electrode contacts"))
disp(strcat(num2str(length(allSelContacts.node))," of these contact locs are for the recordable radius study"))

%%
% save contact locs
save(strcat(dataWorkPath,patientDir,'/DemoOut/contactLocs.mat'),'contactLocs')
disp('contactLocsIndsforLinElectrodes')
% and indices of contacts corresponding to each electrode
size(contactLocsIndsforLinElectrodes)

save(strcat(dataWorkPath,patientDir,'/DemoOut/contactLocsIndsforLinElectrodes.mat'),'contactLocsIndsforLinElectrodes')
    
% save depth storage
save(strcat(dataWorkPath,patientDir,'/DemoOut/normals_reInd_lines.mat'),'normals_reInd_lines')
save(strcat(dataWorkPath,patientDir,'/DemoOut/slocs_reInd_lines.mat'),'slocs_reInd_lines')
save(strcat(dataWorkPath,patientDir,'/DemoOut/electrodes_insertionLocCorr_depth.mat'),'track_insertionLocCorr_depth')
save(strcat(dataWorkPath,patientDir,'/DemoOut/track_jkIndsForMappingSTGeo_lines.mat'),'track_jkIndsForMappingSTGeo')

% save linear storage 
save(strcat(dataWorkPath,patientDir,'/DemoOut/track_jkIndsForMappingSTGeo.mat'),'track_jkIndsForMappingSTGeo')
save(strcat(dataWorkPath,patientDir,'/DemoOut/electrodes_insertionLocCorr.mat'),'track_insertionLocCorrespondence')
save(strcat(dataWorkPath,patientDir,'/DemoOut/electrodes_depthICorr.mat'),'track_depthICorrespondence')
save(strcat(dataWorkPath,patientDir,'/DemoOut/electrodes_angleICorr.mat'),'track_angleICorrespondence')
save(strcat(dataWorkPath,patientDir,'/DemoOut/normals_reInd.mat'),'normals_reInd')
save(strcat(dataWorkPath,patientDir,'/DemoOut/slocs_reInd.mat'),'slocs_reInd')



tRind = toc;
disp(['Done reindex electrodes fo linear and depth/lines storage. This took' num2str(tRind/60) 'minutes'])

disp('calculate BB overlaps')
tic

%% calculate BB overlaps

threshold = 4; %mm. so 2mm added to each side of each side of both boxes

insertionLocs_Einds_depth = cell(1,numSLocs);
insertionLocs_boundingboxes_boxes = cell(1,numSLocs);
for i=1:numSLocs
    % get indices of electrodes (L-basis) that have this insertion loc
    insertionLocs_Einds_depth{i} = find(track_insertionLocCorr_depth==i);
    sloc = 0;
    for j = 1:10
        if starting_locs{i,j,1} ~= 0
            sloc = starting_locs{i,j,1};
            continue
        end
    end
    if sloc == 0
        disp(strcat("Insertion Point ",num2str(i),' has no valid electrodes'))
        continue
    end
    elocs = zeros(3,10);
    for j = 1:10
        maxK = normals{i,j,2};
        elocs(:,j) = normals{i,j,1}*(54.5+3*maxK) + sloc;
    end
    elocs(:,sum(abs(elocs),1)==0) = [];
    points = [sloc,elocs];
    insertionLocs_boundingboxes_boxes{i} = [min(points(1,:)) min(points(2,:)) min(points(3,:)); max(points(1,:)) max(points(2,:)) max(points(3,:))];
    % add half of threshold distance to both sides of every side
    insertionLocs_boundingboxes_boxes{i}(1,:) = insertionLocs_boundingboxes_boxes{i}(1,:) - threshold/2;
    insertionLocs_boundingboxes_boxes{i}(2,:) = insertionLocs_boundingboxes_boxes{i}(2,:) + threshold/2;
%     end
%     size(points);
end

save(strcat(dataWorkPath,patientDir,'/DemoOut/insertionLocs_Einds_depth.mat'),'insertionLocs_Einds_depth') % what electrodes (in L-basis) are in what bounding boxes?


% get Bounding box overlaps
BBOverlap = zeros(numSLocs,numSLocs);
% only BBs under diagonal show intersects
for w=1:numSLocs % for each bounding box
    %disp(num2str(w))
    wBox = insertionLocs_boundingboxes_boxes{w};
    if isempty(wBox)
        continue
    end
    % go through all bounding boxes
    for v = 1:w-1 % so v never == w. we know that those would intersect. 
        vBox = insertionLocs_boundingboxes_boxes{v};
        if isempty(vBox)
            continue
        end
        % test if two bounding boxes intersect
        % two boxes intersect if each of their x y and z coordinates intersect
        numCoordsInt = 1;
        for c = 1:3 % x, y, z
            if overlap1D(wBox(1,c),wBox(2,c),vBox(1,c),vBox(2,c))==false % do not overlap on this coordinate, therefore 3D boxes cannot overlap
               numCoordsInt = 0;
               break
            end
        end
        if numCoordsInt == 1 % boxes overlap in all three dimensions -> they intersect
            BBOverlap(w,v)=1;
            % only fill in 1s below diagonal to prevent double collision tests
        end
    end
end
percOverlap = sum(sum(BBOverlap))/numel(BBOverlap) * 100; % 2.5 % bounding boxes overlap
disp(strcat("There is ",num2str(percOverlap),'% overlap for all bounding boxes'))

save(strcat(dataWorkPath,patientDir,'/DemoOut/insertionLocs_boundingboxes_boxes.mat'),'insertionLocs_boundingboxes_boxes')

save(strcat(dataWorkPath,patientDir,'/DemoOut/BBOverlap.mat'),'BBOverlap') % what BBs overlap with what BBs? this is the same for L and N basis because all electrodes in one line are in the same bounding box

tBBo = toc;
disp(['Done calculate BB overlaps. This took' num2str(tBBo/60) 'minutes'])

disp('count electrodes')
tic


%%
% create figures of BBs and electrodes to make sure everything is looking correct
% All (most) electrodes


f = figure('visible','off');
clf
% plot 0 ks 
is = 1:1:numSLocs;
js = 1:1:10;
k = 0;
for x = 1:length(is)
    for y = 1:length(js)
        i = is(x);
        j = js(y);
        normal = normals{i,j,1};
        if normal == 0
            continue
        end
        k = normals{i,j,2};
        sloc = starting_locs{i,j,1}+3*normal*k;
        eloc = sloc + 54.5*normal;
        plot3([sloc(1),eloc(1)],[sloc(2),eloc(2)],[sloc(3),eloc(3)],'LineWidth',2)
        hold on
        break
    end
end
stlPlot(cortexSurf.Points,cortexSurf.ConnectivityList,[],"")
%%
saveas(f,strcat(dataHomePath,patientDir,'/DemoOut/electrodeVis_k0'),'fig')
saveas(f,strcat(dataHomePath,patientDir,'/DemoOut/electrodeVis_k0'),'png')
% openfig('electrodeVis_k0.fig','new','visible')


% plot max ks
f = figure('visible','off');
clf

is = 1:numSLocs;
js = 1:10;
for x = 1:length(is)
    for y = 1:length(js)
        i = is(x);
        j = js(y);
        normal = normals{i,j,1};
        if normal == 0
            continue
        end
        k = normals{i,j,2};
        sloc = starting_locs{i,j,1}+3*normal*k;
        eloc = sloc + 54.5*normal;
        plot3([sloc(1),eloc(1)],[sloc(2),eloc(2)],[sloc(3),eloc(3)],'LineWidth',2)
        hold on
    end
end
stlPlot(cortexSurf.Points,cortexSurf.ConnectivityList,[],"")
saveas(f,strcat(dataHomePath,patientDir,'/DemoOut/electrodeVis_kMax'),'fig')
saveas(f,strcat(dataHomePath,patientDir,'/DemoOut/electrodeVis_kMax'),'png')

% openfig('electrodeVis_kMax.fig','new','visible')


% plot random ks - SUBSET 
f = figure('visible','off');
clf
is = 1:numSLocs;
js = 1:10;
for x = 1:length(is)
     for y = 1:length(js)
        i = is(x);
        j = js(y);
        normal = normals{i,j,1};
        if normal == 0
            continue
        end
        k = normals{i,j,2};
        sloc = starting_locs{i,j,1}+3*normal*(randi(k+1)-1);
        eloc = sloc + 54.5*normal;
        plot3([sloc(1),eloc(1)],[sloc(2),eloc(2)],[sloc(3),eloc(3)],'LineWidth',2)
        hold on
     end
end
stlPlot(cortexSurf.Points,cortexSurf.ConnectivityList,[],"")
saveas(f,strcat(dataHomePath,patientDir,'/DemoOut/electrodeVis_kRand_Subset'),'fig')
saveas(f,strcat(dataHomePath,patientDir,'/DemoOut/electrodeVis_kRand_Subset'),'png')
% openfig('electrodeVis_kRand_Subset.fig','new','visible')


%%

% Bounding boxes
plotEsToo = 1;
f = figure('visible','off');
clf
% plot trisurf
stlPlot(cortexSurf.Points,cortexSurf.ConnectivityList,[],"")
hold on
% plot bounding boxes
for i=1:numSLocs
    box_points = insertionLocs_boundingboxes_boxes{i};
    if isempty(box_points)
        continue
    end
    plotcube([box_points(1,1)-box_points(2,1),box_points(1,2)-box_points(2,2),box_points(1,3)-box_points(2,3)],box_points(2,:),0.05,[0.9 0.9 0.9])
    if plotEsToo
        for j = 1:10
            normal = normals{i,j,1};
            if normal == 0
                continue
            end
            k = normals{i,j,2};
            sloc = starting_locs{i,j,1}+3*normal*(randi(k+1)-1);
            eloc = sloc + 54.5*normal;
            plot3([sloc(1),eloc(1)],[sloc(2),eloc(2)],[sloc(3),eloc(3)],'LineWidth',2)
            hold on
         end
    end
end
saveas(f,strcat(dataHomePath,patientDir,'/DemoOut/BBvis'),'fig')
saveas(f,strcat(dataHomePath,patientDir,'/DemoOut/BBvis'),'png')

% openfig('BBvis.fig','new','visible')
%cortexSurf = stlread(['/cortexSurf.stl']);

%%

% get valid electrodes in each line for depth to linear indexing

% get matrix of all valid electrodes in all possible electrodes in lines
valid_electrodes_in_each_line = zeros(L,14); % use this to reduce 3rd dimension (L1 electrodes)
valid_electrodes_in_all_lines = zeros(L*14,1); % use this to reduce 4th dimension (L2 electrodes)
indOfLineValid = 1; % 1 to L

N = 0; % get total number of valid electrodes
for i = 1:numSLocs
    insertion_loc_all_electrodes_validity = track_jkIndsForMappingSTGeo{i};
    % get valid lines in all 10 of these lines
    valid_linesST_thisILoc = find(sum(insertion_loc_all_electrodes_validity,2)>0); % range 1 to 10
    indices_of_these_linesValid = indOfLineValid:indOfLineValid+length(valid_linesST_thisILoc)-1; % 1 to 48253
    indOfLineValid = indOfLineValid + length(valid_linesST_thisILoc);  % 1 to 48253
    % for each valid line in this insertion loc group (<= 10)
    for validLine = 1:length(valid_linesST_thisILoc)
        % index of this line in this insertion loc group (1 to 10)
        lineIndST_thisILoc = valid_linesST_thisILoc(validLine);
        % index of this line in all valid electrodes (1 to 48253)
        this_lineValidI = indices_of_these_linesValid(validLine);
        
        line_exists = 1;
        kmax = normals{i,lineIndST_thisILoc,2};
        for k=0:kmax
            % valid electrode!
            N=N+1;
            % this electrode (1-12) is valid for this valid line 
            valid_electrodes_in_each_line(this_lineValidI,k+1) = 1;
            % this electrode is valid for ALL possible electrodes in valid lines
            valid_electrodes_in_all_lines((this_lineValidI-1)*14+(k+1))=1;
        end
    end
end
sum(sum(valid_electrodes_in_each_line)) % = N. perfect!!
sum(sum(valid_electrodes_in_all_lines)) % = N. perfect!!
size(valid_electrodes_in_each_line)
size(valid_electrodes_in_all_lines)

save(strcat(dataWorkPath,patientDir,'/DemoOut/valid_electrodes_in_each_line.mat'),'valid_electrodes_in_each_line') 
save(strcat(dataWorkPath,patientDir,'/DemoOut/valid_electrodes_in_all_lines.mat'),'valid_electrodes_in_all_lines') 


tCe = toc;
disp(['Done count electrodes. This took' num2str(tCe/60) 'minutes'])



function [overlap] = overlap1D(a1,a2,b1,b2)
% a1 is starting value a2 is ending value of first line segment in 1D
% b1 is starting value and b2 is end value of second line segment in 1D
% a1 must be < a2 and b1 must be < b2!
overlap = false;
if (a1>a2) 
% if a1 >= a2, switch them
    points = [a1,a2];
    a1 = points(2);
    a2 = points(1);
end
if (b1>b2) 
% if b1 >= b2, switch them
    points = [b1,b2];
    b1 = points(2);
    b2 = points(1);
end
    
if (a2>b1 && b2>a1)
    overlap = true;
end

end


function [n,V,p] = affine_fit(X)

% Copyright (c) 2016, Fangdi Sun
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
% 
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in
%       the documentation and/or other materials provided with the distribution
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.


    %Computes the plane that fits best (lest square of the normal distance
    %to the plane) a set of sample points.
    %INPUTS:
    %
    %X: a N by 3 matrix where each line is a sample point
    %
    %OUTPUTS:
    %
    %n : a unit (column) vector normal to the plane
    %V : a 3 by 2 matrix. The columns of V form an orthonormal basis of the
    %plane
    %p : a point belonging to the plane
    %
    %NB: this code actually works in any dimension (2,3,4,...)
    %Author: Adrien Leygue
    %Date: August 30 2013
    
    %the mean of the samples belongs to the plane
    p = mean(X,1);
    
    %The samples are reduced:
    R = bsxfun(@minus,X,p);
    %Computation of the principal directions if the samples cloud
    [V,D] = eig(R'*R);
    %Extract the output from the eigenvectors
    n = V(:,1);
    V = V(:,2:end);
end


function stlPlot(v, f, pf, name)
%STLPLOT is an easy way to plot an STL object
%V is the Nx3 array of vertices
%F is the Mx3 array of faces
%NAME is the name of the object, that will be displayed as a title
object.vertices = v;
object.faces = f;
c = [0.8 0.8 1.0];
color = repmat(c,length(f),1);
for k = 1:length(pf)
    change = 1 - (1/(2*length(pf)))*(k-1);
    color(pf(k),:) = [change change 0];
end
patch(object,'FaceVertexCData',       color, ...
         'FaceColor', 'flat',                ...
         'FaceAlpha', 0.3, ...
         'EdgeColor',       'none',        ...
         'FaceLighting',    'gouraud',     ...
         'AmbientStrength', 0.15);
     
% Add a camera light, and tone down the specular highlighting
camlight('headlight');
material('dull');
% Fix the axes scaling, and set a nice view angle
%axis('image');
view([-135 35]);
grid on;
title(name);
end

function [] = visualize_electrodes(is,js,ks,normals,starting_locs,trisurf)

figure(1)
clf
for x = 1:length(is)
    if length(ks) == 1
        k = ks;
    else
        k = ks(x);
    end
    i = is(x);
    j = js(x);
    normal = normals{i,j,1};
    sloc = starting_locs{i,j,1}+3.0037*normal*k;
    eloc = sloc + 54.5*normal;
    plot3([sloc(1),eloc(1)],[sloc(2),eloc(2)],[sloc(3),eloc(3)],'LineWidth',5)
    hold on
end
stlPlot(trisurf.Points,trisurf.ConnectivityList,[],"")

end

function midLoc = checkMidline(sloc, endloc, midlinePlane)
% get side of electrode or whether any part of it is in the midline
% say that an point is inside the midline if it is <= 1cm away
n = midlinePlane(:,1);
p = midlinePlane(:,2);

% check what side sloc is on and distance to midline
% get shortest distance from sloc point to midline plane
d1 = dot((sloc-p),n);

% check what side endloc is on and distance to midline
d2 = dot((endloc-p),n);

% if either distance is < 10mm or side1 ~= side2, return 0 
%   meaning electrode is inside midline
if sign(d1)~=sign(d2) || abs(d1) <= 10 || abs(d2) <= 10
    midLoc = 0;
else
    midLoc = sign(d1);
    % return -1 if both are on left side (towards negative x)
    % return 1 if both are on right side (towards positive x)
end

end



function plotcube(varargin)
% PLOTCUBE - Display a 3D-cube in the current axes
%
%   PLOTCUBE(EDGES,ORIGIN,ALPHA,COLOR) displays a 3D-cube in the current axes
%   with the following properties:
%   * EDGES : 3-elements vector that defines the length of cube edges
%   * ORIGIN: 3-elements vector that defines the start point of the cube
%   * ALPHA : scalar that defines the transparency of the cube faces (from 0
%             to 1)
%   * COLOR : 3-elements vector that defines the faces color of the cube
%
% Example:
%   >> plotcube([5 5 5],[ 2  2  2],.8,[1 0 0]);
%   >> plotcube([5 5 5],[10 10 10],.8,[0 1 0]);
%   >> plotcube([5 5 5],[20 20 20],.8,[0 0 1]);

% Default input arguments
inArgs = { ...
  [10 56 100] , ... % Default edge sizes (x,y and z)
  [10 10  10] , ... % Default coordinates of the origin point of the cube
  .7          , ... % Default alpha value for the cube's faces
  [0.3 0 0]       ... % Default Color for the cube
  };

% Replace default input arguments by input values
inArgs(1:nargin) = varargin;

% Create all variables
[edges,origin,alpha,clr] = deal(inArgs{:});

XYZ = { ...
  [0 0 0 0]  [0 0 1 1]  [0 1 1 0] ; ...
  [1 1 1 1]  [0 0 1 1]  [0 1 1 0] ; ...
  [0 1 1 0]  [0 0 0 0]  [0 0 1 1] ; ...
  [0 1 1 0]  [1 1 1 1]  [0 0 1 1] ; ...
  [0 1 1 0]  [0 0 1 1]  [0 0 0 0] ; ...
  [0 1 1 0]  [0 0 1 1]  [1 1 1 1]   ...
  };

XYZ = mat2cell(...
  cellfun( @(x,y,z) x*y+z , ...
    XYZ , ...
    repmat(mat2cell(edges,1,[1 1 1]),6,1) , ...
    repmat(mat2cell(origin,1,[1 1 1]),6,1) , ...
    'UniformOutput',false), ...
  6,[1 1 1]);


cellfun(@patch,XYZ{1},XYZ{2},XYZ{3},...
  repmat({clr},6,1),...
  repmat({'FaceAlpha'},6,1),...
  repmat({alpha},6,1)...
  );

view(3);

end



function visBBs(BBcell, fieldPoints, plotBool, pathAndPlotName)

% plot trisurf
if plotBool == 1
    f = figure();
else
    f = figure('visible','off');
end
clf

if length(fieldPoints(:,1)) < length(fieldPoints(1,:))
    fieldPoints = fieldPoints';
end
scatter3(fieldPoints(:,1),fieldPoints(:,2),fieldPoints(:,3),'.')
hold on

% plot (first 500) bounding boxes
for i=1:min(500,length(BBcell))
    box_points = BBcell{i};
    if length(box_points(:,1)) <= 1
        continue
    end
    plotcube([box_points(1,1)-box_points(2,1),box_points(1,2)-box_points(2,2),box_points(1,3)-box_points(2,3)],box_points(2,:),0.05,[0.9 0.9 0.9])
end
if plotBool ~= 1
    saveas(f,pathAndPlotName,'fig')
    saveas(f,pathAndPlotName,'png')
end
% openfig('BBvis.fig','new','visible')

end

function rot = rotz(angle)
    rot = [cos(angle) -sin(angle) 0; sin(angle) cos(angle) 0; 0 0 1];
end

function rot = rotx(angle)
    rot = [1 0 0; 0 cos(angle) -sin(angle); 0 sin(angle) cos(angle)];
end

function rot = roty(angle)
    rot = [cos(angle) 0 sin(angle); 0 1 0; -sin(angle) 0 cos(angle)];
end









