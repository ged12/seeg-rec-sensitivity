% 11/11/21
% get ROIs to test tree search methods on finding best electrode configurations

% $$ UPDATE PARAMETERS $$
patientID = 25;
dataWorkPath = '';
dataHomePath = '';
ROIs = ["clinicianROI"]; 

patientDir = ['Patient_' num2str(patientID)];

LH_white = stlread(strcat(dataHomePath,patientDir,'/Inputs/lh_white_20k.stl'));
RH_white = stlread(strcat(dataHomePath,patientDir,'/Inputs/rh_white_20k.stl'));
cortexSurf = stlread(strcat(dataHomePath,patientDir,'/Inputs/cortexSurf.stl'));

load(strcat(dataWorkPath,patientDir,'/DemoOut/lh_areas.mat'), 'lh_areas')
load(strcat(dataWorkPath,patientDir,'/DemoOut/rh_areas.mat'), 'rh_areas')
load(strcat(dataWorkPath,patientDir,'/DemoOut/lh_normals.mat'), 'lh_normals')
load(strcat(dataWorkPath,patientDir,'/DemoOut/rh_normals.mat'), 'rh_normals')
load(strcat(dataWorkPath,patientDir,'/DemoOut/cortex_areas.mat'), 'cortex_areas')
load(strcat(dataWorkPath,patientDir,'/DemoOut/cortex_normals.mat'), 'cortex_normals')


patches.dipoles = cell(1,length(ROIs));
patches.areas = zeros(1,length(ROIs));
patches.patchfield = cell(1,length(ROIs));
patches.name = cell(1,length(ROIs));

for ROIi = 1:length(ROIs)
    
    ROI_id = ROIs(ROIi)
    
    patches.name{ROIi} = ROIs(ROIi);
    
    if ROI_id == "LH"
        patches.dipoles{ROIi} = [1:length(lh_areas)];
        patches.areas(ROIi) = sum(lh_areas);
        patches.patchfield{ROIi} = LH_white;
    elseif ROI_id == "RH"
        patches.dipoles{ROIi} = length(lh_areas)+ [1:length(rh_areas)];
        patches.patchfield{ROIi} = RH_white;
        patches.areas(ROIi) = sum(rh_areas);
    elseif ROI_id == "WC"
        patches.dipoles{ROIi} = [1:length(lh_areas)+length(rh_areas)];
        patches.patchfield{ROIi} = cortexSurf;
        patches.areas(ROIi) = sum(cortex_areas);
    elseif (ROI_id == "LTL") || (ROI_id == "RTL")
        disp('REMEMBER TO MAKE LTL AND RTL SEGMENTATION!!')
        if ROI_id == "RTL"
            disp('ERROR, RTL IS NOT MADE!!!! USING LTL')
        end
        try
            LTL = stlread(strcat(dataHomePath,patientDir,'/Inputs/leftTemporalLobe.stl'));
            disp(strcat(dataHomePath,patientDir,'/Inputs/leftTemporalLobe.stl'))
        catch
            disp('ERROR, LTL IS NOT MADE!!!! Skipping for now, hopefully no errors down the pipeline!')
            continue
        end
        LTL_normals = get_cortex_normals(LTL);
        [~,dLTL,~] = intersect(round(lh_normals.node',3), round(LTL_normals.node',3),'rows');
        patches.patchfield{ROIi} = LTL;
        patches.dipoles{ROIi} = dLTL;
        patches.areas(ROIi) = sum(lh_areas(dLTL));
        
    elseif (ROI_id == "clinicianROI")
        disp("Attempting to load clinician ROI surface")
        clinROI = stlread(strcat(dataHomePath,patientDir,'/Inputs/clinicianROI.stl'));
        clinROI_normals = get_cortex_normals(clinROI);
        
        %cortex_normals = get_cortex_normals(LH_white);
        [~,dclinROI,~] = intersect(round(cortex_normals.node',3), round(clinROI_normals.node',3),'rows');
        patches.patchfield{ROIi} = clinROI;
        patches.dipoles{ROIi} = dclinROI;
        patches.areas(ROIi) = sum(cortex_areas(dclinROI));
    
    elseif (ROI_id == "clinicianROI_broad")
        disp("Attempting to load clinician ROI surface")
        clinROI = stlread(strcat(dataHomePath,patientDir,'/Inputs/clinicianROI_broad.stl'));
        clinROI_normals = get_cortex_normals(clinROI);
        
        %cortex_normals = get_cortex_normals(LH_white);
        [~,dclinROI,~] = intersect(round(cortex_normals.node',3), round(clinROI_normals.node',3),'rows');
        patches.patchfield{ROIi} = clinROI;
        patches.dipoles{ROIi} = dclinROI;
        patches.areas(ROIi) = sum(cortex_areas(dclinROI));
        
    else % patch % Code in form "L10" meaning Left 10 cm^2
        % parse side and area (in cm^2)
        rng(ROIi)
        ROI_char = char(ROI_id);
        if ROI_char(1) == 'L'
            sface = randi(length(lh_areas));
        else
            sface = length(lh_areas) + randi(length(rh_areas));
        end
        atarget = str2double(ROI_char(2:end)) * 100; % in mm^2
        %
        [patch,total_area,patchfield] = make_patch(cortexSurf,cortex_areas,sface,atarget);
        %
        patches.dipoles{ROIi} = patch;
        patches.areas(ROIi) = total_area;
        patches.patchfield{ROIi} = patchfield;
    end
end
save(strcat(dataWorkPath,patientDir,'/DemoOut/ROIs.mat'),'patches')
disp("Done saving ROIs!")


function [patch,total_area,patchfield] = make_patch(FV,areas,sface,atarget)
% adapted from make_patch_startface
%
% Iteratively expand from starting face until patch is at least as large as atarget
% Then calculate patchfield for visualization

h.faces = FV.ConnectivityList;
h.vertices = FV.Points;

outer_vertices = [];
total_area = areas(sface);
patch = sface;
for y = 1:3
    outer_vertices = [outer_vertices,h.faces(sface,y)];
end

% iteratively expand patch by outervertices (one vertex at a time)
start = 1;
while total_area < atarget
    disconnected = 0;
     for v = outer_vertices
        if total_area >= atarget
            break
        end
        [ia,~] = find(h.faces==v);
        ia = unique(ia);
        ia(ismember(ia,patch)==1)=[];
        if ~isempty(ia) 
            for f = transpose(ia)
                if total_area >= atarget
                    break
                end
                patch = [patch;f];
                total_area = total_area + areas(f);
                for coord = 1:3
                    if ismember(h.faces(f,coord),outer_vertices) == 0
                        outer_vertices = [outer_vertices,h.faces(f,coord)];
                        start = 0;
                    end
                end
            end 
        else
            disconnected = disconnected + 1;
        end
        outer_vertices(outer_vertices==v)=[];
     end
     if disconnected == 3 && start == 1
         disconnected = "first dipole is disconnected! no patch can be made."
         break
     end
end

% make triangulation!
Points = [];
ConnectivityList = zeros(length(patch),3);
point_index = 1;
for x = 1:length(patch) % x = patch number
    for c = 1:3
        % h.faces(patch(x),c) = vertex coord index (in h.vertices)
        a = h.vertices(h.faces(patch(x),c),:); % = new vertex coords
        Points = [Points;a];
        ConnectivityList(x,c) = point_index;
        point_index = point_index + 1;
    end
end
patchfield = triangulation(ConnectivityList,Points);    
    
% % calc centroid and netDipole
% acc_mass = 0;
% acc_area = 0;
% b = [0;0;0];
% for index = 1:length(patch)
%     ctr = normals.node(:,patch(index));
%     dipoleM = normals.field(:,patch(index))*areas(patch(index))*cdmd;
%     b = b + dipoleM;
%     acc_mass = acc_mass + (ctr * areas(patch(index)));
%     acc_area = acc_area + areas(patch(index));
% end 
% a = acc_mass ./ acc_area;
% if side == 1
%     a(3) = a(3) + 68;
%     a(1) = a(1) + 3;
% elseif side == 2
%     a(3) = a(3) + 70;
%     a(1) = a(1) + 3;
% end
% 
% centroid = a;
% netDipole = b;

end

function [normals] = get_cortex_normals(FV)

    FVP = FV.Points;
    FVC = FV.ConnectivityList;
    %
    for k = 1:length(FVC)
        %get three points of each triangle
        v1 = FVC(k,1);
        v2 = FVC(k,2);
        v3 = FVC(k,3);
    
        %get coordinates of each point
        a = FVP(v1,:);
        b = FVP(v2,:);
        c = FVP(v3,:);
    
        %calc cross product of two sides
        m = cross(b-a,c-a);
        
        %calc normal vector
        n = m/norm(m);
    
        %calc position by components
        x = (a(1) + b(1) + c(1)) / 3;
        y = (a(2) + b(2) + c(2)) / 3;
        z = (a(3) + b(3) + c(3)) / 3;
    
    %assemble position into vector
        p = [x,y,z];
    
    %put normal vector and position into matrix of row k
        if k == 1
            CDN_n = zeros([length(FVC),3]);
            CDN_p = zeros([length(FVC),3]);
        end
        CDN_n(k,:) = n;
        CDN_p(k,:) = p;
        
    end
  
    normals.node = transpose(CDN_p);
    normals.field = transpose(CDN_n);
    
    %figure(1)
    %clf
    %quiver3(CDN_p(:,1),CDN_p(:,2),CDN_p(:,3),CDN_n(:,1),CDN_n(:,2),CDN_n(:,3),'LineWidth',1);
    %hold on
    %trimesh(FV)
    % how to make sure normals are pointing correct way??
    
end


