% getSkinInsertionLocs
% Grace Dessert
% 3/7/22

function scirunfield = getSkinInsertionLocs_demo(skinSurf, dataHomePath, dataWorkPath, midline, patientID, LH_white, RH_white)

% DEMO VERSION:
% only select ~100 points on the skin
% (full version selects ~800)


patientDir = ['Patient_' num2str(patientID)];

% open MNI skin, LH, RH, MNI ins locs template, and midline 
% also open patient skin, midline, LH and RH

% MNI files

local = 0;
MNI_template = stlread(strcat(dataHomePath,"InputGeneral/MNI_SkinValidForElectrodeImplant.stl"));
MNI_full = stlread(strcat(dataHomePath,'InputGeneral/MNI_Skin_Final.stl'));
LH_MNI = stlread(strcat(dataHomePath,'InputGeneral/MNI_lh_white_20k.stl'));
RH_MNI = stlread(strcat(dataHomePath,'InputGeneral/MNI_rh_white_20k.stl'));
load(strcat(dataHomePath,"InputGeneral/MNI_midlinePlane.mat"), 'midlinePlane')
MNIMid = midlinePlane;


% display original MNI brain with midline and patient brain with midline
if  local
    patient_original = figure();
else
    patient_original = figure('visible','off');
end
clf
hold on
trimesh(LH_white,'FaceAlpha',0.1,'FaceColor',[0.3 0 0],'EdgeAlpha',0.2,'EdgeColor',[0.3 0 0])
trimesh(RH_white,'FaceAlpha',0.1,'FaceColor',[0.3 0 0],'EdgeAlpha',0.2,'EdgeColor',[0.3 0 0])
trimesh(skinSurf,'FaceAlpha',0.1,'FaceColor',[0.3 0 0],'EdgeAlpha',0.2,'EdgeColor',[0.3 0 0])
n = midline(:,1);
p = midline(:,2);
z = linspace(min(skinSurf.Points(:,3)),max(skinSurf.Points(:,3)),10);
y = linspace(min(skinSurf.Points(:,2)),max(skinSurf.Points(:,2)),10);
[z2,y2]=meshgrid(z,y);
% ax + by + cz + d = 0
d = -p'*n;
x2 = (-n(2)*y2 - n(3)*z2 - d)/n(1);
% plot plane
surf(x2,y2,z2,'FaceAlpha',0.1,'FaceColor',[0.3 0 0]);

%%
% MNI brain
if  local
    MNI_original = figure();
else
    MNI_original = figure('visible','off');
end
clf
hold on
trimesh(LH_MNI,'FaceAlpha',0.1,'FaceColor',[0 0.3 0],'EdgeAlpha',0.2,'EdgeColor',[0 0.3 0])
trimesh(RH_MNI,'FaceAlpha',0.1,'FaceColor',[0 0.3 0],'EdgeAlpha',0.2,'EdgeColor',[0 0.3 0])
trimesh(MNI_full,'FaceAlpha',0.1,'FaceColor',[0 0.3 0],'EdgeAlpha',0.2,'EdgeColor',[0 0.3 0])
n = MNIMid(:,1);
p = MNIMid(:,2);
z = linspace(min(MNI_full.Points(:,3)),max(MNI_full.Points(:,3)),10);
y = linspace(min(MNI_full.Points(:,2)),max(MNI_full.Points(:,2)),10);
[z2,y2]=meshgrid(z,y);
% ax + by + cz + d = 0
d = -p'*n;
x2 = (-n(2)*y2 - n(3)*z2 - d)/n(1);
% plot plane
surf(x2,y2,z2,'FaceAlpha',0.1,'FaceColor',[0 0.3 0]);

% get MNI midline (save this for future use)
% see getMNImidline.m script get MNI midline (save this for future use)

%%
% visualize midlines 
if local
    midlinePlot = figure('visible','off');
else
    midlinePlot = figure();
end
% ax + by + cz + d = 0
n = MNIMid(:,1);
p = MNIMid(:,2);
d = -p'*n;
x2 = (-n(2)*y2 - n(3)*z2 - d)/n(1);
% plot plane
if local
    midlineInsLocPlot = figure(1);
else
    midlineInsLocPlot = figure('visible','off');
end
clf 
hold on
m1 = surf(x2,y2,z2,'FaceAlpha',0.6,'EdgeAlpha',0.5,'FaceColor',[0 0.3 0],'DisplayName',"Original MNI Midline");
trimesh(LH_MNI,'FaceAlpha',0.1,'FaceColor',[0 0.3 0],'EdgeAlpha',0.2,'EdgeColor',[0 0.3 0])
trimesh(RH_MNI,'FaceAlpha',0.1,'FaceColor',[0 0.3 0],'EdgeAlpha',0.2,'EdgeColor',[0 0.3 0])

n = midline(:,1);
p = midline(:,2);
d = -p'*n;
x2 = (-n(2)*y2 - n(3)*z2 - d)/n(1);
% plot plane
m2 = surf(x2,y2,z2,'FaceAlpha',0.3,'EdgeAlpha',0.5,'FaceColor',[0 0.9 0],'DisplayName',"Patient Midline");
legend([m1, m2])

% save if not local
if ~local
    % save plot 1
    saveas(midlineInsLocPlot,strcat(dataHomePath,patientDir, '/DemoOut/midlineInsLocPlot'),'jpg');
    saveas(midlineInsLocPlot,strcat(dataHomePath,patientDir, '/DemoOut/midlineInsLocPlot'),'fig');
    % remember when opening, you must turn visibility to 'on'
    % ie: openfig('midlinePlot.fig','new','visible')
end
%%
% transform MNI skin to match patient skin, then take pieces of patient skin

n = midline(:,1);
p = midline(:,2);
d = -p'*n;
x2 = (-n(2)*y2 - n(3)*z2 - d)/n(1);

PatSkinMins = [min(skinSurf.Points(:,1)),min(skinSurf.Points(:,2)),min(skinSurf.Points(:,3))];
PatSkinMaxs = [max(skinSurf.Points(:,1)),max(skinSurf.Points(:,2)),max(skinSurf.Points(:,3))];
MNISkinMins = [min(MNI_full.Points(:,1)),min(MNI_full.Points(:,2)),min(MNI_full.Points(:,3))];
MNISkinMaxs = [max(MNI_full.Points(:,1)),max(MNI_full.Points(:,2)),max(MNI_full.Points(:,3))];

% rotate MNI to match midlines
% MNIMid to midline
% rotate MNI_template
r = vrrotvec(midline(:,1),MNIMid(:,1)); % i checked this, it is the correct order now. This is because we are right-multiplying by the rotation matrix, maybe?
rotationMatrix = vrrotvec2mat(r);
newPoints = (MNI_full.Points - MNISkinMins) * rotationMatrix + MNISkinMins;
newSkinTemplatePoints = (MNI_template.Points - MNISkinMins) * rotationMatrix + MNISkinMins;

% get x, y, and z lengths
% scale MNI_template to match, and move all to the same min and max
% positions
PatSkinMins = [min(skinSurf.Points(:,1)),min(skinSurf.Points(:,2)),min(skinSurf.Points(:,3))];
PatSkinMaxs = [max(skinSurf.Points(:,1)),max(skinSurf.Points(:,2)),max(skinSurf.Points(:,3))];
MNISkinMins = [min(MNI_full.Points(:,1)),min(MNI_full.Points(:,2)),min(MNI_full.Points(:,3))];
MNISkinMaxs = [max(MNI_full.Points(:,1)),max(MNI_full.Points(:,2)),max(MNI_full.Points(:,3))];

MNISkinlengths = PatSkinMaxs-PatSkinMins;
PatSkinlengths = MNISkinMaxs-MNISkinMins;
scale_factor = MNISkinlengths./PatSkinlengths;

newPoints = newPoints - MNISkinMins;
newPoints = newPoints .* scale_factor;
newPoints = newPoints + PatSkinMins;

newSkinTemplatePoints = newSkinTemplatePoints - MNISkinMins;
newSkinTemplatePoints = newSkinTemplatePoints .* scale_factor;
newSkinTemplatePoints = newSkinTemplatePoints + PatSkinMins;


%%
% plot additional plots if local
if local
    % original MNI skin (green) vs Patient skin (blue)
    figure(1)
    clf
    hold on
    trimesh(skinSurf,'FaceAlpha',0.3,'EdgeColor',[0.3 0 0],'FaceColor',[0.3 0 0],'EdgeAlpha',0.3,'DisplayName','Patient Skin')
    trimesh(MNI_full,'FaceAlpha',0.1,'EdgeColor',[0 0.6 0],'FaceColor',[0 0.6 0],'EdgeAlpha',0.3,'DisplayName','Original MNI Skin')
    temp1 = triangulation(MNI_full.ConnectivityList, newPoints);
    trimesh(temp1,'FaceAlpha',0.4,'EdgeColor',[0 0.3 0],'FaceColor',[0 0.3 0],'EdgeAlpha',0.1,'DisplayName','Transformed MNI Skin')
    legend()
end

%%
% for every point on transformed template, get closest patient skin point
dists = zeros(length(newSkinTemplatePoints),length(skinSurf.Points));
for x = 1:length(newSkinTemplatePoints)
    tempPoint = newSkinTemplatePoints(x,:);
    dists(x,:) = sum((skinSurf.Points-tempPoint).^2,2);
end
[~,insPoints] = find(dists==min(dists,[],2));
insPoints = unique(insPoints);
skinInsPoints = skinSurf.Points(insPoints,:);

disp(['There are ' num2str(length(insPoints)) ' points for ins']);


%% sample 100 points 
% randomly

if (length(insPoints) > 100) 
    while (length(insPoints) > 100) 
        closePt = randi(length(insPoints));
        insPoints(closePt) = [];
    end
end
disp(['There are ' num2str(length(unique(insPoints))) ' points for ins']);

skinInsPoints = skinSurf.Points(insPoints,:);
scirunfield.node = skinInsPoints;
scirunfield.field = zeros(length(insPoints),1);

% save skin insertion points!
if ~ local
    save(strcat(dataWorkPath,patientDir,'/DemoOut/SkinValidForElectrodeImplant_insPoints.mat'),'insPoints');
    save(strcat(dataWorkPath,patientDir,'/DemoOut/SkinValidForElectrodeImplant.mat'),'scirunfield');
end

%%
% save surfaces and plots for debugging visualization
if local
    insLocPlot = figure(2);
else
    insLocPlot = figure('visible','off');
end
clf
hold on 
scatter3(newSkinTemplatePoints(:,1), newSkinTemplatePoints(:,2),newSkinTemplatePoints(:,3),'.','DisplayName','Fully Transformed MNI Template')
trimesh(skinSurf,'FaceAlpha',0.1,'FaceColor',[0.3 0 0],'EdgeAlpha',0.2,'EdgeColor',[0.3 0 0],'DisplayName','Patient Skin')
scatter3(skinInsPoints(:,1), skinInsPoints(:,2),skinInsPoints(:,3),'filled','MarkerFaceColor',[0 1 0],'DisplayName','Insertion Points')
surf(x2,y2,z2,'FaceAlpha',0,'EdgeAlpha',0.5,'FaceColor',[0 0.9 0],'DisplayName',"Patient Midline");
legend()

%%
% save plots and products
if ~ local
    % save plot 2
    saveas(insLocPlot,strcat(dataHomePath,patientDir, '/DemoOut/insLocPlot'),'jpg');
    saveas(insLocPlot,strcat(dataHomePath,patientDir, '/DemoOut/insLocPlot'),'fig');
    % remember when opening, you must turn visibility to 'on'
    % ie: openfig('midlinePlot.fig','new','visible')
    
    % save intermediate surfaces
    MNI_t = triangulation(MNI_full.ConnectivityList, newPoints);
    newSkinT = triangulation(MNI_template.ConnectivityList, newSkinTemplatePoints);
    stlwrite(newSkinT,strcat(dataHomePath,patientDir,'/DemoOut/transformedSkinTemp.stl'))
    stlwrite(MNI_t,strcat(dataHomePath,patientDir,'/DemoOut/transformedMNIFull.stl'))
end


end



