% Grace Dessert
% 14 Sep 2022
% Finish E Congig visualization by writing files for SCIRun network visConfigRec.srn5
% using precomputed visualization data from visConfigSolution.m


% $$ UPDATE PARAMETERS $$
cdmds = [0.465]; %nAm/mm^2
patchAreas = [10]; % cm^2
dataWorkPath = '';
dataHomePath = '';
patientID = 25;
sourceInd = 1;

patientDir = ['Patient_' num2str(patientID)];

% SELECT DESIRED threshold, cost function, and ROI
%   this script automatically selects the best cost function for your threshold. 
thrI = 2; % 1, 2, or 3, corresponding to 200µV, 500µV, and 1000 µV
ROI_str = "LTL"; %"LTL"; %"LTL"; %"clinicianROI";
thrPrioritys = ["200_500_1000_thrPriority","500_1000_200_thrPriority","1000_500_200_thrPriority","1000_200_500_thrPriority","500_1000_200_thrPriority","200_1000_500_thrPriority"];
costFnSel = thrI;


% SELECT if you want to generate recording strength fields too
%   this adds ~2 minutes to open large file
plotOptRecStr = false;
if ~plotOptRecStr
    disp("We are NOT generating the recording strength field for the optimized configuration for the SCIRun visualization.")
    disp("If you would like to, set 'plotOptRecStr' to 'true'")
end

% load data
thrPriority = thrPrioritys(costFnSel);
tys = strcat(num2str(patchAreas(sourceInd)),"cm2_",num2str(cdmds(sourceInd)),"cdmd");
case_id = strcat(tys,'_ROI_',ROI_str,'_singleMap_',thrPriority);

try
    fP_RS = strcat(dataWorkPath,patientDir,'/DemoOut/NBSPercRec_solution_integratedCost',case_id,'.mat');
    fP = strcat(dataWorkPath,patientDir,'/DemoOut/NBS_solution_integratedCost',case_id,'.mat');
    load(fP, 'solConfigs')
    load(fP_RS,'percRecAllThrsSingMap')
catch
    disp(strcat("ERROR loading opt solution for case ",case_id))
    disp(fP)
    return
end

load(strcat(dataHomePath,patientDir,'/DemoOut/cortex_areas.mat'),'cortex_areas')
load(strcat(dataHomePath,patientDir,'/DemoOut/cortex_normals.mat'),'cortex_normals')

load(strcat(dataHomePath,patientDir,'/Inputs/ContactLocations.mat'),'scirunfield')
contLocs = scirunfield;

LH_fileName = strcat(dataHomePath,patientDir,'/Inputs/lh_white_20k.stl');
LH_white = stlread(LH_fileName);
RH_fileName = strcat(dataHomePath,patientDir,'/Inputs/rh_white_20k.stl');
RH_white = stlread(RH_fileName);
load(strcat(dataWorkPath,patientDir,'/DemoOut/ROIs.mat'),'patches')
skinSurf = stlread(strcat(dataHomePath,patientDir,'/Inputs/SkinFinal.stl'));
load(strcat(dataWorkPath,patientDir,'/DemoOut/SulciSurface_refined.mat'),'sulciPoints');
load(strcat(dataWorkPath,'RawData/ElectrodeLocsData/normals_reInd_full.mat'),'normals_reInd')
load(strcat(dataWorkPath,'RawData/ElectrodeLocsData/slocs_reInd_full.mat'),'slocs_reInd')


% calculate implanted config RS and recording strength across cortex
impCaseId = strcat(tys,"_",thrPriority,"_ROI",ROI_str);
[rec_tissueDat_imp_allThr, configConvergROIPercent_imp_allThr, eAssocNOrder, config] = getImplantedElectrodeData(tys, dataWorkPath, patientDir, patches, thrPriority, dataHomePath, ROI_str);
%
percMapROI_imp_allThr = configConvergROIPercent_imp_allThr;

imp_RS = percMapROI_imp_allThr{5};

% RS
numImpEs = length(imp_RS);
thrs = [200,500,1000];
disp(strcat("Implanted configuration maps ",num2str(imp_RS(numImpEs)*100),"% of the ROI with ",thrPriority," cost function and ",num2str(thrs(thrI))," threshold mapping"))

% full imp config
save(strcat(patientDir,'/DemoOut/FullImplantedConfig.mat'),'contLocs')
disp(strcat("There are ",num2str(numImpEs)," implanted electrodes for this ROI"));
save(strcat(patientDir,'/DemoOut/ImplantedConfig.mat'),'config')

% imp contacts rec strength
imp_rec_tissue = rec_tissueDat_imp_allThr{5};
rec_tissue_imp.field = sum(imp_rec_tissue,1);
rec_tissue_imp.node = cortex_normals.node;
save(strcat(patientDir,'/DemoOut/rec_tissue_imp.mat'),'rec_tissue_imp')


% get optimized configur and calculate rec strength

solConfig = solConfigs{1,1};
opt_RS = percRecAllThrsSingMap(:,3); 

% get config field 


% get rec strength across cortex
if plotOptRecStr 
    if thrs(thrI) == 200
        cNumStr = "C1";
    elseif thrs(thrI) == 500
        cNumStr = "C2";
    else
        cNumStr = "C3";
    end
    recDipoleMFN = strcat(dataWorkPath,'RawData/RecArea/recDipolesbyE_recLocs',cNumStr,'_',tys,'.mat');
    load(recDipoleMFN,'recDipolesbyE')
    size(recDipolesbyE)
    info = whos('recDipolesbyE');
    sizeGB = info.bytes/(1024^3);
    disp(['Electrode-dipole recordability matrix is ' num2str(sizeGB) ' GB'])
else
    recDipolesbyE = [];
end

[rec_tissue_by_E, configField] = getVisFieldsConfig(plotOptRecStr, solConfig, recDipolesbyE, normals_reInd, slocs_reInd, cortex_normals, 0);
        
% contacts
numEs = numImpEs;
configFieldd.node = configField.node(configField.field<=numEs,:);
configFieldd.field = configField.field(configField.field<=numEs);
save(strcat(patientDir,'/DemoOut/configField.mat'),'configFieldd')

% recArea
if plotOptRecStr 
    rec_tissue.node = rec_tissue_by_E.node;
    rec_tissue.field = sum(rec_tissue_by_E.field(1:numEs,:),1);
    save(strcat(patientDir,'/DemoOut/rec_tissue.mat'),'rec_tissue')
end

% lh/rh cortex
stlwrite(LH_white,strcat(patientDir,'/DemoOut/lh.stl'))
stlwrite(RH_white,strcat(patientDir,'/DemoOut/rh.stl'))

% ROI
ROIPI = 0;
for i = 1:length(patches.name)
    if patches.name{i}(1) == ROI_str
        ROIPI = i;
    end
end
if ROIPI == 0
    disp("DIDN't FIND RIGHT ROI NAME!")
    ROI_str
end
ROIfield = patches.patchfield{ROIPI};
stlwrite(ROIfield,strcat(patientDir,'/DemoOut/ROI.stl'))

% RS
disp(strcat("Optimized configuration maps ",num2str(opt_RS(numImpEs)*100),"% of the ROI with ",thrPriority," cost function and ",num2str(thrs(thrI))," threshold mapping"))

% skin
stlwrite(skinSurf,strcat(patientDir,'/skinSurf.stl'))

disp("Visualize configurations and recording sensitivity in the SCIRun Network visEConfig.srn5")
disp("Note that DEMO may have different configuration results due to simplified methodology")

% Plot convg plots
% open the best cost fn case for each threshold for Opt cases
thrPrioritys = ["200_500_1000_thrPriority","500_200_1000_thrPriority","1000_500_200_thrPriority","1000_200_500_thrPriority","500_1000_200_thrPriority","200_1000_500_thrPriority"];
opt_RS_all = zeros(3,30);
for thrI = 1:3
    thrPriority = thrPrioritys(thrI);
    case_id = strcat(tys,'_ROI_',ROI_str,'_singleMap_',thrPriority);
    try
        fP_RS = strcat(dataWorkPath,patientDir,'/DemoOut/NBSPercRec_solution_integratedCost',case_id,'.mat');
        load(fP_RS,'percRecAllThrsSingMap')
    catch
        disp(strcat("ERROR loading opt solution for case ",case_id))
        disp(fP)
        return
    end
    
    opt_RS_all(thrI,:) = percRecAllThrsSingMap(:,3)';
    numEs = length(percRecAllThrsSingMap(:,3));
end
opt_RS_all = [[0;0;0],opt_RS_all*100];



% get best cost fn case for each threshold for Imp cases
imp_RS_all = [];
for thrI = 1:3
    thrPriority = thrPrioritys(thrI);
    [~, configConvergROIPercent_imp_allThr, ~, ~] = getImplantedElectrodeData(tys, dataWorkPath, patientDir, patches, thrPriority, dataHomePath, ROI_str);
    RS = configConvergROIPercent_imp_allThr{5};
    imp_RS_all = [imp_RS_all,RS];
    numImpEs = length(RS);
end
imp_RS_all = [[0,0,0];imp_RS_all*100];

% run imp opt for other cost functions and pick best for each threshold
% imp_RS_all = zeros(3,31);
% imp_RS_all = [percMapROI_imp_allThr{1},percMapROI_imp_allThr{3},percMapROI_imp_allThr{5}];
% imp_RS_all = [[0,0,0];imp_RS_all*100];

% open opt configs for other cost functions and pick best for each
% threshold
% numEs = 30;
% opt_RS_all = zeros(3,31);
% opt_RS_all = percRecAllThrsSingMap*100;
% opt_RS_all = [[0,0,0];opt_RS_all];
% opt_RS_all(length(opt_RS_all):31,:) = opt_RS_all(end,:);

figure(2)
clf
hold on
colorz = [0 0.4470 0.7410;0.8500 0.3250 0.0980;0.9290 0.6940 0.1250;0.4940 0.1840 0.5560;0.4660 0.6740 0.1880;0.3010 0.7450 0.9330;0.6350 0.0780 0.1840];
colorz = lines(4);
colorz = colorz(2:4,:);
grid on
xdata = [0:numEs];
xdataImp = [0:numImpEs];
oneA = plot(xdata,opt_RS_all(1,:),'LineWidth',2.5,'color',colorz(1,:));
twoA = plot(xdata,opt_RS_all(2,:),'LineWidth',2.5,'color',colorz(2,:));
threeA = plot(xdata,opt_RS_all(3,:),'LineWidth',2.5,'color',colorz(3,:));
oneA_i = plot(xdataImp,imp_RS_all(:,1),'--','LineWidth',2.5,'color',colorz(1,:));
twoA_i = plot(xdataImp,imp_RS_all(:,2),'--','LineWidth',2.5,'color',colorz(2,:));
threeA_i = plot(xdataImp,imp_RS_all(:,3),'--','LineWidth',2.5,'color',colorz(3,:));
x = plot([-2,-1],[0,0],'k-','LineWidth',2);
y = plot([-2,-1],[0,0],'k--','LineWidth',2);
legend([oneA, twoA, threeA, x,y], {'200 µV', '500 µV', '1000 µV','Optimized','Implanted'})
legend('location','southeast')
ylim([0,100])
yticks([0:0.25:1]*100)
ylabel('Recording sensitivity')
xticks([0:4:20])
xlim([0,numImpEs+4])
xlabel('Electrodes')
caseID_disp = split(case_id,"_");
caseID_disp = join(caseID_disp([1:2])," ");
caseID_disp = strcat("Patient ",num2str(patientID),", ",caseID_disp);
title([strcat("Recording sensitivity of ",ROI_str," configurations"),caseID_disp])
ax = gca; 
ax.FontSize = 14; 
set(gcf,'position',[860,530,550,250])



function [rec_tissueDat_imp_allThr, configConvergROIPercent_imp_allThr, eAssocNOrder, config] = getImplantedElectrodeData(tys, dataWorkPath, patientDir, patches, orderThrsCost, dataHomePath, ROI_str)

    % the order of cost function priority is standard in the naming scheme:
    %   orderThrsCost= 1000_500_200_thr means 1000 µV is first, 500 µV is 2nd, 200
    %   µV is 3rd. 
    % For the inputs to this function, runDefaultSearchTrials,
    %   recDipolesbyE_C1 correspodns to 200 µV.
    %   recDipolesbyE_C2 corresponds to 500 µV.
    %   recDipolesbyE_C3 corresponds to 1000 µV. 
    
    % For the inputs to the search methods and getBestEs(), the three recDipolesbyE matrices
    % should be OPPOSITE order to their priority order. So for
    % the above example, (1000_500_200_thrPriority), recDipolesbyE_C1 should be assigned to 200µV, 
    %   then recDipolesbyE_C2 should be 500 µV, then recDipolesbyE_C3
    %   shoudl be 1000 µV.
    
    
% open all thresholds
% cNumStrings = ["C1","C2","C3"];
orderThrsCost = split(orderThrsCost,"thr");
orderThrsCost = orderThrsCost(1);
orderThrsCost = double(split(orderThrsCost,"_"));
for i = 1:3
    if orderThrsCost(i) == 200
        cNumStr = "C1";
    elseif orderThrsCost(i) == 500
        cNumStr = "C2";
    else
        cNumStr = "C3";
    end
    % C1 is 200, C2 is 500, C3 is 1000
    load(strcat(dataWorkPath,'RawData/RecArea/recDipolesbyE_implantedLocs',cNumStr,'_',tys,'.mat'),'recDipolesbyE_implanted')
    % C1 is LAST PRIORITY
    % C2 is middle, and C3 is FIRST priority
    if i == 3 
        recDipolesbyE_C1_og = recDipolesbyE_implanted;
    elseif i == 2
        recDipolesbyE_C2_og = recDipolesbyE_implanted;
    else
        recDipolesbyE_C3_og = recDipolesbyE_implanted;
    end
end
%
% get ROI info
ROI_i = 0;
for i = 1:length(patches.name)
    if patches.name{i} == ROI_str
        ROI_i = i;
    end
end
if ROI_i == 0
    disp("WE DIDNT FIND THIS ROI!")
end
ROI_dips = patches.dipoles{ROI_i};
ROI_locs = patches.patchfield{ROI_i}.Points;

% clip to only ROI electrodes
% get distances of all contacts to all ROI points.
% keep all electrodes that have min dist < 5mm
load(strcat(dataWorkPath,'RawData/RecArea/cToE_impC1_',tys,'.mat'),'cToE_imp')
load(strcat(dataHomePath,patientDir,'/Inputs/ContactLocations.mat'),'scirunfield')

ROIEs = getValidImpEs(cToE_imp, scirunfield, ROI_locs);

% clip data by valid electrodes for this ROI
recDipolesbyE_implanted = recDipolesbyE_implanted(ROIEs,:);
recDipolesbyE_C1_og = recDipolesbyE_C1_og(ROIEs,:);
recDipolesbyE_C2_og = recDipolesbyE_C2_og(ROIEs,:);
recDipolesbyE_C3_og = recDipolesbyE_C3_og(ROIEs,:);

recDipolesbyEAll = {recDipolesbyE_C1_og,recDipolesbyE_C2_og,recDipolesbyE_C3_og};

%disp(strcat("Implanted config has ",num2str(length(ROIEs))," valid electrodes for this ROI"))
if isempty(ROIEs)
   disp(strcat("Moving on, no imp config to build"))
   rec_tissueDat_imp_allThr = [];
   configConvergROIPercent_imp_allThr = [];
   eAssocNOrder = [];
   config = [];
   return
end
%%
%disp(strcat("Clinical ROI has ",num2str(length(ROI_dips))," dipoles"))

recDipolesbyE_C1_1 = recDipolesbyE_C1_og(:,ROI_dips);
recDipolesbyE_C2_1 = recDipolesbyE_C2_og(:,ROI_dips);
recDipolesbyE_C3_1 = recDipolesbyE_C3_og(:,ROI_dips);

E = size(recDipolesbyE_implanted,1);
D = size(recDipolesbyE_implanted,2);

remDipolesTrack = [];
clipped_electrodeInds = [1:E];
%
% pick one electrode at a time until we have no more
bestFirstOptions = getBestEs(recDipolesbyE_C1_1, recDipolesbyE_C2_1, recDipolesbyE_C3_1);
% bestFirstOptions = find(sum(recDipolesbyE,2)==max(sum(recDipolesbyE,2)));
bestFirst = bestFirstOptions(randi(length(bestFirstOptions)));
currConfig = bestFirst;
clipped_electrodeInds(bestFirst) = [];

while 1
    recDipoles = logical(sum(recDipolesbyE_C1_1(currConfig,:),1)>=1);

    remRecDipolesbyE_C1_1 = recDipolesbyE_C1_1;
    remRecDipolesbyE_C1_1(:,recDipoles) = [];

    recDipoles = logical(sum(recDipolesbyE_C2_1(currConfig,:),1)>=1);
    remRecDipolesbyE_C2_1 = recDipolesbyE_C2_1;
    remRecDipolesbyE_C2_1(:,recDipoles) = [];

    recDipoles = logical(sum(recDipolesbyE_C3_1(currConfig,:),1)>=1);
    remRecDipolesbyE_C3_1 = recDipolesbyE_C3_1;
    remRecDipolesbyE_C3_1(:,recDipoles) = [];

    remDsThisRow = [size(remRecDipolesbyE_C1_1,2),size(remRecDipolesbyE_C2_1,2),size(remRecDipolesbyE_C3_1,2)];
    remDipolesTrack = [remDipolesTrack; remDsThisRow];
    
    if length(currConfig)==E
        %disp('All electrodes added!')
        break
    end
    
    remE = [currConfig];
    remRecDipolesbyE_C1_1(remE,:) = [];
    remRecDipolesbyE_C2_1(remE,:) = [];
    remRecDipolesbyE_C3_1(remE,:) = [];
        
    bestOpts = getBestEs(remRecDipolesbyE_C1_1, remRecDipolesbyE_C2_1, remRecDipolesbyE_C3_1);
    bestNext = bestOpts(randi(length(bestOpts)));
    bestNextC = clipped_electrodeInds(bestNext);

    currConfig = [currConfig, bestNextC];

    remRecDipolesbyE_C1_1(bestNext,:) = [];
    remRecDipolesbyE_C2_1(bestNext,:) = [];
    remRecDipolesbyE_C3_1(bestNext,:) = [];
    clipped_electrodeInds(bestNext) = [];
    
end

eOrder = currConfig;
% eAssocNOrder
eAssocNOrder = ROIEs(currConfig);

% get contact field and color by electrode IMPLANT number 
% note this is the same for all three thresholds
cToE_imp = double(full(cToE_imp));
% for each electrode in original full config, multiply all values by order of implantation, or 0 if no implantation
for i = 1:size(cToE_imp,1)
    if ~isempty(find(eAssocNOrder==i, 1))
        cToE_imp(i,:) = cToE_imp(i,:) * find(eAssocNOrder==i);
    else
        cToE_imp(i,:) = cToE_imp(i,:) * 0;
    end
end
% get contacts of implantation (has any positive value across electrodes)
cInds = find(sum(cToE_imp,1)>0);
config.node = scirunfield.node(cInds,:);
config.field = sum(cToE_imp(:,cInds),1);

% get recArea field
rec_tissueDat_imp_allThr = cell(6,1);
configConvergROIPercent_imp_allThr = cell(6,1);
for thrI = 1:6
    recAreaFieldOnly = zeros(length(currConfig),D); % zeros for all dipoles in cortex
    recAreaFieldOnly_sum = zeros(length(currConfig),D); % zeros for all dipoles in cortex
    % configPerc = zeros(length(eOrder),1); % percent ROI mapped at each electrode addition
    
    for i = 1:length(eOrder)
        e = eOrder(i);
        recAreaFieldOnly(i,:) = recDipolesbyEAll{round(thrI/2)}(e,:);
        if i == 1
            recAreaFieldOnly_sum(i,:) = recDipolesbyEAll{round(thrI/2)}(e,:);
        else
            recAreaFieldOnly_sum(i,:) = recAreaFieldOnly_sum(i-1,:) + recDipolesbyEAll{round(thrI/2)}(e,:);
        end
    end
    if mod(thrI,2) == 0
        recAreaFieldOnly_sum(recAreaFieldOnly_sum<=1)=0;
    end
    recAreaFieldOnly_log = recAreaFieldOnly_sum(:,ROI_dips);
    recAreaFieldOnly_log(recAreaFieldOnly_log>0) = 1;
    configPerc = sum(recAreaFieldOnly_log,2)/length(ROI_dips);
    % recInds(recInds<2) = 0; % min number of electrodes for recordable dipole is 2
    recAreaFieldOnly = recAreaFieldOnly(:,1:D);  % clip to correct size
    rec_tissueDat_imp_allThr{thrI} = recAreaFieldOnly;
    configConvergROIPercent_imp_allThr{thrI} = configPerc;
end

end

function bestOpts = getBestEs(recDipolesbyE_C1_1, recDipolesbyE_C2_1, recDipolesbyE_C3_1)
    % given the 6 recDbyE matrices, find the best electrode or electrodes
    % using a combined cost function.
    % The recDbyE matrices are E' by D' matrices of ones and zeros
    % indicating what electrodes can record discernible voltage from what
    % dipoles. 
    
    % single map integrated (we only care about single mapping, and use other thresholds to break ties)
    singleMapCost = 1;
    if singleMapCost == 1
        weights1s = sum(recDipolesbyE_C3_1,2);
        bestOpts = find(weights1s==max(weights1s));
        if length(bestOpts) > 1 || isempty(bestOpts)
            % restrict domain of second threshold rec Array to only best Es
            weights2s = sum(recDipolesbyE_C2_1(bestOpts,:),2);
            bestOpts = bestOpts(weights2s==max(weights2s));
            if length(bestOpts) > 1 || isempty(bestOpts)
                % restrict domain of second threshold rec Array to only best Es
                weights3s = sum(recDipolesbyE_C1_1(bestOpts,:),2);
                bestOpts = bestOpts(weights3s==max(weights3s));
            end
        end
        return
    end
    
end

function ROIEs = getValidImpEs(cToE_imp, scirunfield, ROI_locs)
% clip implanted electrodes to only those that are on average close to ROI

ROIEs = []; % inds of valid electrodes in implanted config
for eI = 1:size(cToE_imp,1)
    % get all distances of contacts to ROI
    cInds = find(cToE_imp(eI,:)>0);
    minDists = zeros(length(cInds),1);
    for i = 1:length(cInds)
        cLoc = scirunfield.node(cInds(i),:);
        minDists(i) = min(sqrt(sum((ROI_locs-cLoc).^2,2)));
    end
    % if average of closest distances is < 3mm 
    %   and at least 6 contacts are within 1 cm of the ROI, add it!
    if min(minDists) < 3 && sum(minDists<10) >= length(cInds)/2
       ROIEs = [ROIEs,eI]; 
    end
    
    % also get closest point to each electrode on skin
%     minDists = zeros(length(cInds),1);
%     minDistSkinInd = zeros(length(cInds),1);
%     for i = 1:length(cInds)
%         cLoc = scirunfield.node(cInds(i),:);
%         dists = sqrt(sum((skinPoints-cLoc).^2,2));
%         minDists(i) = min(dists);
%         minDistSkinInd(i) = find(dists==min(dists));
%     end
%     minSkinPoint = skinPoints(minDistSkinInd(minDists==min(minDists)),:);
%     % and then get dist of that skin point to ROI
%     skinInsDisttoROI = min(sqrt(sum((ROI_locs-minSkinPoint).^2,2)));
%     
%     if skinInsDisttoROI < 30 % if ins point < 30 mm from ROI, include
%         ROIEs = [ROIEs, eI];
%     end
        
end

% ROIEs
% % plot
% figure(12)
% clf
% hold on
% scatter3(ROI_locs(:,1),ROI_locs(:,2),ROI_locs(:,3))
% contInds = scirunfield.node(find(sum(cToE_imp(ROIEs,:),1)==1),:);
% scatter3(contInds(:,1),contInds(:,2),contInds(:,3),'filled')
% scatter3(scirunfield.node(:,1),scirunfield.node(:,2),scirunfield.node(:,3),'filled')
end

function [rec_tissue_by_E, configField] = getVisFieldsConfig(plotOptRecStr, solnCongigEInds, recDipolesbyE, normals_reInd, slocs_reInd, cortex_normals, sOrD)

% get recArea field
if plotOptRecStr
    recAreaFieldOnly = zeros(length(solnCongigEInds),size(recDipolesbyE,2)); % zeros for all dipoles (but careful cuz recDipolesbyE is sparse, so this might not be the full length!)
    for i = 1:length(solnCongigEInds)
        e = solnCongigEInds(i);
        recAreaFieldOnly(i,:) = recDipolesbyE(e,:);
    end
    rec_tissue_by_E.node = cortex_normals.node;
    % recInds(recInds<2) = 0; % min number of electrodes for recordable dipole is 2
    recInds = recAreaFieldOnly(:,1:length(cortex_normals.node));  % clip to correct size
    rec_tissue_by_E.field = recInds;
    % save(strcat(dataHomePath,'OptSolutions/rec_tissue.mat'),'rec_tissue')
else
   rec_tissue_by_E = []; 
end

% get contact locs field
contactsLs = cell(length(solnCongigEInds),1);
configField_field =  cell(length(solnCongigEInds),1);
for i = 1:length(solnCongigEInds)
    e = solnCongigEInds(i);
    contactsLs{i} = slocs_reInd(e,:) + [1:3.5:(3.5*(15+1)+1)]' .* normals_reInd(e,:);
    configField_field{i} = repmat(i,length(contactsLs{i}),1);
end
configField.node = cell2mat(contactsLs);
configField.field =  cell2mat(configField_field); 
% save(strcat(dataWorkPath,patientDir,'OptSolutions/contacts.mat'),'configField')

end

