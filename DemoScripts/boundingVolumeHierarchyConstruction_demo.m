function [BVH_surface_pointInds,BVH_boundingBoxes,childrenInds] = boundingVolumeHierarchyConstruction_demo(trisurf)
% precompute boundary volume hierarchy tree

% store tree nodes in array using level-order traversal.
% no not store empty nodes
% need to store matrix of children node indices for use of linear storage

%  store indices of points on surface within each node
BVH_surface_pointInds = cell(1,10);
% first node has all points in it
BVH_surface_pointInds{1} = 1:length(trisurf.Points);

% store bounding box for each node
% bounding boxes, since all are in standard basis, only need two points to define
BVH_boundingBoxes  = cell(1,10);

% get first bounding box
BVH_boundingBoxes{1} = [min(trisurf.Points(:,1)) min(trisurf.Points(:,2)) min(trisurf.Points(:,3)); max(trisurf.Points(:,1)) max(trisurf.Points(:,2)) max(trisurf.Points(:,3))];

% create the tree
% while any leaf node has more than one point inside of it, create two
% children nodes
i=1; % index of level-order node
heightOld = 0;
height=1;
numLeafs=0;
k = 1; % counter of total number of nodes we have, so we can always add to the end

childrenInds = zeros(10,2);
s  = 0;
% go until i == k i think
while true % use in-order traversal
    s = s+1;
    % break box i into two boxes
    % DO NOT store if either is null. 
    % thus, this is not a full binary tree and simple indexing will not
    % give us the positions of the children nodes 
    % we must save indices of l and r children nodes for every node
    
    % find longest dimension of parent  box and split in that direction
    % box of interest is box that was determined to have intersection 
    parent_box = BVH_boundingBoxes{i};
    
    isleaf = 0;
    
    if size(parent_box,1)>1 % node has more than one point inside it. 
    % if node has more than one point aka there exists a bounding box, add its two children nodes to the end, whatever index that is (k+1). and store that index 
    %disp(size(parent_box,1))
    box_dimensions = abs(parent_box(1,:)-parent_box(2,:));
    dim = find(box_dimensions==max(box_dimensions));
    dim = dim(1);
    midpoint = max(box_dimensions)/2+min(parent_box(1,dim),parent_box(2,dim));
    % split points that are in box of interest into two groups,and store
    % ~indices~
    parent_points = trisurf.Points(BVH_surface_pointInds{i},:);
    parent_inds = BVH_surface_pointInds{i};

    left_points = parent_inds(parent_points(:,dim)>midpoint);
    if ~isempty(left_points) % left child is not null
        BVH_surface_pointInds{k+1} = left_points;
        childrenInds(i,1) = k+1; % left child index
        
        if length(BVH_surface_pointInds{k+1})==1 % left child is a leaf node
          % put point in for bounding box for left child
            BVH_boundingBoxes{k+1} = trisurf.Points(BVH_surface_pointInds{k+1},:);
        else % left child is not a leaf node (and not null)
            % create bounding box for left child
            BVH_boundingBoxes{k+1}(1,:) = min(trisurf.Points(BVH_surface_pointInds{k+1},:));
            BVH_boundingBoxes{k+1}(2,:) = max(trisurf.Points(BVH_surface_pointInds{k+1},:));
        end
        % don't build in buffer distance here. build it in during traversal so
        % the buffer distance can be changed easily with the threshold. 
        % if node is leaf, put single point as bounding box. it will be built
        % into an actual box when buffer is added during traversal
        
        k = k+1;
        
    else % left child is null
        childrenInds(i,1) = nan; % left child index
    end
    
    
    right_points = parent_inds(parent_points(:,dim)<=midpoint);
    if ~isempty(right_points) % right child is not null
        BVH_surface_pointInds{k+1} = right_points;
        childrenInds(i,2) = k+1; % right child index
    
        if length(BVH_surface_pointInds{k+1})==1 % right child is a leaf node
            % put point in for bounding box for right child
            BVH_boundingBoxes{k+1} = trisurf.Points(BVH_surface_pointInds{k+1},:);
        else % right child is not a leaf node (and not null)
            % create bounding box for right child
            BVH_boundingBoxes{k+1}(1,:) = min(trisurf.Points(BVH_surface_pointInds{k+1},:));
            BVH_boundingBoxes{k+1}(2,:) = max(trisurf.Points(BVH_surface_pointInds{k+1},:));
        end
        % don't build in buffer distance here. build it in during traversal so
        % the buffer distance can be changed easily with the threshold. 
        % if node is leaf, put single point as bounding box. it will be built
        % into an actual box when buffer is added during traversal

            k = k+1;
    
    else % right child is null
        childrenInds(i,2) = nan; % right child index
    end
    

    
    elseif size(parent_box,1)==1 % parent node is a leaf node
        % don't add any nodes
        % store inds
        isleaf = 1;
        childrenInds(i,1) =  nan;       
        childrenInds(i,2) =  nan;                                                                           % the children of nodes with only one point are null, so all nodes with only one point are leaf nodes
    else % parent node is null
        % don't do anything
        %  we should never get here. 
        disp('ERROR. node is null:')
        disp(strcat('i=',num2str(i),'; k=',num2str(k)))
        break
    end

    % we know there has to be one single point in each node,   and that
    % each point has to be in a leaf node, so there has to  be the same
    % number of leaf nodes as the number of points in the surface
    numLeafs = numLeafs + isleaf;
    i = i+1;
    %disp(strcat(num2str(numLeafs),';',num2str(i),';',num2str(k)))
    
    if numLeafs==size(trisurf.Points,1)
        disp('BVH is complete!')
        break
    end
end

save('BVH_surface_pointInds_sulci.mat','BVH_surface_pointInds')
save('BVH_boundingBoxes_sulci.mat','BVH_boundingBoxes')
save('childrenInds_sulci.mat','childrenInds')

end

