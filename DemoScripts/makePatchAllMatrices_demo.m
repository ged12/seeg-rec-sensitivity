% create 20 PatchAll matrices, one per area

% DEMO VERSION:
%   only computes 1/100 of all patches

% ***************************************************************
% Grace Dessert
% 10/29/20 
% ***************************************************************

% make 20 sparse matrices of ones and zeros
% for every 40000 dipoles, create a patch of given area and store the
% included dipoles as ones in the that column

% 4/13/21
% make all patch all matrices at once. remove huge amount of redundancy
% we know that each patch all matrix of index i is a subset of that of
% index i+1. so we can just make it for i=100 and make the others along
% the way, but break these 100 jobs into 20 workers
% takes about 10 minutes for # dipoles = 40k

% 3/6/22
% make only patches with specified areas
% set parameters to be changed with tag search
% save in: load(strcat(dataWorkPath,patientDir,'/HeadModel/PatchAllMatrices/PatchAllSt_',num2str(patchArea),'cm2.mat'),'patchAll');

% 9/1/22 
% add validation test for # elements in HeadTetMesh

% $$ UPDATE PARAMETERS $$
patientID = 19;
dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';
dataHomePath = '/hpc/group/wmglab/ged12/cluster/Optimize_sEEG_implant/electrode-config-optimization-seeg/';
patchAreas = [6,10,20]; % cm^2

uniquePatchAreas = unique(patchAreas)*100; % mm^2
patientDir = ['Patient_' num2str(patientID)];

tic
% load HeadTetMesh_numElements.mat and read # elements in HTM to validate
filePathHTM = strcat(dataWorkPath,patientDir,"/DemoOut/HeadTetMesh_numElements.mat");
if ~isfile(filePathHTM)
    errMessage = strcat("ERROR: ",filePathHTM," NOT found!");
    disp(errMessage)
else
    load(filePathHTM,'scirunmatrix')
    if scirunmatrix > 19000000 && scirunmatrix < 22000000
        errMessage = strcat("The HeadTetMesh created has ",num2str(scirunmatrix)," elements. This is between 19M and 22M. Nice!");
        disp(errMessage)
    else
        errMessage = strcat("ERROR: The HeadTetMesh created has ",num2str(scirunmatrix)," elements. This is NOT between 19M and 22M.");
        disp(errMessage)
    end
end

% load cortex inputs
cortexSurf = stlread(strcat(dataHomePath,patientDir,'/Inputs/cortexSurf.stl'));
load(strcat(dataWorkPath,patientDir,'/DemoOut/cortex_areas.mat'),'cortex_areas');

total_num_patches = length(cortex_areas);
disp(strcat("Total num patches is ",num2str(total_num_patches)))

% for parallelization
numJobs = 100; 

% set up patch storage structure
patchAll_IndsForSparse = cell(10,1); % 10 workers
for job = 1:numJobs
    patchAll_IndsForSparse{job} = cell(length(uniquePatchAreas),1); % an Nx3 matrix of I and J inds of dipoles in patchAll matrix. Third column is all 1's. N is the number of face indicies in each patch.
    for areaI = 1:length(uniquePatchAreas)
        patchAll_IndsForSparse{job}{areaI} = [];
    end
end

% create all patches, all areas together
total_num_patches_each_job = [[0:numJobs-1]*ceil(total_num_patches / numJobs)+1;[1:numJobs]*ceil(total_num_patches / numJobs)];
total_num_patches_each_job(2,end) = total_num_patches;

for job = 1 %:numJobs
    dipoles_this_job = total_num_patches_each_job(:,job);
    for dind = dipoles_this_job(1):dipoles_this_job(2)
%         disp([ num2str(dind/total_num_patches * 100) ' percent done'])
        dipole = dind; % making a patch at every dipole so dipole number = patch number
        [patches,~] = make_patch_startface_allAreas(cortexSurf,dipole,uniquePatchAreas,cortex_areas); % standard indices
        % store each patch and patch area in larger storage
        % store as linear indices in patcAll matrix (linInd = sub2lin([total_num_patches,total_num_patches], dind, patch))
        for areaI = 1:length(uniquePatchAreas)
            patch =  patches{areaI};
            % don't need to save patches or patch areas. can easily reconstruct
            %patches_s{i}(dind,1:length(patches{i})) = patch;
            patchAll_IndsForSparse{job}{areaI} = [patchAll_IndsForSparse{job}{areaI};[repmat(dipole,length(patch),1),patch,ones(length(patch),1)]];
%             patchAll_IndsForSparse{job}{areaI}(size(patchAll_IndsForSparse{job}{areaI},1)+1:size(patchAll_IndsForSparse{job}{areaI},1)+length(patch),:) = [repmat(dipole,length(patch),1),patch,ones(length(patch),1)];
            %patch_areas_s{i}(dind) = total_areas(i);
        end
    end
end

disp('patch construction done')
parallel = toc
tic


% full patchAll matrix is about 11.9GB. need to request large storage.
% concat data from all workerrs
patchAll_IndsForSparse_cat = cell(length(uniquePatchAreas),1); % patch inds from all workers for each area index
for i = 1:length(uniquePatchAreas)
    patchAll_IndsForSparse_cat{i} = [];
    for job = 1:numJobs
        patchAll_IndsForSparse_cat{i} = [patchAll_IndsForSparse_cat{i};patchAll_IndsForSparse{job}{i}];
    end
    % construct sparse matrix 39954x39954 where each column gives indices of faces in patch
    % build sparse matrix with I, J inds and V values (1)
    patchAll = sparse(patchAll_IndsForSparse_cat{i}(:,1),patchAll_IndsForSparse_cat{i}(:,2),patchAll_IndsForSparse_cat{i}(:,3));
    save(strcat(dataWorkPath,patientDir,'/DemoOut/PatchAllSt_',num2str(uniquePatchAreas(i)/100),'cm2.mat'),'patchAll');
end

constructSparse = toc

disp(['Creating all patches for all areas took ' num2str(constructSparse) ' seconds to run'])

%%
function [patches,total_areas] = make_patch_startface_allAreas(cortexSurf,sface,ainput,cortex_areas) %,LH_white, RH_white)
% 07/21/20
% ********************************
% make_patch_startface_allAreas(cortexSurf,sface,ainput,cortex_areas)
% returns an array containing the indices of faces in the cortical surface triangulation 
% and the total areas of this patch in square millimeters. 
% makes length(ainput) number patches at the same time.
% inputs: cortex surface triangulation (either or both hemispheres)
%         sface - face index number (index of face in triangular cortex surface)
%         ainput - array of target areas (minima) for patch (in mm^2)
%         cortex_areas - areas of faces in cortex surface triangulation
% ********************************
%
% if side == 0 % sface input in standard indices! use number of sface to find side
%     if sface > 19967
%         side = 2;
%         sface = sface - 19967;
%     else
%         side = 1;
%     end
% end
% if local_or_cluster == 2
%     rv = stlread('/hpc/group/wmglab/ged12/cluster/rh_white_20k.stl');
%     lv = stlread('/hpc/group/wmglab/ged12/cluster/lh_white_20k.stl');
%     load('/hpc/group/wmglab/ged12/cluster/lh_white_20k_areas.mat','area');
%     areasL = area;
%     load('/hpc/group/wmglab/ged12/cluster/rh_white_20k_areas.mat','area');
%     areasR = area;
%     area = [[areasL;zeros(87-67,1)],areasR];
% else
%     rv = stlread('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/rh_white_20k.stl');
%     lv = stlread('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/lh_white_20k.stl');
%     load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/lh_white_20k_areas.mat','area');
%     areasL = area;
%     load('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/rh_white_20k_areas.mat','area');
%     areasR = area;
%     area = [[areasL;zeros(87-67,1)],areasR];
% end
% if side == 1
% elseif side ==2
%     h.faces = rv.ConnectivityList;
%     h.vertices = rv.Points;
% end

h.faces = cortexSurf.ConnectivityList;
h.vertices = cortexSurf.Points;

outer_vertices = [];
total_areas = zeros(length(ainput),1);
total_area = cortex_areas(sface);
patches = cell(length(ainput),1);

patchS = [sface];
for y = 1:3
    outer_vertices = [outer_vertices,h.faces(sface,y)];
end

% iteratively expand patch by outervertices (one vertex at a time)
start = 1;
iPatch = 1; % 1 to length(ainput)
while total_area < max(ainput)
     disconnected = 0;
     for v = outer_vertices
%         disp(strcat("outer vertices has ", num2str(length(outer_vertices)), " elements"))
        [ia,~] = find(h.faces==v);
        ia = unique(ia);
        ia(ismember(ia,patchS)==1)=[];
        if total_area >= max(ainput)
            break
        end
        if ~isempty(ia) 
            for f = transpose(ia)
                patchS = [patchS;f];
                total_area = total_area + cortex_areas(f);
                for coord = 1:3
                    if ismember(h.faces(f,coord),outer_vertices) == 0
                        outer_vertices = [outer_vertices,h.faces(f,coord)];
                        start = 0;
                    end
                end
                if total_area >= ainput(iPatch)
                    % if this area is greater than or equal to our current goal,
                    % save data and continue
                    patches{iPatch} = patchS;
                    total_areas(iPatch) = total_area;
                    % plot patch!
%                     patchfield = triangulation(cortexSurf.ConnectivityList(patchS,:),cortexSurf.Points);
%                     patch(patchfield)
                    
                    %figure(1)
                    %clf
                    %quiver3(CDN_p(:,1),CDN_p(:,2),CDN_p(:,3),CDN_n(:,1),CDN_n(:,2),CDN_n(:,3),'LineWidth',1);
                    %hold on
                    %trimesh(FV)
                    

                    % make triangulation!
                    Points = [];
                    ConnectivityList = zeros(length(patchS),3);
                    point_index = 1;
                    for x = 1:length(patchS) % x = patch number
                        for c = 1:3
                            % h.faces(patch(x),c) = vertex coord index (in h.vertices)
                            a = h.vertices(h.faces(patchS(x),c),:); % = new vertex coords
                            Points = [Points;a];
                            ConnectivityList(x,c) = point_index;
                            point_index = point_index + 1;
                        end
                    end
%                     patchfield = triangulation(ConnectivityList,Points);    

                    % plot to test. 
%                     figure(1)
%                     clf
%                     hold on
%                     trimesh(LH_white,'FaceAlpha',0)
%                     trimesh(RH_white,'FaceAlpha',0)
%                     trimesh(patchfield, 'FaceColor', 'yellow')
    
                    iPatch = iPatch + 1;
                    if total_area >= max(ainput)
                        break
                    end
                end
            end 
        else
            disconnected = disconnected + 1;
        end
        outer_vertices(outer_vertices==v)=[];
     end
     if disconnected == 3 && start == 1
         disconnected = "dipole is disconnected! no patch can be made."
         break
     end
end

end



