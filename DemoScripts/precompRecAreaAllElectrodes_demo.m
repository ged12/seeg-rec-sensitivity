% get recordable dipoles for all valid electrodes using 
%   lead field matrix with data at contact locations
%   (no interpolation needed)
% 10/12/21

% 1/19/22 for each parameter case (ty), build three recDipolebyE matrixes
%   for each of the three voltage thresholds (100, 500, 1000 µV)
% in the future, if we are only running this for one parameter case,
%   restructure so input int is the threshold case and we have an
%   embarassingly parallel structure so 3 thresholds is same time as 1. 

% inputCase = 1:(length(cdmds)*3) % 3*number of source types. 
function precompRecAreaAllElectrodes_demo(inputCase)

sourceNum = ceil((inputCase)/3);
cNum = mod(inputCase-1,3)+1;

% $$ UPDATE PARAMETERS $$
cdmds = [1, 1, 0.2, 0.2]; %nAm/mm^2
patchAreas = [5, 9.5, 5, 9.5]; % cm^2
dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';
dataHomePath = '';
patientID = 1; 

patientDir = ['Patient_' num2str(patientID)];

cdmd = cdmds(sourceNum) % nAm/mm^2
patchArea = patchAreas(sourceNum) % cm^2

discVoltThreshs = [100, 500, 1000]; % µV
threshold = discVoltThreshs(cNum) % µV

maxInt = 2^15-1; % LF to be stored at 16bit initegers

maxVolts = [150, 750, 1500]; % µV
maxVolt = maxVolts(cNum) % µV

cNumStrings = ["C1","C2","C3"];
cNumStr = cNumStrings(cNum)

tys = strcat(num2str(patchArea),"cm2_",num2str(cdmd),"cdmd")


tic
% import thresholded LF with voltages at contact locs
%   CxD where C is #contacts and D is #dipoles
%   394201 x 39965 logical matrix. ~15GB
%   C x D logical matrix. ~15GB

disp(['Loading LF matrix with threshold ' num2str(threshold)])
load(strcat(dataWorkPath,patientDir,'/RawData/DemoInputs/PatchLeadField_RecLocs',cNumStr,'_thr_',tys,'.mat'),'leadFieldThr') % leadFieldThr
disp('Lead Field matrix load is done')
info = whos('leadFieldThr');
sizeGB = info.bytes/(1000^3);
disp(['Lead Field matrix is ' num2str(sizeGB) ' GB'])
x = toc;
disp(['This took ' num2str(x/60) ' minutes'])
disp(num2str(size(leadFieldThr)))
disp(num2str(sum(sum(leadFieldThr))))

% import contact-to-electrode association matrix
load(strcat(dataWorkPath,patientDir,'/DemoOut/contactLocsIndsforLinElectrodes.mat'),'contactLocsIndsforLinElectrodes')
%load([dataWorkPath patientDir '/DemoOut/electrode_contactInds.mat'],'electrode_contactInds') 
disp('ContactLocsIndsforLinElectrodes load is done')

info = whos('contactLocsIndsforLinElectrodes');
sizeGB = info.bytes/(1000^3);  % if not sparse: 83596*394201/(1024^3)
disp(['contactLocsIndsforLinElectrodes is ' num2str(sizeGB) ' GB'])

numContactsForSearch = max(max(contactLocsIndsforLinElectrodes))

% make rec dipoles by E (an E by D matrix giving which electrodes can
% record from which electrodes with at least two contacts)
recDipolesbyE = zeros(size(contactLocsIndsforLinElectrodes,2),size(leadFieldThr,2),'logical');
info = whos('recDipolesbyE');
sizeGB = info.bytes/(1000^3);
disp(['RecDipolesbyE before filling is ' num2str(sizeGB) ' GB'])
% for every electrode, get relevant contacts from contactLocsIndsforLinElectrodes
E = size(contactLocsIndsforLinElectrodes,2)
numContactsForSearch = max(max(contactLocsIndsforLinElectrodes))
for i = 1:E
    contactsThis = contactLocsIndsforLinElectrodes(1,i):contactLocsIndsforLinElectrodes(2,i);
    recDipolesbyE(i,:) = sum(leadFieldThr(contactsThis,:),1) >= 2;
end


leadFieldThr_other = leadFieldThr(numContactsForSearch+1:end,:);
leadFieldThr = [];

% threshold recordable dipole (by electrodes) matrix by 2
%   We say that a given electrode can record from a given dipole if at
%   least two contacts at that electrode can record from that dipole.
recDipolesbyE = logical(recDipolesbyE>=2);
disp(num2str(size(recDipolesbyE)))
disp(num2str(sum(sum(recDipolesbyE))))

% save recordable dipole matrix
save(strcat(dataWorkPath,patientDir,'/DemoOut/recDipolesbyE_recLocs',cNumStr,'_',tys,'.mat'),'recDipolesbyE','-v7.3')
recDipolesbyE = [];  % clear up memory

% do the same for implanted locs 
implantedELocations = load(strcat(dataHomePath,patientDir,'/Inputs/ContactLocations.mat'),'scirunfield');
implantedELocations = implantedELocations.scirunfield.node;
cToE_imp = getImplantedElectrodeAssociation(implantedELocations, dataHomePath, patientDir);
disp(strcat("For index ",num2str(inputCase)," cToE_imp has size ",num2str(size(cToE_imp))," and leadFieldThr has size ",num2str(size(leadFieldThr))))
disp(strcat("Clipping LF_imp to ",num2str(length(implantedELocations))," contacts"))
leadFieldThrImp = leadFieldThr_other(1:length(implantedELocations),:);
recDipolesbyE_implanted = cToE_imp * leadFieldThrImp;
recDipolesbyE_implanted = logical(recDipolesbyE_implanted>=2);
leadFieldThrImp = [];

% save LF for rec radius contacts of interest
cIndsRecRadius = length(implantedELocations)+1:size(leadFieldThr_other,1);
disp(strcat("Clipping LF_recRadius to ",num2str(length(cIndsRecRadius))," contacts"))
leadFieldThr = leadFieldThr_other(cIndsRecRadius,:);
save(strcat(dataWorkPath,patientDir,'/DemoOut/recDipolesbyC_recRadius',cNumStr,'_',tys,'.mat'),'leadFieldThr','-v7.3')

% save recordable dipole matrix
save(strcat(dataWorkPath,patientDir,'/DemoOut/cToE_imp',cNumStr,'_',tys,'.mat'),'cToE_imp')
save(strcat(dataWorkPath,patientDir,'/DemoOut/recDipolesbyE_implantedLocs',cNumStr,'_',tys,'.mat'),'recDipolesbyE_implanted','-v7.3')


% optimization algorithm will take linear combinatios of the ExD matrix to
% get a 1xD matrix for each electrode configuration, which will be
% converted into a metric of recordable area and the cost function

% end

end

function cToE_imp = getImplantedElectrodeAssociation(implantedELocations, dataHomePath, patientDir)
    % get electrode-to-contact association matrix
    % by clustering contact locations. Assume all contacts for one
    % electrode are adjacent in the implantedELocations matrix.
    % also make visualization with different colors to check.
    % DO NOT offset all of these contact numbering and electrode numbering to
    % after the generated electrodes and contacts. Use this only with the
    % clipped lead field, with data only corresponding to these contacts
    
    if size(implantedELocations,1) ~= 3
        implantedELocations = implantedELocations';
    end
    dists = sum(diff(implantedELocations,1,2).^2,1);
    numEs = 0;
    eStartAndEnds = [];
    currCDist = dists(1);
    currCStart = 1;
    for i = 1:length(dists)
        if abs(dists(i) - currCDist) > 20
            numEs = numEs + 1;
            eStartAndEnds = [eStartAndEnds;[currCStart,i]];
            currCStart = i+1;
            currCDist = dists(i+1);
        end
    end
    numEs = numEs + 1;
    eStartAndEnds = [eStartAndEnds;[currCStart,i+1]];
    
    % get cToE_imp logical matrix
    E = numEs;
    % C = size(leadFieldThr,1);
    %cToE = zeros(E,C,'logical');
    is = [];
    js = [];
    for i = 1:E
        is = [is;i*ones(eStartAndEnds(i,2)-eStartAndEnds(i,1)+1,1)]; % electrode indices
        js = [js;[eStartAndEnds(i,1):eStartAndEnds(i,2)]']; % contact indices
    end
    vs = true(length(js),1);
    size(is)
    size(js)
    size(vs)

    cToE_imp = sparse(double(is),double(js),vs);
    
    
    % plot contact locs and save
    f = figure('visible','off');
    clf
    hold on
    % plot each set of contacts in another color
    for i = 1:E
        scatter3(implantedELocations(1,eStartAndEnds(i,1):eStartAndEnds(i,2)),implantedELocations(2,eStartAndEnds(i,1):eStartAndEnds(i,2)),implantedELocations(3,eStartAndEnds(i,1):eStartAndEnds(i,2)),'filled','DisplayName',strcat("Electrode ",num2str(i)))
    end
    legend()
    title(strcat("Implanted Recording Locations with Electrode Clustering "),"FontSize",14)
    saveas(f,strcat(dataHomePath,patientDir,'/DemoOut/implantedLocsWithEClusters'),'jpg');
    saveas(f,strcat(dataHomePath,patientDir,'/DemoOut/implantedLocsWithEClusters'),'fig');
    % openfig('Patient_7/DemoOut/inputSurfaces.fig','new','visible')
   
end
