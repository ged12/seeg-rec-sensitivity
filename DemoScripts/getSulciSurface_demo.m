% getSulci Surface
% 10/28/21

function [sulciPoints, LH_smoothSurfSulciTri, RH_smoothSurfSulciTri] = getSulciSurface_demo(lh, rh, lhSmooth, rhSmooth, midlinePlane, dataHomePath, dataWorkPath, patientDir)


% Get a surface of the sulci for both hemispheres by smoothing each cortex
%   surface a lot, shrinking slightly and translating towards the midline slightly, 
%   taking the intersection (points), removing midline points, and converting
%   to a surface


% do this beforehand with pymeshlab !
% smooth them A LOT and save different 
% if exist('ContentScripts','dir')
%     addpath('ContentScripts')
% end
% V = lh.Points;
% for i=1:4
%     tic
%     V = SurfaceSmooth(V, lh.ConnectivityList, [], [], 0.1, 100, 2, true);
%     V = SurfaceSmooth(V, lh.ConnectivityList, [], [], 0.1, 10, 2, true);
%     toc
% end
% 
% %
% VR = rh.Points;
% for i=1:4
%     tic
%     VR = SurfaceSmooth(VR, rh.ConnectivityList, [], [], 0.1, 100, 2, true);
%     VR = SurfaceSmooth(VR, rh.ConnectivityList, [], [], 0.1, 10, 2, true);
%     toc
% end

%
% RH
% get centroid of cortex
pointsR = rhSmooth.Points;
centroid = [mean(pointsR(:,1)), mean(pointsR(:,2)), mean(pointsR(:,3))];
% scale down but keep center in same spot
pointsT = pointsR - centroid;
pointsTS = pointsT * 0.95;
pointsTST = pointsTS + centroid;
% transform toward midline (along normal)
normalVec = midlinePlane(:,1)';
dir = sign(dot(centroid,normalVec));
pointsTST = pointsTST + -1*dir*8*normalVec;
outR = triangulation(rhSmooth.ConnectivityList, pointsTST);

% LH
% get centroid of cortex
points = lhSmooth.Points;
centroid = [mean(points(:,1)), mean(points(:,2)), mean(points(:,3))];
% scale down but keep center in same spot
pointsT = points - centroid;
pointsTS = pointsT * 0.95;
pointsTST = pointsTS + centroid;
% transform toward midline (along normal)
normalVec = midlinePlane(:,1)';
dir = sign(dot(centroid,normalVec));
pointsTST = pointsTST + -1*dir*8*normalVec;
out = triangulation(lhSmooth.ConnectivityList, pointsTST);

% save smoothed cortex surfaces
stlwrite(out,strcat(dataHomePath,patientDir,'/DemoOut/SmoothLH_forSulciSurface.stl'))
stlwrite(outR,strcat(dataHomePath,patientDir,'/DemoOut/SmoothRH_forSulciSurface.stl'))


%{
% use new smooth method in /smoothpatch
% addpath('smoothpatch')
% 
% mex smoothpatch_curvature_double.c -v
% mex smoothpatch_inversedistance_double.c -v
% mex vertex_neighbours_double.c -v
% 
% % 1) smooth sulci surfaces
% smoothLH.faces = lh.ConnectivityList;
% smoothLH.vertices = lh.Points;
% smoothRH.faces = rh.ConnectivityList;
% smoothRH.vertices = rh.Points;
% for i=1:3
%     smoothLH = smoothpatch(smoothLH,1,1,1,1);
% %     smoothRH = SurfaceSmooth(smoothRH,1,1,1,1);
% %     FV2=smoothpatch(FV,mode,itt,lambda,sigma);
% end
% 
% stlwrite(smoothLH,'SulciSurface_smoothLH.stl')
%}

%
% 2) get points of original surfaces that are inside smoothed surface 
%
% refine original cortex to get about 86k points after get intersection
if exist('refinepatch_version2b','dir')
    addpath('refinepatch_version2b')
end
if exist('DemoScripts','dir')
    addpath('DemoScripts')
    addpath('DemoScripts/refinepatch_version2b')
end
cortexIn.faces = lh.ConnectivityList;
cortexIn.vertices = lh.Points;
cortexRef = refinepatch(cortexIn);
cortexRef = refinepatch(cortexRef);
cortexRefPlot = triangulation(cortexRef.faces, cortexRef.vertices);

cortexInR.faces = rh.ConnectivityList;
cortexInR.vertices = rh.Points;
cortexRefR = refinepatch(cortexInR);
cortexRefR = refinepatch(cortexRefR);
%
% get intersection
testpoints = cortexRef.vertices;
insideInds = intriangulation(out.Points, out.ConnectivityList, testpoints);
pointsInside = testpoints(insideInds,:);

testpointsR = cortexRefR.vertices;
insideIndsR = intriangulation(outR.Points, outR.ConnectivityList, testpointsR);
pointsInsideR = testpointsR(insideIndsR,:);

%
% clip out midline points (anything within 2 cm of midline)
% get distances to midline along normal
normal = midlinePlane(:,1);
pointOnPlane = midlinePlane(:,2); %+[3;0;68]; *** remove correction here!**
midlineDistances = sum((pointsInside-pointOnPlane').*normal',2);
pointsInInd = abs(midlineDistances)<20;
pointsInside(pointsInInd,:) = [];
insideInds = find(insideInds==1);
insideInds(pointsInInd) = [];

midlineDistancesR = sum((pointsInsideR-pointOnPlane').*normal',2);
pointsInIndR = abs(midlineDistancesR)<20;
pointsInsideR(pointsInIndR,:) = [];
insideIndsR = find(insideIndsR==1);
insideIndsR(pointsInIndR) = [];
%
% get corresponding surface. Include all faces that reference points
% inside the surface. Reindex points

ix = max(ismember(cortexRef.faces,insideInds),[],2);
LH_smoothSurfSulci.ConnectivityList = cortexRef.faces(ix,:);
pointsInclude = unique(reshape(LH_smoothSurfSulci.ConnectivityList,[],1));
LH_smoothSurfSulci.Points = cortexRef.vertices(pointsInclude,:);
for i = 1:numel(LH_smoothSurfSulci.ConnectivityList)
    LH_smoothSurfSulci.ConnectivityList(i) = find(pointsInclude==LH_smoothSurfSulci.ConnectivityList(i));
end
LH_smoothSurfSulciTri = triangulation(LH_smoothSurfSulci.ConnectivityList, LH_smoothSurfSulci.Points);

ix = max(ismember(cortexRefR.faces,insideIndsR),[],2);
RH_smoothSurfSulci.ConnectivityList = cortexRefR.faces(ix,:);
pointsInclude = unique(reshape(RH_smoothSurfSulci.ConnectivityList,[],1));
RH_smoothSurfSulci.Points = cortexRefR.vertices(pointsInclude,:);
for i = 1:numel(RH_smoothSurfSulci.ConnectivityList)
    RH_smoothSurfSulci.ConnectivityList(i) = find(pointsInclude==RH_smoothSurfSulci.ConnectivityList(i));
end
RH_smoothSurfSulciTri = triangulation(RH_smoothSurfSulci.ConnectivityList, RH_smoothSurfSulci.Points);
%

% 4) combine LH and RH points and save
% combine LH and RH
sulciPoints = [pointsInside;pointsInsideR];
disp(strcat("There are ",num2str(length(sulciPoints))," points in the sulci field!"));

% temporary location. save sulciPoints and sulciSurfs. 
% sulciPointsVis.node = sulciPoints;
% sulciPointsVis.field = zeros(length(sulciPoints),1);
% save(strcat(dataWorkPath,patientDir,'/HeadModel/SulciSurface_refined_field.mat'), 'sulciPointsVis')
% save(strcat(dataWorkPath,patientDir,'/HeadModel/SulciSurface_refined.mat'),'sulciPoints');
% stlwrite(LH_smoothSurfSulciTri,strcat(dataWorkPath,patientDir,'/HeadModel/LH_smoothSurfSulciTri.stl'));
% stlwrite(RH_smoothSurfSulciTri,strcat(dataWorkPath,patientDir,'/HeadModel/RH_smoothSurfSulciTri.stl'));
%

% plot to check and save plots
LH_sulciPlot = figure("visible","off");

clf
hold on
trimesh(lh,'FaceAlpha',0.1,'FaceColor',[0.1 0.1 0.1],'EdgeColor',[0 0.1 0],'EdgeAlpha',0.0,'DisplayName','LH')
trimesh(out,'FaceAlpha',0.1,'FaceColor',[1,0.5,0.5],'EdgeColor',[1,0.5,0.5],'EdgeAlpha',0.2,'DisplayName','Smoothed LH')
scatter3(pointsInside(:,1),pointsInside(:,2),pointsInside(:,3),'.','DisplayName','Sulci Points')
trimesh(LH_smoothSurfSulciTri,'FaceAlpha',0.9,'FaceColor',[0 0 0.4],'EdgeColor',[0 0, 0.4],'EdgeAlpha',0.1,'DisplayName','Sulci Surface')
legend()
title("Sulci field generation with super-smoothed cortex surface (LH)","FontSize",14)

%
saveas(LH_sulciPlot,strcat(dataHomePath,patientDir, '/DemoOut/LH_sulciPlot'),'jpg');
saveas(LH_sulciPlot,strcat(dataHomePath,patientDir, '/DemoOut/LH_sulciPlot'),'fig');
% remember when opening, you must turn visibility to 'on'
% ie: openfig('midlinePlot.fig','new','visible')

RH_sulciPlot = figure("visible","off");
clf
hold on
trimesh(rh,'FaceAlpha',0.1,'FaceColor',[0.1,0.1,0.1],'EdgeColor',[0,0.1,0],'EdgeAlpha',0.0,'DisplayName','RH')
trimesh(outR,'FaceAlpha',0.1,'FaceColor',[1,0.5,0.5],'EdgeColor',[1,0.5,0.5],'EdgeAlpha',0.2,'DisplayName','Smoothed RH')
scatter3(pointsInsideR(:,1),pointsInsideR(:,2),pointsInsideR(:,3),'DisplayName','Sulci Points')
trimesh(RH_smoothSurfSulciTri,'FaceAlpha',0.9,'FaceColor',[0 0 0.4],'EdgeColor',[0 0, 0.4],'EdgeAlpha',0.1,'DisplayName','Sulci Surface')
legend()
title("Sulci field generation with super-smoothed cortex surface (RH)","FontSize",14)
saveas(RH_sulciPlot,strcat(dataHomePath,patientDir, '/DemoOut/RH_sulciPlot'),'jpg');
saveas(RH_sulciPlot,strcat(dataHomePath,patientDir, '/DemoOut/RH_sulciPlot'),'fig');
% remember when opening, you must turn visibility to 'on'
% ie: openfig('midlinePlot.fig','new','visible'

% Also plot just sulci surface with sulci points
sulciSurfPlot = figure("visible","off");

clf
hold on
scatter3(pointsInside(:,1),pointsInside(:,2),pointsInside(:,3),'DisplayName','LH Points')
scatter3(pointsInsideR(:,1),pointsInsideR(:,2),pointsInsideR(:,3),'DisplayName','RH Points')
trimesh(LH_smoothSurfSulciTri,'FaceAlpha',0.9,'FaceColor',[0 0 0.4],'EdgeColor',[0 0, 0.4],'EdgeAlpha',0.3,'DisplayName','LH Surface')
trimesh(RH_smoothSurfSulciTri,'FaceAlpha',0.9,'FaceColor',[0 0 0.4],'EdgeColor',[0 0, 0.4],'EdgeAlpha',0.3,'DisplayName','RH Surface')
legend()
title("Sulci points and surfaces generated from them","FontSize",14)
saveas(sulciSurfPlot,strcat(dataHomePath,patientDir, '/DemoOut/sulciSurfPlot'),'jpg');
saveas(sulciSurfPlot,strcat(dataHomePath,patientDir, '/DemoOut/sulciSurfPlot'),'fig');


end
