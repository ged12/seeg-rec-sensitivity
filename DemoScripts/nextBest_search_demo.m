% Next-best search algorithm for electrode configuration optimization
% DEMO VERSION
% Grace Dessert

% 1/20/22
% Add two other recDipolesbyE matrices to prioritize electrodes
%   that map dipoles with higher thresholds. 
%   Use tiered combined cost so we only look at higher threshold info 
%       if there are many electrodes with the same # lower threshold
%       new mappings. 
% And, weight mapping dipoles one time higher than mapping them a second
% time. Use integrated combined cost so it is possible to add electrodes
% with no new mappings and only double up current mappings but that new
% mappings are preferenced 2x. 

% 1/25/22
% For tiered combination cost, pick electrodes with max profit at highest
% threshold level first. If tied or none, go to mid threshold, and if still
% ties or none, go to lowest threshold. 


function solConfigs = nextBest_search_demo(case_id, recDipolesbyE_C1, recDipolesbyE_C2, recDipolesbyE_C3, ROI_dips, dataWorkPath, seed, patientDir)

% the order of cost function priority is C3, then C2, then C1.
% this is confusing i know. It was originally done this way because 1000 µV
% was always prioritized first, then 500 µV, then 200 µV, so the naming
% followed increasing voltage order, while the priority order was opposite.
% 

% test locally
test_local = 0;
if test_local
    disp("TESTING LOCALLY!")
    parallel = 0;
    rng(seed)
    recDipolesbyE_C1 = randi(20,83,40)>17;
    recDipolesbyE_C2 = randi(20,83,40)>18;
    recDipolesbyE_C3 = randi(20,83,40)>19;
    ROI_dips = [1:10];
    case_id = "test";
end


% clip recDipolesbyE matrices to ROI dipoles and only helpful electrodes
recDipolesbyE_cell = cell(3,1);
recDipolesbyE_cell{1} = recDipolesbyE_C1;
recDipolesbyE_cell{2} = recDipolesbyE_C2;
recDipolesbyE_cell{3} = recDipolesbyE_C3;

% clip dipoles to only those in the ROI
for c = 1:3
    disp(strcat('Clipping recDipolesbyE w c= ',num2str(c)))
    recDipolesbyE_cell{c} = recDipolesbyE_cell{c}(:,ROI_dips);
end

% remove all electrodes that don't improve the priority cost function (C3)
% get non-zero row indices: electrode indices that are included
recDipolesbyE = recDipolesbyE_cell{3};
esRecAny = find(any(recDipolesbyE,2)==1); % this gets indices of rows of recDbyE matrix that are non-zero
recDipolesbyE = recDipolesbyE(esRecAny,:);
recDipolesbyE_cell{3} = recDipolesbyE;
recDipolesbyE_cell{2} = recDipolesbyE_cell{2}(esRecAny,:);
recDipolesbyE_cell{1} = recDipolesbyE_cell{1}(esRecAny,:);
E = size(recDipolesbyE,1);
N = size(recDipolesbyE,2);
size(recDipolesbyE)
info = whos('recDipolesbyE');
sizeGB = info.bytes/(1024^3);
disp(strcat('Clipped electrode-dipole recordability matrix is ',num2str(sizeGB),' GB'))

dipolesImpossible = find(sum(recDipolesbyE,1)<2);
if isempty(dipolesImpossible)
    disp(strcat(num2str(length(dipolesImpossible)),' dipoles are impossible to record from at priority threshold!'))
end

% copy all 3 rec matrices to track single and double mappings
recDipolesbyE_C1_1 = recDipolesbyE_cell{1};
recDipolesbyE_C2_1 = recDipolesbyE_cell{2};
recDipolesbyE_C3_1 = recDipolesbyE_cell{3};
recDipolesbyE_C1_2 = recDipolesbyE_cell{1};
recDipolesbyE_C2_2 = recDipolesbyE_cell{2};
recDipolesbyE_C3_2 = recDipolesbyE_cell{3};

% open electrode intersection files
eColls = []; % ExE logical sparse matrix (E=83596 11/11/21)
if test_local
    rng(seed)
    eColls = double(randi(19,83,83)>18);
    size(eColls)
    info = whos('eColls');
    sizeGB = info.bytes/(1024^3);
    disp(strcat('eColls matrix is ',num2str(sizeGB),' GB'))
else
    disp('Opening sparse slices')
    filename = strcat(dataWorkPath,'RawData/ElectrodeCollisionMatrices/electrodeCollisions_sparseSlice_job1.mat');
    load(strcat(dataWorkPath,'RawData/ElectrodeLocsData/normals_reInd_full.mat'),'normals_reInd')
    load(strcat(dataWorkPath,'RawData/ElectrodeLocsData/slocs_reInd_full.mat'),'slocs_reInd')
        
    if isfile(filename)
        for i = 1:3 % DEMO: ONLY USE FIRST 3 COLL MATS FOR FASTER RUNNING
            filename = strcat(dataWorkPath,'RawData/ElectrodeCollisionMatrices/electrodeCollisions_sparseSlice_job',num2str(i),'.mat');
            load(filename,'sparse_slice')
            disp(i)
            disp(size(sparse_slice,1))
            eColls = [eColls;sparse_slice];
        end
        size(eColls)
        info = whos('eColls');
        sizeGB = info.bytes/(1024^3);
        % DEMO: fix size of eColls matrix 
        eColls(length(normals_reInd)+1,length(normals_reInd)+1) = 0;
    else
        disp(strcat("Collision matrix does not exist!",filename))
        disp("Computing manual electrode collisions")
        eColls = [];
    end
end

% given 3 recDipolesbyE matrices (ExD where D is # dipoles in ROI and E is the
%   number of electrodes that can record from any dipole in ROI)
%   Where the values are logicals of if each electrode can record (≥ 2
%   contacts) from each dipole
% iteratively add the next electrode that will minimize the cost function
%   the most, and does not have any collisions with current electrodes

% start one trial
% set seed
solConfigs = cell(20,4); % store solns (full or partial) for 10 seeds. 2nd column stores # dipoles not fully mapped (if it is 0, this is a full solution). 3rd column stores stopping criteria (success, no more valid electrodes, or too many electrodes). 4th column stores time of trial.
numTrials = 0; % setting numTrials to 0 means 1 trial

while numTrials < 1
    tic
    
    remDipolesTrack = [];
    timesTrack = [];
    seed = seed + 1;
    numTrials = numTrials + 1;
    rng(seed)
    disp(strcat('Starting trial ',num2str(numTrials),', using seed ',num2str(seed)))
    
    eIntersects = [];
    soln = 0;
    fail = 0;
    
    % get first best electrode choices 
    bestFirstOptions = getBestEs(recDipolesbyE_C1_1, recDipolesbyE_C1_2, recDipolesbyE_C2_1, recDipolesbyE_C2_2, recDipolesbyE_C3_1, recDipolesbyE_C3_2);

    % bestFirstOptions = find(sum(recDipolesbyE,2)==max(sum(recDipolesbyE,2)));
    disp(strcat("The best options are: ",num2str(bestFirstOptions)))
    bestFirst = bestFirstOptions(randi(length(bestFirstOptions)));
    clipped_electrodeInds = [1:length(esRecAny)];
    disp(strcat("The best options IN ST BASIS are: ",num2str(clipped_electrodeInds(bestFirstOptions))))
    disp(strcat("clipped_electrodeInds array is of size ",num2str(length(esRecAny))))
    disp(strcat("Adding electrode bestNext: ",num2str(bestFirst),", bestNextC:",num2str(bestFirst),"to the configuration"))
    currConfig = clipped_electrodeInds(bestFirst)
        
    while true  % while cost fn is not 0

        % if # electrodes is > 30, break!
        if length(currConfig) > 30
            disp(strcat('More than 30 electrodes. Quitting trial ',num2str(numTrials),'!'))
            break
        end
        
        % remove columns from recDipolesbyE of dipoles already recordable
        % double mapped for "_2" matrices and single mapped for "_1" matrices
        % each of 6 matrices is treated separately here
        recDipoles = logical(sum(recDipolesbyE_C1_1(currConfig,:),1)>=1);
        remRecDipolesbyE_C1_1 = recDipolesbyE_C1_1;
        remRecDipolesbyE_C1_1(:,recDipoles) = [];
        
        recDipoles = logical(sum(recDipolesbyE_C1_2(currConfig,:),1)>=2);
        remRecDipolesbyE_C1_2 = recDipolesbyE_C1_2;
        remRecDipolesbyE_C1_2(:,recDipoles) = [];
        
        recDipoles = logical(sum(recDipolesbyE_C2_1(currConfig,:),1)>=1);
        remRecDipolesbyE_C2_1 = recDipolesbyE_C2_1;
        remRecDipolesbyE_C2_1(:,recDipoles) = [];
        
        recDipoles = logical(sum(recDipolesbyE_C2_2(currConfig,:),1)>=2);
        remRecDipolesbyE_C2_2 = recDipolesbyE_C2_2;
        remRecDipolesbyE_C2_2(:,recDipoles) = [];
        
        recDipoles = logical(sum(recDipolesbyE_C3_1(currConfig,:),1)>=1);
        remRecDipolesbyE_C3_1 = recDipolesbyE_C3_1;
        remRecDipolesbyE_C3_1(:,recDipoles) = [];
        
        recDipoles = logical(sum(recDipolesbyE_C3_2(currConfig,:),1)>=2);
        remRecDipolesbyE_C3_2 = recDipolesbyE_C3_2;
        remRecDipolesbyE_C3_2(:,recDipoles) = [];
        
        remDsThisRow = [size(remRecDipolesbyE_C1_1,2),size(remRecDipolesbyE_C1_2,2),size(remRecDipolesbyE_C2_1,2),size(remRecDipolesbyE_C2_2,2),size(remRecDipolesbyE_C3_1,2),size(remRecDipolesbyE_C3_2,2)];
        remDipolesTrack = [remDipolesTrack; remDsThisRow];
        
        nextTime = toc;
        timesTrack = [timesTrack;nextTime];
        
        if isempty(remRecDipolesbyE_C3_2)
            disp('All dipoles are recordable at highest threshold!')
            soln = 1;
            break
        end

        % remove electrodes that will not decrease cost function
        %   only have to check first threshold "C1"
        %   Only have to check "_2" matrix (holds same dipoles as "_1"
        %   matrix but contains more dipoles: all that are not mapped at
        %   least twice. This is because all of the electrodes that have
        %   zero rows here necessarily have zero rows also in the "_1"
        %   matrix, but some electrodes with zero rows in the "_1" matrix
        %   can have nonzero rows here. We are fine keeping electrodes that
        %   would only contribute to double mapping dipole (they can't map a new dipole). 
        esDontHelp = find(any(remRecDipolesbyE_C1_2,2)==0);

        % also remove electrodes already in our configuration, 
        % and those that we know already intersect,
        % Keep track of standard electrode indices!
        remE = [currConfig, eIntersects, esDontHelp'];
        remRecDipolesbyE_C1_1(remE,:) = [];
        remRecDipolesbyE_C1_2(remE,:) = [];
        remRecDipolesbyE_C2_1(remE,:) = [];
        remRecDipolesbyE_C2_2(remE,:) = [];
        remRecDipolesbyE_C3_1(remE,:) = [];
        remRecDipolesbyE_C3_2(remE,:) = [];
        OG_electrodeInds = esRecAny;
        OG_electrodeInds(remE) = [];
        clipped_electrodeInds = [1:length(esRecAny)];
        clipped_electrodeInds(remE) = [];

        % find electrode that maps the most next
        % note that electrodes in remRecDipolesbyE are referenced 1:N in
        % remaining-electrode basis (bestNext). then transferre
        bestNextOptions = getBestEs(remRecDipolesbyE_C1_1, remRecDipolesbyE_C1_2, remRecDipolesbyE_C2_1, remRecDipolesbyE_C2_2, remRecDipolesbyE_C3_1, remRecDipolesbyE_C3_2);

        disp(strcat("The best options are: ",num2str(bestNextOptions)))
        disp(strcat("The best options IN ST BASIS are: ",num2str(clipped_electrodeInds(bestNextOptions))))
        disp(strcat("clipped_electrodeInds array is of size ",num2str(length(clipped_electrodeInds))))
        if isempty(bestNextOptions)
            disp('There are no electrodes left that would improve cost function and do not intersect!')
            fail = 1;
            break 
        end
        bestNext = bestNextOptions(randi(length(bestNextOptions)))
        
        % get potential configuration and make sure none intersect
        % need to transform into original electrode basis first!
        bestNextC = clipped_electrodeInds(bestNext);
        potConfig = [currConfig, bestNextC];

        % check intersects between any pairs of electrodes
        %   Should change this to check only pairs that include last
        %   electrode. But won't make much difference. 
        intersect = anyPairsIntersect(potConfig, esRecAny, eColls,slocs_reInd,normals_reInd); %, track_insertionLocCorrespondence, test_local);

        % if no collision, add to config and continue
        %   else, find next best C
        while intersect
            disp(strcat("Electrode bestNext:",num2str(bestNext),", bestNextC:",num2str(bestNextC)," intersects!"))
            eIntersects = [eIntersects,bestNextC];
            % remove collision electrode from matrix
            % remRecDipolesbyE(bestNext,:) = [];
            remRecDipolesbyE_C1_1(bestNext,:) = [];
            remRecDipolesbyE_C1_2(bestNext,:) = [];
            remRecDipolesbyE_C2_1(bestNext,:) = [];
            remRecDipolesbyE_C2_2(bestNext,:) = [];
            remRecDipolesbyE_C3_1(bestNext,:) = [];
            remRecDipolesbyE_C3_2(bestNext,:) = [];
            clipped_electrodeInds(bestNext) = [];
            % re-calculate next next
            bestNextOptions = getBestEs(remRecDipolesbyE_C1_1, remRecDipolesbyE_C1_2, remRecDipolesbyE_C2_1, remRecDipolesbyE_C2_2, remRecDipolesbyE_C3_1, remRecDipolesbyE_C3_2);

            disp(strcat("The best options are: ",num2str(bestNextOptions)))
            disp(strcat("The best options IN ST BASIS are: ",num2str(clipped_electrodeInds(bestNextOptions))))
            disp(strcat("clipped_electrodeInds array is of size ",num2str(length(clipped_electrodeInds))))
            if isempty(bestNextOptions)
                disp('There are no electrodes left that would improve cost function and do not intersect!')
                fail = 1;
                break 
            end
            bestNext = bestNextOptions(randi(length(bestNextOptions)));
            
            % get potential configuration and make sure none intersect
            % need to transform into original electrode basis first!
            bestNextC = clipped_electrodeInds(bestNext);
            potConfig = [currConfig, bestNextC];
            
            % check collision again
            intersect = anyPairsIntersect(potConfig, esRecAny, eColls,slocs_reInd,normals_reInd); %, track_insertionLocCorrespondence, test_local);
        end
        
        if fail
            disp(strcat('Trial failed! Quitting trial ',num2str(numTrials)))
            break
        end
        
        % add to config
        disp(strcat("Adding electrode bestNext: ",num2str(bestNext),", bestNextC:",num2str(bestNextC),"to the configuration"))
        currConfig = [currConfig, bestNextC]

    end
    
    nextTime = toc;
    timesTrack = [timesTrack;nextTime];
    disp(strcat('This trial took ',num2str(timesTrack(end)),' seconds'))
    solConfigs{numTrials,4} = timesTrack;
    
    % map to full e-basis
    solConfigs{numTrials,1} = esRecAny(currConfig);
    if soln
        solConfigs{numTrials,2} = remDipolesTrack;
        solConfigs{numTrials,3} = "solution";
    else
        solConfigs{numTrials,2} = remDipolesTrack; % # dipoles not fully mapped
        if fail
            solConfigs{numTrials,3} = "no more electrodes";
        else
            solConfigs{numTrials,3} = "more than 30 electrodes";
        end
    end
    
end

% also calculate recording sensitivity
totalDipoles = length(ROI_dips);
% we stored number of remaining dipoles at every level and threshold (single and double map) in solConfigs{numTrials,2}
% convert to percent by div by total Dipoles, then make positive RS
percRecAllThrsSingMap = 1-(solConfigs{numTrials,2}(:,[1,3,5])/totalDipoles); 


% save results
if ~test_local
    save(strcat(dataWorkPath,patientDir,'/DemoOut/NBSPercRec_solution_integratedCost',case_id,'.mat'), 'percRecAllThrsSingMap')
    save(strcat(dataWorkPath,patientDir,'/DemoOut/NBS_solution_integratedCost',case_id,'.mat'), 'solConfigs')
end

end

function bestOpts = getBestEs(recDipolesbyE_C1_1, recDipolesbyE_C1_2, recDipolesbyE_C2_1, recDipolesbyE_C2_2, recDipolesbyE_C3_1, recDipolesbyE_C3_2)
    % given the 6 recDbyE matrices, find the best electrode or electrodes
    % using a combined cost function.
    % The recDbyE matrices are E' by D' matrices of ones and zeros
    % indicating what electrodes can record discernible voltage from what
    % dipoles. E' must be the same across all 6 matrices, but D' can differ
    % between the 6 matrices. E' represents the electrodes that have the
    % potential to decrease the cost function (at any component / matrix).
    % D' represents the dipoles that have not yet been mapped by at least 
    %   1 (for the "_1" matrices) or by at least 2 (for the "_2") matrices.
    % The "C1" "C2" and "C3" represents the threshold case: 100 µV, 500 µV,
    % or 1000 µV. 
    % Profit(i) = 2*(# dipoles mapped once or more) + (# dipoles mapped twice or more)
    % where Cost(i) = 3*N - Profit(i)
    
    % simple cost (balance single and double map evenly at one thr only)
    simpleCost = 0;
    if simpleCost == 1
        weights1s = sum(recDipolesbyE_C1_1,2) + sum(recDipolesbyE_C1_2,2);
        bestOpts = find(weights1s==max(weights1s));
        return
    end
    
    % integrated cost (single map is worth twice double map and use all three thrs to break ties. 
    integratedCost = 0;
    if integratedCost == 1
        weights1s = 2 * sum(recDipolesbyE_C3_1,2) + sum(recDipolesbyE_C3_2,2);
        bestOpts = find(weights1s==max(weights1s));
        if length(bestOpts) > 1 || isempty(bestOpts)
            % restrict domain of second threshold rec Array to only best Es
            weights2s = 2 * sum(recDipolesbyE_C2_1(bestOpts,:),2) + sum(recDipolesbyE_C2_2(bestOpts,:),2);
            bestOpts = bestOpts(weights2s==max(weights2s));
            if length(bestOpts) > 1 || isempty(bestOpts)
                % restrict domain of second threshold rec Array to only best Es
                weights3s = 2 * sum(recDipolesbyE_C1_1(bestOpts,:),2) + sum(recDipolesbyE_C1_2(bestOpts,:),2);
                bestOpts = bestOpts(weights3s==max(weights3s));
            end
        end
        return
    end
    
    % single map integrated (we only care about single mapping, and use other thresholds to break ties)
    singleMapCost = 1;
    if singleMapCost == 1
        weights1s = sum(recDipolesbyE_C3_1,2);
        bestOpts = find(weights1s==max(weights1s));
        if length(bestOpts) > 1 || isempty(bestOpts)
            % restrict domain of second threshold rec Array to only best Es
            weights2s = sum(recDipolesbyE_C2_1(bestOpts,:),2);
            bestOpts = bestOpts(weights2s==max(weights2s));
            if length(bestOpts) > 1 || isempty(bestOpts)
                % restrict domain of second threshold rec Array to only best Es
                weights3s = sum(recDipolesbyE_C1_1(bestOpts,:),2);
                bestOpts = bestOpts(weights3s==max(weights3s));
            end
        end
        return
    end
    
end

function intersect = anyPairsIntersect(eInds, esRecAny, eColls,slocs_reInd,normals_reInd) %, insertionLocsAllEs, test_local)
    intersect = 0;
    if length(eInds) > 1
        %disp('make sure none of these choices intersect!')
        pairs = nchoosek(eInds,2);
        %disp(pairs)
        % convert to full electrode basis
        pairs = esRecAny(pairs);
        if size(pairs)==[2,1]
            pairs = pairs';
        end
        %disp(pairs)
        intersect = 0;
        for k = 1:size(pairs,1)
            % get BB of first e
            % not needed as BB intersects are placed in eColls matrix
%             if ~test_local
%                 BB1 = insertionLocsAllEs(pairs(k,1));
%                 BB2 = insertionLocsAllEs(pairs(k,2));
%                 % check if second e is in same BB
%                 if BB1 == BB2
%                     intersect = 1;
%                     return   
%                 end
%             end
            % look up coord in collision matrix
            if ~isempty(eColls)
                if eColls(max(pairs(k,:)), min(pairs(k,:))) == 1
                    intersect = 1;
                    return
                end
            else
                % if dont't have collision matrix, do this manually
                % 128 pts per electrode. electrodes are 54.5 mm long (3.5
                % mm contact spacing, 16 contacts, 2 mm contact diameter)
                threshold = 4;
                eI = pairs(k,1);
                eI2 = pairs(k,2);
                endpoint  = slocs_reInd(eI,:) + 54.5*normals_reInd(eI,:); 
                EPoints = slocs_reInd(eI,:) + [0:1:128-1]'*((endpoint-slocs_reInd(eI,:))/(128-1)); 
                endpoint2  = slocs_reInd(eI2,:) + 54.5*normals_reInd(eI2,:); 
                EPoints2 = slocs_reInd(eI2,:) + [0:1:128-1]'*((endpoint2-slocs_reInd(eI2,:))/(128-1)); 
                for i = 1:size(EPoints,1)
                    dists = sqrt(sum((EPoints2-EPoints(i,:)).^2,2));
                    if min(dists) < threshold
                        intersect=1;
                        return
                    end
                end 
            end
        end
    end
end


