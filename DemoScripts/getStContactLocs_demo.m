% get standard contact locations for all patients for rec radius data
% Grace Dessert
% 9/23/22


% $$ UPDATE PARAMETERS $$
patientID = 25;
dataWorkPath = '';
dataHomePath = '';

getContactLocs(patientID,dataWorkPath,dataHomePath)


function getContactLocs(patientID,dataWorkPath,dataHomePath)

patientDir = ['Patient_' num2str(patientID)];

% load cortex and labelled full cortex
try
    load(strcat(dataHomePath,patientDir,"/Inputs/lh_brain_WithLabels.mat"),'scirunfield')
catch
    disp(strcat("ERROR from getStContactLocs.m: ",strcat(dataHomePath,patientDir,"/Inputs/lh_brain_WithLabels.mat")," NOT FOUND!"));
    return
end
rh = stlread(strcat(dataHomePath,patientDir,"/Inputs/rh_white_20k.stl"));

% randomly pick three nodes from each coritcal region 
subLocs = unique(scirunfield.field);
allSelContacts.node = zeros(3,length(subLocs)*3);
for i = 1:length(subLocs)
    subLocInd = subLocs(i);
    indsInSub = find(scirunfield.field==subLocInd);
    randInds = indsInSub(randi(length(indsInSub),3,1));
    allSelContacts.node(:,(i-1)*3+1:i*3) = scirunfield.node(:,randInds);
end
allSelContacts.field = ceil([1:length(allSelContacts.node)]/3);
allSelContacts.field = subLocs(allSelContacts.field);

try
    load(strcat(dataHomePath,patientDir,"/Inputs/rh_brain_WithLabels.mat"),'scirunfield')
catch
    disp(strcat("ERROR from getStContactLocs.m: ",strcat(dataHomePath,patientDir,"/Inputs/rh_brain_WithLabels.mat")," NOT FOUND!"));
    return
end
% randomly pick three nodes from each coritcal region 
for i = length(subLocs)+1:2*length(subLocs)
    subLocInd = subLocs(i-length(subLocs));
    indsInSub = find(scirunfield.field==subLocInd);
    randInds = indsInSub(randi(length(indsInSub),3,1));
    allSelContacts.node(:,(i-1)*3+1:i*3) = scirunfield.node(:,randInds);
end
allSelContacts.field = ceil([1:length(allSelContacts.node)]/3);
subLocs2 = [subLocs,subLocs+length(subLocs)];
allSelContacts.field = subLocs2(allSelContacts.field);
allSelContacts.field = [allSelContacts.field,allSelContacts.field];

save(strcat(dataWorkPath,patientDir,'/DemoOut/recRadiusContactLocs.mat'),'allSelContacts');

%%
f = figure('visible','off');
clf
hold on
trimesh(rh)
scatter3(allSelContacts.node(1,:),allSelContacts.node(2,:),allSelContacts.node(3,:),'r','filled')
savefig(f,strcat(dataHomePath,patientDir,"/DemoOut/allSelContacts.fig"))

end




