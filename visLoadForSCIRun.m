% Grace Dessert
% 14 Sep 2022
% Finish E Congig visualization by writing files for SCIRun network visConfigRec.srn5
% using precomputed visualization data from visConfigSolution.m


% $$ UPDATE PARAMETERS $$
cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
dataWorkPath = '';
dataHomePath = '';
patientID = 33
sourceInd = 4

outFileName = 'temp_testOut.txt';
errFileName = 'temp_testErr.txt';

patientDir = ['Patient_' num2str(patientID)];

% store errors in errFile 
errFile = fopen(errFileName,'a');
% store messages in outFile
outFile = fopen(outFileName,'a');

% plot added benefit or normal perc mapping
addedBenefitInstead = false;

% use precomp perc rec or recalculate with recDipoles and ROI
recalcRecPerc = false;

% weight dipole rec by area
weightByArea = false;

% load data
disp("Loading Single Map data")
tys_cA = strcat(num2str(patchAreas(sourceInd)),"cm2_",num2str(cdmds(sourceInd)),"cdmd");
load(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_cis',tys_cA,'.mat'),'visDataConfigs')
load(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataImplantConfig_singleMap_crossAnalyze',tys_cA,'.mat'),'visDataImplantConfig')
load(strcat(dataHomePath,patientDir,'/HeadModel/cortex_areas.mat'),'cortex_areas')
load(strcat(dataHomePath,patientDir,'/Inputs/ContactLocations.mat'),'scirunfield')
contLocs = scirunfield;

LH_fileName = strcat(dataHomePath,patientDir,'/Inputs/lh_white_20k.stl');
LH_white = stlread(LH_fileName);
RH_fileName = strcat(dataHomePath,patientDir,'/Inputs/rh_white_20k.stl');
RH_white = stlread(RH_fileName);
load(strcat(dataWorkPath,patientDir,'/OptSolutions/ROIs.mat'),'patches')
skinSurf = stlread(strcat(dataHomePath,patientDir,'/Inputs/SkinFinal.stl'));
load(strcat(dataWorkPath,patientDir,'/HeadModel/SulciSurface_refined.mat'),'sulciPoints');
load(strcat(dataWorkPath,patientDir,'/ElectrodeLocsData/normals_reInd.mat'),'normals_reInd')
load(strcat(dataWorkPath,patientDir,'/ElectrodeLocsData/slocs_reInd.mat'),'slocs_reInd')
    
%%
thrI = 2;
ROI_str = "clinicianROI"; %"LTL"; %"LTL"; %"clinicianROI_broad";

recalcRecPerc = false;
% if no specs, get list of case IDs in vis struct
if isempty(ROI_str)
    caseIDs = {};
    disp("Here are the current precomputed visualizations.");
    for i = 1:length(visDataConfigs)
        if ~isfield(visDataConfigs{i},'full_case_id')
            continue
        end
        disp(strcat(num2str(i),": ",visDataConfigs{i}.full_case_id));
        caseIDs{i} = visDataConfigs{i}.full_case_id;
    end
    % get user input to determine which case to run
    indexVisSelect = input("Which index would you like to load?");
    caseID_select = caseIDs{indexVisSelect};
    visSelect = visDataConfigs{indexVisSelect};
else
    thrPrioritys = ["200_500_1000_thrPriority","500_200_1000_thrPriority","1000_500_200_thrPriority"]; %,"1000_200_500_thrPriority","500_1000_200_thrPriority","200_1000_500_thrPriority"];
    thrPriority = thrPrioritys(thrI);
    thrs = [200,500,1000];
    mapStrStr = strcat("single mapping at ",num2str(thrs(thrI))," µV threshold");
    full_case_id = strcat(tys_cA,'_ROI_',ROI_str,"_singleMap_",thrPriority,mapStrStr,"_crossAnalyze",tys_cA);
    found = 0;
    for i = 1:length(visDataConfigs)
        if ~isfield(visDataConfigs{i},'full_case_id')
            continue
        end
        if visDataConfigs{i}.full_case_id ~= full_case_id
            continue
        end
        found = 1;
        indexVisSelect = i;
        caseID_select = visDataConfigs{i}.full_case_id;
        visSelect = visDataConfigs{indexVisSelect};
        disp(strcat("we found this case! ",visDataConfigs{i}.full_case_id));
    end
    if found == 0
        disp(strcat("We DID NOT find this case!",full_case_id))
    end
end
%
ROIs = ["cl","LT","LH"]; % array 
ROIs_full = ["clinicianROI","LTL","LH","clinicianROI_broad"];

ROI_i = split(caseID_select,"ROI_");
ROI_i = ROI_i(2);
ROI_s = extractBetween(ROI_i,1,2);
ROI_i = find(ROIs==ROI_s);
if contains(caseID_select,"clinicianROI_broad")
    ROI_i = 4;
    ROI_s = "clinicianROI_broad";
    ROIs = ["cl","LT","LH","cl"];
end
ROI_i
ROIs
disp(strcat("This configuration is for ",visSelect.full_case_id))
disp(strcat("There are ",num2str(length(visSelect.config))," electrodes"))



% write files to VisCurrent dir

% lh/rh cortex
stlwrite(LH_white,'VisCurrent/lh.stl')
stlwrite(RH_white,'VisCurrent/rh.stl')

% ROI
ROIPI = 0;
for i = 1:length(patches.name)
    if patches.name{i}(1) == ROI_str
        ROIPI = i;
    end
end
if ROIPI == 0
    disp("DIDN't FIND RIGHT ROI NAME!")
    ROI_str
end
ROIfield = patches.patchfield{ROIPI};
stlwrite(ROIfield,'VisCurrent/ROI.stl')


numEs = input("How many electrodes would you like to implant?");
numEs_imp = numEs;
% recArea
rec_tissue.node = visSelect.rec_tissue_by_E.node;
rec_tissue.field = sum(visSelect.rec_tissue_by_E.field(1:numEs,:),1);
save('VisCurrent/rec_tissue.mat','rec_tissue')

% contacts
configField.node = visSelect.configField.node(visSelect.configField.field<=numEs,:);
configField.field = visSelect.configField.field(visSelect.configField.field<=numEs);
save('VisCurrent/configField.mat','configField')
%
% check intersection with sulci points
plotT = true;
electrodeInds = visSelect.config;
checkConfigSulciInt(sulciPoints, LH_white, RH_white, plotT, electrodeInds, normals_reInd, slocs_reInd)
%
% sulci points
sulciF.node = sulciPoints;
sulciF.field = zeros(length(sulciPoints),1);
save('VisCurrent/sulciField.mat','sulciF')

% disp info
disp(strcat("Solution configuration maps ",num2str(100-visSelect.percMapROI(numEs)*100),"% of the ROI with ",extractBetween(caseID_select,21,44)))

%
% implanted recArea
% get case for implant config
tys = split(caseID_select,"_ROI");
tys = tys(1);
thrI = split(caseID_select,"thrPriority");
thrI = thrI(2);
thrI = extractBetween(thrI,1,21);
thrsS = ["single mapping at 200", "double mapping at 200", "single mapping at 500", "double mapping at 500", "single mapping at 100", "double mapping at 100"];
thrI = find(thrsS==thrI);
costFnthrPri = split(caseID_select,"_thrPriority");
costFnthrPri = costFnthrPri(1);
costFnthrPri = extractBetween(costFnthrPri,strlength(costFnthrPri)-11,strlength(costFnthrPri));
found = 0;
thrOrder = zeros(1,3);
thrsOrder = split(costFnthrPri,"_");
thrs = (str2double(thrsOrder));
thrOrder(1) = find(thrs==200);
thrOrder(2) = find(thrs==500);
thrOrder(3) = find(thrs==1000);
thrOrde_f = thrOrder*2-1;
thrOrder = reshape(repmat(thrOrde_f,2,1),6,[]);
thrFlip = flip(thrs);
imp_rec_tissue_all = cell(3,1);
imp_rec_tissue_all_perc = cell(3,1);
bestCost = true;
if bestCost 
    thrCaseRep = ["200_1000_500", "500_200_1000", "1000_500_200"];
    selThrBC = [5,5,5];
    thrOrde = [1,2,3];
else
    thrCaseRep = [costFnthrPri, costFnthrPri, costFnthrPri];
    selThrBC = [5,3,1];
    thrOrde = round(thrOrde_f/2);
end
%

for impSSelect = 1:length(visDataImplantConfig)
    if isempty(visDataImplantConfig{impSSelect})
        continue
    end
    if isempty(visDataImplantConfig{impSSelect}.config)
        continue
    end
    if visDataImplantConfig{impSSelect}.imp_case_id == strcat(tys,"_",costFnthrPri,"_thrPriority","_ROI",ROIs_full(ROI_i))

        found = 1;
        impSSelected = impSSelect;
        %temp = [200,500,1000];
        %ok = find(flip(thrs)==temp(round(thrI/2)))
        imp_rec_tissue = visDataImplantConfig{impSSelect}.rec_tissueDat_imp_allThr{5}; %find(flip(thrs)==temp(round(thrI/2)))};
        imp_rec_perc = visDataImplantConfig{impSSelect}.percMapROI_imp_allThr{5};
    end
    if visDataImplantConfig{impSSelect}.imp_case_id == strcat(tys,"_",thrCaseRep(1),"_thrPriority","_ROI",ROIs_full(ROI_i))
        numEE = length(visDataImplantConfig{impSSelect}.rec_tissueDat_imp_allThr{1});
        imp_rec_tissue_all{thrOrde(1)} = visDataImplantConfig{impSSelect}.rec_tissueDat_imp_allThr{5};
        imp_rec_tissue_all_perc{thrOrde(1)} = visDataImplantConfig{impSSelect}.percMapROI_imp_allThr{5};
        impSSelect
        visDataImplantConfig{impSSelect}.imp_case_id
    end
    if visDataImplantConfig{impSSelect}.imp_case_id == strcat(tys,"_",thrCaseRep(2),"_thrPriority","_ROI",ROIs_full(ROI_i))
        numEE = length(visDataImplantConfig{impSSelect}.rec_tissueDat_imp_allThr{selThrBC(2)});
        imp_rec_tissue_all{thrOrde(2)} = visDataImplantConfig{impSSelect}.rec_tissueDat_imp_allThr{5};
        imp_rec_tissue_all_perc{thrOrde(2)} = visDataImplantConfig{impSSelect}.percMapROI_imp_allThr{5};
        impSSelect
        visDataImplantConfig{impSSelect}.imp_case_id
    end
    if visDataImplantConfig{impSSelect}.imp_case_id == strcat(tys,"_",thrCaseRep(3),"_thrPriority","_ROI",ROIs_full(ROI_i))
        if isempty(visDataImplantConfig{impSSelect}.config)
            imp_rec_tissue_all{thrOrde(3)} = 0;
            imp_rec_tissue_all_perc{thrOrde(3)} = 0;
            continue
        end
        numEE = length(visDataImplantConfig{impSSelect}.rec_tissueDat_imp_allThr{selThrBC(3)});
        imp_rec_tissue_all{thrOrde(3)} = visDataImplantConfig{impSSelect}.rec_tissueDat_imp_allThr{5};
        imp_rec_tissue_all_perc{thrOrde(3)} = visDataImplantConfig{impSSelect}.percMapROI_imp_allThr{5};
        impSSelect
        visDataImplantConfig{impSSelect}.imp_case_id
    end
end
impSSelect = impSSelected;

%
% implantedContacts
% select specific electrode inds to implant.
% this is no longer necessary because it is done automatically in
% visConfigSolution.m to select Es inside or within <3 mm of the ROI
%{
% **** NOTE these are different for different cost functions!
% disp('Using selected electrode inds for single map cost fn!')
% if ROI_i == 2
%     if patientID == 25
%         impEsSelectInds = [1,2,3,4,5,6,7]; %[2,4,6,7,8,9,11];
%     elseif patientID == 2
%         impEsSelectInds = [1:6];
%         numEs_imp = length(impEsSelectInds);
%         %impEsSelectInds = [1:max(visDataImplantConfig{impSSelect}.config.field)];
%     elseif patientID == 26
%         impEsSelectInds = [1:4];
%     elseif patientID == 20
%         impEsSelectInds = [1:7,9,10]; % [1:9]
%     elseif patientID == 24
%         impEsSelectInds = [1:6];
%     elseif patientID == 30
%         impEsSelectInds = [1:8];
%     elseif patientID == 27
%         impEsSelectInds = [1:8,10,12];
%     elseif patientID == 31
%         impEsSelectInds = [1:6];
%     else
%         impEsSelectInds = [1:max(visDataImplantConfig{impSSelect}.config.field)];
%     end
% else
%     if patientID == 25
%         impEsSelectInds = [1:14];
%     elseif patientID == 26
%         impEsSelectInds = [1:13];
%     elseif patientID == 24
%         impEsSelectInds = [1:12];
%     elseif patientID == 27
%         impEsSelectInds = [1:10]; % just use LTL electrodes
%     elseif patientID == 31
%         impEsSelectInds = [1:6];
%     else
%         impEsSelectInds = [1:max(visDataImplantConfig{impSSelect}.config.field)];
%     end
% end
%}
% full imp config
save('VisCurrent/FullImplantedConfig.mat','contLocs')

numImpEs = length(imp_rec_perc);
disp(strcat("There are ",num2str(numImpEs)," implanted electrodes for this ROI"));
scirunfield = visDataImplantConfig{impSSelect}.config;
save('VisCurrent/ImplantedConfig.mat','scirunfield')



% imp contacts rec area
rec_tissue_imp.field = sum(imp_rec_tissue,1);
rec_tissue_imp.node = visSelect.rec_tissue_by_E.node;
save('VisCurrent/rec_tissue_imp.mat','rec_tissue_imp')

% skin
stlwrite(skinSurf,'VisCurrent/skinSurf.stl')

disp("Saved files to VisCurrent directory")


% check intersections
dists = zeros(length(configField.node));
for i = 1:length(configField.node)
    dists(i,:) = sqrt(sum((configField.node-configField.node(i,:)).^2,2));
end
dists(dists==0)=100;
minDS = min(min(dists));
disp(strcat("The min distance between any contacts in search solution configuration is ",num2str(minDS)))

dists = zeros(length(scirunfield.node));
for i = 1:length(scirunfield.node)
    dists(i,:) = sqrt(sum((scirunfield.node-scirunfield.node(i,:)).^2,2));
end
dists(dists==0)=100;
minDI = min(min(dists));
disp(strcat("The min distance between any contacts in implanted configuration is ",num2str(minDI)))


% plot conv plot

% indexVisSelect
%
i=1;
dat = zeros(50,6);
selID = visDataConfigs{indexVisSelect}.full_case_id;
selID = split(selID,costFnthrPri);
selID2 = extractBetween(selID(2)," at "," µV ");

dipolesROI = patches.dipoles{ROI_i};

for i = 1:length(visDataConfigs)
    if visDataConfigs{i}.full_case_id == replace(join(selID,thrCaseRep(1)),thrsS(thrI),thrsS(1))
        numEEE = length(visDataConfigs{i}.percMapROI);
        % dat(1:numEEE,1) = visDataConfigs{i}.percMapROI;
        if recalcRecPerc
            recD = visDataConfigs{i}.rec_tissue_by_E.field;
            for j = 1:numEEE
                percRec = sum(recD(1:j,dipolesROI),1);
                percRec(percRec>1) = 1;
                if weightByArea
                    percRec = sum(percRec.*cortex_areas(dipolesROI)',2)/sum(cortex_areas(dipolesROI));
                else
                    percRec = sum(percRec,2)/length(dipolesROI);
                end
                dat(j,1) = percRec;
            end
        else
            dat(1:length(visDataConfigs{i}.percMapROI),1) = visDataConfigs{i}.percMapROI;
            dat(length(visDataConfigs{i}.percMapROI)+1:end,1) = visDataConfigs{i}.percMapROI(end);
        end
        
        visDataConfigs{i}.full_case_id
    end
    if visDataConfigs{i}.full_case_id == replace(join(selID,thrCaseRep(2)),thrsS(thrI),thrsS(3))
        numEEE = length(visDataConfigs{i}.percMapROI);
        if recalcRecPerc
            recD = visDataConfigs{i}.rec_tissue_by_E.field;
            for j = 1:numEEE
                percRec = sum(recD(1:j,dipolesROI),1);
                percRec(percRec>1) = 1;
                if weightByArea
                    percRec = sum(percRec.*cortex_areas(dipolesROI)',2)/sum(cortex_areas(dipolesROI));
                else
                    percRec = sum(percRec,2)/length(dipolesROI);
                end
                dat(j,3) = percRec;
            end
        else
            dat(1:length(visDataConfigs{i}.percMapROI),3) = visDataConfigs{i}.percMapROI;
            dat(length(visDataConfigs{i}.percMapROI)+1:end,3) = visDataConfigs{i}.percMapROI(end);
        end
        visDataConfigs{i}.full_case_id
    end
    if visDataConfigs{i}.full_case_id == replace(join(selID,thrCaseRep(3)),thrsS(thrI),strcat(thrsS(5),"0"))
        numEEE = length(visDataConfigs{i}.percMapROI);
        % dat(1:numEEE,5) = visDataConfigs{i}.percMapROI;
        if recalcRecPerc
            recD = visDataConfigs{i}.rec_tissue_by_E.field;
            for j = 1:numEEE
                percRec = sum(recD(1:j,dipolesROI),1);
                percRec(percRec>1) = 1;
                if weightByArea
                    percRec = sum(percRec.*cortex_areas(dipolesROI)',2)/sum(cortex_areas(dipolesROI));
                else
                    percRec = sum(percRec,2)/length(dipolesROI);
                end
                dat(j,5) = percRec;
            end
        else
            dat(1:length(visDataConfigs{i}.percMapROI),5) = visDataConfigs{i}.percMapROI;
            dat(length(visDataConfigs{i}.percMapROI)+1:end,5) = visDataConfigs{i}.percMapROI(end);
        end
        visDataConfigs{i}.full_case_id
    end
end
numEs = length(dat(:,1));
percentROI = [zeros(1,6);dat]*100;
%
i=1;
numEEE = length(visDataImplantConfig{impSSelect}.percMapROI_imp_allThr{i});


dat = zeros(numImpEs,6);
for i = [1,3,5]
%     mapAddEachE = [imp_rec_tissue_all_perc{round(i/2)}(1);diff(imp_rec_tissue_all_perc{round(i/2)})];
%     dat(1:length(impEsSelectInds),i) = cumsum(mapAddEachE(impEsSelectInds)); % visDataImplantConfig{impSSelect}.percMapROI_imp_allThr{thrOrder(i)};
    if recalcRecPerc
        for j = 1:numImpEs
            percRec = sum(imp_rec_tissue_all{round(i/2)}(:,dipolesROI),1);
            percRec(percRec>1) = 1;
            if weightByArea
                percRec = sum(percRec.*cortex_areas(dipolesROI)',2)/sum(cortex_areas(dipolesROI));
            else
                percRec = sum(percRec,2)/length(dipolesROI);
            end
            dat(j,i) = percRec;
        end
    else
        dat(:,i) = imp_rec_tissue_all_perc{round(i/2)};
        dat(:,i) = imp_rec_tissue_all_perc{round(i/2)};
    end
end
percentROI_implant = [zeros(1,6);dat] * 100;

disp(strcat("Implanted configuration maps ",num2str(percentROI_implant(numImpEs+1,3)),"% of the ROI with ",thrsS(thrI)))

figure(2)
clf
hold on
colorz = [0 0.4470 0.7410;0.8500 0.3250 0.0980;0.9290 0.6940 0.1250;0.4940 0.1840 0.5560;0.4660 0.6740 0.1880;0.3010 0.7450 0.9330;0.6350 0.0780 0.1840];
colorz = lines(4);
colorz = colorz(2:4,:);

grid on
%
if addedBenefitInstead
    percentROI = diff(percentROI,[],1);
    percentROI_implant = diff(percentROI_implant,[],1)
    xdata = [1:numEs];
    xdataImp = [1:numImpEs];
else
    xdata = [0:numEs];
    xdataImp = [0:numImpEs];
end

if contains(caseID_select,"single")
    oneA = plot(xdata,percentROI(:,1),'LineWidth',2.5,'color',colorz(1,:));
    % oneB = plot([0:numEs],percentROI(:,2),'LineWidth',2,'color',colorz(2,:));
    twoA = plot(xdata,percentROI(:,3),'LineWidth',2.5,'color',colorz(2,:));
    % twoB = plot([0:numEs],percentROI(:,4),'LineWidth',2,'color',colorz(4,:));
    threeA = plot(xdata,percentROI(:,5),'LineWidth',2.5,'color',colorz(3,:));
    % threeB = plot([0:numEs],percentROI(:,6),'LineWidth',2,'color',colorz(6,:));
    % colorz2 = parula(15);
    oneA_i = plot(xdataImp,percentROI_implant(:,1),'--','LineWidth',2.5,'color',colorz(1,:));
    % oneB_i = plot([0:numEs_imp],percentROI_implant(:,2),'--','LineWidth',2,'color',colorz(2,:));
    twoA_i = plot(xdataImp,percentROI_implant(:,3),'--','LineWidth',2.5,'color',colorz(2,:));
    % twoB_i = plot([0:numEs_imp],percentROI_implant(:,4),'--','LineWidth',2,'color',colorz(4,:));
    threeA_i = plot(xdataImp,percentROI_implant(:,5),'--','LineWidth',2.5,'color',colorz(3,:));
    % threeB_i = plot([0:numEs_imp],percentROI_implant(:,6),'--','LineWidth',2,'color',colorz(6,:));
else
    oneB = plot([0:numEs],percentROI(:,2),'LineWidth',2,'color',colorz(2,:));
    twoB = plot([0:numEs],percentROI(:,4),'LineWidth',2,'color',colorz(4,:));
    threeB = plot([0:numEs],percentROI(:,6),'LineWidth',2,'color',colorz(6,:));
    if ROI_i == 8
    oneB_i = plot([0:numEs_imp],percentROI_implant(:,2),'--','LineWidth',2,'color',colorz(2,:));
    twoB_i = plot([0:numEs_imp],percentROI_implant(:,4),'--','LineWidth',2,'color',colorz(4,:));
    threeB_i = plot([0:numEs_imp],percentROI_implant(:,6),'--','LineWidth',2,'color',colorz(6,:));
    end
end
x = plot([-2,-1],[0,0],'k-','LineWidth',2)
y = plot([-2,-1],[0,0],'k--','LineWidth',2)
legend([oneA, twoA, threeA, x,y], {'200 µV', '500 µV', '1000 µV','search','implanted'})
% legend('optimal: 200 µV', 'optimal: 500 µV', 'optimal: 1000 µV', 'implant: 200 µV', 'implant: 500 µV', 'implant: 1000 µV')
legend('location','southeast')
%
if addedBenefitInstead
    ylim([0,40])
    yticks([0:0.05:1]*100)
    xticks([1,4:4:20])
    ylabel('Percent recordability benefit')
else
    ylim([0,100])
    yticks([0:0.25:1]*100)
    ylabel('Percent recordability')
    xticks([0:4:20])
end

xlim([0,numImpEs+4])

xlabel('Electrodes')
ss = split(tys, "_");
s = split(ss(1), "cm2");
sss = split(ss(2), "cdmd");

caseID_disp = replace(caseID_select,"_"," ");
caseID_disp = strcat(extractBetween(caseID_disp,1,14)," ",extractBetween(caseID_disp,21,44));
caseID_disp = extractBetween(replace(caseID_disp," ",", "),1,16);
if contains(caseID_select,"single")
    caseID_disp = strcat(caseID_disp," with single mapping");
else
    caseID_disp = strcat(caseID_disp," with double mapping");
end
ROI_string = ROIs(ROI_i);
if ROI_string == "clinicianROI"
    ROI_string = "clinical";
end
%%
% title([strcat("Mapping of ",ROI_string," ROI across electrode additions, Patient ",num2str(patientID)),caseID_disp])
ax = gca; 
ax.FontSize = 18; 
%%
% set(gcf,'position',[500,500,550,250])
fileName = strcat('Pat',num2str(patientID),'_LTLconvg_bestC3.pdf');
print(gcf,fileName,'-bestfit','-dpdf');


function checkConfigSulciInt(sulciPoints, lh, rh, plotT, electrodeInds, normals, slocs)
% given contactLocs, get 128 points across electrode. note that ends of
% electrodes are 1mm on either size of first and last contact locs


% plot cortex, sulci, midline, and electrodes
if plotT
    figure(1)
    clf
    hold on
    trimesh(lh,'FaceAlpha',0.01,'EdgeAlpha',0.1)
    trimesh(rh,'FaceAlpha',0.01,'EdgeAlpha',0.1)
end

% check for sulci surface intersection
% using 2.5 mm threshold (same as in precomputeValidElectrodeLocs.m)

% just do simple distance check for all config points and all sulci
% poitns
intSulci = 0; % start no intersect
eFail = [];
pointsToPlot = [];
% for every electrode, build 128 pts across and test distances
for e = 1:length(electrodeInds)

    %contThisE = configPoints(cToEAssoc(e,:),:);
    sloc_d = slocs(electrodeInds(e),:);
    endpoint  = sloc_d + 54.5*normals(electrodeInds(e),:); 
    E1_points = sloc_d + [0:1:128-1]'*((endpoint-sloc_d)/(128-1)); 
    pointsToPlot = [pointsToPlot;E1_points];
    for i = 1:128
        dists = sqrt(sum((sulciPoints-E1_points(i,:)).^2,2));
        if min(dists) < 1.5
            intSulci=1;
            disp(strcat("Electrode ",num2str(e)," intersects at point ",num2str(i)," of 128! with a min dist of ",num2str(min(dists))))
            eFail = [eFail,e];
            continue
        end
    end
end


if plotT
    % plot generic midline
    points = [lh.Points', rh.Points'];
    z = linspace(min(points(3,:)),max(points(3,:)),10);
    y = linspace(min(points(2,:)),max(points(2,:)),10);
    [z2,y2]=meshgrid(z,y);
    % ax + by + cz + d = 0
    n = [1;0;0];
    p = [0;0;0];
    d = -p'*n;
    x2 = (-n(2)*y2 - n(3)*z2 - d)/n(1);
    % plot plane
    surf(x2,y2,z2,'FaceAlpha',1,'FaceColor',[1,1,1]);
    
    % plot sulci
    scatter3(sulciPoints(:,1),sulciPoints(:,2),sulciPoints(:,3),'.','DisplayName','Sulci Points')
    colors = parula(31);
    % plot electrode points in diff colors
    for i = 1:length(electrodeInds)
        indsP = (i-1)*128+1:(i)*128;
        if isempty(find(eFail==i, 1))
            scatter3(pointsToPlot(indsP,1),pointsToPlot(indsP,2),pointsToPlot(indsP,3),'filled','MarkerFaceColor',colors(i,:),'DisplayName',strcat("Electrode ",num2str(i)))
        else
            scatter3(pointsToPlot(indsP,1),pointsToPlot(indsP,2),pointsToPlot(indsP,3),'filled','MarkerFaceColor',[1,0,0],'DisplayName',strcat("Electrode ",num2str(i)))
        end
    end
    legend()
end


end


