% combine FSs into LF
% createLF
% 10/28/21

% FS are already scaled by area (NOT by cdmd), and in int16 compressed form
% concatenate, make into patchLF (mult by patchAll), threshold, and save

% input patch Area in mm^2
% discVoltThresh is 100µV
%function createLF(patchArea, discVoltThresh)

% 1/19/22 create 3 thresholded LFs for each of the source types

% 10/3/22 remove integer compression thresholding. using 16-bit floats
% instead

function createLF(inputCase) % inputCase = 1:(3*length(cdmds)) 

% sourceNum = 1:length(cdmds)
% cNum = 1:3 (threshold index)

sourceNum = ceil((inputCase)/3);
cNum = mod(inputCase-1,3)+1;

tic
% $$ UPDATE PARAMETERS $$
cdmds = [1, 1, 0.2, 0.2]; %nAm/mm^2
patchAreas = [5, 9.5, 5, 9.5]; % cm^2
dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';
dataHomePath = '/hpc/group/wmglab/ged12/cluster/Optimize_sEEG_implant/';
patientID = 1; 
outFileName = '';
errFileName = '';

% store errors in errFile 
errFile = fopen(errFileName,'a');
% store messages in outFile
outFile = fopen(outFileName,'a');

patientDir = ['Patient_' num2str(patientID)];

cdmd = cdmds(sourceNum) % nAm/mm^2
patchArea = patchAreas(sourceNum) % cm^2

discVoltThreshs = [200, 500, 1000]; % µV
discVoltThresh = discVoltThreshs(cNum) % µV

% maxInt = 2^15-1; % LF to be stored at 16bit initegers
% maxVolts = [150, 750, 1500]; % µV
% maxVolt = maxVolts(cNum) % µV

cNumStrings = ["C1","C2","C3"];
cNumStr = cNumStrings(cNum)

% ty =  3;
% tyStrs  = ["5cm2_0.2cdmd",
%     "9.5cm2_0.2cdmd",
%     "9.5cm2_1cdmd",
%     "5cm2_1cdmd"];
tys = strcat(num2str(patchArea),"cm2_",num2str(cdmd),"cdmd")

% get total num dipoles
load(strcat(dataWorkPath,patientDir,"/HeadModel/lh_areas.mat"), 'lh_areas')
load(strcat(dataWorkPath,patientDir,"/HeadModel/rh_areas.mat"), 'rh_areas')
numDipoles = length(lh_areas) + length(rh_areas);
message(strcat("There are ",num2str(numDipoles)," dipoles"), errFile, outFile)
toc

% get num contact Locs
load(strcat(dataWorkPath,patientDir,"/HeadModel/rh_areas.mat"), 'rh_areas')
load(strcat(dataWorkPath,patientDir,'/ElectrodeLocsData/contactLocs.mat'),'contactLocs')

tic
% open all compressed FSs, assemble into LF, and delete FSs
% open first FS to get size of LF


% set up parallelization 
numCPUs = feature('numCores'); % Get number of CPUs available
pc_storage_dir = fullfile('pc_temp_storage',getenv('SLURM_JOB_ID'));    % assign JobStorageLocation based on id of SLURM job that called this function   
mkdir(pc_storage_dir);
pc = parcluster('local');   
pc.JobStorageLocation = pc_storage_dir;
fprintf('Number of CPUs requested = %g\n',numCPUs);
poolobj = parpool(pc,numCPUs-1); % initialize pool of workers, i.e. CPUs in this node to assign tasks, leave 1 CPU to handle the overhead

message(['numCpus= ' num2str(numCPUs)], errFile, outFile)

% open all FSs and put into lead field matrix
message(strcat("Creating a compressed lead field matrix with ",num2str(length(contactLocs.node))," rows and ",num2str(numDipoles)," columns"), errFile, outFile)
FSdir = strcat(dataWorkPath,patientDir,'/LeadField/FSs_',cNumStr);
leadField = zeros(length(contactLocs.node), numDipoles, 'int16');  % make sure length is same as num contactLocs
lFRows = length(contactLocs.node);
parfor (i = 1:numDipoles, numCPUs-1)
    lFRows;
    filename = strcat(FSdir,'/HeadDipoleVoltageSampledAtRecLoc_',cNumStr,'_',num2str(i),'.mat');
    a = load(filename, 'scirunmatrix');
    try
        leadField(:,i) = a.scirunmatrix;
    catch
       message(strcat("ERROR adding FS #",num2str(i),". LH #rows is ",num2str(lFRows)," but FS length is",num2str(length(a.scirunmatrix))), errFile, outFile)
    end
    %delete(filename)
end
tic

tic
% turn into patch LF
info = whos('leadField');
sizeGB = info.bytes/(1024^3);
message(['Lead Field matrix is ' num2str(sizeGB) ' GB'], errFile, outFile)

% open patchAll matrix. 39954x39954 sparse matrix of all patches of
% specificed area.
% areas = [0.1,[0.5:0.5:9.5]]*100;
load(strcat(dataWorkPath,patientDir,'/HeadModel/PatchAllMatrices/PatchAllSt_',num2str(patchArea),'cm2.mat'),'patchAll');
% load(strcat(dataWorkPath,patientDir,'/HeadModel/PatchAllMatrices/PatchAllSt_',num2str(find((areas==patchArea)==1)),'.mat'),'patchAll');

info = whos('patchAll');
sizeGB = info.bytes/(1024^3);
message(strcat('PatchAll matrix is ',num2str(sizeGB),' GB'), errFile, outFile)
toc
% don't scale by area. area already added in compressFS step

% new method B: for loop with manual lin comps bc patchAll is so sparse
%   and leadField is integers
%   This should take less than or about 1 min. Compare to 3.3 hours with
%   sigle LF mult by full patchAll matrix !!!
tic
patchLeadField = zeros(size(leadField),'int16');
for i = 1:size(leadField,2)
    dipoles = patchAll(:,i)==1;
    patchLeadField(:,i) = sum(leadField(:,dipoles),2)*cdmd;
end
leadField = [];
size(patchLeadField)
info = whos('patchLeadField');
sizeGB = info.bytes/(1024^3);
message(strcat('patchLeadField matrix is ',num2str(sizeGB),' GB'), errFile, outFile)
toc

tic
% get threshold for ADC voltage 
% discVoltThreshInt = discVoltThresh/maxVolt * maxInt 
leadFieldThr = abs(patchLeadField) >= discVoltThresh; 
info = whos('leadFieldThr');
sizeGB = info.bytes/(1024^3);
message(['leadFieldThr matrix is ' num2str(sizeGB) ' GB'], errFile, outFile)
toc
message(num2str(size(leadFieldThr)), errFile, outFile)

% save thresholded patch LF 
save(strcat(dataWorkPath,patientDir,'/LeadField/PatchLeadField_RecLocs',cNumStr,'_thr_',tys,'.mat'),'leadFieldThr','-v7.3') % leadFieldThr

%clean up to end parallelization
delete(poolobj);
rmdir(pc_storage_dir,'s'); % delete parfor temporary files
quit;

%save([dataWorkPath 'AutoLF/LeadField/FullLeadField_int16.mat'],'leadField','-v7.3') % leadField

% temporary version before scale by area in compress FS:
%{
dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';
dataHomePath = '/hpc/group/wmglab/ged12/cluster/Optimize_sEEG_implant/';

load([dataWorkPath 'AutoLF/LeadField/FullLeadField_int16.mat'],'leadField') % leadField

info = whos('leadField');
sizeGB = info.bytes/(1024^3);
message(['Lead Field matrix is ' num2str(sizeGB) ' GB'], errFile, outFile)

% open patchAll matrix. 39954x39954 sparse matrix of all patches of
% specificed area.
areas = [0.1,[0.5:0.5:9.5]]*100;
load([dataHomePath 'PatchAllMatrices/PatchAllSt_' num2str(find((areas==patchArea)==1)) '.mat'],'patchAll');

info = whos('patchAll');
sizeGB = info.bytes/(1024^3);
message(['PatchAll matrix is ' num2str(sizeGB) ' GB']), errFile, outFile)

% open areas 
load([dataWorkPath 'AutoLF/HeadModel/lh_areas.mat'], 'lh_areas')
load([dataWorkPath 'AutoLF/HeadModel/rh_areas.mat'], 'rh_areas')
areaST = [lh_areas;rh_areas];

% scale all faces in patches by their area
patchAll = patchAll .* areaST; %% TEMPORARY!! 

% calculate patch forward solutions = clipped lead field * patchAll
% oops need 40,000 x 40,000 matrix for correct matrix mult
size(leadField)
size(patchAll)
tic
leadField = single(leadField);
size(leadField)
info = whos('leadField');
sizeGB = info.bytes/(1024^3);
message(['leadField matrix is ' num2str(sizeGB) ' GB'], errFile, outFile)
toc

tic
% original method: takes 3.3 hours!
% patchLeadField = leadField * full(patchAll);
% new method A: for loop
% patchLeadField = zeros(size(leadField),'int16');
% for i = 1:size(leadField,2)
%     dipoles = find(patchAll(:,i)==1);
%     patchLeadField(:,i) = sum(leadField(:,dipoles),2);
% end
% new method B: convert LF to doubles, use sparse patchAll
patchLeadField = double(leadField) * patchAll;

size(patchLeadField)
info = whos('patchLeadField');
sizeGB = info.bytes/(1024^3);
message(['patchLeadField matrix is ' num2str(sizeGB) ' GB'], errFile, outFile)
toc
leadField = [];
tic
% get threshold for ADC voltage 
discVoltThreshInt = discVoltThresh/maxVolt * maxInt 
leadFieldThr = abs(patchLeadField) >= discVoltThresh; 
info = whos('leadFieldThr');
sizeGB = info.bytes/(1024^3);
message(['leadFieldThr matrix is ' num2str(sizeGB) ' GB'], errFile, outFile)
toc
size(leadFieldThr)

% save thresholded patch LF 
save([dataWorkPath 'AutoLF/LeadField/PatchLeadField_thr_B.mat'],'leadFieldThr','-v7.3') % leadFieldThr
%}

end


function message(strMessage, errFile, outFile)
    disp(strMessage)
    if contains(strMessage,"ERROR")
        fprintf(errFile,'%s\n',strcat("ERROR MESSAGE from createLF: ",strMessage));
    end
    fprintf(outFile,'%s\n',strcat("createLF: ",strMessage));
end


