% get midline
% 10/21/21

function [n, p, midlinePointField, midlinePlaneField, midlinePlot] = getMidline(lh_normals, rh_normals, lh_areas, rh_areas, lh, rh)
% given LH and RH normal vectors and areas, and the LH & RH triangulation surfaces
% take weighted average of all normal vectors on both hemispheres
%   to get plane of midline, defined by normal vector and one point
% return the normal vector and one point as 3x1 arrays

% get distances between all LH points and all RH points

dists = zeros(length(lh_areas),length(rh_areas));
for x = 1:length(lh_areas)
    lh_pt = lh_normals.node(:,x);
    dists(x,:) = sum((rh_normals.node-lh_pt).^2,1);
end
% get LH and RH points with small distance
th = 10; %mm
[LH_is, RH_is] = find(dists<th);
midlinePts = [lh_normals.node(:,LH_is), rh_normals.node(:,RH_is)];

[n,~,p] = affine_fit(midlinePts');
if size(n,1) == 1
    n = n';
end
if size(p,1) == 1
    p = p';
end

% get midline points for visualization
midlinePointField.node = midlinePts;
midlinePointField.field = zeros(1,length(midlinePts));

% get midline points on plane for visualization
normals = [lh_normals.node, rh_normals.node];
z = linspace(min(normals(3,:)),max(normals(3,:)),10);
y = linspace(min(normals(2,:)),max(normals(2,:)),10);
[z2,y2]=meshgrid(z,y);
d = -p'*n;
x2 = (-n(2)*y2 - n(3)*z2 - d)/n(1);
midlinePlaneField.node = [reshape(x2,10*10,1),reshape(y2,10*10,1),reshape(z2,10*10,1)];
midlinePlaneField.field = zeros(100,1);

% plot but dont display
midlinePlot = figure('visible','off');
clf
trimesh(lh,'FaceAlpha',0.1)
hold on
trimesh(rh,'FaceAlpha',0.1)
normals = [lh_normals.node, rh_normals.node];
z = linspace(min(normals(3,:)),max(normals(3,:)),10);
y = linspace(min(normals(2,:)),max(normals(2,:)),10);
[z2,y2]=meshgrid(z,y);
% ax + by + cz + d = 0
d = -p'*n;
x2 = (-n(2)*y2 - n(3)*z2 - d)/n(1);
% plot plane
surf(x2,y2,z2,'FaceAlpha',0.1);

% plot midlinePts
scatter3(midlinePts(1,:),midlinePts(2,:),midlinePts(3,:),'filled')


end

function [n,V,p] = affine_fit(X)
    %Computes the plane that fits best (lest square of the normal distance
    %to the plane) a set of sample points.
    %INPUTS:
    %
    %X: a N by 3 matrix where each line is a sample point
    %
    %OUTPUTS:
    %
    %n : a unit (column) vector normal to the plane
    %V : a 3 by 2 matrix. The columns of V form an orthonormal basis of the
    %plane
    %p : a point belonging to the plane
    %
    %NB: this code actually works in any dimension (2,3,4,...)
    %Author: Adrien Leygue
    %Date: August 30 2013
    
    %the mean of the samples belongs to the plane
    p = mean(X,1);
    
    %The samples are reduced:
    R = bsxfun(@minus,X,p);
    %Computation of the principal directions if the samples cloud
    [V,D] = eig(R'*R);
    %Extract the output from the eigenvectors
    n = V(:,1);
    V = V(:,2:end);
end