% Generate all scripts with patient specific file paths and parameters,
%   and initialize data storage structure
% Grace Dessert

% for cluster running only 
% make sure set parameters in each Patient_{ID}.config file
% make sure to set running configurations and Patient IDs in the
% initOpt_sEEG.slurm file. 

function initializePipelineRun(patientID)

% get parameters from Patient_#.config file and write all scripts
fileName = strcat("MainPipeline/Patient_",num2str(patientID),".config");
if isfile(fileName)
    lines = readlines(fileName);
    disp(strcat("Reading ",fileName," file!"));
else
    disp(strcat("Error! ",fileName, " not found!"));
end
queryParamTags = ["dataWorkPath", "dataHomePath","outFile","errFile","cdmds","patchAreas","ROIs"];
queryParamValues = cell(1,length(queryParamTags));
% for each parameter, search file and set value
for q = 1:length(queryParamTags)
    queryTag = queryParamTags(q);
    for i = 1:length(lines)
        if length(lines{i})<2 || lines{i}(1) == "#"
            continue
        end
        if contains(lines{i},queryTag)
            disp(lines{i})
            disp(strcat(queryTag, " found"))
            queryLineSplit = split(lines{i},"=");
            queryValue = strip(queryLineSplit{2});
            queryParamValues{q} = convertCharsToStrings(queryValue);
            break
        end
    end
    if i == length(lines)
        disp(strcat("ERROR: ",queryTag," not found!!"))
    end
end

% Set parameters for whole pipeline!
patientDir = strcat('Patient_',num2str(patientID));
dataWorkPath = queryParamValues{1};
dataHomePath = queryParamValues{2};
outFileName = strrep(queryParamValues{3},"$$HOMEDIR$$",dataHomePath);
outFileName = strrep(outFileName,"$$PATDIR$$",patientDir);
errFileName = strrep(queryParamValues{4},"$$HOMEDIR$$",dataHomePath);
errFileName = strrep(errFileName,"$$PATDIR$$",patientDir);
cdmds = extractBetween(queryParamValues{5},2,strlength(queryParamValues{5})-1);
cdmds = str2double(strip(split(cdmds,",")))';
patchAreas = extractBetween(queryParamValues{6},2,strlength(queryParamValues{6})-1);
patchAreas = str2double(strip(split(patchAreas,",")))';
ROIs = extractBetween(queryParamValues{7},2,strlength(queryParamValues{7})-1);
ROIs = strip(split(ROIs,","))';

% Example:
% dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';
% dataHomePath = '/hpc/group/wmglab/ged12/cluster/Optimize_sEEG_implant/electrode-config-optimization-seeg/';
% cdmds = [1, 1, 0.2, 0.2]; %nAm/mm^2
% patchAreas = [5, 9.5, 5, 9.5]; % cm^2
% ROIs = ["L10","L10","R10","R10","LTL","LH","WC"]; % array of length 7


% store errors in errFile 
disp(strcat("Opening ",errFileName))
errFile = fopen(errFileName,'wt');
% store messages in outFile
disp(strcat("Opening ",outFileName))
outFile = fopen(outFileName,'wt');


% generate all scripts with patient specific file paths and parameters!
addpath('ContentScripts')

% make data directories!
mkdir(strcat(dataWorkPath,patientDir));
mkdir(strcat(dataHomePath,patientDir,"/Out"));
mkdir(strcat(dataHomePath,patientDir,"/Test"));
mkdir(strcat(dataWorkPath,patientDir,"/DipoleAutoScripts"));
mkdir(strcat(dataWorkPath,patientDir,"/DipoleNetworks"));
mkdir(strcat(dataWorkPath,patientDir,"/HeadModel"));
mkdir(strcat(dataWorkPath,patientDir,"/DipoleNetworks"));
mkdir(strcat(dataWorkPath,patientDir,"/LeadField"));
mkdir(strcat(dataWorkPath,patientDir,"/LeadField/FSs"));
mkdir(strcat(dataWorkPath,patientDir,"/LeadField/FSs_C1"));
mkdir(strcat(dataWorkPath,patientDir,"/LeadField/FSs_C2"));
mkdir(strcat(dataWorkPath,patientDir,"/LeadField/FSs_C3"));
mkdir(strcat(dataWorkPath,patientDir,"/ElectrodeLocsData"));
mkdir(strcat(dataWorkPath,patientDir,"/ElectrodeCollisionMatrices"));
mkdir(strcat(dataWorkPath,patientDir,"/HeadModel/PatchAllMatrices"));
mkdir(strcat(dataWorkPath,patientDir,"/recArea"));
mkdir(strcat(dataWorkPath,patientDir,"/OptSolutions"));
mkdir(strcat(dataWorkPath,patientDir,"/OptSolutions/VisualizationDat"));


%%
% set parameters in all scripts

message('Writing Parameters', errFileName, outFileName)
writeParameters(patientID, cdmds, patchAreas, dataWorkPath, dataHomePath, ROIs, outFileName, errFileName)


end

function message(strMessage, errFileName, outFileName)
    errFile = fopen(errFileName,'a');
    outFile = fopen(outFileName,'a');
    if contains(strMessage,"ERROR")
        fprintf(errFile,'%s\n',strcat("ERROR MESSAGE from initializePipelineRun: ",strMessage));
    end
    fprintf(outFile,'%s\n',strcat("initializePipelineRun: ",strMessage));
end

function lines = readlines(fileName)
    % Opens the file
    fid = fopen(fileName,'rt');

    maxl = -1; % read whole file
    lines = textscan(fid,'%s',maxl,'delimiter', '\n', 'whitespace','');
    if ~isempty(lines)
        lines = lines{1};
    end
    % Close file
    fclose(fid);
end

