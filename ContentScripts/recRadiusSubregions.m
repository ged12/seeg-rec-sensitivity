% ANOVA to determine rec radius across cortex subregions

% rec radius data gives % of recordability within radius across 40 distances, 105 patches, 3
% recordability thresholds, and 12 patients. 
dataHomePath = '/Volumes/Seagate Backup Plus Drive/disc-signal-grill/Optimize_sEEG_implant/seeg-rec-sensitivity/';
dataWorkPath = '/Volumes/Seagate Backup Plus Drive/disc-signal-grill/Optimize_sEEG_implant/seeg-rec-sensitivity/';

% use median source type: 10cm2_0.465cdmd
load(strcat(dataHomePath,'RawData/RecRadius/percRecInRadius_10cm2_0.465cdmd.mat'),'percRecInRadius')

% load(strcat(dataHomePath,patientDir,"/Inputs/lh_brain_WithLabels.mat"),'scirunfield')
% lh = stlread(strcat(dataHomePath,patientDir,"/Inputs/lh_brain_final.stl"));
% load(strcat(dataHomePath,patientDir,"/Inputs/rh_brain_WithLabels.mat"),'scirunfield')
% load(strcat(dataWorkPath,patientDir,'/ElectrodeLocsData/recRadiusContactLocs.mat'),'allSelContacts');
% subLocs = unique(scirunfield.field);

% using 20% recording sensitivity, collapse across distance so we have max
% distance across all other variables
patIDs = [2,3,7,19,20,24,25,26,27,30,31,33];
distsPlot = [0:2.5:100]; % mm
thrs = [200,500,1000];
maxRadius = zeros(105,3,12);
maxRadius = squeeze(sum(percRecInRadius>0.2,1));

% select recordability threshold and plot for a single patient
patI = 7;
disp(strcat("selected patient: ",num2str(patIDs(patI))))

for thrI = 1%:3
radAcrossPatch = maxRadius(:,thrI,patI);
regions = reshape(repmat(1:35,3,1),1,[]);
figure(1)
clf
hold on
scatter(regions,radAcrossPatch,'filled')
yticks(distsPlot)
grid on

% do this for each patient
radAcrossPatchAllPats = [];
regionNames = [];
patNames = [];
for patI = 1:12
    radAcrossPatch = maxRadius(:,thrI,patI);
    radAcrossPatchAllPats = [radAcrossPatchAllPats;radAcrossPatch];
    regions = reshape(repmat(1:35,3,1),1,[]);
    regionNames = [regionNames,regions];
    pats = repmat(patI,105,1);
    patNames = [patNames;pats];
end
[p,tbl,stats] = anovan(radAcrossPatchAllPats,{regionNames,patNames},'varnames',{'subregion','patient'});

% save test statistics in table
thrsS = ["200","500","1000"];
writecell(tbl,strcat(dataHomePath,'SourceData/TableS4_',num2str(thrI),'.xls'))


end


%% if significance, run post hoc testing

[c, m, h, gnames] = multcompare(stats,'Dimension',[1],'ctype','bonferroni','alpha',0.05)
cAll = c;
% LATERAL REGIONS:
    % 10 is turquoise. inferior lateral temporal lobe 
    %   (200 µV case shows significance with region 9: inferior parietal)
    %   NO other pairs of any temporal regions have any significance across ANY brain regions. 
    % 16 is green. middle lateral temporal lobe
    % 31 is orange. superior temporal lobe
% MESIAL REGIONS:
    % 17 is green. para-hippocampal 
    % 7 is turquoise. entorhinal 
    % 8 is blue. fusiform. very bottom of the temporal lobe
% OTHER TEMPORAL REGIONS:
    % 35 is red. transverse temporal
    % 34 is red-orange. temporal pole 

% NO PAIRS between these subregions had any significant difference. 
% get summary statistics for temporal regions


% select only temporal subregions
temporalRegions = [10, 16, 31, 34, 35, 17, 7, 8];
temporalRegionNames = {'inferior_temporal',...
    'middle_temporal', ...
    'superior_temporal', ...
    'temporal_pole', ...
    'transverse_temporal', ...
    'para_hippocampal', ...
    'entorhinal', ...
    'fusiform'};
goodInds = find(ismember(c(:,1),temporalRegions) & ismember(c(:,2),temporalRegions));
c = c(goodInds,:);


% make 2D table
for i = 1:length(c)
    c(i,1) = find(temporalRegions==c(i,1));
    c(i,2) = find(temporalRegions==c(i,2));
end

tValues = zeros(length(temporalRegions));
pValues = zeros(length(temporalRegions));

for i = 1:length(c)
    tValues(c(i,1),c(i,2)) = c(i,4);
    tValues(c(i,2),c(i,1)) = c(i,4);
    pValues(c(i,1),c(i,2)) = c(i,6);
    pValues(c(i,2),c(i,1)) = c(i,6);
end

%
% make table and save data
% save test statistics in table
tblT = round(tblT,5)
tblT = array2table(tValues,"RowNames", temporalRegionNames', "VariableNames", temporalRegionNames);
tblP = array2table(pValues,"RowNames", temporalRegionNames, "VariableNames", temporalRegionNames);
writetable(tblT,strcat(dataHomePath,'SourceData/TableS5_200µV_PostHoc_Temporal_TValues.xls'))
writetable(tblP,strcat(dataHomePath,'SourceData/TableS5_200µV_PostHoc_Temporal_PValues.xls'))


%%

% tbl=table2cell(tbl);
for i = 1:length(tbl.Group)
    tbl(i,1) = find(temporalRegions==tbl.Group(i));
     % tbl(i,2) = find(temporalRegions==tbl.2(i));
end

% writetable(tbl,'SourceData/Figure3fStats.xls')

disp(tbl)




% The only significane across all pairs is region 9: inferior parietal
%   with 4 regions. (inferior lateral temporal lobe, and three outside of
%   temporal lobe)

%%
% since all are significant, we can subdivide

% max rec radius (collapse across distance) 

% single source type 
cdmdI = 3;
patchAI = 1;

cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
dataHomePath = '';
distsPlot = [0:2.5:100]; % mm

recPercent = 10;

datCompare = zeros(4, 3 ,105); % pat, thrI, contacts (105 w 3 reps)
L1 = cell(4, 3, 105); 
L2 = cell(4, 3, 105); 
L3 = cell(4, 3, 105); 
L4 = cell(40, 4, 105); 
L5 = cell(40, 4, 105); 

for cI = cdmdI %1:3
    for pI = patchAI % 1:3
        cdmd = unique(cdmds);
        cdmd = cdmd(cI); % nAm/mm^2
        patchArea = unique(patchAreas);
        patchArea = patchArea(pI); % cm^2
        tys = strcat(num2str(patchArea),"cm2_",num2str(cdmd),"cdmd")
        tys_nice = strcat(num2str(patchArea)," cm^2, ",num2str(cdmd)," cdmd,");
        cNumStrings = ["C1","C2","C3"]; % 100, 500, and 1000 µV threshold
        load(strcat(dataHomePath,'RecRadius/percRecInRadius_',tys,'.mat'),'percRecInRadius')
        
        percRecInRadius_r = permute(percRecInRadius,[1,3,4,2]); % mean across contacts
        for thrI = 1:3
            lastRecInd = squeeze(percRecInRadius_r(:,thrI,:,:)*100>recPercent);
            datCompare(:,thrI,:) = squeeze(max(lastRecInd .* distsPlot(2:end)',[],1)/10);
        end
    end
end


% for d = 1:40
    for pat = 1:4
%         for c = 1:3
            for thrI = 1:3
                for co = 1:105
%                     L1{d,pat,co} = num2str(d);
                    L1{pat,thrI,co} = num2str(pat);
                    L2{pat,thrI,co} = num2str(thrI);
%                     L4{d,pat,co} = num2str(p);
                    L3{pat,thrI,co} = num2str(ceil(co/3));
                end
            end
%         end
    end
% end

datCompare = reshape(datCompare,[],1);
L1 = reshape(L1,[],1);
L2 = reshape(L2,[],1);
L3 = reshape(L3,[],1);
% L4 = reshape(L4,[],1);
% L5 = reshape(L5,[],1);
%

[p,tbl, stats] = anovan(datCompare,{L1,L2,L3});



%%
% since all are significant, we can subdivide

% max rec radius (collapse across distance) 

% single source type and threshold
cdmdI = 2;
patchAI = 3;
thrI = 2;

cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
dataHomePath = '';
distsPlot = [0:2.5:100]; % mm

recPercent = 10;

datCompare = zeros(4, 105); % pat, contacts (105 w 3 reps)
L1 = cell(4, 105); 
L2 = cell(4, 105); 
L3 = cell(4, 105); 
L4 = cell(40, 4, 105); 
L5 = cell(40, 4, 105); 

for cI = cdmdI %1:3
    for pI = patchAI % 1:3
        cdmd = unique(cdmds);
        cdmd = cdmd(cI); % nAm/mm^2
        patchArea = unique(patchAreas);
        patchArea = patchArea(pI); % cm^2
        tys = strcat(num2str(patchArea),"cm2_",num2str(cdmd),"cdmd");
        tys_nice = strcat(num2str(patchArea)," cm^2, ",num2str(cdmd)," cdmd,");
        cNumStrings = ["C1","C2","C3"]; % 100, 500, and 1000 µV threshold
        load(strcat(dataHomePath,'RecRadius/percRecInRadius_',tys,'.mat'),'percRecInRadius')
        
        percRecInRadius_r = permute(percRecInRadius,[1,3,4,2]); % mean across contacts
        lastRecInd = squeeze(percRecInRadius_r(:,thrI,:,:)*100>recPercent);
        datCompare = squeeze(max(lastRecInd .* distsPlot(2:end)',[],1)/10);
    end
end


% for d = 1:40
    for pat = 1:4
%         for c = 1:3
%             for p = 1:3
                for co = 1:105
%                     L1{d,pat,co} = num2str(d);
                    L1{pat,co} = num2str(pat);
%                     L3{d,pat,co} = num2str(c);
%                     L4{d,pat,co} = num2str(p);
                    L2{pat,co} = num2str(ceil(co/3));
                end
%             end
%         end
    end
% end

datCompare = reshape(datCompare,[],1);
L1 = reshape(L1,[],1);
L2 = reshape(L2,[],1);
L3 = reshape(L3,[],1);
% L4 = reshape(L4,[],1);
% L5 = reshape(L5,[],1);
%

[p,tbl, stats] = anovan(datCompare,{L1,L2});


%%
results = multcompare(stats,'Dimension',[2 3])

%%
% summary data of max rec radius across contacts
count = 1;
datPats = zeros(9,3,4);
datLabels_1 = cell(9,3,4);
datLabels_2 = cell(9,3,4);
datLabels_3 = cell(9,3,4);
sourceInd = 1;
pC = 1;
for patI = [2,7,19,20]
    count = 1;
    load(strcat(dataHomePath,'RecRadius/datSummary_maxRadAcrossSources',num2str(patI),'.mat'),'caseMetrics')
    for thrI = 1:3
        datPats(:,thrI,pC) = table2array(caseMetrics{thrI}(:,4))';
        for s = 1:9
            datLabels_1{s,thrI,pC} = num2str(s);
            datLabels_2{s,thrI,pC} = num2str(thrI);
            datLabels_3{s,thrI,pC} = num2str(pC);
        end
    end
    pC = pC + 1;
end
datPats = reshape(datPats,[],1);
datLabels_1 = reshape(datLabels_1,[],1);
datLabels_2 = reshape(datLabels_2,[],1);
datLabels_3 = reshape(datLabels_3,[],1);
% columns be different patients, rows be all data (105x4)
% datPats(:,
[p,tbl] = anovan(datPats,{datLabels_1, datLabels_2, datLabels_3});

% The column Prob>F shows the p-values for the patients, then for the
% contacts, then for the interaction. If the first p-value is <0.05, we 

%%
% concat all mean pat data of max rec radius, make into table

patMeanMaxRad = zeros(9,6,3);
pcount = 1;
for pI = [2,7,19,20]
    load(strcat(dataHomePath,'RecRadius/datSummary_maxRadAcrossSources',num2str(pI),'.mat'),'caseMetrics')
    for c = 1:3
        patMeanMaxRad(:,pcount,c) = table2array(caseMetrics{c}(:,4));
    end
    pcount = pcount + 1;
end

patMeanMaxRad(:,5,1) = range((patMeanMaxRad(:,1:4,1)),2);
patMeanMaxRad(:,5,2) = range((patMeanMaxRad(:,1:4,2)),2);
patMeanMaxRad(:,5,3) = range((patMeanMaxRad(:,1:4,3)),2);
patMeanMaxRad(:,6,1) = mean((patMeanMaxRad(:,1:4,1)),2);
patMeanMaxRad(:,6,2) = mean((patMeanMaxRad(:,1:4,2)),2);
patMeanMaxRad(:,6,3) = mean((patMeanMaxRad(:,1:4,3)),2);

for c = 1:3
    T = array2table(patMeanMaxRad(:,:,c));
    T.Properties.VariableNames = {'Patient_2','Patient_7','Patient_19','Patient_20','range','avg'};
    T(:,7) = arraymetrics(:,6);
    save(strcat('RecRadius/meanMaxRad_allPats',num2str(c),'.mat'),'T')
end



