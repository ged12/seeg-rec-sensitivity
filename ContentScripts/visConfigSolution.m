% visualize electrode configuration
% and get implanted configuration data
% Grace Dessert
% 13 Sep 2022

function visConfigSolution(sourceIforCrossAnalyze)

% $$ UPDATE PARAMETERS $$
cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
ROIs = ["clinicianROI","LTL","LH"]; % array 
dataWorkPath = '';
dataHomePath = '';
patientID = 2; 
outFileName = 'temp_testOut.txt';
errFileName = 'temp_testErr.txt';

patientDir = ['Patient_' num2str(patientID)];

% store errors in errFile 
errFile = fopen(errFileName,'a');
% store messages in outFile
outFile = fopen(outFileName,'a');

runOnlyImpData = false;

%%
% open other files  
cortexSurf = stlread(strcat(dataHomePath,patientDir,'/Inputs/cortexSurf.stl'));
implantedELocField = load(strcat(dataHomePath,patientDir,'/Inputs/ContactLocations.mat'),'scirunfield');
implantedELocations = implantedELocField.scirunfield.node;
load(strcat(dataWorkPath,patientDir,'/HeadModel/cortex_normals.mat'),'cortex_normals');
load(strcat(dataWorkPath,patientDir,'/OptSolutions/ROIs.mat'),'patches')
load(strcat(dataWorkPath,patientDir,'/ElectrodeLocsData/normals_reInd.mat'),'normals_reInd')
load(strcat(dataWorkPath,patientDir,'/ElectrodeLocsData/slocs_reInd.mat'),'slocs_reInd')

% select case(s) desired 
ROI_is = reshape(repmat([1:length(ROIs)],length(cdmds),1),[],1)';
sourceNums = reshape(repmat([1:length(cdmds)]',length(ROIs),1),1,[]);

if sourceIforCrossAnalyze == 0
    crossSourceAnalyzeCase = '';
else
    crossSourceAnalyzeCase = strcat(num2str(patchAreas(sourceIforCrossAnalyze)),"cm2_",num2str(cdmds(sourceIforCrossAnalyze)),"cdmd")
end

%%
% cell of already open recDipolesbyE matrices
recDipolesbyEs = {};
openRecDipoleMatrices = [];
visDataConfigs = cell(length(ROI_is)*6*6,1);
visDataImplantConfig = cell(length(ROI_is)*6*6,1);
imp_done_cases = [];

%%
thrPrioritys = ["1000_500_200_thrPriority","200_500_1000_thrPriority","500_200_1000_thrPriority","1000_200_500_thrPriority","500_1000_200_thrPriority","200_1000_500_thrPriority"];
for thrPriI = 1:6
for i = 1:length(ROI_is)
    thrPriority = thrPrioritys(thrPriI);
    
    ROI_i = ROI_is(i);
    sourceNum = sourceNums(i);
    patchArea = patchAreas(sourceNum);
    cdmd = cdmds(sourceNum);
    tys = strcat(num2str(patchArea),"cm2_",num2str(cdmd),"cdmd");
    case_id = strcat(tys,'_ROI_',ROIs(ROI_i),'_singleMap_',thrPriority);
    
    for mapStrIndex = 1:6 % int 1-6 indicating threshold (200, 500, 1000) and single or double mapping
        
        saveIndex = (thrPriI-1)*(length(ROI_is))*6 + (i-1)*6 + mapStrIndex;

        ROI_i = ROI_is(i);    

        message(" ", errFile, outFile)
        message(strcat("Generating visualization fields for case: ",case_id), errFile, outFile)

        sOrD = mod(mapStrIndex,2);
        if sOrD == 1
            mapStrStr = "single";
        else
            mapStrStr = "double";
        end
        mapStrStr = strcat(mapStrStr," mapping at ");
        
        thrsOrder = extractBetween(thrPriority,1,12);
        thrsOrder = split(thrsOrder,"_");
        thrs = flip(str2double(thrsOrder));

        % thrs = [200,500,1000];
        
        thrI = floor((mapStrIndex+1)/2);
        mapStrStr = strcat(mapStrStr,num2str(thrs(thrI))," µV threshold");
        full_case_id = strcat(case_id,mapStrStr);
        if ~isempty(crossSourceAnalyzeCase)
            full_case_id = strcat(full_case_id,"_crossAnalyze",crossSourceAnalyzeCase);
        end
        message(strcat("with recordability strength of ",mapStrStr), errFile, outFile);

        % opening recDipolesbyE file (or note it is already open)
        if thrs(thrI) == 200
            cNumStr = "C1";
        elseif thrs(thrI) == 500
            cNumStr = "C2";
        else
            cNumStr = "C3";
        end
        if runOnlyImpData
            continue %***************
        end
        recDipoleMFN = strcat(dataWorkPath,patientDir,'/recArea/recDipolesbyE_recLocs',cNumStr,'_',crossSourceAnalyzeCase,'.mat');
        indOpen = [];
        if ~isa(openRecDipoleMatrices,'numeric')
            indOpen = find(recDipoleMFN==openRecDipoleMatrices);
        end
        if isempty(indOpen)
            load(recDipoleMFN,'recDipolesbyE')
            size(recDipolesbyE)
            info = whos('recDipolesbyE');
            sizeGB = info.bytes/(1024^3);
            disp(['Electrode-dipole recordability matrix is ' num2str(sizeGB) ' GB'])
            recDipolesbyEs = [recDipolesbyEs; {recDipolesbyE}];
            openRecDipoleMatrices = [openRecDipoleMatrices; recDipoleMFN];
        else
            recDipolesbyE = recDipolesbyEs{indOpen};
        end

        % open soln config file
        try
            fP = strcat(dataWorkPath,patientDir,'/OptSolutions/NBS_solution_integratedCost',case_id,'.mat');
            load(fP, 'solConfigs')
        catch
           message(strcat("ERROR loading opt solution for case ",case_id), errFile, outFile)
           continue
        end
        
        solnConfigEInds = solConfigs{1,1};
        
        % generate fields
        [rec_tissue_by_E, configField] = getVisFieldsConfig(solnConfigEInds, recDipolesbyE, normals_reInd, slocs_reInd, cortex_normals, sOrD);
        
        totalDipoles = length(patches.dipoles{ROI_i});
        % using only trial 1        
        if isempty(crossSourceAnalyzeCase)
            numMapDipoles = solConfigs{1,2}(:,mapStrIndex);
        else
            numMapDipoles = zeros(1,size(rec_tissue_by_E.field,1));
            for k = 1:size(rec_tissue_by_E.field,1)
                numMapDipoles(k) = sum(sum(rec_tissue_by_E.field(1:k,patches.dipoles{ROI_i}'),1)>0);
            end
        end  
        percCover = numMapDipoles(end) / totalDipoles;
        configConvergROIPercent = numMapDipoles ./ totalDipoles;
        
        visDataConfigs{saveIndex}.ROI_i = ROI_i;
        visDataConfigs{saveIndex}.full_case_id = full_case_id;
        visDataConfigs{saveIndex}.rec_tissue_by_E = rec_tissue_by_E;
        visDataConfigs{saveIndex}.configField = configField;
        visDataConfigs{saveIndex}.percCover = percCover;
        visDataConfigs{saveIndex}.config = solnConfigEInds;
        visDataConfigs{saveIndex}.percMapROI = configConvergROIPercent;
    
    end
    %%
    message("Trying to get implanted config data", errFile, outFile);
    % get data for implanted config if we haven't already. compute all threshold cases at once
    % only include electrodes that are in ROI
    impCaseId = strcat(tys,"_",thrPriority,"_ROI",ROIs(ROI_i));
    if isempty(imp_done_cases) || isempty(find(imp_done_cases==impCaseId, 1))        
        orderThrsCost = thrs;
        [rec_tissueDat_imp_allThr, configConvergROIPercent_imp_allThr, eAssocNOrder, config] = getImplantedElectrodeData(crossSourceAnalyzeCase, dataWorkPath, patientDir, patches, errFile, outFile, orderThrsCost, dataHomePath, ROI_i);
        imp_done_cases = [imp_done_cases, impCaseId];
        visDataImplantConfig{length(imp_done_cases)}.imp_case_id = impCaseId;
        visDataImplantConfig{length(imp_done_cases)}.eAssocNOrder = eAssocNOrder;
        visDataImplantConfig{length(imp_done_cases)}.rec_tissueDat_imp_allThr = rec_tissueDat_imp_allThr;
        visDataImplantConfig{length(imp_done_cases)}.percMapROI_imp_allThr = configConvergROIPercent_imp_allThr;
        visDataImplantConfig{length(imp_done_cases)}.config = config;
    end
    
end
end

if ~isempty(crossSourceAnalyzeCase)
    save(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataImplantConfig_singleMap_crossAnalyze',crossSourceAnalyzeCase,'.mat'),'visDataImplantConfig','-v7.3')
    if ~runOnlyImpData
        save(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_crossAnalyze',crossSourceAnalyzeCase,'.mat'),'visDataConfigs','-v7.3')
    end
else
    save(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataImplantConfig_singleMap.mat'),'visDataImplantConfig','-v7.3')
    if ~runOnlyImpData
        save(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap.mat'),'visDataConfigs','-v7.3')
    end
end


end

function [rec_tissueDat_imp_allThr, configConvergROIPercent_imp_allThr, eAssocNOrder, config] = getImplantedElectrodeData(tys, dataWorkPath, patientDir, patches, errFile, outFile, orderThrsCost, dataHomePath, ROI_i)

% open all thresholds
% cNumStrings = ["C1","C2","C3"];

for i = 1:3
    if orderThrsCost(i) == 200
        cNumStr = "C1";
    elseif orderThrsCost(i) == 500
        cNumStr = "C2";
    else
        cNumStr = "C3";
    end
    
    load(strcat(dataWorkPath,patientDir,'/recArea/recDipolesbyE_implantedLocs',cNumStr,'_',tys,'.mat'),'recDipolesbyE_implanted')
    if i == 1 
        recDipolesbyE_C1_og = recDipolesbyE_implanted;
    elseif i == 2
        recDipolesbyE_C2_og = recDipolesbyE_implanted;
    else
        recDipolesbyE_C3_og = recDipolesbyE_implanted;
    end
end
%
% get ROI info
ROI_dips = patches.dipoles{ROI_i};
ROI_locs = patches.patchfield{ROI_i}.Points;
%%
% clip to only ROI electrodes
% get distances of all contacts to all ROI points.
% keep all electrodes that have min dist < 5mm
load(strcat(dataWorkPath,patientDir,'/recArea/cToE_impC1_',tys,'.mat'),'cToE_imp')
load(strcat(dataHomePath,patientDir,'/Inputs/ContactLocations.mat'),'scirunfield')

ROIEs = getValidImpEs(cToE_imp, scirunfield, ROI_locs)

% clip data by valid electrodes for this ROI
recDipolesbyE_implanted = recDipolesbyE_implanted(ROIEs,:);
recDipolesbyE_C1_og = recDipolesbyE_C1_og(ROIEs,:);
recDipolesbyE_C2_og = recDipolesbyE_C2_og(ROIEs,:);
recDipolesbyE_C3_og = recDipolesbyE_C3_og(ROIEs,:);

recDipolesbyEAll = {recDipolesbyE_C1_og,recDipolesbyE_C2_og,recDipolesbyE_C3_og};

message(strcat("Implanted config has ",num2str(length(ROIEs))," valid electrodes for this ROI"), errFile, outFile)
if isempty(ROIEs)
   message(strcat("Moving on, no imp config to build"), errFile, outFile)
   rec_tissueDat_imp_allThr = [];
   configConvergROIPercent_imp_allThr = [];
   eAssocNOrder = [];
   config = [];
   return
end
%%
message(strcat("Clinical ROI has ",num2str(length(ROI_dips))," dipoles"), errFile, outFile)

recDipolesbyE_C1_1 = recDipolesbyE_C1_og(:,ROI_dips);
recDipolesbyE_C1_2 = recDipolesbyE_C1_og(:,ROI_dips);
recDipolesbyE_C2_1 = recDipolesbyE_C2_og(:,ROI_dips);
recDipolesbyE_C3_1 = recDipolesbyE_C3_og(:,ROI_dips);
recDipolesbyE_C2_2 = recDipolesbyE_C2_og(:,ROI_dips);
recDipolesbyE_C3_2 = recDipolesbyE_C3_og(:,ROI_dips);

E = size(recDipolesbyE_implanted,1);
D = size(recDipolesbyE_implanted,2);

remDipolesTrack = [];
clipped_electrodeInds = [1:E];
%
% pick one electrode at a time until we have no more
bestFirstOptions = getBestEs(recDipolesbyE_C1_1, recDipolesbyE_C1_2, recDipolesbyE_C2_1, recDipolesbyE_C2_2, recDipolesbyE_C3_1, recDipolesbyE_C3_2);
% bestFirstOptions = find(sum(recDipolesbyE,2)==max(sum(recDipolesbyE,2)));
bestFirst = bestFirstOptions(randi(length(bestFirstOptions)));
currConfig = bestFirst;
clipped_electrodeInds(bestFirst) = [];

while 1
    recDipoles = logical(sum(recDipolesbyE_C1_1(currConfig,:),1)>=1);

    remRecDipolesbyE_C1_1 = recDipolesbyE_C1_1;
    remRecDipolesbyE_C1_1(:,recDipoles) = [];

    recDipoles = logical(sum(recDipolesbyE_C1_2(currConfig,:),1)>=2);
    remRecDipolesbyE_C1_2 = recDipolesbyE_C1_2;
    remRecDipolesbyE_C1_2(:,recDipoles) = [];

    recDipoles = logical(sum(recDipolesbyE_C2_1(currConfig,:),1)>=1);
    remRecDipolesbyE_C2_1 = recDipolesbyE_C2_1;
    remRecDipolesbyE_C2_1(:,recDipoles) = [];

    recDipoles = logical(sum(recDipolesbyE_C2_2(currConfig,:),1)>=2);
    remRecDipolesbyE_C2_2 = recDipolesbyE_C2_2;
    remRecDipolesbyE_C2_2(:,recDipoles) = [];

    recDipoles = logical(sum(recDipolesbyE_C3_1(currConfig,:),1)>=1);
    remRecDipolesbyE_C3_1 = recDipolesbyE_C3_1;
    remRecDipolesbyE_C3_1(:,recDipoles) = [];

    recDipoles = logical(sum(recDipolesbyE_C3_2(currConfig,:),1)>=2);
    remRecDipolesbyE_C3_2 = recDipolesbyE_C3_2;
    remRecDipolesbyE_C3_2(:,recDipoles) = [];

    remDsThisRow = [size(remRecDipolesbyE_C1_1,2),size(remRecDipolesbyE_C1_2,2),size(remRecDipolesbyE_C2_1,2),size(remRecDipolesbyE_C2_2,2),size(remRecDipolesbyE_C3_1,2),size(remRecDipolesbyE_C3_2,2)];
    remDipolesTrack = [remDipolesTrack; remDsThisRow];
    
    if length(currConfig)==E
        disp('All electrodes added!')
        break
    end
    
    remE = [currConfig];
    remRecDipolesbyE_C1_1(remE,:) = [];
    remRecDipolesbyE_C1_2(remE,:) = [];
    remRecDipolesbyE_C2_1(remE,:) = [];
    remRecDipolesbyE_C2_2(remE,:) = [];
    remRecDipolesbyE_C3_1(remE,:) = [];
    remRecDipolesbyE_C3_2(remE,:) = [];
        
    bestOpts = getBestEs(remRecDipolesbyE_C1_1, remRecDipolesbyE_C1_2, remRecDipolesbyE_C2_1, remRecDipolesbyE_C2_2, remRecDipolesbyE_C3_1, remRecDipolesbyE_C3_2);
    bestNext = bestOpts(randi(length(bestOpts)));
    bestNextC = clipped_electrodeInds(bestNext);

    currConfig = [currConfig, bestNextC];

    remRecDipolesbyE_C1_1(bestNext,:) = [];
    remRecDipolesbyE_C1_2(bestNext,:) = [];
    remRecDipolesbyE_C2_1(bestNext,:) = [];
    remRecDipolesbyE_C2_2(bestNext,:) = [];
    remRecDipolesbyE_C3_1(bestNext,:) = [];
    remRecDipolesbyE_C3_2(bestNext,:) = [];
    clipped_electrodeInds(bestNext) = [];
    
end
eOrder = currConfig;
% eAssocNOrder
eAssocNOrder = ROIEs(currConfig)

% get contact field and color by electrode IMPLANT number 
% note this is the same for all three thresholds
cToE_imp = double(full(cToE_imp));
% for each electrode in original full config, multiply all values by order of implantation, or 0 if no implantation
for i = 1:size(cToE_imp,1)
    if ~isempty(find(eAssocNOrder==i, 1))
        cToE_imp(i,:) = cToE_imp(i,:) * find(eAssocNOrder==i);
    else
        cToE_imp(i,:) = cToE_imp(i,:) * 0;
    end
end
% get contacts of implantation (has any positive value across electrodes)
cInds = find(sum(cToE_imp,1)>0);
config.node = scirunfield.node(cInds,:);
config.field = sum(cToE_imp(:,cInds),1);

% get recArea field
rec_tissueDat_imp_allThr = cell(6,1);
configConvergROIPercent_imp_allThr = cell(6,1);
for thrI = 1:6
    recAreaFieldOnly = zeros(length(currConfig),D); % zeros for all dipoles in cortex
    recAreaFieldOnly_sum = zeros(length(currConfig),D); % zeros for all dipoles in cortex
    % configPerc = zeros(length(eOrder),1); % percent ROI mapped at each electrode addition
    
    for i = 1:length(eOrder)
        e = eOrder(i);
        recAreaFieldOnly(i,:) = recDipolesbyEAll{round(thrI/2)}(e,:);
        if i == 1
            recAreaFieldOnly_sum(i,:) = recDipolesbyEAll{round(thrI/2)}(e,:);
        else
            recAreaFieldOnly_sum(i,:) = recAreaFieldOnly_sum(i-1,:) + recDipolesbyEAll{round(thrI/2)}(e,:);
        end
    end
    if mod(thrI,2) == 0
        recAreaFieldOnly_sum(recAreaFieldOnly_sum<=1)=0;
    end
    recAreaFieldOnly_log = recAreaFieldOnly_sum(:,ROI_dips);
    recAreaFieldOnly_log(recAreaFieldOnly_log>0) = 1;
    configPerc = sum(recAreaFieldOnly_log,2)/length(ROI_dips);
    % recInds(recInds<2) = 0; % min number of electrodes for recordable dipole is 2
    recAreaFieldOnly = recAreaFieldOnly(:,1:D);  % clip to correct size
    rec_tissueDat_imp_allThr{thrI} = recAreaFieldOnly;
    configConvergROIPercent_imp_allThr{thrI} = configPerc;
end

end


function bestOpts = getBestEs(recDipolesbyE_C1_1, recDipolesbyE_C1_2, recDipolesbyE_C2_1, recDipolesbyE_C2_2, recDipolesbyE_C3_1, recDipolesbyE_C3_2)
    % given the 6 recDbyE matrices, find the best electrode or electrodes
    % using a combined cost function.
    % The recDbyE matrices are E' by D' matrices of ones and zeros
    % indicating what electrodes can record discernible voltage from what
    % dipoles. E' must be the same across all 6 matrices, but D' can differ
    % between the 6 matrices. E' represents the electrodes that have the
    % potential to decrease the cost function (at any component / matrix).
    % D' represents the dipoles that have not yet been mapped by at least 
    %   1 (for the "_1" matrices) or by at least 2 (for the "_2") matrices.
    % The "C1" "C2" and "C3" represents the threshold case: 100 µV, 500 µV,
    % or 1000 µV. 
    % Profit(i) = 2*(# dipoles mapped once or more) + (# dipoles mapped twice or more)
    % where Cost(i) = 3*N - Profit(i)
    
    % simple cost (balance single and double map evenly at one thr only)
    simpleCost = 0;
    if simpleCost == 1
        weights1s = sum(recDipolesbyE_C1_1,2) + sum(recDipolesbyE_C1_2,2);
        bestOpts = find(weights1s==max(weights1s));
        return
    end
    
    % integrated cost (single map is worth twice double map and use all three thrs to break ties. 
    integratedCost = 0;
    if integratedCost == 1
        weights1s = 2 * sum(recDipolesbyE_C3_1,2) + sum(recDipolesbyE_C3_2,2);
        bestOpts = find(weights1s==max(weights1s));
        if length(bestOpts) > 1 || isempty(bestOpts)
            % restrict domain of second threshold rec Array to only best Es
            weights2s = 2 * sum(recDipolesbyE_C2_1(bestOpts,:),2) + sum(recDipolesbyE_C2_2(bestOpts,:),2);
            bestOpts = bestOpts(weights2s==max(weights2s));
            if length(bestOpts) > 1 || isempty(bestOpts)
                % restrict domain of second threshold rec Array to only best Es
                weights3s = 2 * sum(recDipolesbyE_C1_1(bestOpts,:),2) + sum(recDipolesbyE_C1_2(bestOpts,:),2);
                bestOpts = bestOpts(weights3s==max(weights3s));
            end
        end
        return
    end
    
    % single map integrated (we only care about single mapping, and use other thresholds to break ties)
    singleMapCost = 1;
    if singleMapCost == 1
        weights1s = sum(recDipolesbyE_C3_1,2);
        bestOpts = find(weights1s==max(weights1s));
        if length(bestOpts) > 1 || isempty(bestOpts)
            % restrict domain of second threshold rec Array to only best Es
            weights2s = sum(recDipolesbyE_C2_1(bestOpts,:),2);
            bestOpts = bestOpts(weights2s==max(weights2s));
            if length(bestOpts) > 1 || isempty(bestOpts)
                % restrict domain of second threshold rec Array to only best Es
                weights3s = sum(recDipolesbyE_C1_1(bestOpts,:),2);
                bestOpts = bestOpts(weights3s==max(weights3s));
            end
        end
        return
    end
    
end


function [rec_tissue_by_E, configField] = getVisFieldsConfig(solnCongigEInds, recDipolesbyE, normals_reInd, slocs_reInd, cortex_normals, sOrD)

% get recArea field
recAreaFieldOnly = zeros(length(solnCongigEInds),size(recDipolesbyE,2)); % zeros for all dipoles (but careful cuz recDipolesbyE is sparse, so this might not be the full length!)
for i = 1:length(solnCongigEInds)
    e = solnCongigEInds(i);
    recAreaFieldOnly(i,:) = recDipolesbyE(e,:);
end
if sOrD == 0
    recAreaFieldOnly(recAreaFieldOnly<=1)=0;
end
rec_tissue_by_E.node = cortex_normals.node;
% recInds(recInds<2) = 0; % min number of electrodes for recordable dipole is 2
recInds = recAreaFieldOnly(:,1:length(cortex_normals.node));  % clip to correct size
rec_tissue_by_E.field = recInds;
% save(strcat(dataHomePath,'OptSolutions/rec_tissue.mat'),'rec_tissue')

% get contact locs field
contactsLs = cell(length(solnCongigEInds),1);
configField_field =  cell(length(solnCongigEInds),1);
for i = 1:length(solnCongigEInds)
    e = solnCongigEInds(i);
    contactsLs{i} = slocs_reInd(e,:) + [1:3.5:(3.5*(15+1)+1)]' .* normals_reInd(e,:);
    configField_field{i} = repmat(i,length(contactsLs{i}),1);
end
configField.node = cell2mat(contactsLs);
configField.field =  cell2mat(configField_field); 
% save(strcat(dataWorkPath,patientDir,'OptSolutions/contacts.mat'),'configField')


end


function message(strMessage, errFile, outFile)
    disp(strMessage)
    if contains(strMessage,"ERROR")
        fprintf(errFile,'%s\n',strcat("ERROR MESSAGE from visConfigSolution: ",strMessage));
    end
    fprintf(outFile,'%s\n',strcat("visConfigSolution: ",strMessage));
end

function ROIEs = getValidImpEs(cToE_imp, scirunfield, ROI_locs)
% clip implanted electrodes to only those that are on average close to ROI

ROIEs = []; % inds of valid electrodes in implanted config
for eI = 1:size(cToE_imp,1)
    % get all distances of contacts to ROI
    cInds = find(cToE_imp(eI,:)>0);
    minDists = zeros(length(cInds),1);
    for i = 1:length(cInds)
        cLoc = scirunfield.node(cInds(i),:);
        minDists(i) = min(sqrt(sum((ROI_locs-cLoc).^2,2)));
    end
    % if average of closest distances is < 3mm 
    %   and at least 6 contacts are within 1 cm of the ROI, add it!
    if min(minDists) < 3 && sum(minDists<10) >= length(cInds)/2
       ROIEs = [ROIEs,eI]; 
    end
    
    % also get closest point to each electrode on skin
%     minDists = zeros(length(cInds),1);
%     minDistSkinInd = zeros(length(cInds),1);
%     for i = 1:length(cInds)
%         cLoc = scirunfield.node(cInds(i),:);
%         dists = sqrt(sum((skinPoints-cLoc).^2,2));
%         minDists(i) = min(dists);
%         minDistSkinInd(i) = find(dists==min(dists));
%     end
%     minSkinPoint = skinPoints(minDistSkinInd(minDists==min(minDists)),:);
%     % and then get dist of that skin point to ROI
%     skinInsDisttoROI = min(sqrt(sum((ROI_locs-minSkinPoint).^2,2)));
%     
%     if skinInsDisttoROI < 30 % if ins point < 30 mm from ROI, include
%         ROIEs = [ROIEs, eI];
%     end
        
end

% ROIEs
% % plot
% figure(12)
% clf
% hold on
% scatter3(ROI_locs(:,1),ROI_locs(:,2),ROI_locs(:,3))
% contInds = scirunfield.node(find(sum(cToE_imp(ROIEs,:),1)==1),:);
% scatter3(contInds(:,1),contInds(:,2),contInds(:,3),'filled')
% scatter3(scirunfield.node(:,1),scirunfield.node(:,2),scirunfield.node(:,3),'filled')
end


