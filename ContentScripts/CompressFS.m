% CompressFS
% 10/28/21
% Immediately after a forward solution is created by SCIRun,
% open the output file, scale by area and cdmd,
%   perform ADC to compress, and replace file

% 1/19/22 
% Create three compressed LFs, each for a different threshold
%   thresholds: 100 µV, 500 µV, 1000 µV.
%   So, give each compressed LF a range 50% greater than the threshold.
%   max volt levels: 150 µV, 750 µV, 1500 µV 

% 10/3/22
% Just use 16-bit floats!! 

function CompressFS(i)

% $$ UPDATE PARAMETERS $$
patientID = 1;
dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';

patientDir = ['Patient_' num2str(patientID)];
voltCaps = [150, 750, 1500]; % µV. for 100, 500, and 1000 thresholds

% open FS
filename = strcat(dataWorkPath,patientDir,'/LeadField/FSs/HeadDipoleVoltageSampledAtRecLoc',num2str(i),'.mat');
load(filename, 'scirunmatrix')
scirunmatrixIn = scirunmatrix;

if ~exist(strcat(dataWorkPath,patientDir,'/LeadField/FSs_C1/'), 'dir')
   mkdir(strcat(dataWorkPath,patientDir,'/LeadField/FSs_C1/'))
end
if ~exist(strcat(dataWorkPath,patientDir,'/LeadField/FSs_C2/'), 'dir')
   mkdir(strcat(dataWorkPath,patientDir,'/LeadField/FSs_C2/'))
end
if ~exist(strcat(dataWorkPath,patientDir,'/LeadField/FSs_C3/'), 'dir')
   mkdir(strcat(dataWorkPath,patientDir,'/LeadField/FSs_C3/'))
end

filenameS1 = strcat(dataWorkPath,patientDir,'/LeadField/FSs_C1/HeadDipoleVoltageSampledAtRecLoc_C1_',num2str(i),'.mat');
filenameS2 = strcat(dataWorkPath,patientDir,'/LeadField/FSs_C2/HeadDipoleVoltageSampledAtRecLoc_C2_',num2str(i),'.mat');
filenameS3 = strcat(dataWorkPath,patientDir,'/LeadField/FSs_C3/HeadDipoleVoltageSampledAtRecLoc_C3_',num2str(i),'.mat');
filenamesOut = [string(filenameS1); string(filenameS2); string(filenameS3)];

% open areas 
load(strcat(dataWorkPath,patientDir,'/HeadModel/lh_areas.mat'), 'lh_areas')
load(strcat(dataWorkPath,patientDir,'/HeadModel/rh_areas.mat'), 'rh_areas')
areaST = [lh_areas;rh_areas];

% just use 16-bit floats!
for t = 1:3
    scirunmatrix = scirunmatrixIn * areaST(i);
    scirunmatrix = half(scirunmatrix);        
    filenameO = filenamesOut(t);
    save(filenameO, 'scirunmatrix')
end

% ADC
% % clip max and min
% for t = 1:3
%     voltCap = voltCaps(t);
%     maxInt = 2^15 - 1; % given int16 datatype (32767)
%     scirunmatrix = scirunmatrixIn * areaST(i);
%     scirunmatrix(scirunmatrix > voltCap) = voltCap;
%     scirunmatrix(scirunmatrix < (-1*voltCap)) = -1*voltCap;
%     scirunmatrix = int16(scirunmatrix./voltCap .* maxInt);        
%     % replace
%     filenameO = filenamesOut(t);
%     save(filenameO, 'scirunmatrix')
% end
% clear full FS
% blank = [];
%save(filename, 'blank')

end

