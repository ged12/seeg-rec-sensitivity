% get recordable dipoles for all valid electrodes using 
%   lead field matrix with data at contact locations
%   (no interpolation needed)
% 10/12/21

% 1/19/22 for each parameter case (ty), build three recDipolebyE matrixes
%   for each of the three voltage thresholds (100, 500, 1000 µV)
% in the future, if we are only running this for one parameter case,
%   restructure so input int is the threshold case and we have an
%   embarassingly parallel structure so 3 thresholds is same time as 1. 

% inputCase = 1:(length(cdmds)*3) % 3*number of source types. 
function precompRecAreaAllElectrodes(inputCase)

sourceNum = ceil((inputCase)/3);
cNum = mod(inputCase-1,3)+1;

% $$ UPDATE PARAMETERS $$
cdmds = [1, 1, 0.2, 0.2]; %nAm/mm^2
patchAreas = [5, 9.5, 5, 9.5]; % cm^2
dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';
dataHomePath = '';
patientID = 1; 
outFileName = '';
errFileName = '';

% store errors in errFile 
errFile = fopen(errFileName,'a');
% store messages in outFile
outFile = fopen(outFileName,'a');

patientDir = ['Patient_' num2str(patientID)];

cdmd = cdmds(sourceNum) % nAm/mm^2
patchArea = patchAreas(sourceNum) % cm^2

discVoltThreshs = [100, 500, 1000]; % µV
threshold = discVoltThreshs(cNum) % µV

maxInt = 2^15-1; % LF to be stored at 16bit initegers

maxVolts = [150, 750, 1500]; % µV
maxVolt = maxVolts(cNum) % µV

cNumStrings = ["C1","C2","C3"];
cNumStr = cNumStrings(cNum)

tys = strcat(num2str(patchArea),"cm2_",num2str(cdmd),"cdmd")


tic
% import thresholded LF with voltages at contact locs
%   CxD where C is #contacts and D is #dipoles
%   394201 x 39965 logical matrix. ~15GB
%   C x D logical matrix. ~15GB

message(['Loading LF matrix with threshold ' num2str(threshold)], errFile, outFile)
load(strcat(dataWorkPath,patientDir,'/LeadField/PatchLeadField_RecLocs',cNumStr,'_thr_',tys,'.mat'),'leadFieldThr') % leadFieldThr
message('Lead Field matrix load is done', errFile, outFile)
info = whos('leadFieldThr');
sizeGB = info.bytes/(1000^3);
message(['Lead Field matrix is ' num2str(sizeGB) ' GB'], errFile, outFile)
x = toc;
message(['This took ' num2str(x/60) ' minutes'], errFile, outFile)
message(num2str(size(leadFieldThr)), errFile, outFile)
message(num2str(sum(sum(leadFieldThr))), errFile, outFile)

%ty =  5;
% tyStrs  = ["5cm2_0.2cdmd",
%     "9.5cm2_0.2cdmd",
%     "9.5cm2_1cdmd",
%     "5cm2_1cdmd"];
% tys = tyStrs(ty);

% import contact-to-electrode association matrix
load(strcat(dataWorkPath,patientDir,'/ElectrodeLocsData/contactLocsIndsforLinElectrodes.mat'),'contactLocsIndsforLinElectrodes')
%load([dataWorkPath patientDir '/ElectrodeLocsData/electrode_contactInds.mat'],'electrode_contactInds') 
message('ContactLocsIndsforLinElectrodes load is done', errFile, outFile)

info = whos('contactLocsIndsforLinElectrodes');
sizeGB = info.bytes/(1000^3);  % if not sparse: 83596*394201/(1024^3)
message(['contactLocsIndsforLinElectrodes is ' num2str(sizeGB) ' GB'], errFile, outFile)

numContactsForSearch = max(max(contactLocsIndsforLinElectrodes))

% make rec dipoles by E (an E by D matrix giving which electrodes can
% record from which electrodes with at least two contacts)
recDipolesbyE = zeros(size(contactLocsIndsforLinElectrodes,2),size(leadFieldThr,2),'logical');
info = whos('recDipolesbyE');
sizeGB = info.bytes/(1000^3);
message(['RecDipolesbyE before filling is ' num2str(sizeGB) ' GB'], errFile, outFile)
% for every electrode, get relevant contacts from contactLocsIndsforLinElectrodes
E = size(contactLocsIndsforLinElectrodes,2)
numContactsForSearch = max(max(contactLocsIndsforLinElectrodes))
for i = 1:E
    contactsThis = contactLocsIndsforLinElectrodes(1,i):contactLocsIndsforLinElectrodes(2,i);
    recDipolesbyE(i,:) = sum(leadFieldThr(contactsThis,:),1) >= 2;
end


%{
% contactLocsIndsforLinElectrodes is a 2xE matrix of ints that stores the start and end index of contacts in contactLocs.node matrix that belong to each electrode
% int32 type
% change to ExC sparse matrix of logicals
tic
%cToE = zeros(E,C,'logical');
is = [];
js = [];
for i = 1:E
    is = [is;i*ones(length(contactLocsIndsforLinElectrodes(1,i):contactLocsIndsforLinElectrodes(2,i)),1)];
    js = [js;[contactLocsIndsforLinElectrodes(1,i):contactLocsIndsforLinElectrodes(2,i)]'];
    %cToE(i,contactLocsIndsforLinElectrodes(1,i):contactLocsIndsforLinElectrodes(2,i)) = 1;
end
vs = true(length(js),1);
size(is)
size(js)
size(vs)

info = whos('is');
sizeGB = info.bytes/(1000^3);  % if not sparse: 83596*394201/(1024^3)
message(['is indices array is ' num2str(sizeGB) ' GB'], errFile, outFile)

cToE = sparse(double(is),double(js),vs);

% cToE = electrode_contactInds;

size(cToE)
sum(sum(cToE))

info = whos('cToE');
sizeGB = info.bytes/(1000^3);  % if not sparse: 83596*394201/(1024^3)
message(['cToE is ' num2str(sizeGB) ' GB'], errFile, outFile)
x = toc;
message(['This took ' num2str(x/60) ' minutes'], errFile, outFile)

% 
% maxVolts = [150, 750, 1500]; % µV
% cNumStrings = ["C1","C2","C3"];
% thresholds = [100, 500, 1000]; % µV
% 
% for cNum = 1:3
%     maxVolt = maxVolts(cNum); % µV
%     cNumStr = cNumStrings(cNum);
%     threshold = thresholds(cNum);

% multiply LFThr by c-to-e association matrix and threshold to get
%   matrix of number of contacts for each electrode that can record from
%   each dipole (ExD where E is number of electrodes and D is #dipoles)
%   This is taking E linear combinations of rows in the LF matrix.
% remember we cannot mult int types
tic
message("Multiplying leadFieldThr by cToE", errFile, outFile)
message(strcat("Clipping lead field down to only auto-generated contacts (not implanted contacts). #Rows =",num2str(size(cToE,2))), errFile, outFile)
leadFieldThr(numContactsForSearch+1:end,:) = [];
recDipolesbyE = cToE * leadFieldThr; % ExD = ExC * CxD
% recDipolesbyE is a ExD matrix of doubles from multiplying two matrices of logicals
message(strcat("For index ",num2str(inputCase)," cToE has size ",num2str(size(cToE))," and leadFieldThr has size ",num2str(size(leadFieldThr))), errFile, outFile)

%}

leadFieldThr_other = leadFieldThr(numContactsForSearch+1:end,:);
leadFieldThr = [];

%{ 
% ******************************
x = toc;
message(['This took ' num2str(x/60) ' minutes'], errFile, outFile)
info = whos('recDipolesbyE');
sizeGB = info.bytes/(1000^3);
message(['RecDipolesbyE after filling is ' num2str(sizeGB) ' GB'], errFile, outFile)
message(num2str(size(recDipolesbyE)), errFile, outFile)
message(num2str(sum(sum(recDipolesbyE))), errFile, outFile)
%}

% threshold recordable dipole (by electrodes) matrix by 2
%   We say that a given electrode can record from a given dipole if at
%   least two contacts at that electrode can record from that dipole.
recDipolesbyE = logical(recDipolesbyE>=2);
message(num2str(size(recDipolesbyE)), errFile, outFile)
message(num2str(sum(sum(recDipolesbyE))), errFile, outFile)

% save recordable dipole matrix
save(strcat(dataWorkPath,patientDir,'/recArea/recDipolesbyE_recLocs',cNumStr,'_',tys,'.mat'),'recDipolesbyE','-v7.3')
recDipolesbyE = [];  % clear up memory

% do the same for implanted locs 
implantedELocations = load(strcat(dataHomePath,patientDir,'/Inputs/ContactLocations.mat'),'scirunfield');
implantedELocations = implantedELocations.scirunfield.node;
cToE_imp = getImplantedElectrodeAssociation(implantedELocations, dataHomePath, patientDir);
message(strcat("For index ",num2str(inputCase)," cToE_imp has size ",num2str(size(cToE_imp))," and leadFieldThr has size ",num2str(size(leadFieldThr))), errFile, outFile)
message(strcat("Clipping LF_imp to ",num2str(length(implantedELocations))," contacts"), errFile, outFile)
leadFieldThrImp = leadFieldThr_other(1:length(implantedELocations),:);
recDipolesbyE_implanted = cToE_imp * leadFieldThrImp;
recDipolesbyE_implanted = logical(recDipolesbyE_implanted>=2);
leadFieldThrImp = [];

% save LF for rec radius contacts of interest
cIndsRecRadius = length(implantedELocations)+1:size(leadFieldThr_other,1);
message(strcat("Clipping LF_recRadius to ",num2str(length(cIndsRecRadius))," contacts"), errFile, outFile)
leadFieldThr = leadFieldThr_other(cIndsRecRadius,:);
save(strcat(dataWorkPath,patientDir,'/recArea/recDipolesbyC_recRadius',cNumStr,'_',tys,'.mat'),'leadFieldThr','-v7.3')

% save recordable dipole matrix
save(strcat(dataWorkPath,patientDir,'/recArea/cToE_imp',cNumStr,'_',tys,'.mat'),'cToE_imp')
save(strcat(dataWorkPath,patientDir,'/recArea/recDipolesbyE_implantedLocs',cNumStr,'_',tys,'.mat'),'recDipolesbyE_implanted','-v7.3')


% optimization algorithm will take linear combinatios of the ExD matrix to
% get a 1xD matrix for each electrode configuration, which will be
% converted into a metric of recordable area and the cost function

% end

end

function cToE_imp = getImplantedElectrodeAssociation(implantedELocations, dataHomePath, patientDir)
    % get electrode-to-contact association matrix
    % by clustering contact locations. Assume all contacts for one
    % electrode are adjacent in the implantedELocations matrix.
    % also make visualization with different colors to check.
    % DO NOT offset all of these contact numbering and electrode numbering to
    % after the generated electrodes and contacts. Use this only with the
    % clipped lead field, with data only corresponding to these contacts
    
    if size(implantedELocations,1) ~= 3
        implantedELocations = implantedELocations';
    end
    dists = sum(diff(implantedELocations,1,2).^2,1);
    numEs = 0;
    eStartAndEnds = [];
    currCDist = dists(1);
    currCStart = 1;
    for i = 1:length(dists)
        if abs(dists(i) - currCDist) > 20
            numEs = numEs + 1;
            eStartAndEnds = [eStartAndEnds;[currCStart,i]];
            currCStart = i+1;
            currCDist = dists(i+1);
        end
    end
    numEs = numEs + 1;
    eStartAndEnds = [eStartAndEnds;[currCStart,i+1]];
    
    % get cToE_imp logical matrix
    E = numEs;
    % C = size(leadFieldThr,1);
    %cToE = zeros(E,C,'logical');
    is = [];
    js = [];
    for i = 1:E
        is = [is;i*ones(eStartAndEnds(i,2)-eStartAndEnds(i,1)+1,1)]; % electrode indices
        js = [js;[eStartAndEnds(i,1):eStartAndEnds(i,2)]']; % contact indices
    end
    vs = true(length(js),1);
    size(is)
    size(js)
    size(vs)

    cToE_imp = sparse(double(is),double(js),vs);
    
    
    % plot contact locs and save
    f = figure('visible','off');
    clf
    hold on
    % plot each set of contacts in another color
    for i = 1:E
        scatter3(implantedELocations(1,eStartAndEnds(i,1):eStartAndEnds(i,2)),implantedELocations(2,eStartAndEnds(i,1):eStartAndEnds(i,2)),implantedELocations(3,eStartAndEnds(i,1):eStartAndEnds(i,2)),'filled','DisplayName',strcat("Electrode ",num2str(i)))
    end
    legend()
    title(strcat("Implanted Recording Locations with Electrode Clustering "),"FontSize",14)
    saveas(f,strcat(dataHomePath,patientDir,'/Test/implantedLocsWithEClusters'),'jpg');
    saveas(f,strcat(dataHomePath,patientDir,'/Test/implantedLocsWithEClusters'),'fig');
    % openfig('Patient_7/Test/inputSurfaces.fig','new','visible')
   
end


function message(strMessage, errFile, outFile)
    disp(strMessage)
    if contains(strMessage,"ERROR")
        fprintf(errFile,'%s\n',strcat("ERROR MESSAGE from precompRecAreaAllElectrodes: ",strMessage));
    end
    fprintf(outFile,'%s\n',strcat("precompRecAreaAllElectrodes: ",strMessage));
end
