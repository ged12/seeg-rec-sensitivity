% Bounding Volume Hierarchy 
% new method 12/7/2020
% inspired by Sparks 2017 BVH method


% precompute tree to traverse
% store indices of points in each node
% as well as store bounding box of each cube
% and matrix of indices of children nodes so don't have to store null nodes
% for in-order traversal linear indexing

% each node represents one box
% that has a number of surface points inside it



% purpose: to get actual distance of electrode to surface
%           to remove intrinsic variability in previous method
%

% now, we calculate the intersection of points with boxes, rather than 
% boxes with boxes.
% now, we will traverse the tree all the way down to a leaf node every
% time in order to find the smallest distance of electrode to surface
% rather than stopping when a certain level was reached (side length < a
% threshold value, say 4mm) and we had only a range of distances that the
% electrode could be away from the surface. 

% inputs:
%   electrode starting location and normal vector
%       all electrodes are 54.5mm long.
%   surface triangulation 
%   threshold distance

%   three parameters that store precomputed BVH tree
%       for skull:
%       load('BVH_surface_pointInds.mat','BVH_surface_pointInds')
%       load('BVH_boundingBoxes.mat','BVH_boundingBoxes')
%       load('childrenInds.mat','childrenInds')
%       for sulci:
%


% outputs:
%   boolean of intersection
%   minimum distance from electrode to surface or electrode

%% 0) load all if not script
%{
local_or_cluster = 1;
% skull:
% if local_or_cluster==1
% trisurf = stlread('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/SkullTest_refined1.5.stl');
% else
%     trisurf = stlread('SkullTest_refined1.5.stl');
% end
% sulci:

local_or_cluster=1;
if local_or_cluster==1
    trisurf = stlread('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/Optimize_sEEG_implant/PrecomputeAllElectrodeLocs/SkullSulciSurfacesResolutions/SulciSurface_refined3.stl');
    patch_precomp = '/Volumes/Seagate Backup Plus Drive/disc-signal-grill/Optimize_sEEG_implant/PrecomputeAllElectrodeLocs/';
end
load([patch_precomp 'ElectrodeLocsData/starting_locs_withDepth.mat'],'starting_locs')
load([patch_precomp 'ElectrodeLocsData/normals_withDepth.mat'],'normals')

%%
indE = 100;
normal1 = normals{indE,1,1};

k=1;
sloc1 = starting_locs{indE,1,1} + k*3*normal1;

surface_or_electrode_intersection = 2;

%% 1) create 100 points along electrode (E1)
endpoint1  = sloc1 + 54.5*normal1;
n = 100;
E1_points = sloc1' + [0:1:n-1]'*((endpoint1-sloc1)/(n-1))';


%% 2) if electrode-electrode case, create 100 points along 2nd electrode (E2)
indE2 = 200;
E2.normal = normals{indE2,1,1};
E2.sloc = starting_locs{indE2,1,1};

if surface_or_electrode_intersection == 2
    sloc2 = E2.sloc;
    normal2 = E2.normal;
end

%% 3) precompute boundary volume hierarchy tree
% store tree nodes in array using level-order traversal.
%{
%  store indices of points on surface within each node
BVH_surface_pointInds = cell(1,10);
% first node has all points in it
BVH_surface_pointInds{1} = 1:length(trisurf.Points);

% store bounding box for each node
% bounding boxes, since all are in standard basis, only need two points to define
BVH_boundingBoxes  = cell(1,10);

% get first bounding box
BVH_boundingBoxes{1} = [min(trisurf.Points(:,1)) min(trisurf.Points(:,2)) min(trisurf.Points(:,3)); max(trisurf.Points(:,1)) max(trisurf.Points(:,2)) max(trisurf.Points(:,3))];

% create the tree
% while any leaf node has more than one point inside of it, create two
% children nodes
i=1 % index of level-order node
heightOld = 0;
height=1;
numLeafs=0;
k = 1; % counter of total number of nodes we have, so we can always add to the end

childrenInds = zeros(10,2);

% go until i == k i think
while true % use in-order traversal
    
    % break box i into two boxes
    % DO NOT store if either is null. 
    % thus, this is not a full binary tree and simple indexing will not
    % give us the positions of the children nodes 
    % we must save indices of l and r children nodes for every node
    
    % find longest dimension of parent  box and split in that direction
    % box of interest is box that was determined to have intersection 
    parent_box = BVH_boundingBoxes{i};
    
    isleaf = 0;
    
    if size(parent_box,1)>1 % node has more than one point inside it. 
    % if node has more than one point aka there exists a bounding box, add its two children nodes to the end, whatever index that is (k+1). and store that index 
    %disp(size(parent_box,1))
    box_dimensions = abs(parent_box(1,:)-parent_box(2,:));
    dim = find(box_dimensions==max(box_dimensions));
    midpoint = max(box_dimensions)/2+min(parent_box(1,dim),parent_box(2,dim));
    % split points that are in box of interest into two groups,and store
    % ~indices~
    parent_points = trisurf.Points(BVH_surface_pointInds{i},:);
    parent_inds = BVH_surface_pointInds{i};
    
    left_points = parent_inds(parent_points(:,dim)>midpoint);
    if ~isempty(left_points) % left child is not null
        BVH_surface_pointInds{k+1} = left_points;
        childrenInds(i,1) = k+1; % left child index
        
        if length(BVH_surface_pointInds{k+1})==1 % left child is a leaf node
          % put point in for bounding box for left child
            BVH_boundingBoxes{k+1} = trisurf.Points(BVH_surface_pointInds{k+1},:);
        else % left child is not a leaf node (and not null)
            % create bounding box for left child
            BVH_boundingBoxes{k+1}(1,:) = min(trisurf.Points(BVH_surface_pointInds{k+1},:));
            BVH_boundingBoxes{k+1}(2,:) = max(trisurf.Points(BVH_surface_pointInds{k+1},:));
        end
        % don't build in buffer distance here. build it in during traversal so
        % the buffer distance can be changed easily with the threshold. 
        % if node is leaf, put single point as bounding box. it will be built
        % into an actual box when buffer is added during traversal
        
        k = k+1;
        
    else % left child is null
        childrenInds(i,1) = nan; % left child index
    end
    
    
    right_points = parent_inds(parent_points(:,dim)<=midpoint);
    if ~isempty(right_points) % right child is not null
        BVH_surface_pointInds{k+1} = right_points;
        childrenInds(i,2) = k+1; % right child index
    
        if length(BVH_surface_pointInds{k+1})==1 % right child is a leaf node
            % put point in for bounding box for right child
            BVH_boundingBoxes{k+1} = trisurf.Points(BVH_surface_pointInds{k+1},:);
        else % right child is not a leaf node (and not null)
            % create bounding box for right child
            BVH_boundingBoxes{k+1}(1,:) = min(trisurf.Points(BVH_surface_pointInds{k+1},:));
            BVH_boundingBoxes{k+1}(2,:) = max(trisurf.Points(BVH_surface_pointInds{k+1},:));
        end
        % don't build in buffer distance here. build it in during traversal so
        % the buffer distance can be changed easily with the threshold. 
        % if node is leaf, put single point as bounding box. it will be built
        % into an actual box when buffer is added during traversal

            k = k+1;
    
    else % right child is null
        childrenInds(i,2) = nan; % right child index
    end
    

    
    elseif size(parent_box,1)==1 % parent node is a leaf node
        % don't add any nodes
        % store inds
        isleaf = 1;
        childrenInds(i,1) =  nan;       
        childrenInds(i,2) =  nan;                                                                           % the children of nodes with only one point are null, so all nodes with only one point are leaf nodes
    else % parent node is null
        % don't do anything
        %  we should never get here. 
        disp('ERROR. node is null:')
        disp(strcat('i=',num2str(i),'; k=',num2str(k)))
        break
    end

    
    % if we have a whole level of null or leaf nodes, break
    % the length of a level at height h is 2^(h-1). so that when height is
    % 1, we have the single root node. and when the height is 2, we have
    % two first children nodes.
    % the height of the tree when we are at an index i is floor(log2(i))+1
%     height = floor(log2(i))+1;
%     if height~=heightOld % beginning of new level, reset counter of null or leaf nodes
%         countnullorleafNodes=thisnullorleafNode;  % set to 1 or 0
%     else
%         countnullorleafNodes = countnullorleafNodes + thisnullorleafNode;
%     end
%     % if all nodes at this level are null or leaf, break!
%     % there are 2^(h-1) nodes at a height h
%     if countnullorleafNodes == 2^(height-1)
%         break
%     end % if not, at least one node at this level is not null or a leaf
%     heightOld = height;
%     i = i+1



%     if numLeafs==98
%         if size(parent_box,1)>1
%             x=1;
%         end
%     end



    
    % ohhh,  or i  could  just count the number of leaf nodes
    % we know there has to be one single point in each node,   and that
    % each point has to be in a leaf node, so there has to  be the same
    % number of leaf nodes as the number of points in the surface
    numLeafs = numLeafs + isleaf;
    i = i+1;
    disp(strcat(num2str(numLeafs),';',num2str(i),';',num2str(k)))
    
    if numLeafs==size(trisurf.Points,1)
        disp('BVH is complete!')
        break
    end
end

save('BVH_surface_pointInds_sulci.mat','BVH_surface_pointInds')
save('BVH_boundingBoxes_sulci.mat','BVH_boundingBoxes')
save('childrenInds_sulci.mat','childrenInds')
%}
% 

load([patch_precomp 'Sensitivity Analysis of Mesh Precision/BVH_surface_pointInds_sulci.mat'],'BVH_surface_pointInds')
load([patch_precomp 'Sensitivity Analysis of Mesh Precision/BVH_boundingBoxes_sulci.mat'],'BVH_boundingBoxes')
load([patch_precomp 'Sensitivity Analysis of Mesh Precision/childrenInds_sulci.mat'],'childrenInds')

%%

[intersect,minDist] = boundingVolumeHierarchy(sloc1,normal1,2.5,trisurf,1,BVH_surface_pointInds,BVH_boundingBoxes,childrenInds,128);

%%

% 3) for each point on E1, traverse tree
%  since we are adding buffer, we know that if a point is not inside of any
%  leaf node bounding box, then that point is at least threshold distance away
%  from every point
%  if point is inside any leaf node bounding box, calculate distance from
%  point to point inside leaf node bounding box. if that distance is <
%  threshold, then say intersection. if point is inside multiple leaf node
%  bounding boxes, save smallest distance. 
%  if point is inside some leaf node bounding boxes, but all those
%  distances are actually greater than threshold, say no intersection
%  (^^ for all 100 points along electrode)

%}


% this method, adapted from Sparks2017, finds whether the distance is > threshold,
% and the specific distance if that distance is < threshold). 
% the method in Sparks2017, in contrast, can find the actual minimum distance
% from the electrode to the surface whether it is above or below the
% threshold. 

function [intersect,minDist] = boundingVolumeHierarchy(sloc1,normal1,threshold,trisurf,visual,BVH_surface_pointInds,BVH_boundingBoxes,childrenInds,n)

intersect = 0;
endpoint1  = sloc1 + 54.5*normal1;
%n = 100;
E1_points = sloc1' + [0:1:n-1]'*((endpoint1-sloc1)/(n-1))';

%threshold = 4; %mm  
% min dist from electrode to surface to be called "no intersect"  
%add this distance to ~both sides~ of each dimension of bounding box
% adding this distance has the same effect as finding the closest distance from the
% point to the bounding box and thesholding that to decide if we should add
% the corresponding node to the queue or not

minDist = threshold+0.01; % distance from any electrode point to any point on surface
    % set to the threshold distance because if no intersect is called, we
    % have no idea what the true distance from the point to the surface is,
    % only that it is larger than 4
    
% save bounding_boxes to plot
bounding_boxIs_plot =  [];

% for  each point on E1 
for p=1:n %  n=100
    Epoint = E1_points(p,:);
% breadth first tree traversal
queue_nodeInds = [1];

while ~isempty(queue_nodeInds) % while nodes in queue

    % remove node from  queue
    node = queue_nodeInds(1);

    % shift queue
    queue_nodeInds = queue_nodeInds(2:end);

    % get bounding box of node
    bounding_box = BVH_boundingBoxes{node};
    bounding_boxIs_plot = [bounding_boxIs_plot;bounding_box];

    leafnode=0;
    % add buffer to all sides of box
    if size(bounding_box,1)==1 % node is leaf; there is only one point in this node
        % create box around single point with each side length of threshold
        leafnode = 1;
        leafPoint = bounding_box;
        bounding_box  = [bounding_box;bounding_box];
        bounding_box(1,:) = bounding_box(1,:) - threshold;
        bounding_box(2,:) = bounding_box(2,:) + threshold;
    else % node is not leaf, add buffer to all coords
        bounding_box(1,:) = bounding_box(1,:) - threshold*sign(diff(bounding_box));
        bounding_box(2,:) = bounding_box(2,:) + threshold*sign(diff(bounding_box));
    end

    % is point inside box?
    inside = pointInBox(Epoint,bounding_box);

    % if point is inside box, 
    if inside==true 
        % and if node is not  a leaf node
        if leafnode==0 
            % add both childen nodes to  queue
            queue_nodeInds = [queue_nodeInds,childrenInds(node,1)];
            queue_nodeInds = [queue_nodeInds,childrenInds(node,2)];
        else % and if node IS a leaf node
            % calculate distance from point to point inside leaf node
            dist = norm(leafPoint-Epoint);
            minDist = min(dist,minDist);
        end
    % else. point is not inside box, don't do anything.
    end

end
% if we don't want to find exact distance, only if under threshold:
if minDist <= threshold
    intersect = 1;
    break
end
% now queue is empty
% go to next electrode point. do not reset minDist
% in future, break once minDist for any point is < threshold
end
if minDist <= threshold
    %disp(['min distace from electrode to surface is: ' num2str(minDist) ' mm'])
    intersect = 1;
    %disp('intersect')
else
    %disp('min distace from electrode to surface is: > 4 mm')
end

if visual==1
visualizeBoundaryHierarchySurface(sloc1,sloc1+54.5*normal1,trisurf,bounding_boxIs_plot,[])
end

end


function [boolean] = pointInBox(point,bounding_box)
% if, for ALL of the x, y, and z directions, the length of the projection 
% of the direction vector (vector from point to the center of the box) onto each direction is
% smaller than or equal to half of the length of that corresponding side of
% the box, the point is in the box

% since the box is in the standard basis (x, y, and z, directions),
% the length of the projection of the direction vector onto each direction
% is just the specific coordinate of the vector

box_point1 = bounding_box(1,:);
box_point2 = bounding_box(2,:);

% get direction vector from point to center
ctr = (box_point1 + box_point2)/2;
dir_v = abs(ctr-point);

x_len = abs(box_point2(1)-box_point1(1));
y_len = abs(box_point2(2)-box_point1(2));
z_len = abs(box_point2(3)-box_point1(3));

% for every direction, if the length from ctr to point is less or equal to
% half of the length of that side of the box, the point is in the box
if ((dir_v(1)<=x_len/2) && (dir_v(2)<=y_len/2) && (dir_v(3)<=z_len/2))
    boolean = true; % point is in box
else
    boolean = false; % point is not in box
end
% given intersection of two boxes, the max distance that electrodes can be
% away from each other (at their closest point) is the max side length of
% a box. the minimum is of course 0.
%error = max([x_len,y_len,z_len]); % upper limit of distance between two electrodes given intersection
end



function []=visualizeBoundaryHierarchySurface(sloc,endpoint,trisurf,box_points_1,box_points_2)
figure(1)
clf
hold on
% plot electrode

plot3([sloc(1),endpoint(1)],[sloc(2),endpoint(2)],[sloc(3),endpoint(3)],'LineWidth',5)


% plot trisurf OR point field
if isfield(trisurf,'ConnectivityList')
    stlPlot(trisurf.Points,trisurf.ConnectivityList)
else
    scatter3(trisurf(:,1),trisurf(:,2),trisurf(:,3),'.')
end
%STLPLOT is an easy way to plot an STL object
%V is the Nx3 array of vertices
%F is the Mx3 array of faces

if ~isempty(box_points_1) || ~isempty(box_points_2)
% plot bounding box(es)
for m=1:ceil(size(box_points_1,1)/2)-1
    plotcube([box_points_1(m*2-1,1)-box_points_1(m*2,1),box_points_1(m*2-1,2)-box_points_1(m*2,2),box_points_1(m*2-1,3)-box_points_1(m*2,3)],box_points_1(m*2,:),0.05,[0.9 0.9 0.9])
%     if (box_points_2(m*2-1,:)==box_points_2(m*2-1,:))  % is not Nan
%     plotcube([box_points_2(m*2-1,1)-box_points_2(m*2,1),box_points_2(m*2-1,2)-box_points_2(m*2,2),box_points_2(m*2-1,3)-box_points_2(m*2,3)],box_points_2(m*2,:),0.2,[0.9 0.9 0.9])
%     end
end
end
end


function plotcube(varargin)
% PLOTCUBE - Display a 3D-cube in the current axes
%
%   PLOTCUBE(EDGES,ORIGIN,ALPHA,COLOR) displays a 3D-cube in the current axes
%   with the following properties:
%   * EDGES : 3-elements vector that defines the length of cube edges
%   * ORIGIN: 3-elements vector that defines the start point of the cube
%   * ALPHA : scalar that defines the transparency of the cube faces (from 0
%             to 1)
%   * COLOR : 3-elements vector that defines the faces color of the cube
%
% Example:
%   >> plotcube([5 5 5],[ 2  2  2],.8,[1 0 0]);
%   >> plotcube([5 5 5],[10 10 10],.8,[0 1 0]);
%   >> plotcube([5 5 5],[20 20 20],.8,[0 0 1]);

% Default input arguments
inArgs = { ...
  [10 56 100] , ... % Default edge sizes (x,y and z)
  [10 10  10] , ... % Default coordinates of the origin point of the cube
  .7          , ... % Default alpha value for the cube's faces
  [0.3 0 0]       ... % Default Color for the cube
  };

% Replace default input arguments by input values
inArgs(1:nargin) = varargin;

% Create all variables
[edges,origin,alpha,clr] = deal(inArgs{:});

XYZ = { ...
  [0 0 0 0]  [0 0 1 1]  [0 1 1 0] ; ...
  [1 1 1 1]  [0 0 1 1]  [0 1 1 0] ; ...
  [0 1 1 0]  [0 0 0 0]  [0 0 1 1] ; ...
  [0 1 1 0]  [1 1 1 1]  [0 0 1 1] ; ...
  [0 1 1 0]  [0 0 1 1]  [0 0 0 0] ; ...
  [0 1 1 0]  [0 0 1 1]  [1 1 1 1]   ...
  };

XYZ = mat2cell(...
  cellfun( @(x,y,z) x*y+z , ...
    XYZ , ...
    repmat(mat2cell(edges,1,[1 1 1]),6,1) , ...
    repmat(mat2cell(origin,1,[1 1 1]),6,1) , ...
    'UniformOutput',false), ...
  6,[1 1 1]);


cellfun(@patch,XYZ{1},XYZ{2},XYZ{3},...
  repmat({clr},6,1),...
  repmat({'FaceAlpha'},6,1),...
  repmat({alpha},6,1)...
  );

view(3);

end


function stlPlot(v, f)
%STLPLOT is an easy way to plot an STL object
%V is the Nx3 array of vertices
%F is the Mx3 array of faces
%NAME is the name of the object, that will be displayed as a title
object.vertices = v;
object.faces = f;
c = [0.8 0.8 1.0];
color = repmat(c,length(f),1);

patch(object,'FaceVertexCData',       color, ...
         'FaceColor', 'flat',                ...
         'EdgeColor',       'none',        ...
         'FaceLighting',    'gouraud',     ...
         'AmbientStrength', 0.15);
% Add a camera light, and tone down the specular highlighting
camlight('headlight');
material('dull');
% Fix the axes scaling, and set a nice view angle
axis('image');
view([-135 35]);
grid on;
end

