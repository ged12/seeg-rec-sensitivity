% slice analyzed config data to only cis-analyze (not cross analyze) to
% make it easier to deal with
% 21 Nov 2022

function sliceCisData(trialSelect)

% $$ UPDATE PARAMETERS $$
cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
dataWorkPath = '';
patientID = 2; 

patientDir = ['Patient_' num2str(patientID)];

visData_noCross = cell(1,1);
saveIndex = 1;

tys_cA = strcat(num2str(patchAreas(trialSelect)),"cm2_",num2str(cdmds(trialSelect)),"cdmd");

rerunSlice = true;

if ~rerunSlice && isfile(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_cis',tys_cA,'.mat'))
    disp("We found this file")
    disp(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_cis',tys_cA,'.mat'))
    return
else
    fileNameThis = strcat(dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_crossAnalyze',tys_cA,'.mat');

    disp(strcat("Opening: ",fileNameThis))
    load(fileNameThis,'visDataConfigs')

    for j = 1:length(visDataConfigs)
        if isempty(visDataConfigs{j})
            continue
        end
        ck = split(visDataConfigs{j}.full_case_id,"_ROI_");
        ck = ck(1);
        if ck == tys_cA
            visData_noCross{saveIndex} = visDataConfigs{j};
            saveIndex = saveIndex + 1;
        end
    end
    visDataConfigs = visData_noCross';
    save(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_cis',tys_cA,'.mat'),'visDataConfigs','-v7.3')
end

end
