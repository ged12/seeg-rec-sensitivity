% getSkinInsertionLocs
% Grace Dessert
% 3/7/22

function scirunfield = getSkinInsertionLocs(skinSurf, dataHomePath, dataWorkPath, midline, patientID, LH_white, RH_white)
%%
patientDir = ['Patient_' num2str(patientID)];

% open MNI skin, LH, RH, MNI ins locs template, and midline 
% also open patient skin, midline, LH and RH

% MNI files
% local = 1;
local = 0;
if local
    patientDir = "Patient_7";
    dataHomePath = "";
end
%try
MNI_template = stlread(strcat(dataHomePath,"/InputGeneral/MNI_SkinValidForElectrodeImplant.stl"));
MNI_full = stlread(strcat(dataHomePath,'/InputGeneral/MNI_Skin_Final.stl'));
LH_MNI = stlread(strcat(dataHomePath,'/InputGeneral/MNI_lh_white_20k.stl'));
RH_MNI = stlread(strcat(dataHomePath,'/InputGeneral/MNI_rh_white_20k.stl'));
load(strcat(dataHomePath,"/InputGeneral/MNI_midlinePlane.mat"), 'midlinePlane')
MNIMid = midlinePlane;
%catch
%    disp("ERROR: at least one MNI file was not found! Exiting getSkinInsertionLocs.m");
%    return
%end



%%
% display original MNI brain with midline and patient brain with midline

% patient brain
%patient_original = figure(1)
if  local
    patient_original = figure();
else
    patient_original = figure('visible','off');
end
clf
hold on
trimesh(LH_white,'FaceAlpha',0.1,'FaceColor',[0.3 0 0],'EdgeAlpha',0.2,'EdgeColor',[0.3 0 0])
trimesh(RH_white,'FaceAlpha',0.1,'FaceColor',[0.3 0 0],'EdgeAlpha',0.2,'EdgeColor',[0.3 0 0])
trimesh(skinSurf,'FaceAlpha',0.1,'FaceColor',[0.3 0 0],'EdgeAlpha',0.2,'EdgeColor',[0.3 0 0])
n = midline(:,1);
p = midline(:,2);
z = linspace(min(skinSurf.Points(:,3)),max(skinSurf.Points(:,3)),10);
y = linspace(min(skinSurf.Points(:,2)),max(skinSurf.Points(:,2)),10);
[z2,y2]=meshgrid(z,y);
% ax + by + cz + d = 0
d = -p'*n;
x2 = (-n(2)*y2 - n(3)*z2 - d)/n(1);
% plot plane
surf(x2,y2,z2,'FaceAlpha',0.1,'FaceColor',[0.3 0 0]);

%%
% MNI brain
if  local
    MNI_original = figure();
else
    MNI_original = figure('visible','off');
end
clf
hold on
trimesh(LH_MNI,'FaceAlpha',0.1,'FaceColor',[0 0.3 0],'EdgeAlpha',0.2,'EdgeColor',[0 0.3 0])
trimesh(RH_MNI,'FaceAlpha',0.1,'FaceColor',[0 0.3 0],'EdgeAlpha',0.2,'EdgeColor',[0 0.3 0])
trimesh(MNI_full,'FaceAlpha',0.1,'FaceColor',[0 0.3 0],'EdgeAlpha',0.2,'EdgeColor',[0 0.3 0])
n = MNIMid(:,1);
p = MNIMid(:,2);
z = linspace(min(MNI_full.Points(:,3)),max(MNI_full.Points(:,3)),10);
y = linspace(min(MNI_full.Points(:,2)),max(MNI_full.Points(:,2)),10);
[z2,y2]=meshgrid(z,y);
% ax + by + cz + d = 0
d = -p'*n;
x2 = (-n(2)*y2 - n(3)*z2 - d)/n(1);
% plot plane
surf(x2,y2,z2,'FaceAlpha',0.1,'FaceColor',[0 0.3 0]);

% get MNI midline (save this for future use)
% see getMNImidline.m script get MNI midline (save this for future use)

%%
% visualize midlines 
if local
    midlinePlot = figure('visible','off');
else
    midlinePlot = figure();
end
% ax + by + cz + d = 0
n = MNIMid(:,1);
p = MNIMid(:,2);
d = -p'*n;
x2 = (-n(2)*y2 - n(3)*z2 - d)/n(1);
% plot plane
if local
    midlineInsLocPlot = figure(1);
else
    midlineInsLocPlot = figure('visible','off');
end
clf 
hold on
m1 = surf(x2,y2,z2,'FaceAlpha',0.6,'EdgeAlpha',0.5,'FaceColor',[0 0.3 0],'DisplayName',"Original MNI Midline");
trimesh(LH_MNI,'FaceAlpha',0.1,'FaceColor',[0 0.3 0],'EdgeAlpha',0.2,'EdgeColor',[0 0.3 0])
trimesh(RH_MNI,'FaceAlpha',0.1,'FaceColor',[0 0.3 0],'EdgeAlpha',0.2,'EdgeColor',[0 0.3 0])

n = midline(:,1);
p = midline(:,2);
d = -p'*n;
x2 = (-n(2)*y2 - n(3)*z2 - d)/n(1);
% plot plane
m2 = surf(x2,y2,z2,'FaceAlpha',0.3,'EdgeAlpha',0.5,'FaceColor',[0 0.9 0],'DisplayName',"Patient Midline");
legend([m1, m2])

% save if not local
if ~local
    % save plot 1
    saveas(midlineInsLocPlot,strcat(dataHomePath,patientDir, '/Test/midlineInsLocPlot'),'jpg');
    saveas(midlineInsLocPlot,strcat(dataHomePath,patientDir, '/Test/midlineInsLocPlot'),'fig');
    % remember when opening, you must turn visibility to 'on'
    % ie: openfig('midlinePlot.fig','new','visible')
end
%%
% transform MNI skin to match patient skin, then take pieces of patient skin

n = midline(:,1);
p = midline(:,2);
d = -p'*n;
x2 = (-n(2)*y2 - n(3)*z2 - d)/n(1);

PatSkinMins = [min(skinSurf.Points(:,1)),min(skinSurf.Points(:,2)),min(skinSurf.Points(:,3))];
PatSkinMaxs = [max(skinSurf.Points(:,1)),max(skinSurf.Points(:,2)),max(skinSurf.Points(:,3))];
MNISkinMins = [min(MNI_full.Points(:,1)),min(MNI_full.Points(:,2)),min(MNI_full.Points(:,3))];
MNISkinMaxs = [max(MNI_full.Points(:,1)),max(MNI_full.Points(:,2)),max(MNI_full.Points(:,3))];

% rotate MNI to match midlines
% MNIMid to midline
% rotate MNI_template
r = vrrotvec(midline(:,1),MNIMid(:,1)); % i checked this, it is the correct order now. This is because we are right-multiplying by the rotation matrix, maybe?
rotationMatrix = vrrotvec2mat(r);
newPoints = (MNI_full.Points - MNISkinMins) * rotationMatrix + MNISkinMins;
newSkinTemplatePoints = (MNI_template.Points - MNISkinMins) * rotationMatrix + MNISkinMins;

% get x, y, and z lengths
% scale MNI_template to match, and move all to the same min and max
% positions
PatSkinMins = [min(skinSurf.Points(:,1)),min(skinSurf.Points(:,2)),min(skinSurf.Points(:,3))];
PatSkinMaxs = [max(skinSurf.Points(:,1)),max(skinSurf.Points(:,2)),max(skinSurf.Points(:,3))];
MNISkinMins = [min(MNI_full.Points(:,1)),min(MNI_full.Points(:,2)),min(MNI_full.Points(:,3))];
MNISkinMaxs = [max(MNI_full.Points(:,1)),max(MNI_full.Points(:,2)),max(MNI_full.Points(:,3))];

MNISkinlengths = PatSkinMaxs-PatSkinMins;
PatSkinlengths = MNISkinMaxs-MNISkinMins;
scale_factor = MNISkinlengths./PatSkinlengths;

newPoints = newPoints - MNISkinMins;
newPoints = newPoints .* scale_factor;
newPoints = newPoints + PatSkinMins;

newSkinTemplatePoints = newSkinTemplatePoints - MNISkinMins;
newSkinTemplatePoints = newSkinTemplatePoints .* scale_factor;
newSkinTemplatePoints = newSkinTemplatePoints + PatSkinMins;


%%
% plot additional plots if local
if local
    % original MNI skin (green) vs Patient skin (blue)
    figure(1)
    clf
    hold on
    trimesh(skinSurf,'FaceAlpha',0.3,'EdgeColor',[0.3 0 0],'FaceColor',[0.3 0 0],'EdgeAlpha',0.3,'DisplayName','Patient Skin')
    trimesh(MNI_full,'FaceAlpha',0.1,'EdgeColor',[0 0.6 0],'FaceColor',[0 0.6 0],'EdgeAlpha',0.3,'DisplayName','Original MNI Skin')
    temp1 = triangulation(MNI_full.ConnectivityList, newPoints);
    trimesh(temp1,'FaceAlpha',0.4,'EdgeColor',[0 0.3 0],'FaceColor',[0 0.3 0],'EdgeAlpha',0.1,'DisplayName','Transformed MNI Skin')
    legend()
end

%%
% for every point on transformed template, get closest patient skin point
dists = zeros(length(newSkinTemplatePoints),length(skinSurf.Points));
for x = 1:length(newSkinTemplatePoints)
    tempPoint = newSkinTemplatePoints(x,:);
    dists(x,:) = sum((skinSurf.Points-tempPoint).^2,2);
end
[~,insPoints] = find(dists==min(dists,[],2));
insPoints = unique(insPoints);
skinInsPoints = skinSurf.Points(insPoints,:);

disp(['There are ' num2str(length(insPoints)) ' points for ins']);

%{
% old method: get small distance points, and then add all above a XY plane
% % get LH and RH points with small distance
% th = 20; %mm
% [~, skinPoints] = find(dists<th);
% 
% % add in points that are above the z level of max and min on y
% zCutMax = skinSurf.Points(find(skinSurf.Points(:,2)==max(skinSurf.Points(:,2))),3);
% zCutMin = skinSurf.Points(find(skinSurf.Points(:,2)==min(skinSurf.Points(:,2))),3);
% zCut = max(zCutMax,zCutMin)+5;
% 
% addPoints = find(skinSurf.Points(:,3)>=zCut);
% insPoints = unique([skinPoints;addPoints]);
% skinInsPoints = skinSurf.Points(insPoints,:);

% disp(['There are ' num2str(length(unique(insPoints))) ' points for ins']);
%}


%% sample 800 points 
% remove random points
% (i was trying to remove points closest to other points but it sampled all
%   from the same area, cutting off a large piece from the back of the head

if (length(insPoints) > 800) 
    while (length(insPoints) > 800) 
        closePt = randi(length(insPoints));
        insPoints(closePt) = [];
    end
end
disp(['There are ' num2str(length(unique(insPoints))) ' points for ins']);

skinInsPoints = skinSurf.Points(insPoints,:);
scirunfield.node = skinInsPoints;
scirunfield.field = zeros(length(insPoints),1);

% save skin insertion points!
if ~ local
    save(strcat(dataWorkPath,patientDir,'/HeadModel/SkinValidForElectrodeImplant_insPoints.mat'),'insPoints');
    save(strcat(dataWorkPath,patientDir,'/HeadModel/SkinValidForElectrodeImplant.mat'),'scirunfield');
end

%%
% save surfaces and plots for debugging visualization
if local
    insLocPlot = figure(2);
else
    insLocPlot = figure('visible','off');
end
clf
hold on 
scatter3(newSkinTemplatePoints(:,1), newSkinTemplatePoints(:,2),newSkinTemplatePoints(:,3),'.','DisplayName','Fully Transformed MNI Template')
trimesh(skinSurf,'FaceAlpha',0.1,'FaceColor',[0.3 0 0],'EdgeAlpha',0.2,'EdgeColor',[0.3 0 0],'DisplayName','Patient Skin')
scatter3(skinInsPoints(:,1), skinInsPoints(:,2),skinInsPoints(:,3),'filled','MarkerFaceColor',[0 1 0],'DisplayName','Insertion Points')
surf(x2,y2,z2,'FaceAlpha',0,'EdgeAlpha',0.5,'FaceColor',[0 0.9 0],'DisplayName',"Patient Midline");
legend()

%%
% save plots and products
if ~ local
    % save plot 2
    saveas(insLocPlot,strcat(dataHomePath,patientDir, '/Test/insLocPlot'),'jpg');
    saveas(insLocPlot,strcat(dataHomePath,patientDir, '/Test/insLocPlot'),'fig');
    % remember when opening, you must turn visibility to 'on'
    % ie: openfig('midlinePlot.fig','new','visible')
    
    % save intermediate surfaces
    MNI_t = triangulation(MNI_full.ConnectivityList, newPoints);
    newSkinT = triangulation(MNI_template.ConnectivityList, newSkinTemplatePoints);
    stlwrite(newSkinT,strcat(dataHomePath,patientDir,'/Test/transformedSkinTemp.stl'))
    stlwrite(MNI_t,strcat(dataHomePath,patientDir,'/Test/transformedMNIFull.stl'))
end

%%
% process manual surfaces created
% insSurf = stlread('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/Optimize_sEEG_implant/Scripts_Publish/PatientData/Patient7/SkinValidForElectrodeImplant_manual.stl');
% skinInsPoints = insSurf.Points;
% insPoints = [1:length(skinInsPoints)];
% 
% disp(['There are ' num2str(length(unique(insPoints))) ' points for ins']);
% 
% if (length(insPoints) > 800) 
%     while (length(insPoints) > 800) 
%         dists = zeros(length(insPoints),length(insPoints));
%         for i = 1:length(insPoints)
%             dists(:,i) = (sum((skinInsPoints - skinInsPoints(i,:).^2),2));
%         end
%         dists(dists==0) = max(max(dists));
%         [closePt,~] = find(dists==min(min(dists)));
%         insPoints(closePt) = [];
%         skinInsPoints = insSurf.Points(insPoints,:);
%     end
% end
% disp(['There are ' num2str(length(unique(insPoints))) ' points for ins']);
% 
% scirunfield.node = skinInsPoints;
% scirunfield.field = zeros(length(insPoints),1);
% save('/Volumes/Seagate Backup Plus Drive/disc-signal-grill/Optimize_sEEG_implant/Scripts_Publish/PatientData/Patient7/SkinValidForElectrodeImplant.mat','scirunfield');


end



