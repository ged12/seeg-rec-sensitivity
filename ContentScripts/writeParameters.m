% 2/28/22
% This function writes all scripts used for sEEG Opt pipeline
%   to change patient-specific and instance-specific filepaths and
%   parameters.
% Also changes slurm scripts for script paths and array values

% function writeParameters(patientID, cdmd, patchArea, dataWorkPath, dataHomePath, numDipoles)
% inputs:
%   patientID - integer of patientID
%   cdmds - array of floats of constant current dipole moment density in nAm/mm^2
%   patchAreas - array of areas in cm^2 of patches for extended dipole sources
%       should be between 0.5 and 9.5 cm^2
%   dataWorkPath, dataHomePath - filepaths of locations of output and input
%       files, respectively
%       Input files must be in a directory named: strcat('Patient_',num2str(patientID)) 
%       inside the dataHomePath
%       All scripts must be located in dataHomePath

% set parameters!
% params = ["patientID", "cdmds", "patchAreas", "dataWorkPath", "dataHomePath","ROIs","numDipoles"];
% patientID = 1; 
% cdmds = [10, 1, 0.2, 0.2]; %nAm/mm^2
% patchAreas = [5, 9.5, 5, 9.5]; % cm^2
% dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';
% dataHomePath = '/hpc/group/wmglab/ged12/cluster/Optimize_sEEG_implant/';
% numDipoles = 39954;
% ROIs = ["L10","L10","R10","R10","LTL","LH","WC"]; % array of length 7
% % future: thresholds parameter (right now hard coded in 100, 500 & 1000 µV);

function writeParameters(patientID, cdmds, patchAreas, dataWorkPath, dataHomePath, ROIs, outFileName, errFileName)
params = ["patientID", "cdmds", "patchAreas", "dataWorkPath", "dataHomePath","ROIs","numDipoles","outFileName", "errFileName"];

% get str versions
patientID_s = strcat(num2str(patientID),";"); %"1;"; 
cdmds_s = strcat("[",strjoin(arrayfun(@(a)num2str(a),cdmds,'uni',0), ", "),"];"); % "[1, 1, 0.2, 0.2];"; %nAm/mm^2
patchAreas_s = strcat("[",strjoin(arrayfun(@(a)num2str(a),patchAreas,'uni',0), ", "),"];"); % "[5, 9.5, 5, 9.5];"; % cm^2
dataWorkPath_s = strcat("'",dataWorkPath,"';"); %"'/work/ged12/Optimize_sEEG_implant/';"
dataHomePath_s = strcat("'",dataHomePath,"';"); %"'/hpc/group/wmglab/ged12/cluster/Optimize_sEEG_implant/';";
ROIs_s = strcat('["',strjoin(ROIs, '", "'),'"];');
numDipoles = 40100; %******************************** SET THIS TO MAX OF VALID NUM DIPOLE RANGE CUZ CANT GET CORRECT VAL WHEN WRITING SCRIPTS INITIALLY!"
numDipoles_s = strcat(num2str(numDipoles),";"); %"1;";
outFileName_s = strcat("'",outFileName,"';"); %"'.../Patient_#/out_main.txt';"
errFileName_s = strcat("'",errFileName,"';"); %"'.../Patient_#/err_main.txt';"
paramValues = [patientID_s, cdmds_s, patchAreas_s, dataWorkPath_s, dataHomePath_s, ROIs_s, numDipoles_s, outFileName_s, errFileName_s];

% open files that contain parameters
test = 0;
scriptLoadPath = 'ContentScripts/';
if ~test
    scriptLoadPath = strcat(dataHomePath,scriptLoadPath);
end

patientDir = ['Patient_' num2str(patientID)];
% filesToEdit = {strcat("exampleChangeParams.m")};
filesToEdit = [strcat(scriptLoadPath, "precompRecAreaAllElectrodes.m"), ...
               strcat(scriptLoadPath, "precomputeValidElectrodeLocs.m") ...
               strcat(scriptLoadPath, "createLF.m") ...
               strcat(scriptLoadPath, "makePatchAllMatrices.m") ...
               strcat(scriptLoadPath, "CompressFS.m") ...
               strcat(scriptLoadPath, "getSkinInsertionLocs.m") ...
               strcat(scriptLoadPath, "eCollCompute_lines.m") ...
               strcat(scriptLoadPath, "TreeSearch.m") ...
               strcat(scriptLoadPath, "getROIs.m") ...
               strcat(scriptLoadPath, "boundingVolumeHierarchy.m") ...
               strcat(scriptLoadPath, "nextBest_search.m") ...
               strcat(scriptLoadPath, "boundingVolumeHierarchyConstruction.m") ...
               strcat(scriptLoadPath, "LeadFieldMain.m") ...
               strcat(scriptLoadPath, "WriteAutoPythonScripts_Singular.m") ... 
               strcat(scriptLoadPath, "getSulciSurface.m") ... 
               strcat(scriptLoadPath, "getMidline.m") ... 
               strcat(scriptLoadPath, "getSkinInsertionLocs.m") ... 
               strcat(scriptLoadPath, "visConfigSolution.m") ... 
               strcat(scriptLoadPath, "getStContactLocs.m") ... 
               strcat(scriptLoadPath, "sliceCisData.m") ...
               ];
           
% search for query string 
queryTag = '% $$ UPDATE PARAMETERS $$';

% parse following lines until empty (or just whitespace) line
for fInd = 1:length(filesToEdit)
    fileName = char(filesToEdit{fInd});
    
    if isfile(fileName)
        lines = readlines(fileName);
    else
        disp(strcat("Error! ",fileName, " not found!"));
    end
    
    for i = 1:length(lines)
        if contains(lines{i},queryTag)
            disp(lines{i})
            nextLine = lines{i};
            while (~isempty(nextLine))
                for p = 1:length(params)
                    nextLineC = char(nextLine);
                    if (length(nextLineC)<length(char(params(p))))
                        continue;
                    end
                    if (nextLineC(1:length(char(params(p)))) == params(p))
                        newParamLine = strcat(params(p), " = ", paramValues(p));
                        lines{i} = char(newParamLine);
                        break;
                    end
                end
                i = i + 1;
                nextLine = lines{i};
            end
            break;
        end
    end
    
    spFN = split(fileName,"/");
%     if (length(spFN)==1)
%         newDirFN = patientDir;
%         fileNameE = strcat(patientDir,"/",spFN);
%     else
%         newDirFN = strcat(join(spFN(1:end-1),"/"),"/",patientDir);
%         fileNameE = strcat(newDirFN,"/",spFN(end));
%     end
    newDirFN = strcat(dataHomePath,patientDir,"/Scripts");
    fileNameE = strcat(newDirFN,"/",spFN(end));
    
    if ~exist(newDirFN, 'dir')
       mkdir(newDirFN)
    end
    fid = fopen(fileNameE,'w');
    for i = 1:length(lines)
        fprintf(fid,'%s\n',lines{i});
    end
    fclose(fid);
    
end
% $$ UPDATE PARAMETERS $$
% patientID = 1;
% cdmd = 1; % nAm/mm^2 %%%%%%%%%%% need a way to automate changing this %%%%
% dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';


%% also change slurm scripts!
% Using template files in home directory, replace all tags with patient
%   specific values.
% Save new scripts in home/PatientDir

tags = ["$$PATDIR$$","$$HOMEDIR$$",'$$WORKDIR$$','$$NUMDIPOLES$$','$$NUMSOURCETYPES$$','$$NUMLFS$$','$$NUMSEARCHCASES$$'];
patientDir = strcat('Patient_',num2str(patientID));

tagReps = {patientDir, strcat(dataHomePath), strcat(dataWorkPath), num2str(numDipoles), num2str(length(cdmds)),num2str(3*length(cdmds)),num2str(length(ROIs)*length(cdmds))};

% for testing!
test = 0;
scriptLoadPath = strcat(dataHomePath, "BashScripts/");


% template files in home directory
filesToEdit = {strcat(scriptLoadPath,"getELocs.slurm"), ...
    strcat(scriptLoadPath,"runOpt_sEEG.slurm"), ... 
    strcat(scriptLoadPath,"runSecond.slurm"), ... 
    strcat(scriptLoadPath,"makePatchAllMatrices.slurm"), ...
    strcat(scriptLoadPath,"makeHeadTetMesh.slurm"), ...
    strcat(scriptLoadPath,"electrodeCollisionsCompute.slurm"), ...
    strcat(scriptLoadPath,"makeAndRunNets.slurm"), ...
    strcat(scriptLoadPath,"createLFfromFSs.slurm"), ...
    strcat(scriptLoadPath,"precompRecAreaAllElectrodes.slurm"), ...
    strcat(scriptLoadPath,"NBS.slurm") ...
    strcat(scriptLoadPath,"getROIs.slurm") ...
    strcat(scriptLoadPath,"analyzeAndPrepVis.slurm") ...
    };

% parse following lines until empty (or just whitespace) line
for fInd = 1:length(filesToEdit)
    fileName = char(filesToEdit{fInd});
    
    if isfile(fileName)
        lines = readlines(fileName);
    else
        disp(strcat("Error! ",fileName, " not found!"));
        break;
    end
    
    for i = 1:length(lines)
        for t = 1:length(tags)
            thisLine = lines{i};
            thisTag = tags(t);
            if contains(thisLine,thisTag)
                lines{i} = char(strrep(thisLine,thisTag,tagReps{t}));
            end
        end

        thisLine = lines{i};
    end
    
%     if (length(spFN)==1)
%         newDirFN = patientDir;
%         fileNameE = strcat(patientDir,"/",spFN);
%     else
%         newDirFN = strcat(join(spFN(1:end-1),"/"),"/",patientDir);
%         fileNameE = strcat(newDirFN,"/",spFN(end));
%     end
    spFN = split(fileName,"/");
    newDirFN = strcat(dataHomePath,patientDir,"/Scripts");
    fileNameE = strcat(newDirFN,"/",spFN(end));
    fileNameInit2 = strcat(dataHomePath,spFN(end));
    
    if ~exist(newDirFN, 'dir')
       mkdir(newDirFN)
    end
    if fileName == strcat(scriptLoadPath,"initOpt2_sEEG.slurm")
        fileNameE = fileNameInit2;
    end
    fid = fopen(fileNameE,'w');
    for i = 1:length(lines)
        fprintf(fid,'%s\n',lines{i});
    end
    fclose(fid);
    
end

end

function lines = readlines(fileName)
    % Opens the file
    fid = fopen(fileName,'rt');

    maxl = -1; % read whole file
    lines = textscan(fid,'%s',maxl,'delimiter', '\n', 'whitespace','');
    if ~isempty(lines)
        lines = lines{1};
    end
    % Close file
    fclose(fid);
end


%{
% exampleChangeParams.m
% Grace Dessert
% March 4, 2022

% $$ UPDATE PARAMETERS $$
patientID = 1;
cdmd = 1; % nAm/mm^2 %%%%%%%%%%% need a way to automate changing this %%%%
dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';

disp(strcat("Current patientID is: ",num2str(patientID)))
disp(strcat("Current cdmd is: ",num2str(cdmd)))
disp(strcat("Current dataWorkPath is: ",dataWorkPath))
%}
