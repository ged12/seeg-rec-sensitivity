% investigate patch projected area

% ***************************************************************
% Grace Dessert
% 5/21/23
% ***************************************************************

% Project each patch onto the skull surface
% Do this for patient 25.

% re-generated patches for patient 25 
%   using makePatchAllMatrices_demo.m with full generation

% function projectedPatchArea(runI)

patientID = 25;
local = true;
if local
    runI = 11;
    dataHomePath = '/Volumes/Seagate Backup Plus Drive/disc-signal-grill/Optimize_sEEG_implant/seeg-rec-sensitivity/';
    dataHomePath = '/Volumes/Seagate Backup Plus Drive/disc-signal-grill/Optimize_sEEG_implant/seeg-rec-sensitivity/';
else
    dataHomePath = '/hpc/group/wmglab/ged12/cluster/Optimize_sEEG_implant/seeg-rec-sensitivity/';
    dataWorkPath = '/hpc/group/wmglab/ged12/cluster/Optimize_sEEG_implant/seeg-rec-sensitivity/';
end

patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2

plotTest = true

uniquePatchAreas = unique(patchAreas)*100; % mm^2
patientDir = ['Patient_' num2str(patientID)];

% load cortex inputs
cortexSurf = stlread(strcat(dataHomePath,patientDir,'/Inputs/cortexSurf.stl'));
load(strcat(dataWorkPath,patientDir,'/HeadModel/cortex_areas.mat'),'cortex_areas');

% load skull surface
skullFN = strcat(dataHomePath,patientDir,'/Inputs/SkullFinal.stl');
skullSurf = stlread(skullFN);

% get skull face areas. save and load if already run
skullAreaFN = strcat(dataHomePath,patientDir,'/DemoOut/SkullAreas.mat');
if exist(skullAreaFN,'file')
    load(skullAreaFN,'skullAreas')
else
    skullAreas = get_cortex_areas(skullSurf);
    save(skullAreaFN,'skullAreas')
end
%
% generate skull vertex connectivity graph
% this is a sparse logical matrix where each row gives the indices of points
% connected to a point. 

% dilation is taking the union of all current vertex connections
%   currVerts = find(any(vGraph(currVerts,:),1))
% erosion is reducing to only the vertices that are fully connected 
%   this means that included vertices are those whose connections are all
%   members of the current set. 
%   currVerts = find(sum(vGraph(currVerts,:),1)==6)
%       given each vertex has 6 connections / neighbors in the full graph
% fill in vGraph
vGraphFN = strcat(dataHomePath,patientDir,'/DemoOut/SkullPointConnectivity.mat');
if exist(vGraphFN,'file')
    load(vGraphFN,'vGraph')
else
    tic
    vGraphIs = reshape(repmat(1:length(skullSurf.Points),6,1),1,[]);
    vGraphJs = zeros(length(skullSurf.Points),6);
    vGraphVs = true(1,length(vGraphIs));
    for pI = 1:length(skullSurf.Points)
        [connectedFaces,~] = find(skullSurf.ConnectivityList==pI);
        connectedVerts = unique(reshape(skullSurf.ConnectivityList(connectedFaces,:),1,[]));
        % remove self
        connectedVerts(connectedVerts==pI)=[];

        if length(connectedVerts) < 6
            disp(pI)
            disp("LESS THAN 6 CONNECTIONS?")
            connectedVerts = [connectedVerts,zeros(1,6-length(connectedVerts))];
        elseif length(connectedVerts) > 6
            disp(pI)
            disp("MORE THAN 6 CONNECTIONS?")
            connectedVerts = connectedVerts(1:6);
        end

        vGraphJs(pI,:) = connectedVerts;
    end

    vGraphJs = reshape(vGraphJs',1,[]);
    % remove zeros for vertices that have < 6 connections ... 
    zeroInds = find(vGraphJs==0);
    vGraphJs(zeroInds) = [];
    vGraphIs(zeroInds) = [];
    vGraphVs(zeroInds) = [];

    vGraph = sparse(vGraphIs,vGraphJs,vGraphVs);
    save(vGraphFN,'vGraph')
    graphConstruction = toc
end

warning('off','MATLAB:triangulation:PtsNotInTriWarnId')

% slurm script runs 30 jobs
% each 10 indicate different patch size
i = ceil(runI/10);
% full jobs split into 10 pieces
patchPortionI = runI-10*(i-1);

% load already-created patches. sparse logical matrix.
load(strcat(dataWorkPath,patientDir,'/DemoOut/PatchAllSt_',num2str(uniquePatchAreas(i)/100),'cm2.mat'),'patchAll');

% get indices for one patch 
if plotTest
    testPatchInds = 900;
else
    sizeEachPortion = ceil(length(patchAll)/10);
    disp((patchPortionI-1)*sizeEachPortion+1)
    disp(patchPortionI*sizeEachPortion)
    testPatchInds = (patchPortionI-1)*sizeEachPortion+1:min(patchPortionI*sizeEachPortion,length(patchAll));
end

pInfoFN = strcat(dataHomePath,patientDir,'/DemoOut/projectedPatchInfo_',num2str(uniquePatchAreas(i)/100),'_',num2str(patchPortionI),'cm2.mat');

for patchI = 1:length(testPatchInds)
    
    tic
    patchIndex = testPatchInds(patchI)
    
    % patchI
    
    patchFaces = find(patchAll(patchIndex,:)==1);
    
    % get area and triangulation
    fullPatchArea = sum(cortex_areas(patchFaces))/100; %cm2
    patchfield = triangulation(cortexSurf.ConnectivityList(patchFaces,:),cortexSurf.Points);

    % exclude if its along midline or on bottom of cortex

    % project patch onto skull surfaces
    % map each patch vertex onto its nearest neighbor skull surface vertex
    % construct patch from all included vertices
    
    [newSkullPoints, skullFacesThis, skullPatchField, maxDistToSkull] = projectPatch(patchfield,skullSurf,plotTest,vGraph);
    if isempty(skullFacesThis)
        continue
    end

    % get area of projection
    skullPatchArea = sum(skullAreas(skullFacesThis))/100 % cm2
    patchInfo.projectedAreas(patchI) = skullPatchArea;
    patchInfo.maxDistToSkull(patchI) = maxDistToSkull/10; % cm % if > 3cm away (any point of patch is > 3 cm away) eliminate bc projection is likely not continuous. cannot measure area well.
    
    % get farthest distance
    [maxDiameter, meanRadiusFlat] = getDiameterPatch(newSkullPoints, plotTest); % cm
    maxRadius = maxDiameter/2;
    patchInfo.maxRadius(patchI) = maxDiameter/2;
    patchInfo.meanRadiusFlat(patchI) = meanRadiusFlat;
    
    % plot
    if plotTest
        figure(1)
        trimesh(cortexSurf,'FaceAlpha',0,'EdgeColor',[0.3,0.3,0.5],'EdgeAlpha',0.4)
        trimesh(patchfield, 'FaceColor', 'yellow','FaceAlpha',0.2,'EdgeColor','black')
    end

    toc
    % store partial solution
    if ~plotTest && mod(patchI,1000)==0 

        save(pInfoFN,'patchInfo')
    end

end

if ~plotTest
    save(pInfoFN,'patchInfo')
end


return

%%
% plot results


%% concatenate all files

i = 3;
uniquePatchAreas = [600,1000,2000];
dataHomePath = '/Volumes/Seagate Backup Plus Drive/disc-signal-grill/Optimize_sEEG_implant/seeg-rec-sensitivity/';
dataWorkPath = dataHomePath;
patientDir = 'Patient_25';
load(strcat(dataWorkPath,patientDir,'/DemoOut/PatchAllSt_',num2str(uniquePatchAreas(i)/100),'cm2.mat'),'patchAll');

numPatches = length(patchAll);
patchInfoFull.projectedAreas = zeros(1,numPatches);
patchInfoFull.maxDistToSkull = zeros(1,numPatches);
patchInfoFull.maxRadius = zeros(1,numPatches);
patchInfoFull.meanRadiusFlat = zeros(1,numPatches);
for patchPortionI = 1:10
    sizeEachPortion = ceil(numPatches/10);
    testPatchInds = (patchPortionI-1)*sizeEachPortion+1:min(patchPortionI*sizeEachPortion,length(patchAll));

    pInfoFN = strcat(dataHomePath,patientDir,'/DemoOut/projectedPatchInfo_',num2str(uniquePatchAreas(i)/100),'_',num2str(patchPortionI),'cm2.mat');
    load(pInfoFN,'patchInfo')

    patchInfoFull.projectedAreas(testPatchInds) = patchInfo.projectedAreas;
    patchInfoFull.maxDistToSkull(testPatchInds) = patchInfo.maxDistToSkull;
    patchInfoFull.maxRadius(testPatchInds) = patchInfo.maxRadius;
    patchInfoFull.meanRadiusFlat(testPatchInds) = patchInfo.meanRadiusFlat;
end

pInfoFullFN = strcat(dataHomePath,patientDir,'/DemoOut/projectedPatchInfo_',num2str(uniquePatchAreas(i)/100),'cm2.mat');
save(pInfoFullFN,'patchInfoFull')

%% A) Plot of a 10cm2 patch with projection

% select patch of interest
patchIndex = 900;
patchAreaI = 2;
% load already-created patches. sparse logical matrix.
load(strcat(dataWorkPath,patientDir,'/DemoOut/PatchAllSt_',num2str(uniquePatchAreas(patchAreaI)/100),'cm2.mat'),'patchAll');
patchFaces = find(patchAll(patchIndex,:)==1); 
% get triangulation and save patch field
patchfield = triangulation(cortexSurf.ConnectivityList(patchFaces,:),cortexSurf.Points);
stlwrite(patchfield,'RawData/VisCurrent/ROI.stl')

% skull
% load skull surface
skullFN = strcat(dataHomePath,patientDir,'/Inputs/SkullFinal.stl');
skullSurf = stlread(skullFN);
stlwrite(skullSurf,'RawData/VisCurrent/skull.stl')

% get projected patch again
plotTest = 1;
% get vGraph
vGraphFN = strcat(dataHomePath,patientDir,'/DemoOut/SkullPointConnectivity.mat');
load(vGraphFN,'vGraph')
[newSkullPoints, skullFacesThis, skullPatchField, maxDistToSkull] = projectPatch(patchfield,skullSurf,plotTest,vGraph);
stlwrite(skullPatchField,'RawData/VisCurrent/projectedPatch.stl')

% get projected patch area and max radius
% patch area of original patch
fullPatchArea = sum(cortex_areas(patchFaces))/100; %cm2
disp(fullPatchArea) % cm2
skullAreaFN = strcat(dataHomePath,patientDir,'/DemoOut/SkullAreas.mat');
load(skullAreaFN,'skullAreas')
skullPatchArea = sum(skullAreas(skullFacesThis))/100; % cm2
disp(skullPatchArea) % cm2

disp(maxDistToSkull/10) % cm

[maxDiameter, meanRadFlat] = getDiameterPatch(newSkullPoints, plotTest); % cm
maxRadius = maxDiameter/2;
disp(maxRadius) % cm


% cortex surface
stlwrite(cortexSurf,'RawData/VisCurrent/lh.stl')
stlwrite(cortexSurf,'RawData/VisCurrent/rh.stl')

% run SCIRun file ContentScripts/visConfigRec.srn5


%% B) Histogram of projected area
% exclude patches with max dist > 3cm

% select area of interest
f = figure(2)
clf
hold on

projectedAreasAll = cell(3,1);
% exclude same patches across all areas. union of invalid patches for each.
excludedPatches = [];
for tI = 1:3
    uniquePatchAreas = [600,1000,2000];
    pInfoFullFN = strcat(dataHomePath,patientDir,'/DemoOut/projectedPatchInfo_',num2str(uniquePatchAreas(tI)/100),'cm2.mat');
    load(pInfoFullFN,'patchInfoFull')
    for i = 1:length(patchInfoFull.maxDistToSkull)
        if patchInfoFull.maxDistToSkull(i) > 3 || patchInfoFull.maxDistToSkull(i)==0
            excludedPatches = [excludedPatches, i];
        end
    end
end
excludedPatches = unique(excludedPatches);
disp(strcat("EXCLUDING ",num2str(length(excludedPatches))," of ",num2str(length(patchInfoFull.maxDistToSkull)),"patches."))


for tI = 1:3
    uniquePatchAreas = [600,1000,2000];
    pInfoFullFN = strcat(dataHomePath,patientDir,'/DemoOut/projectedPatchInfo_',num2str(uniquePatchAreas(tI)/100),'cm2.mat');
    load(pInfoFullFN,'patchInfoFull')
    
    projectedAreas = [];
    for i = 1:length(patchInfoFull.maxDistToSkull)
        if ~ismember(i,excludedPatches)
            projectedAreas = [projectedAreas, patchInfoFull.projectedAreas(i)];
        end
    end
    
    histogram(projectedAreas,20,'FaceAlpha',0.8,'EdgeAlpha',1)

    projectedAreasAll{tI} = projectedAreas;

    avg = mean(projectedAreas)
end

xlabel("Area of projected patch (cm^2)")
ylabel("Count")
ax = gca; 
ax.FontSize = 20; 
grid on
f.Position = [800 350 530 280];
xticks([0:1:15])
yticks([0:1000:6000])
legend(["6cm2", "10cm2","20cm2"])

fileName = strcat(dataHomePath,'Figures/FigureS5b.pdf');
print(gcf,fileName,'-bestfit','-dpdf');

%% save in table 
varNames = ["Projected area (cm2) for 6cm2 patch","Projected area (cm2) for 10cm2 patch","Projected area (cm2) for 20cm2 patch"];
T = table(projectedAreasAll{1}',projectedAreasAll{2}',projectedAreasAll{3}','VariableNames',varNames);
writetable(T,'SourceData/FigureS5b.xls')


%%

% plot max radius as a fn of max distance away
% to set exclusion criteria
% figure(3)
% clf
% xs = patchInfoFull.maxDistToSkull;
% ys = patchInfoFull.maxRadius;
% scatter(xs,ys,'filled')
% xlabel('max distance to skull (cm)')
% ylabel('projected area (cm^2)')



%% C) Histogram of MEAN radius
% exclude patches with max dist > 3cm

f = figure(3)
clf
hold on


% exclude same patches across all areas. union of invalid patches for each.
excludedPatches = [];
for tI = 1:3
    uniquePatchAreas = [600,1000,2000];
    pInfoFullFN = strcat(dataHomePath,patientDir,'/DemoOut/projectedPatchInfo_',num2str(uniquePatchAreas(tI)/100),'cm2.mat');
    load(pInfoFullFN,'patchInfoFull')
    for i = 1:length(patchInfoFull.meanRadiusFlat)
        if patchInfoFull.maxDistToSkull(i) > 3 || patchInfoFull.maxDistToSkull(i)==0
            excludedPatches = [excludedPatches, i];
        end
    end
end
excludedPatches = unique(excludedPatches);
disp(strcat("EXCLUDING ",num2str(length(excludedPatches))," of ",num2str(length(patchInfoFull.maxDistToSkull)),"patches."))

meanRadAll = cell(3,1);
for patchI = 1:3
    uniquePatchAreas = [600,1000,2000];
    pInfoFullFN = strcat(dataHomePath,patientDir,'/DemoOut/projectedPatchInfo_',num2str(uniquePatchAreas(patchI)/100),'cm2.mat');
    load(pInfoFullFN,'patchInfoFull')

    meanRadii = [];
    for i = 1:length(patchInfoFull.maxDistToSkull)
        if ~ismember(i,excludedPatches)
            meanRadii = [meanRadii, patchInfoFull.meanRadiusFlat(i)];
        end
    end
    
    histogram(meanRadii,20,'FaceAlpha',0.8,'EdgeAlpha',1)
    meanRadAll{patchI} = meanRadii;

    avg = mean(meanRadii)
end

xlabel("Mean radius projected patch (cm)")
ylabel("Count")
ax = gca; 
ax.FontSize = 20; 
grid on
f.Position = [800 350 530 280];
xticks([0:0.25:15])
yticks([0:1000:7000])
legend(["6cm2", "10cm2","20cm2"])
fileName = strcat(dataHomePath,'Figures/FigureS5c.pdf');
print(gcf,fileName,'-bestfit','-dpdf');


%% save in table 
varNames = ["MEAN radius (cm) for projected patch of 6cm2 patch","MEAN radius (cm) for projected patch of 10cm2 patch","MEAN radius (cm) for projected patch of 20cm2 patch"];
T = table(meanRadAll{1}',meanRadAll{2}',meanRadAll{3}','VariableNames',varNames);
writetable(T,'SourceData/FigureS5c.xls')


% end




function [maxDiameter, meanRadiusFlat] = getDiameterPatch(points, plotTest)
    % get max distance between any two points in patch
    dists = zeros(length(points));
    for i = 1:length(points)
        dists(i,:) = sqrt(sum((points-points(i,:)).^2,2));
    end
    maxDiameter = max(max(dists))/10; % cm

    % project patch onto plane
    [n,V,p] = affine_fit(points);
    flatPoints = points - sum(((points - p).* repmat(n',length(points),1)),2) * n';

    % rotate onto XY plane so we can treat as 2D
    % need to find Rotation matrix R such that
    %   R * v1 = [1;0;0] and R * v2 = [0;1;0] and R * n = [0;0;1]
    % this means:
    %   R * [v1|v2|n] = [1,0,0;0,1,0;0,0,1]
    %  R = eye(3) * inverse([v1|v2|n])
    % R = eye(3) * inv([V,n]); % notice that inverse of [V,n] is just the transpose bc ortonogonal matrix
    % R = [V,n]'; % so R is just the orthogonal basis as rows!
    
    flatPointsXY = ([V,n]' * flatPoints')';
    % translate to z=0
    flatPointsXY(:,3)=0;

    % plot to see if this worked
    if plotTest
        % plot original points
        scatter3(points(:,1),points(:,2),points(:,3),'filled')
        % plot plane
        z = linspace(min(points(:,3)),max(points(:,3)),10);
        y = linspace(min(points(:,2)),max(points(:,2)),10);
        [z2,y2]=meshgrid(z,y);
        % ax + by + cz + d = 0
        d = -p*n;
        x2 = (-n(2)*y2 - n(3)*z2 - d)/n(1);
        % plot plane
        surf(x2,y2,z2,'FaceAlpha',0.1);
        % plot projected points
        scatter3(flatPoints(:,1),flatPoints(:,2),flatPoints(:,3),'filled')
        % plot rotated points
        scatter3(flatPointsXY(:,1),flatPointsXY(:,2),flatPointsXY(:,3),'filled')
        % plot XY plane
        x = linspace(-100,100,10);
        y = linspace(-100,100,10);
        [x2,y2]=meshgrid(x,y);
        % plot plane
        plot(x2,y2);
    end
    
    % get boundary points
    shp = alphaShape(flatPointsXY(:,1),flatPointsXY(:,2));
    a = area(shp)/100; % cm2
    
    % get centroid (mean of all coordinates)
    k = boundary(flatPointsXY(:,1),flatPointsXY(:,2));
    boundaryPoints = flatPointsXY(k,:);
    
    centroid = [mean(flatPointsXY(:,1)),mean(flatPointsXY(:,2))];
    % get all radii and mean
    radii = sqrt(sum((boundaryPoints(:,1:2) - centroid).^2,2));
    meanRadiusFlat = mean(radii)/10; % cm
    
    if plotTest
        scatter(centroid(1),centroid(2),100,'filled');
        disp(strcat("area is: ",num2str(a)," cm2"))
        disp(strcat("meanRadiusFlat is: ",num2str(meanRadiusFlat)," cm"))
    end

end


function [areas] = get_cortex_areas(FV)
    areas = zeros(length(FV.ConnectivityList),1);
    for i = 1:length(areas) % for each face
        face_points = [FV.ConnectivityList(i,1),FV.ConnectivityList(i,2),FV.ConnectivityList(i,3)];
        a = face_points(1);
        pa = FV.Points(a,1:3);
        b = face_points(2);
        pb = FV.Points(b,1:3);
        b = face_points(3);
        pc = FV.Points(b,1:3);
        %calc area from points
        areas(i) = 0.5 * norm(cross(pb-pa,pc-pa));
        %format = "face: %.0f \n    face_points: %.0f %.0f %.0f \n    area: %f \n";
        %fprintf(format, x, face_points, area)
    end
end


function [newSkullPoints, skullFacesThis, skullPatchField, maxDistToSkull] = projectPatch(patchfield,skullSurf,plotTest,vGraph)
    % project patch onto skull surfaces
    % map each patch vertex onto its nearest neighbor skull surface vertex
    % construct patch from all included vertices
    
    % just output distance so we can eliminate later flexibly
    % if min distance between patch and skull is too large, eliminate case
    %   this is true if patch is on inner side of cortex, in which case the
    %   projection is usually wonky.
    % eliminate = false;

    % exclude unreferenced vertices and reindex 
    allReferencedInds = unique(reshape(patchfield.ConnectivityList,[],1));
    patchFaces = patchfield.ConnectivityList;
    for i = 1:length(patchfield.ConnectivityList )
        for j = 1:3
            patchFaces(i,j) = find(patchfield.ConnectivityList(i,j)==allReferencedInds)';
        end
    end
    patchVertices = patchfield.Points(allReferencedInds,:);

    % refine patch so it is more refined than the skull
    % if exist('DemoScripts','dir')
    %     addpath('DemoScripts')
    %     addpath('DemoScripts/refinepatch_version2b')
    % end
    % refine surface
    % patch_refined.faces = patchFaces;
    % patch_refined.vertices = patchVertices;
    % for i = 1
    %     patch_refined = refinepatch(patch_refined);
    % end

    % patchVertices = patch_refined.vertices;

    % for each patch vertex, find the closest vertex on the skull surface
    minDists = zeros(size(patchVertices,1),1); % mm
    skullPatchPointIs = zeros(size(patchVertices,1),1);
    
    for i = 1:size(patchVertices,1)
        patchVertxPoint = patchVertices(i,:);
        dists = sum((patchVertxPoint-skullSurf.Points).^2,2);
        minDists(i) = sqrt(min(dists)); % mm
        skullPatchPointIs(i) = find(dists==min(dists));
    end
    maxDistToSkull =  max(minDists); % mm
    % if ~plotTest && max(minDists) > 30 % mm
    %     eliminate = true;
    %     newSkullPoints = [];
    %     skullFacesThis = [];
    %     skullPatchField = [];
    %     return
    % end

    % dialate 3x
    for p = 1:3
        skullPatchPointIs = find(any(vGraph(skullPatchPointIs,:),1));
    end
    % erode 3x (but keep adjacent count >= 5 cuz some vertices only have 5
    % neighbors)
    for p = 1:3
        skullPatchPointIs = find(sum(vGraph(skullPatchPointIs,:),1)>=5);
    end
    newSkullPoints = skullSurf.Points(skullPatchPointIs,:);
    % get all faces whos vertices are all members
    includedFaces = find(sum(ismember(skullSurf.ConnectivityList,skullPatchPointIs),2)==3);
    
    if isempty(includedFaces)
        skullPatchField = [];
        skullFacesThis = [];
        if plotTest
            disp("no faces in projection!")
        end
        return
    end

    if plotTest
        skullConn = skullSurf.ConnectivityList(includedFaces,:);
        % reindex point inds in connectivity list
        for i = 1:length(skullConn)
            for j = 1:3
                skullConn(i,j) = find(skullConn(i,j)==skullPatchPointIs);
            end
        end
        newSkullSurf = triangulation(skullConn,newSkullPoints);
        figure(1)
        clf 
        hold on
        trimesh(newSkullSurf,'FaceColor','cyan')
        trimesh(skullSurf,'EdgeColor','black','FaceAlpha',0,'EdgeAlpha',0.1)
        % scatter3(newSkullPoints(:,1),newSkullPoints(:,2),newSkullPoints(:,3),20,'black','filled')
    else
        newSkullSurf =[];
    end

    %{
    % get all faces that have all any vertex included
    %includedFaces = find(sum(ismember(skullSurf.ConnectivityList,minDistIndsSkull),2)==3);
    
    %skullConn = skullSurf.ConnectivityList(includedFaces,:);
    %fullPoints = unique(reshape(skullConn,[],1),'stable');

    % includedFaces = find(sum(ismember(skullSurf.ConnectivityList,fullPoints),2)>0);
    % skullConn = skullSurf.ConnectivityList(includedFaces,:);
    % fullPoints = unique(reshape(skullConn,[],1),'stable');
    % 
    % includedFaces = find(sum(ismember(skullSurf.ConnectivityList,fullPoints),2)>0);
    % skullConn = skullSurf.ConnectivityList(includedFaces,:);
    % fullPoints = unique(reshape(skullConn,[],1),'stable');

    % check and clean
    % includedFaces = find(sum(ismember(skullSurf.ConnectivityList,fullPoints),2)==3);
    % skullConn = skullSurf.ConnectivityList(includedFaces,:);
    % fullPoints = unique(reshape(skullConn,[],1),'stable');
    
    % erode 3x
    % for all vertices, remove those whose neighbors are not all in patch
    %   aka, those members of faces that are not added
    % for k = 1:3
    %     removePointInds = [];
    %     for vI = 1:length(fullPoints)
    %         % get all faces in full skull that reference this point
    %         [connectedFaces,~] = find(skullSurf.ConnectivityList==fullPoints(vI));
    %         % if there are any connected faces that are NOT in the patch,
    %         %   this vertex is on the boundary and we can remove it
    %         if any(~ismember(connectedFaces,includedFaces))
    %             removePointInds = [removePointInds,vI];
    %         end
    %     end
    %     fullPoints(removePointInds) = [];
    %     includedFaces = find(sum(ismember(skullSurf.ConnectivityList,fullPoints),2)==3);
    % end

    % get all faces whos vertices are all members
    % skullConn = skullSurf.ConnectivityList(includedFaces,:);
    %}
    
    % get skull faces from skull vertices
    skullFacesThis = includedFaces;
    skullPatchField = newSkullSurf;

end



function [n,V,p] = affine_fit(X)

% Copyright (c) 2016, Fangdi Sun
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
% 
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in
%       the documentation and/or other materials provided with the distribution
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.


    %Computes the plane that fits best (lest square of the normal distance
    %to the plane) a set of sample points.
    %INPUTS:
    %
    %X: a N by 3 matrix where each line is a sample point
    %
    %OUTPUTS:
    %
    %n : a unit (column) vector normal to the plane
    %V : a 3 by 2 matrix. The columns of V form an orthonormal basis of the
    %plane
    %p : a point belonging to the plane
    %
    %NB: this code actually works in any dimension (2,3,4,...)
    %Author: Adrien Leygue
    %Date: August 30 2013
    
    %the mean of the samples belongs to the plane
    p = mean(X,1);
    
    %The samples are reduced:
    R = bsxfun(@minus,X,p);
    %Computation of the principal directions if the samples cloud
    [V,D] = eig(R'*R);
    %Extract the output from the eigenvectors
    n = V(:,1);
    V = V(:,2:end);
end


function rot = rotz(angle)
    rot = [cos(angle) -sin(angle) 0; sin(angle) cos(angle) 0; 0 0 1];
end

function rot = rotx(angle)
    rot = [1 0 0; 0 cos(angle) -sin(angle); 0 sin(angle) cos(angle)];
end

function rot = roty(angle)
    rot = [cos(angle) 0 sin(angle); 0 1 0; -sin(angle) 0 cos(angle)];
end



