% Plot Figure 3 and create Source Data files
% Grace Dessert

% global vars
cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
dataWorkPath = '';
dataHomePath = '';
patientIDs = [2,3,7,19,20,24,25,26,27,30,31,33];
costFnStrs = ["200_1000_500","500_1000_200","1000_500_200"];
degMapStrs = ["single mapping at 200 µV threshold", "single mapping at 500 µV threshold", "single mapping at 1000 µV threshold"];
ROIs = ["clinicianROI","LTL","LH"]; % array 

% select case
patientID = 25;
sourceInd = 4;

%% load data for this patient and source type
% note that only Patient 25 data is given in the repository
% for other patients, you must run the full pipeline to generate data

patientDir = ['Patient_' num2str(patientID)];
tys_cA = strcat(num2str(patchAreas(sourceInd)),"cm2_",num2str(cdmds(sourceInd)),"cdmd");
load(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_cis',tys_cA,'.mat'),'visDataConfigs')
load(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataImplantConfig_singleMap_crossAnalyze',tys_cA,'.mat'),'visDataImplantConfig')
load(strcat(dataHomePath,patientDir,'/Inputs/ContactLocations.mat'),'scirunfield')
contLocs = scirunfield;

LH_fileName = strcat(dataHomePath,patientDir,'/Inputs/lh_white_20k.stl');
LH_white = stlread(LH_fileName);
RH_fileName = strcat(dataHomePath,patientDir,'/Inputs/rh_white_20k.stl');
RH_white = stlread(RH_fileName);
load(strcat(dataWorkPath,patientDir,'/OptSolutions/ROIs.mat'),'patches')


%% Figure3a-c
% make sure you run the previous sections in this script
% Figure 3 is saved automatically in Figures/ directory
% You need to use 
addpath('MainPipeline')
thrI = 2;
ROI_str = "LTL"; 
fileName = "Figure3c";
visThisConfig(fileName,patientID,visDataConfigs,thrI,ROI_str,contLocs,tys_cA,LH_white,RH_white,patches,visDataImplantConfig)

% Figure 3a and 3b must be generated with the SCIRun network 'InputGeneral/visConfigRec.srn5',
%   after running this section.
%   This section creates custom files in the 'RawData/VisCurrent/' directory for
%   the SCIRun network to load.
%   Make sure to update the filepaths of all vis files in the SCIRun
%   network.
% InputGeneral/visConfigRec.srn5

%% Figure3 d-e
for ROI_i = 1:2
    redoCalc = false;
    [data_imp,data_imp_ROIAreas] = saveOrOpenCisImplant(redoCalc, ROI_i,sourceInd,patchAreas,cdmds,dataWorkPath,patientIDs,costFnStrs);
    saveAndDispTable = true;
    saveTag = "compOpt"; % for comparing RS, we use different TL surfaces
    dat = saveOrOpenCisSearch(ROI_i,sourceInd,redoCalc, patientIDs,degMapStrs,costFnStrs,dataWorkPath,patchAreas,cdmds,ROIs,saveTag);
    plotDiff = false;
    noColorVersion = true;
    [~,T] = boxOptImpData(ROI_i, dat, patientIDs, data_imp, plotDiff, noColorVersion);
end

%% Figure3 f-g
for ROI_i = 1
    redoCalc = false;
    [data_imp,data_imp_ROIAreas] = saveOrOpenCisImplant(redoCalc, ROI_i,sourceInd,patchAreas,cdmds,dataWorkPath,patientIDs,costFnStrs);
    saveAndDispTable = true;
    saveTag = "compOpt"; % for comparing RS, we use different TL surfaces
    dat = saveOrOpenCisSearch(ROI_i, sourceInd,redoCalc, patientIDs,degMapStrs,costFnStrs,dataWorkPath,patchAreas,cdmds,ROIs,saveTag);
    percD = false;
    plotDiff = false;
    noColorVersion = true;
    numElectrodesCompareBox(saveAndDispTable, ROI_i, dat, patientIDs, data_imp, percD, plotDiff, noColorVersion);
end


function f = numElectrodesCompareBox(saveAndDispTable, ROI_i, dat, patientIDs, data_imp, percD, plotDiff, noColorVersion)

% plot num electrodes implanted configurations
% and num electrodes to get ≥ final RS with opt configurations
% and plot difference

f = figure(4)
clf
hold on
colorsPoints = parula(length(patientIDs)+1);
colors = parula(4);
if noColorVersion
    c7 = colorsPoints(7,:) - 0.4;
    c7(c7<0)=0;
    c7 = c7 + 0.1;
    c7(c7>1)=1;
    for i = 1:length(patientIDs)+1
        colorsPoints(i,:) = c7;
    end
end
colorsLines = colorsPoints;
if noColorVersion
    colorsLines = colorsLines + 0.2;
    colorsLines(colorsLines>1)= 1;
end

dat_mapPerc = cell(3,1);
dat_mapPerc_imp = cell(3,1);

for t = 1:3
    dat_mapPerc{t} = zeros(length(patientIDs),1);
    dat_mapPerc_imp{t} = zeros(length(patientIDs),1);
end

patI = 1;
positions = (rand(length(patientIDs),1)-0.5)/20;

patIs = [];

for patientID = patientIDs
    
    if isempty(data_imp{patI,t})
        patI = patI + 1;
        continue
    end
    for t = 1:3
        numEsImp = length(data_imp{patI,t});
        RSImp = data_imp{patI,t}(end);
        numEsOpt = find(dat{patI,t}>=RSImp,1);
        if isempty(numEsOpt)
            disp("opt never gets to same RS?? for patient: ")
            disp(patI)
            numEsOpt = 31;
        end
        dat_mapPerc{t}(patI) = numEsOpt;
        dat_mapPerc_imp{t}(patI) = numEsImp;
        
        % plot pat 25 with special symbols 
        if patientIDs(patI) == 25
            patI25 = patI;
            plot(positions(patI)+[0,0.5]+t,[numEsImp,numEsOpt],'--','LineWidth',3,'color',colorsLines(patI,:),'DisplayName',strcat("Patient ",num2str(patientID)),'LineWidth',1,'HandleVisibility','off');
            scatter(positions(patI)+t,numEsImp,250,colorsPoints(patI,:),'X','LineWidth',1.5,'HandleVisibility','off');
            scatter(positions(patI)+0.5+t,numEsOpt,250,colorsPoints(patI,:),'X','LineWidth',1.5,'HandleVisibility','off');
        else
            plot(positions(patI)+[0,0.5]+t,[numEsImp,numEsOpt],'.-','color',colorsLines(patI,:),'DisplayName',strcat("Patient ",num2str(patientID)),'LineWidth',0.6,'HandleVisibility','off');
            scatter(positions(patI)+t,numEsImp,25,colorsPoints(patI,:),'filled','HandleVisibility','off');
            scatter(positions(patI)+0.5+t,numEsOpt,25,colorsPoints(patI,:),'filled','HandleVisibility','off');            
        end
        
        patIs = [patIs, patI];
    end
    patI = patI + 1;
end

addpath('ContentScripts/')
% check normality of opt and imp groups for all 3 thrs
% use Shapiro-Wilk Test
normBool = true(3,2); % null hypothesis of normality 
for t = 1:3
    datOpt = dat_mapPerc{t};
    datOpt(datOpt==0) = [];
    datImp = dat_mapPerc_imp{t};
    datImp(datImp==0) = [];
    % opt configs
    Results=normalitytest(datOpt');
    pVal = Results(7,2); % Shapiro-Wilk Test
    % if p value is < 0.05, we reject the null hyp of normality
    if pVal < 0.05
        normBool(t,1) = false;
        disp(strcat("Data for thrI OPT of ",num2str(t)," is NOT normal"))
    end
    % imp configs
    Results=normalitytest(datImp');
    pVal = Results(7,2); % Shapiro-Wilk Test
    % if p value is < 0.05, we reject the null hyp of normality
    if pVal < 0.05
        normBool(t,2) = false;
        disp(strcat("Data for thrI IMP of ",num2str(t)," is NOT normal"))
    end
end
% do significance testing   
% for each case, if normal, do paired t test
pVals = zeros(3,1);
tVals = zeros(3,1);
testsUsed = {"","",""};
for t = 1:3
    if sum(normBool(t,:)) == 2 % both opt and imp normal
        testsUsed{t} = "ttest";
        datOpt = dat_mapPerc{t};
        datOpt(datOpt==0) = [];
        datImp = dat_mapPerc_imp{t};
        datImp(datImp==0) = [];
        [h,p,ci,stats] = ttest(datOpt,datImp);
        pVals(t) = p;
        tVals(t) = stats.tstat;
    else % if not normal, do Wilcoxon signed rank (paired version of Wilcoxon ranked sum test)
        testsUsed{t} = "Wilcoxon signed rank test";
        disp(strcat("This case (t=",num2str(t),") is NOT normal! Do Wilcoxon signed rank test!"))
        datOpt = dat_mapPerc{t};
        datOpt(datOpt==0) = [];
        datImp = dat_mapPerc_imp{t};
        datImp(datImp==0) = [];
        [p,h,stats] = signrank(datOpt,datImp,'method','approximate');
        pVals(t) = p;
        tVals(t) = stats.zval;
    end
end
disp(strcat("The p values for 3 thresholds are:"))
pVals
disp(strcat("The t values for 3 thresholds are:"))
tVals

% all cases normal for sourceI = 4
%   EXCEPT Opt electrodes LTL thrI=3
% all Wilcoxon signed rank:
%   LTL: pVals = 0.0156, 0.0078, 0.0078
%   clinROI: pVals =  1.0e-03 * 0.4883, 0.4883, 0.4883
% all paired ttest:
%   LTL: pVals = 0.0058, 0.0004, 0.0013
%   clinROI: pVals = 1.0e-04 * 0.0264, 0.1723, 0.0072


% save test statistics in table
rowNames = ["200","500","1000"];
varNames = ["threshold (µV)","test used","p value","t or z value"];
T = table(rowNames',testsUsed',pVals,tVals,'VariableNames',varNames,'RowNames',rowNames);
if ROI_i == 1  % clinician-ROI
    writetable(T,'SourceData/Figure3gStats.xls')
elseif ROI_i==2  % LTL
    writetable(T,'SourceData/Figure3fStats.xls')
else
    disp("WHY COMPARE AT LH ROI?? CHECK")
end
disp(T)

% save raw data in table
if saveAndDispTable
    varNames = ["Imp_200","Opt_200","Diff_200","Imp_500","Opt_500","Diff_500","Imp_1000","Opt_1000","Diff_1000"];
    rowNames = string(patientIDs);
    T = table(dat_mapPerc_imp{1}, dat_mapPerc{1}, -1*(dat_mapPerc_imp{1}-dat_mapPerc{1}),dat_mapPerc_imp{2}, dat_mapPerc{2}, -1*(dat_mapPerc_imp{2}-dat_mapPerc{2}), dat_mapPerc_imp{3},dat_mapPerc{3},-1*(dat_mapPerc_imp{3}-dat_mapPerc{3}),'VariableNames',varNames,'RowNames',rowNames);
    if ROI_i == 1
        writetable(T,"SourceData/Figure3g.xls")
    elseif ROI_i==2
        writetable(T,"SourceData/Figure3f.xls")
    end
    disp(T)
end
% plot
f.Position = [800 350 430 300];

for t = 1:3
    noDatPatInds = find(dat_mapPerc{t}==0 & dat_mapPerc_imp{t}==0);
    dat_mapPerc{t}(noDatPatInds) = [];
    dat_mapPerc_imp{t}(noDatPatInds) = [];
    boxplot([dat_mapPerc_imp{t},dat_mapPerc{t}],'Colors',[0,0,0;0.4,0.4,0.4],'Positions',[0,0.5]+t,'Widths',0.25) %'Colors',colors(3,:)) %,'PlotStyle','compact','Whisker',0,'Symbol','') %,'positions', positions)
end
patIs = unique(patIs);

plot([-1,0],[0,0],'k','LineWidth',1,'DisplayName','Implanted')
plot([-1,0],[0,0],'color',[0.4,0.4,0.4],'LineWidth',1,'DisplayName','Optimized')
if plotDiff
    plot([-1,0],[0,0],'r','LineWidth',1,'DisplayName','Difference')
end
plot([-1,0],[0,0],'x--','color',colorsPoints(7,:),'LineWidth',2,'MarkerSize',140,'DisplayName','Patient 25');
%legend('location','southwest')
% boxplot(dat_mapPerc_imp'*100,'Colors','k','Positions',[2]) %'Colors',colors(3,:)) %,'PlotStyle','compact','Whisker',0,'Symbol','') %,'positions', positions)

xticks([1:3]+0.5)
xlim([0.7,3.8])
xticklabels(["200 µV","500 µV", "1000 µV"])
ax = gca; 
ax.FontSize = 20; 
grid on

if plotDiff
% diff box plot
diffs = cell(3,1);
for t = 1:3
    if percD
        diffs{t} = -1*(dat_mapPerc{t} - dat_mapPerc_imp{t})./dat_mapPerc_imp{t}*100; % # es that we save
    else
        diffs{t} = -1*(dat_mapPerc{t} - dat_mapPerc_imp{t}); % # es that we save
    end
end


% if plotting perc, remap to numE axis
if percD
    if ROI_i==1
        yMax = 20;
    else
        yMax = 12;
    end
    yticks([0:yMax/4:yMax])
    ylim([0,yMax])
end

positions = (rand(length(patientIDs),1)-0.5)/10;
%

% f = figure(5)
% clf
%
% hold on
% colorsPoints = parula(length(patientIDs)+1);
if percD
    yyaxis right
end
for t = 1:3
    for p = 1:length(diffs{t})
        if patIs(p) == find(patientIDs==25)
            scatter(positions(p)+t+0.22,diffs{t}(p),140,colorsPoints(patIs(p),:),'X','LineWidth',2,'HandleVisibility','off');
        else
            scatter(positions(p)+t+0.22,diffs{t}(p),50,colorsPoints(patIs(p),:),'*','HandleVisibility','off');
        end
    end
    boxplot(diffs{t},'Colors',['r'],'Positions',t+0.25,'Widths',0.2) %'Colors',colors(3,:)) %,'PlotStyle','compact','Whisker',0,'Symbol','') %,'positions', positions)
    % plot pat 25 special
end
end

if percD
    ax = gca;
    ax.YColor = 'r';
    yticks([0:100/4:100])
    ylim([0,100])
else
    ylim([0,max(dat_mapPerc_imp{1})+1])
end
xlim([0.7,3.8])
xticks([1:3]+0.25)
xlim([0.7,3.8])
xticklabels(["200 µV","500 µV", "1000 µV"])
ax = gca; 
ax.FontSize = 20; 
grid on

set(findobj(gca,'type','line'),'linew',1)


xticklabels(["200 µV","500 µV", "1000 µV"])

% ylim([0,35])
ax = gca; 
ax.FontSize = 20; 
grid on
xlabel("Voltage threshold")

if percD
    yyaxis left
end
if ROI_i==1
    fileName = strcat('Figures/Figure3g.pdf');
    ylabel("# Electrodes for clinician-ROI")
elseif ROI_i==2
    fileName = strcat('Figures/Figure3f.pdf');
    ylabel("# Electrodes for TL")
end
print(f,fileName,'-bestfit','-dpdf');


end

function [f,T] = boxOptImpData(ROI_i, dat, patientIDs, data_imp, plotDiff, noColorVersion)
f = figure(4)
clf
hold on
colorsPoints = parula(length(patientIDs)+1);

if noColorVersion
    c7 = colorsPoints(7,:) - 0.4;
    c7(c7<0)=0;
    c7 = c7 + 0.1;
    c7(c7>1)=1;
    for i = 1:length(patientIDs)+1
        colorsPoints(i,:) = c7;
    end
end
colorsLines = colorsPoints;
if noColorVersion
    colorsLines = colorsLines + 0.2;
    colorsLines(colorsLines>1)= 1;
end

% scatter final rec percentage for all search solutions and implanted configurations
% and prepare data for boxplot
dat_mapPerc = cell(3,1);
dat_mapPerc_imp = cell(3,1);

for t = 1:3
    dat_mapPerc{t} = zeros(length(patientIDs),1);
    dat_mapPerc_imp{t} = zeros(length(patientIDs),1);
end

patI = 1;
positions = (rand(length(patientIDs),1)-0.5)/20;

patIs = [];

for patientID = patientIDs
    
    if isempty(data_imp{patI,t})
        patI = patI + 1;
        continue
    end
    for t = 1:3
        numEs = length(data_imp{patI,t});
        dat_mapPerc{t}(patI) = dat{patI,t}(min(numEs,length(dat{patI,t})));
        dat_mapPerc_imp{t}(patI) = data_imp{patI,t}(end);
        
        if patientIDs(patI) == 25
            patI25 = patI;
            if t == 1
                plot(positions(patI)+[0,0.5]+t,[dat_mapPerc_imp{t}(patI)*100,dat_mapPerc{t}(patI)*100],'.--','LineWidth',1,'color',colorsLines(patI,:),'DisplayName',strcat("Patient ",num2str(patientID)),'LineWidth',1,'HandleVisibility','off');
            else
                plot(positions(patI)+[0,0.5]+t,[dat_mapPerc_imp{t}(patI)*100,dat_mapPerc{t}(patI)*100],'.--','LineWidth',1,'color',colorsLines(patI,:),'DisplayName',strcat("Patient ",num2str(patientID)),'LineWidth',1,'HandleVisibility','off');
            end
            scatter(positions(patI)+t,dat_mapPerc_imp{t}(patI)*100,250,colorsPoints(patI,:),'X','LineWidth',1.5,'HandleVisibility','off');
            scatter(positions(patI)+0.5+t,dat_mapPerc{t}(patI)*100,250,colorsPoints(patI,:),'X','LineWidth',1.5,'HandleVisibility','off');
        else
            if t == 1
                plot(positions(patI)+[0,0.5]+t,[dat_mapPerc_imp{t}(patI)*100,dat_mapPerc{t}(patI)*100],'.-','color',colorsLines(patI,:),'DisplayName',strcat("Patient ",num2str(patientID)),'LineWidth',0.7,'HandleVisibility','off');
            else
                plot(positions(patI)+[0,0.5]+t,[dat_mapPerc_imp{t}(patI)*100,dat_mapPerc{t}(patI)*100],'.-','color',colorsLines(patI,:),'DisplayName',strcat("Patient ",num2str(patientID)),'LineWidth',0.7,'HandleVisibility','off');
            end
            scatter(positions(patI)+t,dat_mapPerc_imp{t}(patI)*100,20,colorsPoints(patI,:),'filled','HandleVisibility','off');
            scatter(positions(patI)+0.5+t,dat_mapPerc{t}(patI)*100,20,colorsPoints(patI,:),'filled','HandleVisibility','off');            
        end
        patIs = [patIs, patI];
    end
    patI = patI + 1;
end

addpath('ContentScripts/')
% check normality of opt and imp groups for all 3 thrs
% use Shapiro-Wilk Test
normBool = true(3,2); % null hypothesis of normality 
for t = 1:3
    datOpt = dat_mapPerc{t};
    datOpt(datOpt==0) = [];
    datImp = dat_mapPerc_imp{t};
    datImp(datImp==0) = [];
    % opt configs
    Results=normalitytest(datOpt');
    pVal = Results(7,2); % Shapiro-Wilk Test
    % if p value is < 0.05, we reject the null hyp of normality
    if pVal < 0.05
        normBool(t,1) = false;
        disp(strcat("Data for thrI OPT of ",num2str(t)," is NOT normal"))
    end
    % imp configs
    Results=normalitytest(datImp');
    pVal = Results(7,2); % Shapiro-Wilk Test
    % if p value is < 0.05, we reject the null hyp of normality
    if pVal < 0.05
        normBool(t,2) = false;
        disp(strcat("Data for thrI IMP of ",num2str(t)," is NOT normal"))
    end
end
% do significance testing   
% for each case, if normal, do paired t test
pVals = zeros(3,1);
tVals = zeros(3,1); % for t-tests, gives the t value, or the radio of diff bt mean of the two sample sets. 
                    % for signedrank tests, gives z val

testsUsed = {"","",""};
for t = 1:3
    if sum(normBool(t,:)) == 2 % both opt and imp normal
        testsUsed{t}="ttest";
        datOpt = dat_mapPerc{t};
        datOpt(datOpt==0) = [];
        datImp = dat_mapPerc_imp{t};
        datImp(datImp==0) = [];
        [h,p,ci,stats] = ttest(datOpt,datImp);
        pVals(t) = p;
        tVals(t) = stats.tstat;
    else % if not both normal, do Wilcoxon signed rank (paired version of Wilcoxon ranked sum test)
        testsUsed{t}="Wilcoxon signed rank test";
        disp(strcat("This case (t=",num2str(t),") is NOT normal! Do Wilcoxon signed rank test!"))
        datOpt = dat_mapPerc{t};
        datOpt(datOpt==0) = [];
        datImp = dat_mapPerc_imp{t};
        datImp(datImp==0) = [];
        [p,h,stats] = signrank(datOpt,datImp,'method','approximate');
        pVals(t) = p;
        tVals(t) = stats.zval;
    end
end
disp(strcat("The p values for 3 thresholds are:"))
pVals
disp(strcat("The t values for 3 thresholds are:"))
tVals

%%
% save test statistics in table
rowNames = ["200","500","1000"];
varNames = ["threshold (µV)","test used","p value","t or z value"];
T = table(rowNames',testsUsed',pVals,tVals,'VariableNames',varNames,'RowNames',rowNames);
if ROI_i == 1  % clinician-ROI
    writetable(T,'SourceData/Figure3eStats.xls')
elseif ROI_i==2  % LTL
    writetable(T,'SourceData/Figure3dStats.xls')
else
    disp("WHY COMPARE AT LH ROI?? CHECK")
end
disp(T)


% save raw data in table
varNames = ["Imp_200","Opt_200","Diff_200","Imp_500","Opt_500","Diff_500","Imp_1000","Opt_1000","Diff_1000"];
rowNames = string(patientIDs);
T = table(dat_mapPerc_imp{1}, dat_mapPerc{1}, -1*(dat_mapPerc_imp{1}-dat_mapPerc{1}),dat_mapPerc_imp{2}, dat_mapPerc{2}, -1*(dat_mapPerc_imp{2}-dat_mapPerc{2}), dat_mapPerc_imp{3},dat_mapPerc{3},-1*(dat_mapPerc_imp{3}-dat_mapPerc{3}),'VariableNames',varNames,'RowNames',rowNames);
if ROI_i == 1  % clinician-ROI
    writetable(T,'SourceData/Figure3e.xls')
elseif ROI_i==2  % LTL
    writetable(T,'SourceData/Figure3d.xls')
else
    disp("WHY COMPARE AT LH ROI?? CHECK")
end
disp(T)

% plot
f.Position = [800 350 430 300];

for t = 1:3
    noDatPatInds = find(dat_mapPerc{t}==0 & dat_mapPerc_imp{t}==0);
    dat_mapPerc{t}(noDatPatInds) = [];
    dat_mapPerc_imp{t}(noDatPatInds) = [];
    boxplot([dat_mapPerc_imp{t},dat_mapPerc{t}]*100,'Colors',[0,0,0;0.4,0.4,0.4],'Positions',[0,0.5]+t,'Widths',0.25,'Symbol', '') %'Colors',colors(3,:)) %,'PlotStyle','compact','Whisker',0,'Symbol','') %,'positions', positions)
end
patIs = unique(patIs);

plot([-1,0],[0,0],'k','LineWidth',1,'DisplayName','Implanted')
plot([-1,0],[0,0],'color',[0.4,0.4,0.4],'LineWidth',1,'DisplayName','Optimized')
if plotDiff
    plot([-1,0],[0,0],'r','LineWidth',1,'DisplayName','Difference')
end
plot([-1,0],[0,0],'x--','color',colorsPoints(7,:),'LineWidth',2,'MarkerSize',140,'DisplayName','Patient 25');

xticks([1:3]+0.5)
xlim([0.7,3.8])
xticklabels(["200 µV","500 µV", "1000 µV"])
ylim([0,110])
ax = gca; 
ax.FontSize = 20; 
grid on

ylim([0,100])

% diff box plot
if plotDiff
    diffs = cell(3,1);
    for t = 1:3
        diffs{t} = dat_mapPerc{t} - dat_mapPerc_imp{t}; % percent that we do better
    end

    positions = (rand(length(patientIDs),1)-0.5)/10;

    for t = 1:3
        for p = 1:length(diffs{t})
            if patIs(p) == find(patientIDs==25)
                scatter(positions(p)+t+0.22,diffs{t}(p)*100,120,colorsPoints(patIs(p),:),'X','LineWidth',2,'HandleVisibility','off');
            else
                scatter(positions(p)+t+0.22,diffs{t}(p)*100,30,colorsPoints(patIs(p),:),'*','HandleVisibility','off');
            end
        end
        boxplot(diffs{t}*100,'Colors',['r'],'Positions',t+0.25,'Widths',0.2,'Symbol', '') %'Colors',colors(3,:)) %,'PlotStyle','compact','Whisker',0,'Symbol','') %,'positions', positions)
    end
    
end

xlim([0.7,3.8])
xticks([1:3]+0.25)
xlim([0.7,3.8])
xticklabels(["200 µV","500 µV", "1000 µV"])
ax = gca; 
ax.FontSize = 20; 
grid on

set(findobj(gca,'type','line'),'linew',1)

ylim([0,100]) 

xticklabels(["200 µV","500 µV", "1000 µV"])

ax = gca; 
ax.FontSize = 20; 
grid on
xlabel("Voltage threshold")

if ROI_i==1
    fileName = strcat('Figures/Figure3e.pdf');
    ylabel("Recording sensitivity of ROI")
elseif ROI_i==2
    fileName = strcat('Figures/Figure3d.pdf');
    ylabel("Recording sensitivity of TL")
end
print(f,fileName,'-bestfit','-dpdf');

end

function dat = saveOrOpenCisSearch(ROI_i,sourceInd, redoCalc, patientIDs,degMapStrs,costFnStrs,dataWorkPath,patchAreas,cdmds,ROIs,saveTag)

fileName = strcat('RawData/OptSolutions/Search_ROI',num2str(ROI_i),'_source',num2str(sourceInd),"_",saveTag,".mat");

if redoCalc || ~isfile(fileName)
dat = cell(length(patientIDs),length(degMapStrs)); % patients, thrs, LTL mapping percs
patIDi = 1;


if contains(saveTag,"compOpt")
    useImpTLROIs = true;
else
    useImpTLROIs = false;
end

for patientID = patientIDs
    patientDir = ['Patient_' num2str(patientID)]
    
    ROINameSearch = ROIs(ROI_i);
    ROI_s = ROI_i;
    if useImpTLROIs && ROINameSearch=="LTL" && (patientID == 24 || patientID == 31)
        ROINameSearch = "clinicianROI";
        ROI_s = find(ROIs=="clinicianROI");
    end
    
    tys_cA = strcat(num2str(patchAreas(sourceInd)),"cm2_",num2str(cdmds(sourceInd)),"cdmd");
    load(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_cis',tys_cA,'.mat'),'visDataConfigs')   
    
    count = 1;
    for d = 1:length(degMapStrs)
        degMapStr = degMapStrs(d);
        for i = 1:length(visDataConfigs)
            caseID = visDataConfigs{i}.full_case_id;
            if contains(caseID,costFnStrs(d)) && contains(caseID,degMapStr)
                if contains(caseID,ROINameSearch)
                    % if were analyzing TLs and pat is 24 or 31, need to use normal clinROI (not clinROI_broad)
                    if useImpTLROIs && ROIs(ROI_i)=="LTL" && ~isempty(dat{patIDi,count}) && ( patientID == 24 || patientID == 31  )
                        % for pats whos TL ROIs are 'clinROI' surface, dont
                        % pick the broad clinROI!
                        break
                    end
                    datT = visDataConfigs{i}.percMapROI;
                    dat{patIDi,count} = datT';
                    caseID
                end
            end
        end
        count = count + 1;
    end
    patIDi = patIDi + 1;
    
end

save(fileName,'dat')

else
    load(fileName,'dat')
end

end

function [data_imp,data_imp_ROIAreas] = saveOrOpenCisImplant(redoCalc, ROI_i,sourceInd,patchAreas,cdmds,dataWorkPath,patientIDs,costFnStrs)
ROIs_full = ["clinicianROI","LTL","LH"];
ROI_string = ROIs_full(ROI_i);
data_imp = cell(length(patientIDs),3);
data_imp_ROIAreas = cell(length(patientIDs),3);

% valid imp electrodes for ROIs are already selected 
%{
numEsToIncludeImpConfigs = cell(length(patientIDs),1); % which electrodes are in TL ROI for each patient implanted configuration
if ROI_i == 2
    % patientIDs = [2,3,7,19,20,24,25,26,27,30,31,33];
    % numEsToIncludeImpConfigs = [7,0,0,0,9,6,10,4,8];
    % [] means do not include in TL analysis
    numEsToIncludeImpConfigs{1} = [1:6]; % pat 2
    numEsToIncludeImpConfigs{2} = []; % pat 3
    numEsToIncludeImpConfigs{3} = []; % pat 7
    numEsToIncludeImpConfigs{4} = []; % pat 19
    numEsToIncludeImpConfigs{5} = [1:9]; % pat 20
    numEsToIncludeImpConfigs{6} = [1:6]; % pat 24
    numEsToIncludeImpConfigs{7} = [1:7]; % pat 25
    numEsToIncludeImpConfigs{8} = [1:4]; % pat 26
    numEsToIncludeImpConfigs{9} = [1:10]; %[1:8,10,12]; % pat 27 
    numEsToIncludeImpConfigs{10} = [1:8]; % pat 30
    numEsToIncludeImpConfigs{11} = [1:6]; % pat 31 
    numEsToIncludeImpConfigs{12} = []; % pat 33
else
    % 0 means include all
    numEsToIncludeImpConfigs{1} = 0; % pat 2
    numEsToIncludeImpConfigs{2} = 0; % pat 3
    numEsToIncludeImpConfigs{3} = 0; % pat 7
    numEsToIncludeImpConfigs{4} = 0; % pat 19
    numEsToIncludeImpConfigs{5} = 0; % pat 20
    numEsToIncludeImpConfigs{6} = [1:12]; % pat 24
    numEsToIncludeImpConfigs{7} = [1:14]; % pat 25
    numEsToIncludeImpConfigs{8} = [1:13]; % pat 26
    numEsToIncludeImpConfigs{9} = [1:10]; % pat 27 LTL
    numEsToIncludeImpConfigs{10} = 0; % pat 30 
    numEsToIncludeImpConfigs{11} = 0; % pat 31 clinROI_broad for all 15
    numEsToIncludeImpConfigs{12} = 0; % pat 33 clinROI all 14
end
%}

fileName = strcat('RawData/OptSolutions/Imp_ROI',num2str(ROI_i),'_source',num2str(sourceInd),".mat");

clinBroad = true;

if redoCalc || exist(fileName,'file')==0
patIDi = 1;
for patientID = patientIDs
    patientDir = ['Patient_' num2str(patientID)]
    
    load(strcat(dataWorkPath,patientDir,'/OptSolutions/ROIs.mat'),'patches')
    load(strcat(dataWorkPath,patientDir,'/HeadModel/cortex_areas.mat'),'cortex_areas')
    
    % four patients have no implanted TL ROI
    if ROI_i == 2 && (patientID == 3 || patientID == 7 || patientID == 19 || patientID == 33)
        patIDi = patIDi + 1;
        continue
    end
        
    tys_cA = strcat(num2str(patchAreas(sourceInd)),"cm2_",num2str(cdmds(sourceInd)),"cdmd");
    load(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataImplantConfig_singleMap_crossAnalyze',tys_cA,'.mat'),'visDataImplantConfig')
    
    % two patients have RTL ROIs, so choose clinicianROI that corresponds
    if ROI_i == 2 && ( patientID == 24 || patientID == 31  )
        ROI_string = "clinicianROI";
        dipolesROI = patches.dipoles{1};
    else
        ROI_string = ROIs_full(ROI_i);
        dipolesROI = patches.dipoles{ROI_i};
    end
    clinBI = 0;
    for roii = 1:length(patches.name)
        if patches.name{roii} == "clinicianROI_broad"
            clinBI = roii;
            break
        end
    end
    if ROI_i == 1 && clinBroad && clinBI>0
        ROI_string = "clinicianROI_broad";
        dipolesROI = patches.dipoles{clinBI};
    end
    
    for i = 1:length(visDataImplantConfig)
        if isempty(visDataImplantConfig{i})
            continue
        end
        caseID = visDataImplantConfig{i}.imp_case_id;
        for thrI = 1:3
            if isempty(data_imp{patIDi,thrI}) &&  contains(caseID,tys_cA) && contains(caseID,costFnStrs(thrI)) && contains(caseID,ROI_string)
                data_imp{patIDi,thrI} = visDataImplantConfig{i}.percMapROI_imp_allThr{5};
                data_imp_ROIAreas{patIDi,thrI} = sum(cortex_areas(dipolesROI));
            end
        end
    end
    patIDi = patIDi + 1
end
    save(strcat('RawData/OptSolutions/Imp_ROI',num2str(ROI_i),'_source',num2str(sourceInd),".mat"),'data_imp')
    save(strcat('RawData/OptSolutions/ImpArea_ROI',num2str(ROI_i),'_source',num2str(sourceInd),".mat"),'data_imp_ROIAreas')
else
    load(strcat('RawData/OptSolutions/Imp_ROI',num2str(ROI_i),'_source',num2str(sourceInd),".mat"),'data_imp')
    load(strcat('RawData/OptSolutions/ImpArea_ROI',num2str(ROI_i),'_source',num2str(sourceInd),".mat"),'data_imp_ROIAreas')
end

end


