% Plot Figure 2 and create Source Data files
% Grace Dessert

% Recordable Radius analysis

%{
Figure 2: Recording sensitivity of sEEG electrode contacts. 
a Recording sensitivity as a function of distance between the center of the 
simulated epileptiform activity and the electrode contact for 10 representative contacts. 
b Area of recording sensitivity (colored cortex) for 10 contacts (pink dots) 
corresponding to each trace seen in A using a 500 µV signal detection threshold. 
c Median and interquartile range of recording sensitivity as a function of 
source to contact distance for 105 contacts in each of 12 patients. 
d Recording sensitivity as a function of source to contact distance for a 
range of source modeling parameters and signal detection thresholds. 
The source modeling parameters are 10 cm2 and 0.465 nAm/mm2 for panels a-c. 
%}

%% Figure 2A
% Plot 10 representative contacts' recording sensitivity across radius from
% contacts.

load('RawData/RecRadius/percRecInRadius_10cm2_0.465cdmd.mat','percRecInRadius')
% avg across all contacts and cdmds
distsPlot = [0:2.5:100]; % mm
selectedInds = [22,25,35,55,75,105,66,96,39,10];
cNum = 2
patI = 4
percRecInRadius_p = percRecInRadius(:,:,cNum,patI);
title = ["Percent recordability within radius","500 µV, 10cm^2, 0.465 nAm/mm^2"];
tenVeridisColors = [77,0,95;74,75,145;56,113,152; 41,141,153; 33,165,148; 52,187,135; 98,205,112; 152,219,77; 186,208,35; 233,214,40];

tenMixedRainbowColors = [158,0,0;76,240,195; 0,106,255; 31,201,229; 255,51,8; 0,142,252; 246,161,5; 230,217,30; 0,89,254; 0,0,142];

f = plotPercRecRadius(percRecInRadius_p, selectedInds,[],0, tenMixedRainbowColors./255)
xlim([1,21])
ylabel('Recording sensitivity')
ax = gca; 
ax.FontSize = 20; 
grid on
f.Position = [800 350 530 280];
fileName = 'Figures/Figure2a.pdf';
print(gcf,fileName,'-bestfit','-dpdf');

% create data for Source Data file
matWrite = vertcat(strcat(string(distsPlot(2:end))," mm"),percRecInRadius_p(:,selectedInds)'*100);
writematrix(matWrite,'SourceData/Figure2a.xls')

%% Figuer 2B
% requires SCIRun and cortex surfaces

%% Figure 2C
% RS across distance for all contacts and all patients
% for median source type

load('RawData/RecRadius/percRecInRadius_10cm2_0.465cdmd.mat','percRecInRadius')
% across all contacts and cdmds
selectedInds = 1:420;
percRecInRadius_p1 = reshape(percRecInRadius(:,:,1,:),40,[]);
colorsContacts = parula(length(selectedInds)+2);
distsPlot = [2.5:2.5:100]; % mm

colors = (lines(10));

f = figure(1)
clf
hold on

positions = [1:40]-0.25;
a = plot(positions,median(percRecInRadius_p1*100,2),'.-','color',colors(2,:),'LineWidth',2);
boxplot(percRecInRadius_p1'*100,'PlotStyle','compact','Colors',colors(2,:),'Whisker',0,'Symbol','','positions', positions)

positions = [1:40]+0;
percRecInRadius_p2 = reshape(percRecInRadius(:,:,2,:),40,[]);
b = plot(positions,median(percRecInRadius_p2*100,2),'.-','color',colors(3,:),'LineWidth',2);
boxplot(percRecInRadius_p2'*100,'PlotStyle','compact','Colors',colors(3,:),'Whisker',0,'Symbol','','positions', positions)

positions = [1:40]+0.25;
percRecInRadius_p3 = reshape(percRecInRadius(:,:,3,:),40,[]);
c = plot(positions,median(percRecInRadius_p3*100,2),'.-','color',colors(4,:),'LineWidth',2);
boxplot(percRecInRadius_p3'*100,'PlotStyle','compact','Colors',colors(4,:),'Whisker',0,'Symbol','','positions', positions)
f.Position = [800 350 530 280];

xlim([0,21])
xticks([0:2:40])
yticks([0:20:100])
xticklabels([0:0.5:10])
xlabel("Radius from contact (cm)")
legend([a,b,c],["200 µV","500 µV","1000 µV"]) 

ylim([-5,110])
ylabel('Recording sensitivity')

ax = gca; 
ax.FontSize = 20; 
grid on
fileName = strcat('Figures/Figure2c.pdf');
print(f,fileName,'-bestfit','-dpdf');

% create data for Source Data file
matWrite = vertcat(strcat(string(distsPlot)," mm"),percRecInRadius_p1'*100);
writematrix(matWrite,'SourceData/Figure2c_200µV.xls')
matWrite = vertcat(strcat(string(distsPlot)," mm"),100*percRecInRadius_p2');
writematrix(matWrite,'SourceData/Figure2c_500µV.xls')
matWrite = vertcat(strcat(string(distsPlot)," mm"),100*percRecInRadius_p3');
writematrix(matWrite,'SourceData/Figure2c_1000µV.xls')


%% Figure 2D
% all sources across distance plots
patIDs = [2,3,7,19,20,24,25,26,27,30,31,33];

cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
thrs = [200,500,1000];
distsPlot = [0:2.5:100]; % mm
dataHomePath = ''

patI = [1:12];
disp(strcat("Plotting for patients: ",num2str(patIDs(patI))))

% load dat for all sources
maxRecRadii = cell(length(unique(cdmds)),length(unique(patchAreas)),3); % source types vs thresholds
for cI = 1:length(unique(cdmds))
    for pI = 1:length(unique(patchAreas))
        cdmd = unique(cdmds);
        cdmd = cdmd(cI); % nAm/mm^2
        patchArea = unique(patchAreas);
        patchArea = patchArea(pI); % cm^2
        tys = strcat(num2str(patchArea),"cm2_",num2str(cdmd),"cdmd");
        tys_nice = strcat(num2str(patchArea)," cm^2, ",num2str(cdmd)," cdmd,");
        cNumStrings = ["C1","C2","C3"]; % 100, 500, and 1000 µV threshold
        load(strcat('RawData/RecRadius/percRecInRadius_',tys,'.mat'),'percRecInRadius')
        
        for cNum = 1:3
            if length(patI) > 1
                percRecInRadius_r = reshape(squeeze(percRecInRadius(:,:,cNum,patI)),40,105*length(patI));
            else
                percRecInRadius_r = percRecInRadius(:,:,cNum,patI);
            end
            maxRecRadii{cI,pI,cNum} = percRecInRadius_r;
        end
    end
end
%
f = figure(patI(1))
f.Position = [50 120 400 600];
clf
hold on

colors = parula(4);

distsPlot = [0:2.5:100]; % mm
t = tiledlayout(3,1, 'Padding', 'compact'); 
% t.TileSpacing = 'compact';
% t.Padding = 'compact';

caseMetrics = cell(3,1);

for cNum=1:3    
    nexttile   
    hold on

    cdmdLs = string(unique(cdmds))+" nAm/mm^2";
    patchLs = string(unique(patchAreas))+" cm^2 patch area";
    
    for c = 1:3
        for p = 1:3
            if c == 1
                plot(distsPlot(2:41)/10, median(maxRecRadii{c,p,cNum},2)*100,':','color',colors(p,:),'LineWidth',3)
            elseif c == 2
               plot(distsPlot(2:41)/10, median(maxRecRadii{c,p,cNum},2)*100,'-','color',colors(p,:),'LineWidth',2.5)
            else
                plot(distsPlot(2:41)/10, median(maxRecRadii{c,p,cNum},2)*100,'--','color',colors(p,:),'LineWidth',2.5)
            end
        end
    end
%     caseMetrics{cNum} = arraymetrics;
        
    if cNum == 2
        ylabel('Median recording sensitivity')
    end
    if cNum == 2
        a = plot([0,0], [0,0.1],'-','color',colors(1,:),'LineWidth',3)
        b = plot([0,0], [0,0.1],'-','color',colors(2,:),'LineWidth',2.5)
        cp = plot([0,0], [0,0.1],'-','color',colors(3,:),'LineWidth',2.5)
        d = plot([0,0], [0,0.1],':','color','k','LineWidth',3)
        e = plot([0,0], [0,0.1],'-','color','k','LineWidth',2.5)
        f = plot([0,0], [0,0.1],'--','color','k','LineWidth',2.5)
        legend([a,b,cp,d,e,f],{"0.16 nAm/mm^2", "0.465 nAm/mm^2", "0.77 nAm/mm^2", "6 cm^2", "10 cm^2", "20 cm^2"})
        legend('location','northeast')
    else
        legend('off')
    end
    xlim([0.25,5])
%     if cNum == 1
%         title([strcat("Median recordable radius vs distance for all cases"),strcat("for a ",num2str(thrs(cNum))," µV threshold")])%, and patient #",num2str(patIDs(patI)))])
%     else
%         title([strcat("for ",num2str(thrs(cNum))," µV threshold")])
%     end
    if cNum == 3
        xlabel("Radius from contact (cm)")
    end
    ax = gca; 
    ax.FontSize = 20; 
    grid on
    xticks([0:0.5:6])
    xticklabels([0:0.5:10])
    yticks([0:20:100])
end

fileName = 'Figures/Figure2d.pdf';
print(gcf,fileName,'-bestfit','-dpdf');

% create data for Source Data file
patchAreas = unique(patchAreas);
cdmds = unique(cdmds);
thrs = [200,500,1000];
for cNum = 1:3
    for c = 1:3
        for p = 1:3
            sourceStr = strcat(string(cdmds(c)),"nAmmm2_",string(patchAreas(p)),"cm2_",num2str(thrs(cNum)),"µV");
            matWrite = vertcat(strcat(string(distsPlot(2:41)/10)," mm"),maxRecRadii{c,p,cNum}'*100);
            writematrix(matWrite,strcat('SourceData/Figure2d_',sourceStr,'.xls'))
        end
    end
end



function f = plotPercRecRadius(percRecInRadius, selectedInds, lab,plotBox, colorsContacts)

%     colorsContacts = viridis(length(selectedInds)+2);
    distsPlot = [0:2.5:100]; % mm
    
    % plot % map
    f = figure(3)
    clf
    hold on
    for i = 1:length(selectedInds)
        sI = selectedInds(i);
        plot([1:size(percRecInRadius(:,sI),1)],percRecInRadius(:,sI)*100,'.-','color',colorsContacts(i,:),'DisplayName',strcat("Contact #",num2str(i)),'LineWidth',2.5)
    end
    if plotBox
        boxplot(percRecInRadius(:,selectedInds)'*100,'PlotStyle','compact') %,'.-','color',colorsContacts(i,:),'DisplayName',strcat("Contact #",num2str(i)))
    end
    % plot(distsPlot./10,ones(1,length(distsPlot))*90,'LineWidth',1.5,'color','black','DisplayName',"90% recordable")
    xticks([2:2:size(percRecInRadius,1)])
    xticklabels(distsPlot(3:2:end)./10)
    xlim([0,20.5])
    ylim([0,102])
    ax = gca;
    % legend('location','best')
    ax.FontSize = 14; 
    title(lab) 
    ylabel("Percent patches recordable")
    xlabel("Radius from contact (cm)")
%     savefig(f,strcat(dataHomePath,'RecRadius/Vis/percRecInRadius_',full_case_id,'.fig'))

end


