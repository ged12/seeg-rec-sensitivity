% plot sensitivity analysis of source type
% Grace Dessert
% Supplemental Figure 4abcd

%% FigureS4ac
% plot median source avg convergence plots
trialSelect = 4; % mean case
ROIs = ["clinicianROI","LTL","LH"]; 

for ROI_i = 2:3
    thrI = 2;
    f = plotMedConvg(ROIs, ROI_i, thrI, trialSelect)
    if ROI_i == 2 % LTL
        fileName = strcat("Figures/Figure4a.pdf");
        savefig(f,strcat("Figures/Figure4b.fig"))
    elseif ROI_i == 3 % LH
        fileName = strcat("Figures/Figure4b.pdf");
        savefig(f,strcat("Figures/Figure4b.fig"))
    end
    print(gcf,fileName,'-bestfit','-dpdf');
end

%% FigureS4cd
% plot confusion matrix
ROIs = ["clinicianROI","LTL","LH"]; 
thrI = 2;

for ROI_i = 2:3
    f = plotConfMats(ROIs, ROI_i, thrI)
    if ROI_i == 2 % LTL
        fileName = strcat("Figures/Figure4c.pdf");
        savefig(f,strcat("Figures/Figure4c.fig"))
    elseif ROI_i == 3 % LH
        fileName = strcat("Figures/Figure4d.pdf");
        savefig(f,strcat("Figures/Figure4d.fig"))
    end
    print(gcf,fileName,'-bestfit','-dpdf');
end


function f = plotConfMats(ROIs, ROI_i,thrI)
    allAvgDs = zeros(12,9,3); % 12 patients,  9 sources, 3 thresholds
    for trialSelect = 1:9
        load(strcat('RawData/SensitivityAn/diffRS_allPats_singleMap_ROI',ROIs(ROI_i),'_source',num2str(trialSelect),'.mat'),'diffRS_allPats')
        for j = 1:12 % for each patient subtract by ground truth val
            if isempty(diffRS_allPats{j})
                disp(strcat("Patient ",num2str(j),"isempty(diffRS_allPats{j})"))
                continue
            end
            diffRS_allPats{j} = (diffRS_allPats{j} - diffRS_allPats{j}(trialSelect,:))./diffRS_allPats{j}(trialSelect,:)*-1;
        end
        avgDiff = zeros(9,3);
        numV = 0;
        for j = 1:12 % for each pat
            if isempty(diffRS_allPats{j})
                continue
            end
            avgDiff = avgDiff + diffRS_allPats{j};
            numV = numV + 1;
        end
        avgDiff = avgDiff / numV;
        allAvgDs(trialSelect,:,1) = avgDiff(:,1);
        allAvgDs(trialSelect,:,2) = avgDiff(:,2);
        allAvgDs(trialSelect,:,3) = avgDiff(:,3);
    end

    % reorder 
    order = [3,1,2,6,4,5,9,7,8];
    allAvgDs = allAvgDs(order,order,:);

    f = figure(1)
    clf
    image(allAvgDs(:,:,thrI)*100,'CDataMapping','scaled')
    colorbar
    lims = caxis
    if thrI == 2
        caxis([0 70])
    else
        caxis([0 100])
    end
    % ax = gca; 
    % c = ax.Color;
    % c('CLim') = [0 70];

    axis equal
    ax = gca; 
    ax.FontSize = 30; 
    xticks([1:9])
    yticks([1:9])
    cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
    patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
    labels = [];
    for i = 1:9
        labels = [labels,strcat(num2str(cdmds(i)),"nAm/mm^2, ",num2str(patchAreas(i))," cm^2")];
    end
    labels = labels(order)'
    xlim([0.5,9.5])
    ylim([0.5,9.5])
    xticklabels([])
    yticklabels([])
    
    
    matWrite = vertcat(labels',allAvgDs(:,:,thrI)*100);
    matWrite = horzcat(["";labels],matWrite);
    if ROI_i == 2
        writematrix(matWrite,strcat('SourceData/FigureS4c.xls'))
    elseif ROI_i == 3
        writematrix(matWrite,strcat('SourceData/FigureS4d.xls'))
    end

end

function f = plotMedConvg(ROIs, ROI_in, thrI_in, trialSelect)
    % hard coded 500 µV threshold
    % save full uncompressed data
    
    cdmds = ["0.16 nAm/mm^2", "0.465 nAm/mm^2", "0.77 nAm/mm^2"];
    pAs = ["6 cm^2", "10 cm^2", "20 cm^2"];
    
    load(strcat('RawData/SensitivityAn/medSourceCrossAnConverg_source',num2str(trialSelect),'.mat'),'dataAll')
    
    % average across patients to get mean
    dataAllMean = cell(3,3,9); % 3 ROIs, 3 thrs, 9 source types
    for ROI_i = 1:3
        for thrI =  1:3
            for sourceI = 1:9
                numNonZeroPats = 0;
                for patI = 1:12
                    dat = dataAll{ROI_i,thrI,patI,sourceI}
                    disp(strcat("dat is of size: ",num2str(size(dat))))
                    if isempty(dataAllMean{ROI_i,thrI,sourceI})
                        dataAllMean{ROI_i,thrI,sourceI} = zeros(31,1);
                    end
                    if sum(dat)==0
                        disp('zero dat')
                    else
                        numNonZeroPats = numNonZeroPats + 1;
                    end
                    dataAllMean{ROI_i,thrI,sourceI} = dataAllMean{ROI_i,thrI,sourceI} + dat;
                end
                dataAllMean{ROI_i,thrI,sourceI} = dataAllMean{ROI_i,thrI,sourceI}/numNonZeroPats;
            end
        end
    end
    
    thrI = thrI_in;
    ROI_i = ROI_in;
    
    % plot
    % select ROI and thr case
    f = figure(1); %'visible'); %,'off');
    f.Position = [800 350 530 280];
    colors = parula(4);
    colors(4,:) = colors(2,:);
    colors(2,:) = colors(1,:);
    colors(1,:) = colors(4,:);

    colors(4,:) = colors(3,:);
    colors(3,:) = colors(2,:);
    colors(2,:) = colors(4,:);
    clf
    hold on
    matWrite = [];
    labels = [];
    % plot 9 median lines, with labels of source type for opt
    for p = 1:3
        for c = 1:3
            PCind = (p-1)*3+c;
            dat = dataAllMean{ROI_i,thrI,PCind};
            matWrite = vertcat(matWrite,[0;dat]'*100);
            labels = vertcat(labels,strcat(cdmds(c),"_",pAs(p)));
            if p == 1
                plot([0:length(dat)],[0;dat]*100,':','color',colors(c,:),'LineWidth',3)
            elseif p == 2
               plot([0:length(dat)],[0;dat]*100,'-','color',colors(c,:),'LineWidth',2.5)
            else
                plot([0:length(dat)],[0;dat]*100,'--','color',colors(c,:),'LineWidth',2.5)
            end
        end
    end
    xlim([0,31])
    ylim([0,100])
    a = plot([0,0]-2, [0,0.1],'-','color',colors(3,:),'LineWidth',3)
    b = plot([0,0]-2, [0,0.1],'-','color',colors(1,:),'LineWidth',2.5)
    cp = plot([0,0]-2, [0,0.1],'-','color',colors(2,:),'LineWidth',2.5)
    d = plot([0,0]-2, [0,0.1],':','color','k','LineWidth',3)
    e = plot([0,0]-2, [0,0.1],'-','color','k','LineWidth',2.5)
    ff = plot([0,0]-2, [0,0.1],'--','color','k','LineWidth',2.5)

    if ROI_i==2
        legend([a,b,cp,d,e,ff],{"0.16 nAm/mm^2", "0.465 nAm/mm^2", "0.77 nAm/mm^2", "6 cm^2", "10 cm^2", "20 cm^2"})
        legend('location','southeast')
    end

    xlabel('Electrode additions')
    ylabel(strcat("Mean RS for ",ROIs(ROI_i)))
    ax = gca; 
    ax.FontSize = 18; 
    grid on
    
    matWrite = vertcat(string([0:length(dat)]),matWrite);
    matWrite = horzcat(["";labels],matWrite);
    if ROI_i == 2
        writematrix(matWrite,strcat('SourceData/FigureS4a.xls'))
    elseif ROI_i == 3
        writematrix(matWrite,strcat('SourceData/FigureS4b.xls'))
    end

end

