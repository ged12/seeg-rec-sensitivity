% fig3_analyzeSolnConfis
% Grace Dessert
% 10/24/2022


% global vars
cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
dataWorkPath = '';
dataHomePath = '';
patientIDs = [2,3,7,19,20,24,25,26,27,30,31,33];
costFnStrs = ["200_1000_500","500_1000_200","1000_500_200"];
degMapStrs = ["single mapping at 200 µV threshold", "single mapping at 500 µV threshold", "single mapping at 1000 µV threshold"];
ROIs = ["clinicianROI","LTL","LH"]; % array 


%% Figure4 ab
for ROI_i = 2:3
    redoCalc = false;
    saveTag = "normTempROIs";
    dat = saveOrOpenCisSearch(ROI_i, sourceInd,redoCalc, patientIDs,degMapStrs,costFnStrs,dataWorkPath,patchAreas,cdmds,ROIs,saveTag);
    plotMeansSearchData(dat,saveTag,tys_cA,ROIs,ROI_i,patientIDs);
end

function dat = saveOrOpenCisSearch(ROI_i,sourceInd, redoCalc, patientIDs,degMapStrs,costFnStrs,dataWorkPath,patchAreas,cdmds,ROIs,saveTag)

fileName = strcat('RawData/OptSolutions/Search_ROI',num2str(ROI_i),'_source',num2str(sourceInd),"_",saveTag,".mat");

if redoCalc || ~isfile(fileName)
dat = cell(length(patientIDs),length(degMapStrs)); % patients, thrs, LTL mapping percs
patIDi = 1;


if contains(saveTag,"compOpt")
    useImpTLROIs = true;
else
    useImpTLROIs = false;
end

for patientID = patientIDs
    patientDir = ['Patient_' num2str(patientID)]
    
    ROINameSearch = ROIs(ROI_i);
    ROI_s = ROI_i;
    if useImpTLROIs && ROINameSearch=="LTL" && (patientID == 24 || patientID == 31)
        ROINameSearch = "clinicianROI";
        ROI_s = find(ROIs=="clinicianROI");
    end
    
    tys_cA = strcat(num2str(patchAreas(sourceInd)),"cm2_",num2str(cdmds(sourceInd)),"cdmd");
    load(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_cis',tys_cA,'.mat'),'visDataConfigs')   
    
    count = 1;
    for d = 1:length(degMapStrs)
        degMapStr = degMapStrs(d);
        for i = 1:length(visDataConfigs)
            caseID = visDataConfigs{i}.full_case_id;
            if contains(caseID,costFnStrs(d)) && contains(caseID,degMapStr)
                if contains(caseID,ROINameSearch)
                    % if were analyzing TLs and pat is 24 or 31, need to use normal clinROI (not clinROI_broad)
                    if useImpTLROIs && ROIs(ROI_i)=="LTL" && ~isempty(dat{patIDi,count}) && ( patientID == 24 || patientID == 31  )
                        % for pats whos TL ROIs are 'clinROI' surface, dont
                        % pick the broad clinROI!
                        break
                    end
                    datT = visDataConfigs{i}.percMapROI;
                    dat{patIDi,count} = datT';
                    caseID
                end
            end
        end
        count = count + 1;
    end
    patIDi = patIDi + 1;
    
end

save(fileName,'dat')

else
    load(fileName,'dat')
end

end


function f = plotMeansSearchData(dat,saveFigTag,tys_cA,ROIs,ROI_i,patientIDs)
f = figure(2)
clf
hold on
% colorz = [0 0.4470 0.7410;0.8500 0.3250 0.0980;0.9290 0.6940 0.1250;0.4940 0.1840 0.5560;0.4660 0.6740 0.1880;0.3010 0.7450 0.9330;0.6350 0.0780 0.1840];
colorz = parula(4);
colorz = lines(4);
colorz = colorz(2:4,:);

numEsto75PercRS = zeros(size(dat,1),3); % 12 patients, 3 thrs

dat_all = zeros(size(dat,1),32,3); % 12 patients, 32 distance inds, 3 thrs
for i = 1:size(dat,1) % for each patient
    for thrI = 1:3
        if dat{i,thrI}(end)>=0.75
            numEsto75PercRS(i,thrI) = find(dat{i,thrI}>=0.75,1);
        end
        dat_all(i,1:length(dat{i,thrI})+1,thrI) = [0;dat{i,thrI}];
        dat_all(i,length(dat{i,thrI})+2:31,thrI) = repmat(dat{i,thrI}(end),31-(length(dat{i,thrI})+1),1);
    end
end
% get mean and std of numEs to 75% RS
for thrI = 1:3
    numEsValid = numEsto75PercRS(:,thrI);
    numEsValid(numEsValid==0) = [];
    disp(strcat("for thr ",num2str(thrI)))
    disp(strcat("there are only ",num2str(length(numEsValid))," of ",num2str(size(dat,1))," patients that got to 75% rec at this thr"))
    disp(strcat("mean numEs is ",num2str(mean(numEsValid))))
    disp(strcat("std of numEs is ",num2str(std(numEsValid))))
end

means = zeros(31,3);
rangeHigh = zeros(31,3);
rangeLow = zeros(31,3);
for i = 1:31
    nonZeroDat1 = squeeze(dat_all(:,i,1));
    nonZeroDat2 = squeeze(dat_all(:,i,2));
    nonZeroDat3 = squeeze(dat_all(:,i,3));
    nonZeroDat1(nonZeroDat1==0) = [];
    nonZeroDat2(nonZeroDat2==0) = [];
    nonZeroDat3(nonZeroDat3==0) = [];
    if ~isempty(nonZeroDat1)
        means(i,1) = mean(nonZeroDat1);
        rangeHigh(i,1) = mean(nonZeroDat1)+std(nonZeroDat1);
        rangeLow(i,1) = mean(nonZeroDat1)-std(nonZeroDat1);
    end
    if ~isempty(nonZeroDat2)
        means(i,2) = mean(nonZeroDat2);
        rangeHigh(i,2) = means(i,2)+std(nonZeroDat2);
        rangeLow(i,2) = means(i,2)-std(nonZeroDat2);
    end
    if ~isempty(nonZeroDat3)
        means(i,3) = mean(nonZeroDat3);
        rangeHigh(i,3) = means(i,3)+std(nonZeroDat3);
        rangeLow(i,3) = means(i,3)-std(nonZeroDat3);
    end
end
rangeLow = rangeLow * 100;
rangeHigh = rangeHigh * 100;
means = means * 100;

% disp summary statistics
disp("to get ≥ 75% RS, you need:")
for thrI = 1:3
    numEsMean = find(means(:,thrI)>=75,1);
    disp(strcat(num2str(numEsMean)))
end

thrs = [200, 500, 1000];
for j = 1:3
    coord_up = [[0:29,30.1]',rangeHigh(:,j)];
    coord_low = [[0:29,30.1]',rangeLow(:,j)];
    coord_combine = [coord_up;flipud(coord_low)];
    fill(coord_combine(:,1),coord_combine(:,2),colorz(j,:),'EdgeColor',colorz(j,:),'FaceAlpha',0.4,'HandleVisibility','off')

    plot([0:30],means(:,j),'LineWidth',2.5,'color',colorz(j,:),'DisplayName',strcat(num2str(thrs(j))," µV threshold"));
%     plot([0:30],rangeHigh(:,j),'LineWidth',2.5,'color',colorz(j,:),'HandleVisibility','off');
%     plot([0:30],rangeLow(:,j),'LineWidth',2.5,'color',colorz(j,:),'HandleVisibility','off');
end
% legend()
% legend(case_IDs)
% legend('optimal: 200 µV', 'optimal: 500 µV', 'optimal: 1000 µV', 'implant: 200 µV', 'implant: 500 µV', 'implant: 1000 µV')
legend('location','southeast')
xlim([0,14])
xlabel('Electrodes')
ylabel('Recording Sensitivity')
ss = split(tys_cA, "_");
s = split(ss(1), "cm2");
sss = split(ss(2), "cdmd");
ylim([0,1.01*100])
grid on
caseID_disp = strcat(s(1)," cm2, ",sss(1)," nAm/mm2");
yticks([0:0.25:1]*100)
xticks([0:4:30])
ROI_string = ROIs(ROI_i);
if ROI_string == "clinicianROI"
    ROI_string = "clinical";
end
% title([strcat("Mapping of ",ROI_string," ROI across electrode additions with std"),strcat(caseID_disp, ", ",degMapStrs(thrI),", cost:",replace(costFnStrs(thrI),"_"," "))])
ax = gca; 
ax.FontSize = 18; 
% set(gcf,'position',[500,500,550,250])
ylim([0,100])

if ROI_i ==1
    ylabel('Recording Sensitivity for ROI')
    fileName = 'Figures/Figure4_ROI_XX';
elseif ROI_i == 2 
    ylabel('Recording Sensitivity for LTL')
    fileName = 'Figures/Figure4a';
    xlim([0,30])
    xticks([0:4:30])
else
    ylabel('Recording Sensitivity for LH')
    fileName = 'Figures/Figure4b';
    xlim([0,30])
    xticks([0:4:30])
end
fileName = strcat(fileName,saveFigTag,'.pdf');
if ~isempty(saveFigTag)
    print(gcf,fileName,'-bestfit','-dpdf');
end

% create data for Source Data file
if ROI_i == 2 % LTL
    fN = strcat('SourceData/Figure4a');
elseif ROI_i == 3 % LH
    fN = strcat('SourceData/Figure4b');
end

matWrite = vertcat(string([0:31]),dat_all(:,:,1));
matWrite = horzcat(["";string(patientIDs)'],matWrite);
writematrix(matWrite,strcat(fN,'_200µV.xls'))

matWrite = vertcat(string([0:31]),dat_all(:,:,2));
matWrite = horzcat(["";string(patientIDs)'],matWrite);
writematrix(matWrite,strcat(fN,'_500µV.xls'))

matWrite = vertcat(string([0:31]),dat_all(:,:,3));
matWrite = horzcat(["";string(patientIDs)'],matWrite);
writematrix(matWrite,strcat(fN,'_1000µV.xls'))

end
