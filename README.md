# Optimization of Patient-Specific Stereo-EEG Recording Sensitivity

We used patient-specific finite element head models to quantify and optimize the recording sensitivity of sEEG electrode configurations. 

Funding: This work is supported by Duke MEDx, Duke CTSA Grant UL1 TR002553, and NIH F31NS124094. 


## Introduction

Stereo-EEG (sEEG) is a surgical technique used to localize the epileptogenic zone (EZ) in patients with drug-resistant epilepsy. However, the optimal sEEG electrode configurations to record from patient-specific regions of interest (ROIs) are unknown. We developed a semi-automated sEEG planning method to quantify and optimize the spatial extent of recordable tissue in an ROI for any patient. This  repository contains all code for this method as well as for further analyses of recordability and optimization benefit. 

The main pipeline for quantification and optimization of recording sensitivity.

![Pipeline Diagram](/Figures/Figure1.png "Pipeline Diagram")
We used T1 MRI, PostOp CT, and DW-MRI imaging modalities to generate 12 patient-specific finite element method (FEM) head models. We generated a set of valid electrode trajectories, computed recording sensitivity, and optimized configurations given a cost function and a ROI.


## Requirements 
Because of large computational complexity, this code was run and tested on the Duke Computing Cluster, a SLURM-based system, using up to 400 CPUs simultaneously. Certain sections can be run locally on any system with the following requirements and at least 4 GB RAM. We designed demos to run these sections. The full pipeline and demos have been tested on these softwares and systems. 

### Full Pipeline Requirements
- matlabR2019a
- SLURM cluster (with ≥ 400 CPUs total, ≥ 22 CPUs/node and ≥ 200 GB / node). Data storage location with at least 3 TB availability. 
- SCIRun-5.0-beta.Y 
- Python version 3.9.13 with packages: 
    * nilearn, nibabel, numpy, skimage
- freesurfer v6.0.0
- fsl 6.1
- dcm2niix

### Demo Requirements 
- Any standard computer with at least 4 GB RAM.
- matlabR2019a
- Optional: SCIRun-5.0-beta.Y 


## Installation instructions
1. Install this GitLab repository 
    * `git clone https://gitlab.oit.duke.edu/ged12/seeg-rec-sensitivity `
2. Install the required dependencies for the version you want to run, listed in the 'Requirements' section above. See the corresponding software and package installation guides and estimated install times. 

## Data Overview
### Input Data
De-identified patient imaging data, the main inputs to the whole codebase, are located in the `/InputImaging` directory.
Segmented and processed head model data (inputs for demos) are located in the `/InputData` and `/InputGeneral` directories. 
### Source Data
All source data contained in the main and supplementary figures are located in the `/SourceData` directory.
### Raw Data
Precomputed intermediate data required for the demos and figure plotting scripts are located in the `/RawData` directory.

## Running the code

### Demos
The demos are designed to exhibit code functionality on a single patient with a local system with at least 4 GB of RAM. We selected Patient 25 for the demos. The most computationally-heavy analyses and simulations cannot be run on a local system and are therefore excluded from the demos. 

1. Install necessary code and dependencies. 
    * Clone this repository and download dependencies (refer to Demo requirements section). 
2. Run recordable radius demo. (expected runtime: 5 minutes)
    * See `/RecRadius/README.md`
3. Run main pipeline demos 1 and 2 (expected runtime: 30 minutes)
    * See `/MainPipeline/README.md`


### Running the full codebase
The full codebase requries a SLURM-based computing cluster (see requriements section) and is broken into independent sections. 

0. Install necessary code and dependencies. 
    * Clone this repository and download dependencies (refer to requirements section).
1. Run head model generation (expected runtime: < 1 hour)
    * See `/HeadModelGen` README
2. Run the sEEG noise analysis section (expected runtime: < 10 minutes)
    * See `/NoiseSEEG` README
    * For Supplemental Fig 1 generation
2. Run the main pipeline (expected runtime: 12 - 24 hours)
    * See `/MainPipeline` README
    * For Figure 3, Figure 4ab, Supplemental Table 2, and Supplemental Figure 2&3 generation
3. Run recording radius section (expected runtime: < 4 hours)
    * See `/RecRadius` README 
    * For Figure 2 generation
4. Run the cross patient configuration analysis (expected runtime: < 4 hours)
    * See `/CrossPatConfTest` README
    * For Figure 4cd generation
5. Run sensitivity analysis section (expected runtime: < 4 hours)
    * See `/SensitivityAnalysis` README
    * For Supplemental Figure 4 generation


## Authors 
- Grace E. Dessert, Department of Biomedical Engineering, Duke University, Durham, NC, 27708, USA
- Brandon J. Thio,  Department of Biomedical Engineering, Duke University, Durham, NC, 27708, USA
- Warren M. Grill,  Department of Biomedical Engineering, Electrical and Computer Engineering (secondary), Department of Neurobiology (secondary), Department of Neurosurgery (secondary), Duke University, Durham, NC, 27708, USA

## Citation
If you use any part of this code for your work, please cite our paper (https://doi.org/10.1093/braincomms/fcad304) and the DOI of this repository (https://doi.org/10.5281/zenodo.7604030).

## License
The copyrights of this software are owned by Duke University. As such, two licenses to this software are offered:
1. An open-source license under the GPLv2 license for non-commercial use.
2. A custom license with Duke University, for commercial use without the GPLv2 license restrictions. 
 
As a recipient of this software, you may choose which license to receive the code under. Outside contributions to the Duke-owned code base cannot be accepted unless the contributor transfers the copyright to those changes over to Duke University.
To enter a custom license agreement without the GPLv2  license restrictions, please contact the Duke Office for Translation & Commercialization (OTC) (https://olv.duke.edu/) at otcquestions@duke.edu with
reference to “OTC File No. 7982 in your email. 
 
Please note that this software is distributed AS IS, WITHOUT ANY WARRANTY; and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

