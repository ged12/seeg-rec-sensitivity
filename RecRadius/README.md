# Recordable Radius
__Figure 2__

With this code, we determined the distance that sEEG contacts can record discernible signals from neural sources, and the complexity of the relationship between contact location and recording sensitivity. We selected 105 contact locations on each cortex surface of each patient. Using the main simualion pipeline in other sections, we calculated the recording sensitivity of these contact locations for realistic epileptic sources at increasing radii around each contact location. 

We developed a demo version of this code (running instructions below) to main functionality and data generation. Running the full version requires a SLURM-based computing cluster (Duke Computing Cluster) and integration with the main simulation code pipeline in other sections. 

## Software and hardware requirements:
#### Demo
- matlabR2019a
- Optional: SCIRun-5.0-beta.Y 

#### Full Version
- SLURM cluster (with ≥ 400 CPUs total, ≥ 22 CPUs/node and ≥ 200 GB / node). Data storage location with at least 3 TB availability. 
- matlabR2019a
- SCIRun-5.0-beta.Y 

## Running the demo:

The demo can be run on any standard computer, and does not require integration with the main pipeline.

1. Select contact locations for each patient.
    * Run `runRecRadius_demo.m` on Matlab (runtime: < 5 minutes)

2. (Simulate lead field and get recordable dipoles for each contact location.)
    * These steps require integration with the main pipeline and a computing cluster. The minimum output data for Patient 25 is precomputed for the sake of the demo.

3. Calculate recording sensitivity in increasing radii around each patch center element.
    * Run `getStContactLocs_demo.m` on Matlab (runtime: < 1 minute)

4. Plot data
    * Run `Figure2_plot.m` (located in the main directory)
    * Optional: Run `visRecRadius.srn5` on SCIRun for Figure 2b generation
    * Outputs:
	    * Figures/Figure2{a,b,d}.pdf
	    * SourceData/Figure4{a,b,d}_{caseInfo}.xls

## Running the full version

The main pipeline (generation of head models, electrode sets, lead fields, and calcualtion of recordable dipoles) must be run before this section. 

1.	(Select contact locations for each patient.) This is done in the main pipeline.
    * It can be done again by running `getStContactLocs.m` on Matlab (runtime < 1 minute)
	
2.  (Simulate lead field matrices.) This is done in the main pipeline.
    * Selected contact locations are added to main contactLocs fields in `precomputeValidElectrodeLocs.m`

3.  (Get recordable dipoles for each contact locs.) This is done in the main pipeline.
	* Recordable dipoles are calculated in `precompRecAreaAllElectrodes.m`

4.  Calculate percent recordability in increasing radii around each patch center element and create visualization files for SCIRun visualization.
    * Run `recRadius.slurm` on a SLURM-based computing cluster. Make sure to update configurations and paths.
    * This script runs `runRecRadius.m`. Runtime < 60 minutes.

4. Plot data (Run locally)
    * Run `Figure2_plot.m` (located in the main directory)
    * Optional: Run `visRecRadius.srn5` on SCIRun for Figure 2b generation
    * Outputs:
	    * Figures/Figure2{a,b,d}.pdf
	    * SourceData/Figure4{a,b,d}_{caseInfo}.xls


