% run Rec Radius Analysis

% configured for demo run (only Patient 25)
demoRun = true;
currDir = pwd;
d = split(currDir,"/");
if d(end) == "RecRadius"
    cd ..
elseif d(end) ~= "seeg-rec-sensitivity"
    disp("ERROR: Make sure you're running this from the right directory!")
    return
end

% run analysis for each source type
selectedInds = [1:105];
patIDs = [2,3,7,19,20,24,25,26,27,30,31,33];
for sourceIndex = 1:9
    numDipoles = investigateRecRadius_V12(patIDs, sourceIndex, selectedInds,demoRun);
end

function numDipoles = investigateRecRadius_V12(patientIDs, sourceIndex, selectedInds,demoRun)

% Open all files needed
% 20k face cortex surfaces, thresholded patch LF, 
distsPlot = [0:2.5:100]; % mm
percRecInRadius = zeros(length(distsPlot)-1,105,3,length(patientIDs)); % distance, contact, threshold, patient

for patI = 1:length(patientIDs)
        
    patientID = patientIDs(patI);
    patientDir = strcat("Patient_",num2str(patientID));

    if demoRun
        if patientID ~= 25
            continue
        end
        disp("We are running the demo, using only Patient 25!")
    else
        disp("We are running the full version! Make sure you have checked all filepaths!")
    end
    
    dataHomePath = "";
    dataWorkPath = "";

    % load cortex and implanted contact locations
    local = false;
    try
        cortexSurf = stlread(strcat(dataHomePath,patientDir,"/Inputs/cortexSurf.stl"));
    catch
        local = true;
        dataWorkPath = '';
        dataHomePath = '';
        cortexSurf = stlread(strcat(dataHomePath,patientDir,"/Inputs/cortexSurf.stl"));
    end
    load(strcat(dataWorkPath,patientDir,'/HeadModel/cortex_normals.mat'),'cortex_normals')
    load(strcat(dataWorkPath,patientDir,'/ElectrodeLocsData/recRadiusContactLocs.mat'),'allSelContacts'); % contact locs for rec radius study
    
    % get specific case
    cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
    patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
    cdmd = cdmds(sourceIndex); % nAm/mm^2
    patchArea = patchAreas(sourceIndex); % cm^2
    tys = strcat(num2str(patchArea),"cm2_",num2str(cdmd),"cdmd");
    tys_nice = strcat(num2str(patchArea)," cm^2, ",num2str(cdmd)," cdmd,")

    cNumStrings = ["C1","C2","C3"]; % 100, 500, and 1000 µV threshold

    for cNum = 1:3

        cNumStr = cNumStrings(cNum);
        full_case_id = strcat(tys_nice," mapping at ");
        thrs = [200,500,1000];
        full_case_id = strcat(full_case_id,num2str(thrs(cNum))," µV threshold")

        % import thresholded patch LF with voltages at contact locs
        load(strcat(dataWorkPath,patientDir,'/recArea/recDipolesbyC_recRadius',cNumStr,'_',tys,'.mat'),'leadFieldThr')

        % visualize chosen contact locs to make sure they are not in sulci or weird area
        contactLocsSel = allSelContacts.node(:,selectedInds)';
        %     if local
        %         figure(1)
        %         clf
        %         hold on
        %         trimesh(cortexSurf,'FaceAlpha',0)
        % %         scatter3(cortex_normals.node(1,:),cortex_normals.node(2,:),cortex_normals.node(3,:))
        %         scatter3(contactLocsSel(:,1),contactLocsSel(:,2),contactLocsSel(:,3),'filled','red')
        %     end

        % get rec area of chosen contacts
        % visualize points on cortex AND rec area around 
        % on SCIRun
        % network: RecRadius/VisRecArea.srn5
        
        rec_tissue.field = sum(leadFieldThr(selectedInds,:),1);
        rec_tissue.node = cortex_normals.node;
        save(strcat(dataHomePath,'RawData/RecRadius/rec_tissue_demo',num2str(thrs(cNum)),'µV.mat'),'rec_tissue')

        contacts.node = contactLocsSel;
        contacts.field = 1:length(selectedInds);
        save(strcat(dataHomePath,'RawData/RecRadius/contactsDemo.mat'),'contacts')

        % is there a clearly defined radius? no .. 

        % get distances and plot
%         disp("sizes are")
%         size(contactLocsSel)
%         size(cortex_normals.node)
        numDipoles = zeros(length(selectedInds),length(distsPlot)-1);
        for cInd = 1:length(selectedInds)
            contactLocation = contactLocsSel(cInd,:);
            if size(contactLocation,1) ~= 3
                contactLocation = contactLocation';
            end
            distsToDs = sqrt(sum((cortex_normals.node - contactLocation).^2,1)); % mm
            dipolesInRadius = zeros(length(distsPlot)-1,length(cortex_normals.node));
            % get % rec inside each distance
            for distI = 1:length(distsPlot)-1
                dipolesInRadius(distI,:) = min(distsToDs<distsPlot(distI+1),distsToDs>=distsPlot(distI));
            end
            numDipoles(cInd,:) = sum(dipolesInRadius,2)';
            % save(strcat(dataHomePath,'RecRadius/dipolesInRadius.mat'),'dipolesInRadius')
            contactRecordability = leadFieldThr(selectedInds(cInd),:); % logical 
            for distI = 1:length(distsPlot)-1
                if sum(dipolesInRadius(distI,:)) == 0
                    percRecInRadius(distI,cInd,cNum,patI) = 0;
                else
                    percRecInRadius(distI,cInd,cNum,patI) = sum(contactRecordability(logical(dipolesInRadius(distI,:))))/sum(dipolesInRadius(distI,:));
                end
            end
        end

        % plot distances to see spread
        %     if cNum == 1
        %         if local
        %             f = figure(2)
        %         else
        %             f = figure("visible","off")
        %         end
        %         clf
        %         hold on
        %         scatter([1:length(distsToDs)],distsToDs/10)
        %         savefig(f,strcat(dataHomePath,'RecRadius/Vis/distsPlot.fig'))
        %     end
        
    end
    
end

save(strcat(dataHomePath,'RawData/RecRadius/percRecInRadiusDemo_',tys,'.mat'),'percRecInRadius')

end




