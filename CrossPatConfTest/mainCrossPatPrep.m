% main CrossAn Pipeline Prep
% Grace Dessert
% 11/15/22
% called from monitorCrossAn.slurm

function mainCrossPatPrep(patientID)

dataWorkPath='/work/ged12/Optimize_sEEG_implant/'
dataHomePath='/hpc/group/wmglab/ged12/cluster/Optimize_sEEG_implant/electrode-config-optimization-seeg/'

patientDir = strcat("Patient_",num2str(patientID));

cdmds=[0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16];
patchAreas=[6, 6, 6, 10, 10, 10, 20, 20, 20];

% create directories
mkdir(strcat(dataHomePath,"/CrossPatConfTest/",patientDir));
mkdir(strcat(dataHomePath,"/CrossPatConfTest/",patientDir,"/out"));
mkdir(strcat(dataWorkPath,patientDir,"/CrossAn"));
mkdir(strcat(dataWorkPath,patientDir,"/CrossAn/DipoleAutoScripts"));
mkdir(strcat(dataWorkPath,patientDir,"/CrossAn/DipoleNetworks"));
mkdir(strcat(dataWorkPath,patientDir,"/CrossAn/LeadField"));
mkdir(strcat(dataWorkPath,patientDir,"/CrossAn/LeadField/FSs_C1"));
mkdir(strcat(dataWorkPath,patientDir,"/CrossAn/LeadField/FSs"));
mkdir(strcat(dataWorkPath,patientDir,"/CrossAn/recArea"));

% make patient-specific scripts
writeParametersCross(patientID, cdmds, patchAreas, dataWorkPath, dataHomePath)

% make Python scripts to make SCIRun networks
disp('Writing Python Script for Lead Field Generation SciRun Network');
writeAutoPythonScripts_CrossAn(dataWorkPath, dataHomePath, patientID, 40100)




end



function writeAutoPythonScripts_CrossAn(dataWorkPath, dataHomePath, patientID, numDipoles)

patientDir = strcat("Patient_",num2str(patientID));
for i=1:numDipoles
    fid = fopen(strcat(dataWorkPath,patientDir,'/CrossAn/DipoleAutoScripts/DipoleScript',num2str(i),'.py'),'w');

    fprintf(fid,'import os\n');
    fprintf(fid,strcat('scirun_load_network("',dataHomePath,'InputGeneral/PatientForwardDipoleAllAutomation.srn5")\n'));
    fprintf(fid,strcat('scirun_set_module_state("GetMatrixSlice:0","SliceIndex",',num2str(i-1),')\n'));
    fprintf(fid,strcat('scirun_set_module_state("ReadField:94","Filename", "',dataWorkPath,patientDir, '/HeadModel/HeadTetMesh.fld")\n'));
    fprintf(fid,strcat('scirun_set_module_state("ReadField:3","Filename","',dataHomePath, patientDir,'/Inputs/ConductivityTensors.mat")\n'));
    fprintf(fid,strcat('scirun_set_module_state("ReadField:4","Filename","',dataHomePath,patientDir, '/Inputs/SkinFinal.stl")\n'));
    fprintf(fid,strcat('scirun_set_module_state("ReadField:128","Filename","',dataWorkPath,patientDir, '/HeadModel/cortex_normals.mat")\n'));
    % set target error 1x10^(-10)
    fprintf(fid,strcat('scirun_set_module_state("SolveLinearSystem:13","TargetError","1e-10")\n'));
    % auto-gen electrode contact locs + clinically implanted set
    fprintf(fid,strcat('scirun_set_module_state("ReadField:93","Filename","',dataHomePath,'CrossPatConfTest/',patientDir,'/contactLocs.mat")\n'));

    fprintf(fid,strcat('scirun_set_module_state("WriteMatrix:0","FileTypeName", "Matlab Matrix")\n'));
    fprintf(fid,strcat('scirun_set_module_state("WriteMatrix:0","Filename","',dataWorkPath,patientDir, '/CrossAn/LeadField/FSs/HeadDipoleVoltageSampledAtRecLoc',num2str(i),'.mat")\n'));

    fprintf(fid,strcat('scirun_save_network("',dataWorkPath,patientDir,'/CrossAn/DipoleNetworks/PatientForwardDipoleAllAutomation',num2str(i),'.srn5")\n'));
    fclose(fid);
end

end



function writeParametersCross(patientID, cdmds, patchAreas, dataWorkPath, dataHomePath)
params = ["patientID", "cdmds", "patchAreas", "dataWorkPath", "dataHomePath"];

% get str versions
patientID_s = strcat(num2str(patientID),";"); %"1;"; 
cdmds_s = strcat("[",strjoin(arrayfun(@(a)num2str(a),cdmds,'uni',0), ", "),"];"); % "[1, 1, 0.2, 0.2];"; %nAm/mm^2
patchAreas_s = strcat("[",strjoin(arrayfun(@(a)num2str(a),patchAreas,'uni',0), ", "),"];"); % "[5, 9.5, 5, 9.5];"; % cm^2
dataWorkPath_s = strcat("'",dataWorkPath,"';"); %"'/work/ged12/Optimize_sEEG_implant/';"
dataHomePath_s = strcat("'",dataHomePath,"';"); %"'/hpc/group/wmglab/ged12/cluster/Optimize_sEEG_implant/';";

paramValues = [patientID_s, cdmds_s, patchAreas_s, dataWorkPath_s, dataHomePath_s];

% open files that contain parameters
scriptLoadPath = strcat(dataHomePath,"CrossPatConfTest/");

patientDir = ['Patient_' num2str(patientID)];
% filesToEdit = {strcat("exampleChangeParams.m")};
filesToEdit = [strcat(scriptLoadPath, "CompressFS_crossAn.m"), ...
               strcat(scriptLoadPath, "precompRecAreaAllElectrodes_crossAn.m"), ...
               strcat(scriptLoadPath, "createLF_crossAn.m"), ...
               ];
           
% search for query string 
queryTag = '% $$ UPDATE PARAMETERS $$';

% parse following lines until empty (or just whitespace) line
for fInd = 1:length(filesToEdit)
    fileName = char(filesToEdit{fInd});
    
    if isfile(fileName)
        lines = readlines(fileName);
    else
        disp(strcat("Error! ",fileName, " not found!"));
        continue
    end
    
    for i = 1:length(lines)
        if contains(lines{i},queryTag)
            disp(lines{i})
            nextLine = lines{i};
            while (~isempty(nextLine))
                for p = 1:length(params)
                    nextLineC = char(nextLine);
                    if (length(nextLineC)<length(char(params(p))))
                        continue;
                    end
                    if (nextLineC(1:length(char(params(p)))) == params(p))
                        newParamLine = strcat(params(p), " = ", paramValues(p));
                        lines{i} = char(newParamLine);
                        break;
                    end
                end
                i = i + 1;
                nextLine = lines{i};
            end
            break;
        end
    end
    
    spFN = split(fileName,"/");
%     if (length(spFN)==1)
%         newDirFN = patientDir;
%         fileNameE = strcat(patientDir,"/",spFN);
%     else
%         newDirFN = strcat(join(spFN(1:end-1),"/"),"/",patientDir);
%         fileNameE = strcat(newDirFN,"/",spFN(end));
%     end
    newDirFN = strcat(dataHomePath,"CrossPatConfTest/",patientDir);
    fileNameE = strcat(newDirFN,"/",spFN(end));
    
    if ~exist(newDirFN, 'dir')
       mkdir(newDirFN)
    end
    fid = fopen(fileNameE,'w');
    for i = 1:length(lines)
        fprintf(fid,'%s\n',lines{i});
    end
    fclose(fid);
    
end
% $$ UPDATE PARAMETERS $$
% patientID = 1;
% cdmd = 1; % nAm/mm^2 %%%%%%%%%%% need a way to automate changing this %%%%
% dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';


%% also change slurm scripts!
% Using template files in home directory, replace all tags with patient
%   specific values.
% Save new scripts in home/PatientDir

tags = ["$$PATDIR$$","$$HOMEDIR$$",'$$WORKDIR$$'];
patientDir = strcat('Patient_',num2str(patientID));

tagReps = {patientDir, strcat(dataHomePath), strcat(dataWorkPath)};

% for testing!
test = 0;
scriptLoadPath = strcat(dataHomePath, "CrossPatConfTest/");


% template files in home directory
filesToEdit = {strcat(scriptLoadPath,"makeAndRunNets.slurm"), ...
            strcat(scriptLoadPath,"createLFfromFSs.slurm"), ...
            strcat(scriptLoadPath,"precompRecAreaAllElectrodes.slurm"), ...   
    };

% parse following lines until empty (or just whitespace) line
for fInd = 1:length(filesToEdit)
    fileName = char(filesToEdit{fInd});
    
    if isfile(fileName)
        lines = readlines(fileName);
    else
        disp(strcat("Error! ",fileName, " not found!"));
        break;
    end
    
    for i = 1:length(lines)
        for t = 1:length(tags)
            thisLine = lines{i};
            thisTag = tags(t);
            if contains(thisLine,thisTag)
                lines{i} = char(strrep(thisLine,thisTag,tagReps{t}));
            end
        end
        thisLine = lines{i};
    end
    
    spFN = split(fileName,"/");
    newDirFN = strcat(dataHomePath,'CrossPatConfTest/',patientDir);
    fileNameE = strcat(newDirFN,"/",spFN(end));
    if ~exist(newDirFN, 'dir')
       mkdir(newDirFN)
    end
    fid = fopen(fileNameE,'w');
    for i = 1:length(lines)
        fprintf(fid,'%s\n',lines{i});
    end
    fclose(fid);
    
end

end

function lines = readlines(fileName)
    % Opens the file
    fid = fopen(fileName,'rt');

    maxl = -1; % read whole file
    lines = textscan(fid,'%s',maxl,'delimiter', '\n', 'whitespace','');
    if ~isempty(lines)
        lines = lines{1};
    end
    % Close file
    fclose(fid);
end

