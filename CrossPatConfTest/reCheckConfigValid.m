% test sulci intersections of transformed configurations 
% Grace Dessert

patientIDs = [2,3,7,19,20,24,25,26,27,30,31,33];
sourceNum = 4;
ROI_i = 1; % 1 == LTL, 2== LH
plotT = true;
thrI = 1;
cNum = thrI;

addpath('ContentScripts')

% use a distance threshold of 1.5 mm because BVH method is imperfect near
% boundary of threshold (2.5 mm)

% to correct for intersection errors due to mislabeling validity for last
% electrode in each line, exclude all electrodes that intersect in
% cis-config case

electrodesExclude = zeros(12,31);

% 1 is pass, 0 is fail sulci intersection test
validitiyCodes = zeros(12,12); 
for patientID = [2,3,7,19,20,24,25,26,27,30,31,33]
    patientID
    % use distance measurements
    [lh, rh, contactLocs, midlinePlane, sulciPoints] = loadMain(patientID, sourceNum, ROI_i);
    % first do matched-patient case
    [configPoints, cToEAssoc] = getConfig2(cNum, patientID, contactLocs, sourceNum, ROI_i);
    eExclude = [];
    [validityConfigCodes, eExclude] = checkConfigValid(lh, rh, configPoints,cToEAssoc,midlinePlane,sulciPoints,plotT,eExclude); %,BVH_surface_pointInds,BVH_boundingBoxes,childrenInds)
    electrodesExclude(patientIDs==patientID,eExclude) = 1;
    pause
    continue
    for patID2 = [2,3,7,19,20,24,25,26,27,30,31,33]
        [configPoints, cToEAssoc] = getConfig2(cNum, patID2, contactLocs, sourceNum, ROI_i);
        validityConfigCodes = checkConfigValid(lh, rh, configPoints,cToEAssoc,midlinePlane,sulciPoints,plotT,eExclude) %,BVH_surface_pointInds,BVH_boundingBoxes,childrenInds)
        validitiyCodes(patientIDs==patientID,patientIDs==patID2) = validityConfigCodes(2); % 1 is pass, 0 is fail sulci intersection test
    end
end
%%
if ROI_i == 1
    save(strcat('CrossPatConfTest/validitiyCodesLTL_thr',num2str(thrI),'.mat'),'validitiyCodes');
elseif ROI_i == 2
    save(strcat('CrossPatConfTest/validitiyCodesLH_thr',num2str(thrI),'.mat'),'validitiyCodes');
end

function [LH_white, RH_white, contactLocs, midlinePlane,sulciPoints] = loadMain(patientID, sourceNum, ROI_i)

ROI_ss = ["LTL","LH"];
ROI_s = ROI_ss(ROI_i);

% $$ UPDATE PARAMETERS $$
cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
dataWorkPath = '';
dataHomePath = '';

patientDir = ['Patient_' num2str(patientID)];

cdmd = cdmds(sourceNum); % nAm/mm^2
patchArea = patchAreas(sourceNum); % cm^2
tys = strcat(num2str(patchArea),"cm2_",num2str(cdmd),"cdmd");

% import contact-electrode-configuration association data
load(strcat(dataHomePath,'CrossPatConfTest/Data/solnCContactsPat_',num2str(patientID),'.mat'),'configs')
load(strcat(dataHomePath,'CrossPatConfTest/Data/solnContacuniquetsUniquePat_',num2str(patientID),'.mat'),'configsUnique')
load(strcat(dataHomePath,'CrossPatConfTest/Data/solnContactsUTransPat_',num2str(patientID),'.mat'),'confUniqTrans')
load(strcat(dataHomePath,'CrossPatConfTest/Data/solnCLabels_',num2str(patientID),'.mat'),'labels')
load(strcat(dataHomePath,'CrossPatConfTest/Patient_',num2str(patientID),'/contactLocs.mat'),'contactLocs')
contactLocsPat1 = contactLocs;
% contactLocsPat1 field gives patient ID number that each chunk of contacts comes from

% load cortex
LH_fileName = strcat(dataHomePath,patientDir,'/Inputs/lh_white_20k.stl');
LH_white = stlread(LH_fileName);
RH_fileName = strcat(dataHomePath,patientDir,'/Inputs/rh_white_20k.stl');
RH_white = stlread(RH_fileName);

% only use one cost function per threshold
thrPrioritys = ["200_500_1000_thrPriority","500_200_1000_thrPriority","1000_500_200_thrPriority"]; %,"1000_200_500_thrPriority","500_1000_200_thrPriority","200_1000_500_thrPriority"];

% get ROIdipoles for perc rec calc
load(strcat(dataWorkPath,patientDir,'/OptSolutions/ROIs.mat'),'patches')
ROITI = 0;
for i = 1:length(patches.name)
    if patches.name{i}==ROI_s
        ROITI = i;
        break
    end
end
if ROITI==0
    disp("ERROR ROIstring NOT FOUND")
    disp(ROI_s)
end
dipolesROI = patches.dipoles{ROITI};

% get cortex areas for perc rec calcß
load(strcat(dataWorkPath,patientDir,'/HeadModel/cortex_areas.mat'),'cortex_areas');

% get patient surfs for valid electrode calc
load(strcat(dataWorkPath,patientDir,'/HeadModel/midlinePlane.mat'),'midlinePlane');
load(strcat(dataWorkPath,patientDir,'/HeadModel/SulciSurface_refined.mat'),'sulciPoints');

%sulci.Points = sulciPoints;
%[BVH_surface_pointInds,BVH_boundingBoxes,childrenInds] = boundingVolumeHierarchyConstruction(sulci);

end


function [thisContactLocsPat1, cToEAssoc] = getConfig2(cNum, patID2, contactLocsPat1, sourceNum, ROI_i)

ROI_ss = ["LTL","LH"];
ROI_s = ROI_ss(ROI_i);

cNumStrings = ["C1","C2","C3"];
cNumStr = cNumStrings(cNum);

% only use one cost function per threshold
thrPrioritys = ["200_500_1000_thrPriority","500_200_1000_thrPriority","1000_500_200_thrPriority"]; %,"1000_200_500_thrPriority","500_1000_200_thrPriority","200_1000_500_thrPriority"];

% get contactInds of interest for this patient
configIndsPat2 = find(contactLocsPat1.field==patID2);
% get specific config of interest
dataHomePath = '';
load(strcat(dataHomePath,'CrossPatConfTest/Data/solnCLabels_',num2str(patID2),'.mat'),'labels')
configContactIndsUn = [];
for k = 1:size(labels,1)
    if labels{k,5} == sourceNum && labels{k,4} == ROI_s && labels{k,6} == thrPrioritys(cNum)
        disp(strcat("Selecting case: ",ROI_s,", source ",num2str(sourceNum),", thrP ",thrPrioritys(cNum)))
        configContactIndsUn = labels{k,2}; % select unique contact inds in this patient field
        break
    end
end
if isempty(configContactIndsUn)
    disp("We didn't find this case!")
end
fullInds = configIndsPat2(configContactIndsUn);
thisContactLocsPat1 = contactLocsPat1.node(fullInds,:);

% get contact-to-electrode association
% use same method as implant config cToEAssoc gen so we don't have
% to import the full precomputed cToE assoc matrix and we can
% guarantee that we are using the correct configurations
%
plotT = false;
cToEAssoc = getSearchElectrodeAssociation(thisContactLocsPat1,plotT);

numEs = size(cToEAssoc,1);
%disp(strcat("We found ",num2str(numEs)," electrodes in this configuration from ",num2str(length(fullInds))," contacts"))

end


function [validityConfigCodes, eIntersect] = checkConfigValid(lh, rh, configPoints,cToEAssoc,midlinePlane,sulciPoints,plotT,eExclude) %,BVH_surface_pointInds,BVH_boundingBoxes,childrenInds)

% plot cortex, sulci, midline, and electrodes
if plotT
figure(1)
clf
hold on
trimesh(lh,'FaceAlpha',0.01,'EdgeAlpha',0.1)
trimesh(rh,'FaceAlpha',0.01,'EdgeAlpha',0.1)
points = [lh.Points', rh.Points'];
z = linspace(min(points(3,:)),max(points(3,:)),10);
y = linspace(min(points(2,:)),max(points(2,:)),10);
[z2,y2]=meshgrid(z,y);
% ax + by + cz + d = 0
n = midlinePlane(:,1);
p = midlinePlane(:,2);
d = -p'*n;
x2 = (-n(2)*y2 - n(3)*z2 - d)/n(1);
% plot plane
surf(x2,y2,z2,'FaceAlpha',1,'FaceColor',[1,1,1]);
end
    
% Values of 1 mean valid / passing for a certain criteria. Values of 0 mean INVALID aka failure of that criteria.
validityConfigCodes = ones(2,1); % validity by midline intersect and sulci intersect

numEs = size(cToEAssoc,1);
% check if any electrodes cross midline
for i = 1:numEs
    contThisE = configPoints(cToEAssoc(i,:),:);
    % assume that contacts are always ordered continuously by start to end
    midLoc = checkMidline(contThisE(1,:), contThisE(end,:), midlinePlane);
    if midLoc == 0 % electrode is at least partially inside midline
        validityConfigCodes(1) = 0; % mark as invalid from midline intersect
    end
end

% check for sulci surface intersection
% using 2.5 mm threshold (same as in precomputeValidElectrodeLocs.m)

% use BVH 
% for each electrode
% intSulci = 0;
% for i = 1:numEs
%     contThisE = configPoints(cToEAssoc(i,:),:);
%     sloc_d = contThisE(1,:);
%     normal = norm(contThisE(end,:)-sloc_d);
%     [intersection,minDist] = boundingVolumeHierarchy(sloc_d',normal',2.5,sulciPoints',0,BVH_surface_pointInds,BVH_boundingBoxes,childrenInds,128);
%     if intersection==1 % does  intersect
%         disp(strcat("Electrode number ",num2str(i)," intersects!"))
%         intSulci = 1;
%     end
% end

[intSulci, eIntersect, pointsToPlot] = checkSulciIntersect(sulciPoints,configPoints,cToEAssoc, eExclude);

if intSulci == 1 % if at least one contact < threshold
    validityConfigCodes(2) = 0; % mark as invalid from sulci intersect
end

if plotT
    % plot 128 pts for every electrode
    scatter3(pointsToPlot(:,1),pointsToPlot(:,2),pointsToPlot(:,3),'.','DisplayName','E Points')
    % plot sulci
    scatter3(sulciPoints(:,1),sulciPoints(:,2),sulciPoints(:,3),'.','DisplayName','Sulci Points')
    colors = parula(31);
    % plot each set of contacts in another color
    for i = 1:size(cToEAssoc,1)
        if isempty(find(eIntersect==i, 1))
            scatter3(configPoints(cToEAssoc(i,:),1),configPoints(cToEAssoc(i,:),2),configPoints(cToEAssoc(i,:),3),'filled','MarkerFaceColor',colors(i,:),'DisplayName',strcat("Electrode ",num2str(i)))
        else
            scatter3(configPoints(cToEAssoc(i,:),1),configPoints(cToEAssoc(i,:),2),configPoints(cToEAssoc(i,:),3),'filled','MarkerFaceColor',[1,0,0],'DisplayName',strcat("Electrode ",num2str(i))) 
        end
    end
    legend()
end

end

function [intSulci, eFail,pointsToPlot] = checkSulciIntersect(sulciPoints,configPoints,cToEAssoc,eExclude)

    % just do simple distance check for all config points and all sulci
    % poitns
    intSulci = 0; % start no intersect
    eFail = [];
    pointsToPlot = [];
    % for every electrode, build 128 pts across and test distances
    for e = 1:size(cToEAssoc,1)
        if find(eExclude==e)
            disp(strcat("excluding electrode ",num2str(e)))
            continue
        end
        contThisE = configPoints(cToEAssoc(e,:),:);
        sloc_d = contThisE(1,:)';
        normal = (contThisE(end,:)'-sloc_d)/norm(contThisE(end,:)'-sloc_d);
        endpoint1  = sloc_d + 54.5*normal; 
        E1_points = sloc_d' + [0:1:128-1]'*((endpoint1-sloc_d)/(128-1))'; 
        pointsToPlot = [pointsToPlot;E1_points];
        for i = 1:128
            dists = sqrt(sum((sulciPoints-E1_points(i,:)).^2,2));
            if min(dists) < 1.5
                intSulci=1;
                disp(strcat("Electrode ",num2str(e)," intersects at point ",num2str(i)," of 128! with a min dist of ",num2str(min(dists))))
                eFail = [eFail,e];
                continue
            end
        end
    end
    
end


function midLoc = checkMidline(sloc, endloc, midlinePlane)
    % to stay consistent with previous methods in generating electrodes
    %   calc endLoc as 54.5mm from sloc in the same direction
    endloc = 54.5*(endloc-sloc)/norm(endloc-sloc) + sloc;
    
    % get side of electrode or whether any part of it is in the midline
    % say that an point is inside the midline if it is <= 1cm away
    n = midlinePlane(:,1)';
    p = midlinePlane(:,2)';

    % check what side sloc is on and distance to midline
    % get shortest distance from sloc point to midline plane
    d1 = dot((sloc-p),n);

    % check what side endloc is on and distance to midline
    d2 = dot((endloc-p),n);

    % if either distance is < 4mm or side1 ~= side2, return 0 
    %   meaning electrode is inside midline
    if sign(d1)~=sign(d2) || abs(d1) <= 4 || abs(d2) <= 4
        midLoc = 0;
    else
        midLoc = sign(d1);
        % return -1 if both are on left side (towards negative x)
        % return 1 if both are on right side (towards positive x)
    end

end


function cToE_imp = getSearchElectrodeAssociation(configContactLocs,plotT)
    % get electrode-to-contact association matrix
    % by clustering contact locations. Assume all contacts for one
    % electrode are adjacent in the implantedELocations matrix.
    % also make visualization with different colors to check.
    % DO NOT offset all of these contact numbering and electrode numbering to
    % after the generated electrodes and contacts. Use this only with the
    % clipped lead field, with data only corresponding to these contacts
    
    if size(configContactLocs,1) ~= 3
        configContactLocs = configContactLocs';
    end
    dists = sum(diff(configContactLocs,1,2).^2,1);
    numEs = 0;
    eStartAndEnds = [];
    currCDist = dists(1);
    currCStart = 1;
    for i = 1:length(dists)
        if abs(dists(i) - currCDist) > 20
            numEs = numEs + 1;
            eStartAndEnds = [eStartAndEnds;[currCStart,i]];
            currCStart = i+1;
            currCDist = dists(i+1);
        end
    end
    numEs = numEs + 1;
    eStartAndEnds = [eStartAndEnds;[currCStart,i+1]];
    
    % get cToE_imp logical matrix
    E = numEs;
    % C = size(leadFieldThr,1);
    %cToE = zeros(E,C,'logical');
    is = [];
    js = [];
    for i = 1:E
        is = [is;i*ones(eStartAndEnds(i,2)-eStartAndEnds(i,1)+1,1)]; % electrode indices
        js = [js;[eStartAndEnds(i,1):eStartAndEnds(i,2)]']; % contact indices
    end
    vs = true(length(js),1);
    size(is)
    size(js)
    size(vs)

    cToE_imp = sparse(double(is),double(js),vs);
    
    % plot contact locs and save
    %f = figure('visible','off');
    if plotT
        figure(1)
        clf
        hold on
        colors = parula(31);
        % plot each set of contacts in another color
        for i = 1:E
            scatter3(configContactLocs(1,eStartAndEnds(i,1):eStartAndEnds(i,2)),configContactLocs(2,eStartAndEnds(i,1):eStartAndEnds(i,2)),configContactLocs(3,eStartAndEnds(i,1):eStartAndEnds(i,2)),'filled','color',colors(i,:),'DisplayName',strcat("Electrode ",num2str(i)))
        end
        legend()
        title(strcat("Configuration with Electrode Clustering "),"FontSize",14)
    end
%     saveas(f,strcat(dataHomePath,patientDir,'/Test/implantedLocsWithEClusters'),'jpg');
%     saveas(f,strcat(dataHomePath,patientDir,'/Test/implantedLocsWithEClusters'),'fig');
    % openfig('Patient_7/Test/inputSurfaces.fig','new','visible')
   
end
