% CompressFS
% 10/28/21
% Immediately after a forward solution is created by SCIRun,
% open the output file, scale by area and cdmd,
%   perform ADC to compress, and replace file

% 1/19/22 
% Create three compressed LFs, each for a different threshold
%   thresholds: 100 µV, 500 µV, 1000 µV.
%   So, give each compressed LF a range 50% greater than the threshold.
%   max volt levels: 150 µV, 750 µV, 1500 µV 

% 10/3/22
% Just use 16-bit floats!! 

% 11/15/22
% for cross analysis
% only need one compression scheme

function CompressFS_crossAn(i)

% $$ UPDATE PARAMETERS $$
patientID = 1;
dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';

patientDir = ['Patient_' num2str(patientID)];

% open FS
filename = strcat(dataWorkPath,patientDir,'/CrossAn/LeadField/FSs/HeadDipoleVoltageSampledAtRecLoc',num2str(i),'.mat');
load(filename, 'scirunmatrix')
scirunmatrixIn = scirunmatrix;

if ~exist(strcat(dataWorkPath,patientDir,'/CrossAn/LeadField/FSs_C1/'), 'dir')
   mkdir(strcat(dataWorkPath,patientDir,'/CrossAn/LeadField/FSs_C1/'))
end

filenameOut = strcat(dataWorkPath,patientDir,'/CrossAn/LeadField/FSs_C1/HeadDipoleVoltageSampledAtRecLoc_C1_',num2str(i),'.mat');

% open areas 
load(strcat(dataWorkPath,patientDir,'/HeadModel/lh_areas.mat'), 'lh_areas')
load(strcat(dataWorkPath,patientDir,'/HeadModel/rh_areas.mat'), 'rh_areas')
areaST = [lh_areas;rh_areas];

% just use 16-bit floats!
scirunmatrix = scirunmatrixIn * areaST(i);
scirunmatrix = half(scirunmatrix);        
save(filenameOut, 'scirunmatrix')


end

