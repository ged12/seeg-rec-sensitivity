% convert .mat fields to .txt files with columns X, Y, Z separated by a
% space, and rows of coordinates separated by a new line

for patientID = [2,3,7,19,20,24,25,26,27,30,31,33]
    load(strcat('Data/solnContacuniquetsUniquePat_',num2str(patientID),'.mat'),'configsUnique')
    configsUniqueTxt = num2str(configsUnique);
    fileID = fopen(strcat('Data/solnContacuniquetsUniquePat_',num2str(patientID),'.txt'),'w');

    for j = 1:length(configsUniqueTxt)
        temCell = split(strip(configsUniqueTxt(j,:)));
        fprintf(fileID,'%f %f %f \n',str2num(temCell{1}), str2num(temCell{2}), str2num(temCell{3}));
    end
end