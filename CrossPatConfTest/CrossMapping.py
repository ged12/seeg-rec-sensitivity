from subprocess import run
import os 
from nilearn import plotting
import nilearn
import nibabel as nib
import numpy as np
import sys

if(len(sys.argv)!=2):
	print("Wrong number of inputs!!!")
	exit()

patientID = sys.argv[1]
dir_Patient = f"/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient{patientID}"
path_crossMappings="/hpc/group/wmglab/bjt20/HeadModelAutomation/CrossMappings"
dir_work = f'{dir_Patient}/work'

patients=[2,3,7,19,20,24,25,26,27,30,31,33]
import os
path = dir_work
MRI = path+"/MRI.nii"

#One to all MRI
for p in patients:
    MRI_Patient2 = f"/hpc/group/wmglab/bjt20/HeadModelAutomation/Patient{str(p)}/work/MRI.nii"
    outname=f"{path_crossMappings}/Patient{patientID}_Patient{str(p)}_mapping"
    cmd = f"flirt -in {MRI} -ref {MRI_Patient2} -out {outname}.nii -omat {outname}.mat -bins 256 -cost mutualinfo -searchrx -90 90 -searchry -90 90 -searchrz -90 90 -dof 6  -interp trilinear"
    print(cmd)
    result = run(cmd, capture_output=True,shell=True).stdout
t
    cmd = f"img2imgcoord -src {dir_work}/MRI.nii -dest {MRI_Patient2} -xfm {outname}.mat -mm {path_crossMappings}/solnContacuniquetsUniquePat_{patientID}.txt >{outname}_NewContactLocs.txt"
    print(cmd)
    result = run(cmd, capture_output=True,shell=True).stdout
