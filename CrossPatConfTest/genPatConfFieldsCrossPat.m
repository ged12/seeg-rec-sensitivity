% for cross-patient configuration testing,
%   get a field for every patient of their contact locations for all
%   relevant configurations

% after, transfer all to each others bases

% Grace Dessert
% 11/12/22

%for patientID = [2,3,7,19,20,24,25,26,27,30,31,33]
%    genPatConfFields_run(patientID)
%end

function genPatConfFields(patientID)

    patientDir = ['Patient_' num2str(patientID)];
    
    dataHomePath = '/hpc/group/wmglab/ged12/cluster/Optimize_sEEG_implant/electrode-config-optimization-seeg/';
    dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';
    % dataWorkPath = ''; % FOR LOCAL ONLY
    
    ROIs = ["LTL","LH"];
    thrPrioritys = ["200_500_1000_thrPriority","500_200_1000_thrPriority","1000_500_200_thrPriority","1000_200_500_thrPriority","500_1000_200_thrPriority","200_1000_500_thrPriority"];
    thrsPM = [1, 2, 3, 3, 2, 1];
    thrs = [200, 500, 1000];
    
    cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
    patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
    
    % get cases of interest and load 
    configs = [];
    caseIDs = cell(9*2*6,1);
    count = 1;
    labels = cell(9*2*6,6); % gives inds of nodes in contact field for each contact in each config for both full inds and unique inds. also gives full case ID, ROI (LH or LTL), sourceIndex, and cost function
    
    % load(strcat(dataWorkPath,patientDir,'/HeadModel/cortex_normals.mat'),'cortex_normals');
    
    for sourceInd = 1:9        
        for ROI_i = 1:2
            for thrPriI = 1:6
                thrI = thrsPM(thrPriI);
                
                tys = strcat(num2str(patchAreas(sourceInd)),"cm2_",num2str(cdmds(sourceInd)),"cdmd");
                thrPriority = thrPrioritys(thrPriI);
                
                labels{count,4} = ROIs(ROI_i); % ROI string
                labels{count,5} = sourceInd; % sourceInd
                labels{count,6} = thrPrioritys(thrPriI); % thrPriI/ cost fn
                
                % make case IDs
                case_id = strcat(tys,'_ROI_',ROIs(ROI_i),"_singleMap_",thrPriority);
                mapStrStr = strcat("single mapping at ",num2str(thrs(thrI))," µV threshold");
                full_case_id = strcat(case_id,mapStrStr);
                caseIDs{count} = full_case_id;
                count = count + 1;
            end
        end
    end
    currsourceInd = 0;
    configs = [];
    for i = 1:length(caseIDs)
        % find case ID
        thisSourceInd = labels{i,5};
        if thisSourceInd ~= currsourceInd % make sure source Inds are organized together for efficiency!
            tys = strcat(num2str(patchAreas(thisSourceInd)),"cm2_",num2str(cdmds(thisSourceInd)),"cdmd");
            load(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_cis',tys,'.mat'),'visDataConfigs')
        end
        found = false;
        for j = 1:length(visDataConfigs)
            if contains(visDataConfigs{j}.full_case_id,caseIDs{i})
                newRecLocs = visDataConfigs{j}.configField.node;
                % get and add configs
                labels{i,1} = size(configs,1)+1:(size(configs,1)+length(newRecLocs)); % full config IDs
                labels{i,3} = caseIDs{i}; % full case ID
                configs = [configs;newRecLocs];
                found = true;
                disp(strcat("We found this case ID: ",caseIDs{i}))
                break
            end
        end
        if ~found
            disp(strcat("ERROR we didnt find this case ID: ",caseIDs{i}))
        end
    end
    
    [configsUnique,~,confUniqTrans] = unique(configs,'rows');
    % configsUnique(confUniqTrans) = configs
    
    % transfer all inds to unique contact basis
    for i = 1:length(labels)
        fullInds = labels{i,1};
        uniqInds = confUniqTrans(fullInds);
        labels{i,2} = uniqInds;
    end
    
    % save
    save(strcat(dataHomePath,'RawData/CrossPatConfTest/solnCContactsPat_',num2str(patientID),'.mat'),'configs')
    save(strcat(dataHomePath,'RawData/CrossPatConfTest/solnCLabels_',num2str(patientID),'.mat'),'labels')
    save(strcat(dataHomePath,'RawData/CrossPatConfTest/solnContacuniquetsUniquePat_',num2str(patientID),'.mat'),'configsUnique')
    save(strcat(dataHomePath,'RawData/CrossPatConfTest/solnContactsUTransPat_',num2str(patientID),'.mat'),'confUniqTrans')

end
