% Grace Dessert
% 11/22/22
% for cross-patient analysis, using patch lead field matrices,
%   calculate recordable dipoles by Es and rec percentage (weighted by area
%   and not weighted by area) for each ROI (LH & LTL) and source type and
%   configuration (one for each patient).
% in the next script, compare how these rec percentages perform across
% patients

% inputCase = 1:(2ROIs*9sourceTypes) 
function precompRecAreaAllElectrodes_crossAn(inputCase)

sourceNum = ceil((inputCase)/2);
ROI_i = mod(inputCase-1,2)+1; % 1 == LTL, 2 == LH
ROI_ss = ["LTL","LH"];
ROI_s = ROI_ss(ROI_i);

% $$ UPDATE PARAMETERS $$
cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
dataWorkPath = '';
dataHomePath = '';
patientID = 2; 

patientDir = ['Patient_' num2str(patientID)];

cdmd = cdmds(sourceNum) % nAm/mm^2
patchArea = patchAreas(sourceNum) % cm^2
tys = strcat(num2str(patchArea),"cm2_",num2str(cdmd),"cdmd")

discVoltThreshs = [200, 500, 1000]; % µV

% import contact-electrode-configuration association data
load(strcat(dataHomePath,'CrossPatConfTest/Data/solnCContactsPat_',num2str(patientID),'.mat'),'configs')
load(strcat(dataHomePath,'CrossPatConfTest/Data/solnContacuniquetsUniquePat_',num2str(patientID),'.mat'),'configsUnique')
load(strcat(dataHomePath,'CrossPatConfTest/Data/solnContactsUTransPat_',num2str(patientID),'.mat'),'confUniqTrans')
load(strcat(dataHomePath,'CrossPatConfTest/Data/solnCLabels_',num2str(patientID),'.mat'),'labels')
load(strcat(dataHomePath,'CrossPatConfTest/Patient_',num2str(patientID),'/contactLocs.mat'),'contactLocs')
contactLocsPat1 = contactLocs;
% contactLocsPat1 field gives patient ID number that each chunk of contacts comes from

% load cortex
LH_fileName = strcat(dataHomePath,patientDir,'/Inputs/lh_white_20k.stl');
LH_white = stlread(LH_fileName);
RH_fileName = strcat(dataHomePath,patientDir,'/Inputs/rh_white_20k.stl');
RH_white = stlread(RH_fileName);

% only use one cost function per threshold
thrPrioritys = ["200_500_1000_thrPriority","500_200_1000_thrPriority","1000_500_200_thrPriority"] %,"1000_200_500_thrPriority","500_1000_200_thrPriority","200_1000_500_thrPriority"];

% create data storage structure
recDipolePercDat = cell(12,3*4); % 12 patients, 3 thrs x (recDipolesByE, percRecArea, percRecArea weighted by area, validity of config in new patient) grouped by threshold 

% get ROIdipoles for perc rec calc
load(strcat(dataWorkPath,patientDir,'/OptSolutions/ROIs.mat'),'patches')
ROITI = 0;
for i = 1:length(patches.name)
    if patches.name{i}==ROI_s
        ROITI = i;
        break
    end
end
if ROITI==0
    disp("ERROR ROIstring NOT FOUND")
    disp(ROI_s)
end
dipolesROI = patches.dipoles{ROITI};

% get cortex areas for perc rec calcß
load(strcat(dataWorkPath,patientDir,'/HeadModel/cortex_areas.mat'),'cortex_areas');

% get patient surfs for valid electrode calc
load(strcat(dataWorkPath,patientDir,'/HeadModel/midlinePlane.mat'),'midlinePlane');
load(strcat(dataWorkPath,patientDir,'/HeadModel/SulciSurface_refined.mat'),'sulciPoints');


% for all 3 thresholds
for cNum = 1:3
    threshold = discVoltThreshs(cNum) % µV
    cNumStrings = ["C1","C2","C3"];
    cNumStr = cNumStrings(cNum)

    tic
    % import thresholded patch LF with voltages at contact locs
    disp(['Loading LF matrix with threshold ' num2str(threshold)])
    load(strcat(dataWorkPath,patientDir,'/CrossAn/LeadField/PatchLeadField_RecLocs',cNumStr,'_thr_',tys,'.mat'),'leadFieldThr') % leadFieldThr
    disp('Lead Field matrix load is done')
    info = whos('leadFieldThr');
    sizeGB = info.bytes/(1000^3);
    disp(['Lead Field matrix is ' num2str(sizeGB) ' GB'])
    x = toc;
    disp(['This took ' num2str(x/60) ' minutes'])
    disp(num2str(size(leadFieldThr)))
    disp(num2str(sum(sum(leadFieldThr))))

    % for this patient, this source, this ROI, and this threshold,
    %   select configurations of interest and get contact associations
    
    % for each secondary patient, open transferred config in main patient
    %   note that we are dealing with all configurations transferred into a
    %   single patient for a single instance of this script. the main
    %   patient refers to which patient coordinate system we are in.
    pi2 = 1;
    for patID2 = [2,3,7,19,20,24,25,26,27,30,31,33]
        % get contactInds of interest for this patient
        configIndsPat2 = find(contactLocs.field==patID2);
        % get specific config of interest
        load(strcat(dataHomePath,'CrossPatConfTest/Data/solnCLabels_',num2str(patID2),'.mat'),'labels')
        configContactIndsUn = [];
        for k = 1:size(labels,1)
            if labels{k,5} == sourceNum && labels{k,4} == ROI_s && labels{k,6} == thrPrioritys(cNum)
                configContactIndsUn = labels{k,2}; % select unique contact inds in this patient field
                break
            end
        end
        if isempty(configContactIndsUn)
            disp("We didn't find this case!")
        end
        fullInds = configIndsPat2(configContactIndsUn);
        thisContactLocsPat1 = contactLocsPat1.node(fullInds,:);
        
        % get contact-to-electrode association
        % use same method as implant config cToEAssoc gen so we don't have
        % to import the full precomputed cToE assoc matrix and we can
        % guarantee that we are using the correct configurations
        %
        plotT = false;
        cToEAssoc = getSearchElectrodeAssociation(thisContactLocsPat1,plotT);
        %
        % check validity of this configuration in this patient
        % Values of 1 mean valid / passing for a certain criteria. Values of 0 mean INVALID aka failure of that criteria.
        % only have midline check and sulci intersect test
        % do this afterwards, actually!
        % validityConfigCodes = checkConfigValid(LH_white, RH_white, thisContactLocsPat1,cToEAssoc,midlinePlane,sulciPoints,plotT);
        %
        numEs = size(cToEAssoc,1);
        disp(strcat("We found ",num2str(numEs)," electrodes in this configuration from ",num2str(length(fullInds))," contacts"))

        % get rec dipoles by electrodes
        disp(strcat("We found ",num2str(numEs)," electrodes in this configuration from ",num2str(length(fullInds))," contacts"))

        % get rec dipoles by electrodes
        recDipolesbyE = false(size(cToEAssoc,1),size(leadFieldThr,2));
        % an electrode records a dipole if at least 2 contacts record 
        for i = 1:numEs
            recDipolesbyE(i,:) = sum(leadFieldThr(fullInds(cToEAssoc(i,:)),:),1) >= 2;
        end
        
        % get perc rec area from rec dipoles
        % both weighted and not weighted by area
        percRecNoWeight = zeros(numEs,1);
        percRecAreaWeight = zeros(numEs,1);
        for j = 1:numEs
            percRec = sum(recDipolesbyE(1:j,dipolesROI),1);
            percRec(percRec>1) = 1;
            percRecAW = sum(percRec.*cortex_areas(dipolesROI)',2)/sum(cortex_areas(dipolesROI));
            percRec = sum(percRec,2)/length(dipolesROI);
            percRecNoWeight(j) = percRec;
            percRecAreaWeight(j) = percRecAW;
        end
        
        % store recDipolesByE
        recDipolePercDat{pi2,(cNum-1)*4+1} = recDipolesbyE;
        % store perc Rec no area weight
        recDipolePercDat{pi2,(cNum-1)*4+2} = percRecNoWeight;
        % store perc Rec area weight
        recDipolePercDat{pi2,(cNum-1)*4+3} = percRecAreaWeight;
        % store validity of this config in this patient
        % moved this to separate script
        % recDipolePercDat{pi2,(cNum-1)*4+4} = validityConfigCodes;
        
        pi2 = pi2 + 1;
    end
    
end

% save recDipole & recPerc data
%   for this patient, ROI, source Ind, and for ALL thr
if ROI_i == 1
    save(strcat(dataHomePath,'CrossPatConfTest/Data/recDipolePercDat_LTL_Pat',num2str(patientID),'_',tys,'.mat'),'recDipolePercDat')
else
    save(strcat(dataHomePath,'CrossPatConfTest/Data/recDipolePercDat_LH_Pat',num2str(patientID),'_',tys,'.mat'),'recDipolePercDat')
end
    
end


function cToE_imp = getSearchElectrodeAssociation(configContactLocs,plotT)
    % get electrode-to-contact association matrix
    % by clustering contact locations. Assume all contacts for one
    % electrode are adjacent in the implantedELocations matrix.
    % also make visualization with different colors to check.
    % DO NOT offset all of these contact numbering and electrode numbering to
    % after the generated electrodes and contacts. Use this only with the
    % clipped lead field, with data only corresponding to these contacts
    
    if size(configContactLocs,1) ~= 3
        configContactLocs = configContactLocs';
    end
    dists = sum(diff(configContactLocs,1,2).^2,1);
    numEs = 0;
    eStartAndEnds = [];
    currCDist = dists(1);
    currCStart = 1;
    for i = 1:length(dists)
        if abs(dists(i) - currCDist) > 20
            numEs = numEs + 1;
            eStartAndEnds = [eStartAndEnds;[currCStart,i]];
            currCStart = i+1;
            currCDist = dists(i+1);
        end
    end
    numEs = numEs + 1;
    eStartAndEnds = [eStartAndEnds;[currCStart,i+1]];
    
    % get cToE_imp logical matrix
    E = numEs;
    % C = size(leadFieldThr,1);
    %cToE = zeros(E,C,'logical');
    is = [];
    js = [];
    for i = 1:E
        is = [is;i*ones(eStartAndEnds(i,2)-eStartAndEnds(i,1)+1,1)]; % electrode indices
        js = [js;[eStartAndEnds(i,1):eStartAndEnds(i,2)]']; % contact indices
    end
    vs = true(length(js),1);
    size(is)
    size(js)
    size(vs)

    cToE_imp = sparse(double(is),double(js),vs);
    
    % plot contact locs and save
    %f = figure('visible','off');
    if plotT
        figure(1)
        clf
        hold on
        % plot each set of contacts in another color
        for i = 1:E
            scatter3(configContactLocs(1,eStartAndEnds(i,1):eStartAndEnds(i,2)),configContactLocs(2,eStartAndEnds(i,1):eStartAndEnds(i,2)),configContactLocs(3,eStartAndEnds(i,1):eStartAndEnds(i,2)),'filled','DisplayName',strcat("Electrode ",num2str(i)))
        end
        legend()
        title(strcat("Configuration with Electrode Clustering "),"FontSize",14)
    end
%     saveas(f,strcat(dataHomePath,patientDir,'/Test/implantedLocsWithEClusters'),'jpg');
%     saveas(f,strcat(dataHomePath,patientDir,'/Test/implantedLocsWithEClusters'),'fig');
    % openfig('Patient_7/Test/inputSurfaces.fig','new','visible')
   
end
