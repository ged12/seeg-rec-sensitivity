# Cross Patient Configuration Analysis
__Figure 4c&d__

With this code, we determined if patient-specific optimization is necessary for determining configurations with high recording sensitivity that are safe (avoid sulci surfaces). 
We transferred the optimized configurations for each patient into all other patients, and calculated the recording sensitivity of each optimized and transferred configuration. By visualizing the relative recording sensitivity of each configuration between the optimized and transferred cases, we can show how much worse the configurations perform in other patients.

This code requires electrode locations and optimized configurations generated in the MainPipeline sections, and head models generated in the HeadModelGen. 
This code is designed to run on a SLURM-based computing cluster (Duke Computing Cluster). 

## Software and dependencies:
- matlabR2019a
- SLURM cluster (with ≥ 400 CPUs total, ≥ 22 CPUs/node and ≥ 200 GB / node). Data storage location with at least 3 TB availability. 
- SCIRun-5.0-beta.Y 
- Python version ___X___ with packages: nil earn, nibble, bumpy, (sys, os, subprocess). (HMAuto Anaconda3 environment)

## Running process:
1. Configure all scripts for your cluster storage system, making sure to update all paths of software, data, and scripts. 
    * The system is designed for a main /home directory to store main scripts and main data, and secondary /work directory to store intermediate scripts and intermediate high-memory data. 

2. Get contact locations for all selected configurations for each patient (est. runtime < 10 minutes)
    * Cluster-configured
    * Run `runGetConfF.slurm` - SLURM script to run `getPatConfFieldCrossPat.m` on the computing cluster
    * Inputs: optimized configurations for all 12 patients LH and LTL ROIs, for all 9 source types and 6 threshold-priority values
        * storage location: '{dataWorkPath} {patientDir} /OptSolutions /visDataConfigs_singleMap_cis {sourceTypeString} .mat'

3. Transfer contact locations across patients and concatenate to full contactLocs field for each patient, using FSL in Python.
    * Local and cluster 
    * Scripts: Run all of these 
        * convertMatToTxt.m - convert all patient contact location fields to .txt files for FSL 
        * MasterCrossMapping.slurm - SLURM script to run CrossMapping.py 
            *  CrossMapping.py - use FSL's FLIRT function 6 degrees of freedom and a mutual information cost function to map contact locations for each patient into other all patients
        * convertTxtToMat.m - convert transferred configurations to .mat files and concatenate all sets of transferred configurations into one field for each patient basis

4. Simulate Voltages at all contact locations and calculate recording sensitivity for all optimized and transferred configurations
    * Cluster-configured    
    * Main SLURM script: monitorCrossAn.slurm
	1.  Initialize data storage directories and create patient-specific scripts 
		* mainCrossPatPrep.m
	2. Make and execute SCIRun networks to generate LeadField
		* makeAndRunNets.slurm
		* CompressFS_crossAn.m
		* InputGeneral/PatientForwardDipoleAllAutomation.srn5
		* findMissing.m - to find missing forward solutions (columns of the lead field matrices) if needed
	3. Combine forward solutions into full LF matrices, threshold by disc voltage values, and make into Patch LFs 		createLFfromFSs.slurm
		* createLF_crossAn.m
	4. Calculate recordable dipoles and RS for each configuration (optimized and transferred) for each patient 
		* precompRecAreaAllElectrodes.slurm
		* precompRecAreaAllElectrodes.m

5. Check validity (sulci intersections and midline-crossing events) of transferred configurations
    * Local or Cluster
    * Script: reCheckConfigValid.m 

6. Plot and analyze data
	* Run Local
    * Script: Figure4_plot.m
    * Outputs:
        * Figures/Figure4c&d.fig & .pdf
		* SourceData/Figure4c&d.xls
        * SourceData/Figure4c&d_validity.xls











