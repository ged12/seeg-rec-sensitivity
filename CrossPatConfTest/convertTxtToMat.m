% convert .mat fields to .txt files with columns X, Y, Z separated by a
% space, and rows of coordinates separated by a new line

% for each patient
% convert txt files of transformed coordinates to a X by 3 array
% concatenate coords from all patients to each patient

% for each patient destination
for patientID_dest = [2,3,7,19,20,24,25,26,27,30,31,33]
    
    patientDir = strcat("Patient_",num2str(patientID_dest));
    if ~exist(patientDir,'dir')
        mkdir(patientDir)
    end
    
    contactLocs.node = [];
    contactLocs.field = [];
    % load all patient origins
    for patientID_org = [2,3,7,19,20,24,25,26,27,30,31,33]
        fileName = strcat('ContactLocsCross/Patient',num2str(patientID_org),'_Patient',num2str(patientID_dest),'_mapping_NewContactLocs.txt');
        fileID = fopen(fileName,'r');
        
        lines = readlines(fileName);
        theseContacts = zeros(length(lines)-1,3);
        for i = 2:length(lines)
            thisCoord = split(lines{i});
            thisCoord = [str2double(thisCoord{1}),str2double(thisCoord{2}),str2double(thisCoord{3})];
            theseContacts(i-1,:) = thisCoord;
        end
        contactLocs.node = [contactLocs.node;theseContacts];
        contactLocs.field = [contactLocs.field;patientID_org*ones(length(theseContacts),1)]; % contactLocs.field value gives patientID of source patient
        
    end
    save(strcat(patientDir,'/contactLocs.mat'),'contactLocs')

end


function lines = readlines(fileName)
    % Opens the file
    fid = fopen(fileName,'rt');

    maxl = -1; % read whole file
    lines = textscan(fid,'%s',maxl,'delimiter', '\n', 'whitespace','');
    if ~isempty(lines)
        lines = lines{1};
    end
    % Close file
    fclose(fid);
end