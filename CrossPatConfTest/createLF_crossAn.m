% combine FSs into LF
% createLF
% 10/28/21

% FS are already scaled by area (NOT by cdmd), and in int16 compressed form
% concatenate, make into patchLF (mult by patchAll), threshold, and save

% input patch Area in mm^2
% discVoltThresh is 100µV
%function createLF(patchArea, discVoltThresh)

% 1/19/22 create 3 thresholded LFs for each of the source types

% 10/3/22 remove integer compression thresholding. using 16-bit floats
% instead

function createLF_crossAn(sourceNum)

tic
% $$ UPDATE PARAMETERS $$
cdmds = [1, 1, 0.2, 0.2]; %nAm/mm^2
patchAreas = [5, 9.5, 5, 9.5]; % cm^2
dataWorkPath = '/work/ged12/Optimize_sEEG_implant/';
dataHomePath = '/hpc/group/wmglab/ged12/cluster/Optimize_sEEG_implant/';
patientID = 1; 

patientDir = ['Patient_' num2str(patientID)];

cdmd = cdmds(sourceNum) % nAm/mm^2
patchArea = patchAreas(sourceNum) % cm^2

tys = strcat(num2str(patchArea),"cm2_",num2str(cdmd),"cdmd")

% get total num dipoles
load(strcat(dataWorkPath,patientDir,"/HeadModel/lh_areas.mat"), 'lh_areas')
load(strcat(dataWorkPath,patientDir,"/HeadModel/rh_areas.mat"), 'rh_areas')
numDipoles = length(lh_areas) + length(rh_areas);
disp(strcat("There are ",num2str(numDipoles)," dipoles"))
toc

% get contact Locs
load(strcat(dataHomePath,'CrossPatConfTest/',patientDir,'/contactLocs.mat'),'contactLocs')

tic
% open all compressed FSs, assemble into LF, and delete FSs
% open first FS to get size of LF


% set up parallelization 
numCPUs = feature('numCores'); % Get number of CPUs available
pc_storage_dir = fullfile('pc_temp_storage',getenv('SLURM_JOB_ID'));    % assign JobStorageLocation based on id of SLURM job that called this function   
mkdir(pc_storage_dir);
pc = parcluster('local');   
pc.JobStorageLocation = pc_storage_dir;
fprintf('Number of CPUs requested = %g\n',numCPUs);
poolobj = parpool(pc,numCPUs-1); % initialize pool of workers, i.e. CPUs in this node to assign tasks, leave 1 CPU to handle the overhead

disp(['numCpus= ' num2str(numCPUs)])

% open all FSs and put into lead field matrix
disp(strcat("Creating a compressed lead field matrix with ",num2str(length(contactLocs.node))," rows and ",num2str(numDipoles)," columns"))
FSdir = strcat(dataWorkPath,patientDir,'/CrossAn/LeadField/FSs_C1'); %% ONLY ONE COMPRESSED FS 
leadField = zeros(length(contactLocs.node), numDipoles, 'int16');  % make sure length is same as num contactLocs
lFRows = length(contactLocs.node);
parfor (i = 1:numDipoles, numCPUs-1)
    lFRows;
    filename = strcat(FSdir,'/HeadDipoleVoltageSampledAtRecLoc_C1_',num2str(i),'.mat');
    a = load(filename, 'scirunmatrix');
    try
        leadField(:,i) = a.scirunmatrix;
    catch
       disp(strcat("ERROR adding FS #",num2str(i),". LH #rows is ",num2str(lFRows)," but FS length is",num2str(length(a.scirunmatrix))))
    end
    %delete(filename)
end
tic

tic
% turn into patch LF
info = whos('leadField');
sizeGB = info.bytes/(1024^3);
disp(['Lead Field matrix is ' num2str(sizeGB) ' GB'])

% open patchAll matrix. 39954x39954 sparse matrix of all patches of
% specificed area.
% areas = [0.1,[0.5:0.5:9.5]]*100;
load(strcat(dataWorkPath,patientDir,'/HeadModel/PatchAllMatrices/PatchAllSt_',num2str(patchArea),'cm2.mat'),'patchAll');
% load(strcat(dataWorkPath,patientDir,'/HeadModel/PatchAllMatrices/PatchAllSt_',num2str(find((areas==patchArea)==1)),'.mat'),'patchAll');

info = whos('patchAll');
sizeGB = info.bytes/(1024^3);
disp(strcat('PatchAll matrix is ',num2str(sizeGB),' GB'))
toc
% don't scale by area. area already added in compressFS step

% new method B: for loop with manual lin comps bc patchAll is so sparse
%   and leadField is integers
%   This should take less than or about 1 min. Compare to 3.3 hours with
%   sigle LF mult by full patchAll matrix !!!
tic
patchLeadField = zeros(size(leadField),'int16');
for i = 1:size(leadField,2)
    dipoles = patchAll(:,i)==1;
    patchLeadField(:,i) = sum(leadField(:,dipoles),2)*cdmd;
end
leadField = [];
size(patchLeadField)
info = whos('patchLeadField');
sizeGB = info.bytes/(1024^3);
disp(strcat('patchLeadField matrix is ',num2str(sizeGB),' GB'))
toc

% threshold patch lead field w 3 thrs and save
discVoltThreshs = [200, 500, 1000]; % µV
cNumStrs = ["C1","C2","C3"];

for cNum = 1:3
    discVoltThresh = discVoltThreshs(cNum) % µV
    tic
    leadFieldThr = abs(patchLeadField) >= discVoltThresh; 
    info = whos('leadFieldThr');
    sizeGB = info.bytes/(1024^3);
    disp(['leadFieldThr matrix is ' num2str(sizeGB) ' GB'])
    toc
    disp(num2str(size(leadFieldThr)))

    % save thresholded patch LF 
    save(strcat(dataWorkPath,patientDir,'/CrossAn/LeadField/PatchLeadField_RecLocs',cNumStrs(cNum),'_thr_',tys,'.mat'),'leadFieldThr','-v7.3') % leadFieldThr
end

%clean up to end parallelization
delete(poolobj);
rmdir(pc_storage_dir,'s'); % delete parfor temporary files
quit;


end


