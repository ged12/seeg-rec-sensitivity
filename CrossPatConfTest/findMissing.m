% Grace Dessert
% 10/22/22
% find missing files in LF

function findMissing(patientID)

filePath = strcat('/work/ged12/Optimize_sEEG_implant/Patient_',num2str(patientID),'/CrossAn/LeadField/');


% find C1 compressed missing files
missing = [];
disp("C1")
for i = 1:40100
    filename = strcat(filePath,'FSs_C1/HeadDipoleVoltageSampledAtRecLoc_C1_',num2str(i),'.mat');
    if ~isfile(filename)
        missing = [missing,strcat(",",num2str(i))];
    end
end
disp(join(missing,""));

return

% find C2 compressed missing files
missing = [];
disp("C2")
for i = 1:40100
    filename = strcat(filePath,'FSs_C2/HeadDipoleVoltageSampledAtRecLoc_C2_',num2str(i),'.mat');
    if ~isfile(filename)
        missing = [missing,strcat(",",num2str(i))];
    end
end
disp(join(missing,""));


% find C3 compressed missing files
missing = [];
disp("C3")
for i = 1:40100
    filename = strcat(filePath,'FSs_C3/HeadDipoleVoltageSampledAtRecLoc_C3_',num2str(i),'.mat');
    if ~isfile(filename)
        missing = [missing,strcat(",",num2str(i))];
    end
end
disp(join(missing,""));



end




