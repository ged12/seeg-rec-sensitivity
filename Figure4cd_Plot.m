% Plot Figure 4cd and create Source Data files
% Grace Dessert

%{
Figure 4: Necessity for patient-specific optimization. 
A-B) Average recording sensitivity across number of electrodes for optimized 
LTL (A) and LH (B) configurations in 12 patients. Shading shows standard deviation. 
C-D) Reduction in recording sensitivity for 11 transferred configurations 
compared to the optimized configuration in each patient at a 500 µV threshold. 
The number of electrodes included in each configuration is the minimum number 
that yielded ≥ 75% recording sensitivity in the optimized configuration. 
Configurations were optimized for LTL ROIs (C) and LH ROIs (D). 
%}

%% variables

% make sure these are correct
dataHomePath = '';

% global vars
ROIs = ["LTL","LH"];
patientIDs = [2,3,7,19,20,24,25,26,27,30,31,33];
cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2

% use mean source type (10 cm^2 patches, 0.465 nAm/mm^2 dipole moment density)
sourceInd = 4;
tys = strcat(num2str(patchAreas(sourceInd)),"cm2_",num2str(cdmds(sourceInd)),"cdmd");


%% c-d. Percent error in RS for transferred configurations across patients
thrI = 2; % 500 µV threshold
errOrRaw = true; % true == percent error; false == raw difference in RS

for ROI_i = 1:2 % 1==LTL, 2==LH
    f = plotBoxConfigFollow(errOrRaw,dataHomePath,ROI_i,tys,thrI,patientIDs,ROIs)
end



function f = plotBoxConfigFollow(errOrRaw,dataHomePath,ROI_i,tys,thrI,patientIDs,ROIs)

f = figure(4)
clf
hold on

% we're comparing rec sensitivity of configurations placed into all patients
% pick same number of electrodes for each configuration, 
% choose number of electrodes to be first w ≥ 75% mapping of matched case

xs = zeros(11,12); % 12 configs, 11 other pats
xsW1 = zeros(12,12); % 12 configs, 12 pats
colors = parula(13);

configNumEs = zeros(12,1);
configNumEsmatchPercVal = zeros(12,1);
patI1 = 1;
% for each patient, get num electrodes at ≥ 75% mapping for matched case
%   and mapping percentage
% note: NO weighting of rec perc by area!
for patientID = patientIDs
    % open data
    if ROI_i == 1
        load(strcat(dataHomePath,'RawData/CrossPatConfTest/recDipolePercDat_LTL_Pat',num2str(patientID),'_',tys,'.mat'),'recDipolePercDat')
    else
        load(strcat(dataHomePath,'RawData/CrossPatConfTest/recDipolePercDat_LH_Pat',num2str(patientID),'_',tys,'.mat'),'recDipolePercDat')
    end
    if isempty(recDipolePercDat{patI1,(thrI-1)*4+2})
        patI1 = patI1 + 1;
        continue
    end
    tempRec = [0;recDipolePercDat{patI1,(thrI-1)*4+2}];
    numECheck = find(tempRec>0.75,1);
    if isempty(numECheck)
        numECheck = length(tempRec);
    end
    matchPercVal = tempRec(numECheck);
    configNumEs(patI1) = numECheck;
    configNumEsmatchPercVal(patI1) = matchPercVal;
    patI1 = patI1 + 1;
end

% get all data of map perc error
patI1 = 1;
for patientID = patientIDs
    % open data
    if ROI_i == 1
        load(strcat(dataHomePath,'RawData/CrossPatConfTest/recDipolePercDat_LTL_Pat',num2str(patientID),'_',tys,'.mat'),'recDipolePercDat')
    else
        load(strcat(dataHomePath,'RawData/CrossPatConfTest/recDipolePercDat_LH_Pat',num2str(patientID),'_',tys,'.mat'),'recDipolePercDat')
    end
    
    percs = zeros(12,32);
    validity = zeros(12,2);
    weightByArea = false;
    for i = 1:12
        if weightByArea
            tempRec = [0;recDipolePercDat{i,(thrI-1)*4+3}];
            percs(i,1:length(tempRec)) = tempRec;
            percs(i,1+length(tempRec):end) = tempRec(end);
        else
            tempRec = [0;recDipolePercDat{i,(thrI-1)*4+2}];
            percs(i,1:length(tempRec)) = tempRec;
            percs(i,1+length(tempRec):end) = tempRec(end);
        end
        % Values of 1 mean valid / passing for a certain criteria. Values of 0 mean INVALID aka failure of that criteria.
            % only have midline check and sulci intersect test
        validity(i,:) = recDipolePercDat{i,(thrI-1)*4+4};
        if ~errOrRaw
            xsW1(patI1,i) = (percs(i,configNumEs(i)))*100; % - configNumEsmatchPercVal(i))/configNumEsmatchPercVal(i) * 100;
        else
            xsW1(patI1,i) = (percs(i,configNumEs(i)) - configNumEsmatchPercVal(i))/configNumEsmatchPercVal(i) * 100;
        end
    end
    
    indsNotP1 = [1:12];
    indsNotP1(patI1) = [];

    patI1 = patI1 + 1;
end

% plot raw dat
%colorsT = colors(1:12,:);
colors = [[1,0,0];[0,0,0]]; % red and black. 0 means fail = red, 1 means pass = black
if ROI_i == 1
    load(strcat(dataHomePath,'RawData/CrossPatConfTest/validitiyCodesLTL_thr',num2str(thrI),'.mat'),'validitiyCodes');
else
    load(strcat(dataHomePath,'RawData/CrossPatConfTest/validitiyCodesLH_thr',num2str(thrI),'.mat'),'validitiyCodes');
end
for patI1 = 1:12
    indsNotP2 = [1:12];
    if ~errOrRaw
        plot(indsNotP2(patI1),xsW1(patI1,(patI1)),'X','color',colorsT(patI1,:),'MarkerSize',14)
    end
    indsNotP2(patI1) = [];
    xs(:,patI1) = xsW1(indsNotP2,patI1)';
    posOff = (rand(length(indsNotP2),1)-0.5)/5;
    colorsT = colors(validitiyCodes(patI1,indsNotP2)+1,:);
    scatter(indsNotP2+posOff',xsW1(patI1,indsNotP2),20,colorsT,'filled')
end
% disp stats on validitiyCodes (remember, 1s indicate passing, 0 indicates failing sulci intersection tests)
validitiyCodes(eye(12)==1) = 0;
failPerc = sum(sum(validitiyCodes))/(12*12-12) * 100;
disp(strcat(num2str(failPerc)," % of transformed configurations fail sulci intersection tests")) 

% get average values
disp("The median values for each config are:")
for i = 1:12
    median(xs(:,i))
end
disp("The mean value across all configs are:")
mean(mean(xs))
disp("The median value across all configs are:")
median(reshape(xs,1,[]))

disp("The std value across all configs are:")
std(reshape(xs,1,[]))

disp("All values with positive error are:")
[piX,piY] = find(xs>0);
for i = 1:length(piX)
    disp(num2str(xs(piX(i),piY(i))))
end

% plot box
boxplot(xs,'color','k','Symbol','k+')
grid on

set(findobj(gca,'type','line'),'linew',1)
ax = gca; 
ax.FontSize = 20; 
ax.XGrid = "off";

%plot([-5:20],repmat([0],length([-5:20]),1),'k','LineWidth',0.8)
xlim([0,13])
% ytic ks([-100:20:20])

xticklabels(patientIDs)
xlabel('Configuration')
yticks([-80:20:0])
ylim([-80,10])
if errOrRaw
    ylabel(strcat("Percent error for ",ROIs(ROI_i)))
else
    ylabel('Percent recordability')
end

if ROI_i == 1
    savefig(f,strcat(dataHomePath,"Figures/Figure4c.fig"));
    fileName = strcat(dataHomePath,'Figures/Figure4c.pdf');
else
    savefig(f,strcat(dataHomePath,"Figures/Figure4d.fig"));
    fileName = strcat(dataHomePath,'Figures/Figure4d.pdf');
end
print(f,fileName,'-bestfit','-dpdf');

% create data for Source Data file
% A Column corresponds to the same configuration transferred into all patients
matWrite = vertcat(string(patientIDs),xs);
if ROI_i == 1
    writematrix(matWrite,'SourceData/Figure4c.xls')
else
    writematrix(matWrite,'SourceData/Figure4d.xls')
end
validitiyCodesDiagRemoved = zeros(11,12);
for i = 1:12
    notThis = [1:12];
    notThis(i) = [];
    validitiyCodesDiagRemoved(:,i) = validitiyCodes(notThis,i);
end
matWrite = vertcat(string(patientIDs),validitiyCodesDiagRemoved);
if ROI_i == 1
    writematrix(matWrite,'SourceData/Figure4c_validity.xls')
else
    writematrix(matWrite,'SourceData/Figure4d_validity.xls')
end


end


