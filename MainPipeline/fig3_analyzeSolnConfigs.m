% fig3_analyzeSolnConfis
% Grace Dessert
% 10/24/2022

% global vars
cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
dataWorkPath = '';
patientIDs = [2,3,7,19,20,24,25,26,27,30,31,33];
costFnStrs = ["200_1000_500","500_1000_200","1000_500_200"];
degMapStrs = ["single mapping at 200 µV threshold", "single mapping at 500 µV threshold", "single mapping at 1000 µV threshold"];
ROIs = ["clinicianROI","LTL","LH"]; % array 


%% save or open cis data files
% ********* RN, pat 24 and 31 will auto select clinROI surfs for LTL ******
% ********* if plotting or analyzing general search LTLs, need to change this *******
ROI_i = 2;
sourceInd = 4;
redoCalc = false;
weightByArea = false;
calcByDips = false;

%saveTag = "noWeight_compOpt";

saveTag = "noWeight_normTempROIs_retestSulcInts";
dat = saveOrOpenCisSearch(ROI_i, weightByArea, sourceInd,calcByDips,redoCalc, patientIDs,degMapStrs,costFnStrs,dataWorkPath,patchAreas,cdmds,ROIs,saveTag);

%% conv plots raw search soln data
thrI = 1;
tys_cA = strcat(num2str(patchAreas(sourceInd)),"cm2_",num2str(cdmds(sourceInd)),"cdmd");
saveFigTag = ''; % if empty, no save
f = plotRawSearchData(dat,thrI,saveFigTag,tys_cA,ROIs,ROI_i,patientIDs,degMapStrs,costFnStrs);

%% conv plots search mean and range over three thresholds

f = plotMeansSearchData(dat,saveTag,tys_cA,ROIs,ROI_i,patientIDs,degMapStrs,costFnStrs);

%% load or gen and save implant data with box plots and table
ROI_i = 1;
weightByArea = false;
calcByDips = false;

for sourceInd = 4
    redoCalc = false;
    [data_imp,data_imp_ROIAreas] = saveOrOpenCisImplant(redoCalc, calcByDips, ROI_i,sourceInd,patchAreas,cdmds,dataWorkPath,weightByArea,patientIDs,degMapStrs,costFnStrs);
    
    redoCalc = false;
    % ** MAKE SURE SAVED DATA IS CORRECT FOR useImpTLROIs CASE ** 

    plotArea = false;
    plotPerE = false;
    saveAndDispTable = true;
    saveTag = "noWeight_compOpt_retestSulcInts";
    dat = saveOrOpenCisSearch(ROI_i,weightByArea, sourceInd,calcByDips,redoCalc, patientIDs,degMapStrs,costFnStrs,dataWorkPath,patchAreas,cdmds,ROIs,saveTag);
    %[~,T] = boxOptImpData(plotArea, plotPerE, saveAndDispTable, ROI_i, dat, patientIDs, data_imp, data_imp_ROIAreas, sourceInd, saveTag);
    
    percD = false;
    f = numElectrodesCompareBox(saveAndDispTable, ROI_i, dat, patientIDs, data_imp, data_imp_ROIAreas, sourceInd, saveTag, percD);
    
end

%% plot config plots raw imp data
thrI = 2;
tys_cA = strcat(num2str(patchAreas(sourceInd)),"cm2_",num2str(cdmds(sourceInd)),"cdmd");

saveFigTag = "";
f = plotRawImpData(data_imp,thrI,saveFigTag,tys_cA,ROI_i,patientIDs,degMapStrs,costFnStrs);


%% boxplot of diff across all cases!

ROI_i = 1;
weightByArea = false;
calcByDips = false;
ROIs(2) = "TL";

saveFigTag = "noWeight_compOpt_retestSulcInts";
f = boxDiffAllCases(ROI_i, patientIDs,patchAreas,cdmds,dataWorkPath,degMapStrs,costFnStrs,ROIs,saveFigTag)

%%
f = numElectrodesCompareBox(saveAndDispTable, ROI_i, dat, patientIDs, data_imp, data_imp_ROIAreas, sourceInd, saveTag, percD);

    
function f = numElectrodesCompareBox(saveAndDispTable, ROI_i, dat, patientIDs, data_imp, data_imp_ROIAreas, sourceInd, saveTag, percD)

% plot num electrodes implanted configurations
% and num electrodes to get ≥ final RS with opt configurations
% and plot difference

f = figure(4)
clf
hold on
colorsPoints = parula(length(patientIDs)+1);
colors = parula(4);

dat_mapPerc = cell(3,1);
dat_mapPerc_imp = cell(3,1);

for t = 1:3
    dat_mapPerc{t} = zeros(length(patientIDs),1);
    dat_mapPerc_imp{t} = zeros(length(patientIDs),1);
end

patI = 1;
positions = (rand(length(patientIDs),1)-0.5)/20;

patIs = [];

for patientID = patientIDs
    
    if isempty(data_imp{patI,t})
        patI = patI + 1;
        continue
    end
    for t = 1:3
        numEsImp = length(data_imp{patI,t});
        RSImp = data_imp{patI,t}(end);
        numEsOpt = find(dat{patI,t}>=RSImp,1);
        if isempty(numEsOpt)
            disp("opt never gets to same RS?? for patient: ")
            disp(patI)
            numEsOpt = 31;
        end
        dat_mapPerc{t}(patI) = numEsOpt;
        dat_mapPerc_imp{t}(patI) = numEsImp;
        
        % plot pat 25 with special symbols 
        if patientIDs(patI) == 25
            patI25 = patI;
            scatter(positions(patI)+t,numEsOpt,100,colorsPoints(patI,:),'X','LineWidth',2,'HandleVisibility','off');
            plot(positions(patI)+[0,0.5]+t,[numEsOpt,numEsImp],'.--','LineWidth',3,'color',colorsPoints(patI,:),'DisplayName',strcat("Patient ",num2str(patientID)),'LineWidth',1,'HandleVisibility','off');
            scatter(positions(patI)+0.5+t,numEsImp,100,colorsPoints(patI,:),'X','LineWidth',2,'HandleVisibility','off');
        else
            scatter(positions(patI)+t,numEsOpt,25,colorsPoints(patI,:),'filled','HandleVisibility','off');
            plot(positions(patI)+[0,0.5]+t,[numEsOpt,numEsImp],'.-','color',colorsPoints(patI,:),'DisplayName',strcat("Patient ",num2str(patientID)),'LineWidth',1,'HandleVisibility','off');
            scatter(positions(patI)+0.5+t,numEsImp,25,colorsPoints(patI,:),'filled','HandleVisibility','off');            
        end
        
        patIs = [patIs, patI];
    end
    patI = patI + 1;
end

% check normality of opt and imp groups for all 3 thrs
% use Shapiro-Wilk Test
normBool = true(3,2); % null hypothesis of normality 
for t = 1:3
    datOpt = dat_mapPerc{t};
    datOpt(datOpt==0) = [];
    datImp = dat_mapPerc_imp{t};
    datImp(datImp==0) = [];
    % opt configs
    Results=normalitytest(datOpt');
    pVal = Results(7,2); % Shapiro-Wilk Test
    % if p value is < 0.05, we reject the null hyp of normality
    if pVal < 0.05
        normBool(t,1) = false;
        disp(strcat("Data for thrI OPT of ",num2str(t)," is NOT normal"))
    end
    % imp configs
    Results=normalitytest(datImp');
    pVal = Results(7,2); % Shapiro-Wilk Test
    % if p value is < 0.05, we reject the null hyp of normality
    if pVal < 0.05
        normBool(t,2) = false;
        disp(strcat("Data for thrI IMP of ",num2str(t)," is NOT normal"))
    end
end
% do significance testing   
% for each case, if normal, do paired t test
pVals = zeros(3,1);
for t = 1:3
    if sum(normBool(t,:)) == 2 % both opt and imp normal
        datOpt = dat_mapPerc{t};
        datOpt(datOpt==0) = [];
        datImp = dat_mapPerc_imp{t};
        datImp(datImp==0) = [];
        [h,p] = ttest(datOpt,datImp);
        pVals(t) = p;
    else % if not normal, do Wilcoxon signed rank (paired version of Wilcoxon ranked sum test)
        disp(strcat("This case (t=",num2str(t),") is NOT normal! Do Wilcoxon signed rank test!"))
        datOpt = dat_mapPerc{t};
        datOpt(datOpt==0) = [];
        datImp = dat_mapPerc_imp{t};
        datImp(datImp==0) = [];
        p = signrank(datOpt,datImp);
        pVals(t) = p;
    end
end
disp(strcat("The p values for 3 thresholds are:"))
pVals

% all cases normal for sourceI = 4
%   EXCEPT Opt electrodes LTL thrI=3
% all Wilcoxon signed rank:
%   LTL: pVals = 0.0156, 0.0078, 0.0078
%   clinROI: pVals =  1.0e-03 * 0.4883, 0.4883, 0.4883
% all paired ttest:
%   LTL: pVals = 0.0058, 0.0004, 0.0013
%   clinROI: pVals = 1.0e-04 * 0.0264, 0.1723, 0.0072

% save raw data in table
if saveAndDispTable
    varNames = ["Search_200","Imp_200","Diff_200","Search_500","Imp_500","Diff_500","Search_1000","Imp_1000","Diff_1000"];
    rowNames = string(patientIDs);
    T = table(dat_mapPerc{1}, dat_mapPerc_imp{1}, -1*(dat_mapPerc_imp{1}-dat_mapPerc{1}),dat_mapPerc{2}, dat_mapPerc_imp{2}, -1*(dat_mapPerc_imp{2}-dat_mapPerc{2}), dat_mapPerc{3},dat_mapPerc_imp{3},-1*(dat_mapPerc_imp{3}-dat_mapPerc{3}),'VariableNames',varNames,'RowNames',rowNames);
    if ROI_i == 1
        save(strcat('PlotData/NUME_MapPercTable_ROI1_source',num2str(sourceInd),"_",saveTag,".mat"),'T')
    elseif ROI_i==2
        save(strcat('PlotData/NUME_MapPercTable_ROI2_source',num2str(sourceInd),"_",saveTag,".mat"),'T')
    else
        disp("WHY COMPARE AT LH ROI?? CHECK")
    end
    disp(T)
end
% plot
f.Position = [800 350 430 300];

for t = 1:3
    noDatPatInds = find(dat_mapPerc{t}==0 & dat_mapPerc_imp{t}==0);
    dat_mapPerc{t}(noDatPatInds) = [];
    dat_mapPerc_imp{t}(noDatPatInds) = [];
    boxplot([dat_mapPerc{t},dat_mapPerc_imp{t}],'Colors',[0,0,0;0.4,0.4,0.4],'Positions',[0,0.5]+t,'Widths',0.25) %'Colors',colors(3,:)) %,'PlotStyle','compact','Whisker',0,'Symbol','') %,'positions', positions)
end
patIs = unique(patIs);

plot([-1,0],[0,0],'k','LineWidth',1,'DisplayName','Optimized')
plot([-1,0],[0,0],'color',[0.4,0.4,0.4],'LineWidth',1,'DisplayName','Implanted')
plot([-1,0],[0,0],'r','LineWidth',1,'DisplayName','Difference')
plot([-1,0],[0,0],'x--','color',colorsPoints(7,:),'LineWidth',2,'MarkerSize',140,'DisplayName','Patient 25');
%legend('location','southwest')
% boxplot(dat_mapPerc_imp'*100,'Colors','k','Positions',[2]) %'Colors',colors(3,:)) %,'PlotStyle','compact','Whisker',0,'Symbol','') %,'positions', positions)

xticks([1:3]+0.5)
xlim([0.7,3.8])
xticklabels(["200 µV","500 µV", "1000 µV"])
ax = gca; 
ax.FontSize = 20; 
grid on


% diff box plot
diffs = cell(3,1);
for t = 1:3
    if percD
        diffs{t} = -1*(dat_mapPerc{t} - dat_mapPerc_imp{t})./dat_mapPerc_imp{t}*100; % # es that we save
    else
        diffs{t} = -1*(dat_mapPerc{t} - dat_mapPerc_imp{t}); % # es that we save
    end
end

% if plotting perc, remap to numE axis
if percD
    if ROI_i==1
        yMax = 20;
    else
        yMax = 12;
    end
    yticks([0:yMax/4:yMax])
    ylim([0,yMax])
end

positions = (rand(length(patientIDs),1)-0.5)/10;
%

% f = figure(5)
% clf
%
% hold on
% colorsPoints = parula(length(patientIDs)+1);
if percD
    yyaxis right
end
for t = 1:3
    for p = 1:length(diffs{t})
        if patIs(p) == find(patientIDs==25)
            scatter(positions(p)+t+0.22,diffs{t}(p),140,colorsPoints(patIs(p),:),'X','LineWidth',2,'HandleVisibility','off');
        else
            scatter(positions(p)+t+0.22,diffs{t}(p),50,colorsPoints(patIs(p),:),'*','HandleVisibility','off');
        end
    end
    boxplot(diffs{t},'Colors',['r'],'Positions',t+0.25,'Widths',0.2) %'Colors',colors(3,:)) %,'PlotStyle','compact','Whisker',0,'Symbol','') %,'positions', positions)
    % plot pat 25 special
end

if percD
    ax = gca;
    ax.YColor = 'r';
    yticks([0:100/4:100])
    ylim([0,100])
else
    ylim([0,max(dat_mapPerc_imp{1})+1])
end
xlim([0.7,3.8])
xticks([1:3]+0.25)
xlim([0.7,3.8])
xticklabels(["200 µV","500 µV", "1000 µV"])
ax = gca; 
ax.FontSize = 20; 
grid on

set(findobj(gca,'type','line'),'linew',1)


xticklabels(["200 µV","500 µV", "1000 µV"])

% ylim([0,35])
ax = gca; 
ax.FontSize = 20; 
grid on
xlabel("Voltage threshold")

if percD
    yyaxis left
end
if ROI_i==1
    fileName = strcat('fig3_ROI_NUME_BoxPercRec_source',num2str(sourceInd),"_",saveTag,'.pdf');
    ylabel("# Electrodes for clinician-ROI")
elseif ROI_i==2
    fileName = strcat('fig3_LTL_NUME_BoxPercRec_source',num2str(sourceInd),"_",saveTag,'.pdf');
    ylabel("# Electrodes for TL")
end
print(f,fileName,'-bestfit','-dpdf');


end


function f = boxDiffAllCases(ROI_i, patientIDs,patchAreas,cdmds,dataWorkPath,degMapStrs,costFnStrs,ROIs,saveFigTag)
    
    diffs = zeros(length(patientIDs),9,3);
    
    calcByDips = false;
    weightByArea = false;
    
    for sourceInd = 1:9
        
        dat_mapPerc = cell(3,1);
        dat_mapPerc_imp = cell(3,1);

        for t = 1:3
            dat_mapPerc{t} = zeros(length(patientIDs),1);
            dat_mapPerc_imp{t} = zeros(length(patientIDs),1);
        end

        redoCalc = false;
        [data_imp,~] = saveOrOpenCisImplant(redoCalc, calcByDips, ROI_i,sourceInd,patchAreas,cdmds,dataWorkPath,weightByArea,patientIDs,degMapStrs,costFnStrs);
        redoCalc = false;
        
        dat = saveOrOpenCisSearch(ROI_i, weightByArea, sourceInd,calcByDips,redoCalc, patientIDs,degMapStrs,costFnStrs,dataWorkPath,patchAreas,cdmds,ROIs,saveFigTag);
        
        for t = 1:3
            for patI = 1:12
                if isempty(data_imp{patI,t})
                    continue
                end
                dat_mapPerc_imp{t}(patI) = data_imp{patI,t}(end);
                numEs = length(data_imp{patI,t});
                dat_mapPerc{t}(patI) = dat{patI,t}(min(numEs,length(dat{patI,t})));
            end
            diffs(:,sourceInd,t) = (dat_mapPerc{t} - dat_mapPerc_imp{t})*100;
        end
    end
    % reorder sources
    cdmds = [0.465, 0.77, 0.16, 0.465, 0.77, 0.16, 0.465, 0.77, 0.16]; %nAm/mm^2
    patchAreas = [6, 6, 6, 10, 10, 10, 20, 20, 20]; % cm^2
    order = [3,6,9,1,4,7,2,5,8];
    diffs = diffs(:,order,:) ;
    
    % remove patients with no data (for LTL)
    noDatPats = find(sum(sum(diffs,3),2)==0);
    diffs(noDatPats,:,:)=[];
    
    thrs = [200,500,1000];
    positions = [1,2,3,4.25,5.25,6.25,7.5,8.5,9.5];
    
    colors = parula(4);
    
    f = figure(4)
    clf
    hold on
    positionsScat = (rand(size(diffs,1),1)-0.5)/2;
    set(f, 'Position', [100, 100, 460, 500]);   
    
    tiledlayout(3,1)
    for t = 1:3
        nexttile
        hold on
        for s = 1:9
            scatter(positions(s)+positionsScat,diffs(:,s,t),10,[0.1,0.1,0.1],'HandleVisibility','off')
        end
        boxplot(diffs(:,:,t),'Positions',positions,'ColorGroup',[1,2,3,1,2,3,1,2,3],'Colors',colors(1:3,:),'Symbol','') 

        xticks([2,5.25,8.5])
        xticklabels(["0.16","0.465","0.77"])
        set(findobj(gca,'type','line'),'linew',1.5)
        ax = gca; 
        ax.FontSize = 25; 
        grid on
        ylim([0,100])
        yticks([0,25,50,75,100])
        
        if t == 1
            plot([-1,0],[0,0],'color',colors(1,:),'LineWidth',2.5,'DisplayName','6 cm2')
            plot([-1,0],[0,0],'color',colors(2,:),'LineWidth',2.5,'DisplayName','10 cm2')
            plot([-1,0],[0,0],'color',colors(3,:),'LineWidth',2.5,'DisplayName','20 cm2')
            legend('location','northeast')
            xlim([min(positions)-1,max(positions)+1])
            %hsp1 = get(gca, 'Position');
        end
        if t == 2
            ylabel(strcat("Percent recordability increase for ",ROIs(ROI_i)))
            %hsp2 = get(gca, 'Position') ;
            %set(gca, 'Position', [hsp2(1:3)  hsp1(4)])    % Use 2*(2,1,1) Height for (2,1,2)
        end
        if t == 3
            xlabel("Dipole moment density (nAm/mm2)")
            %hsp3 = get(gca, 'Position') ;
            %set(gca, 'Position', [hsp3(1:3)  hsp1(4)])    % Use 2*(2,1,1) Height for (2,1,2)
        end
    end


    if ROI_i==1
        fileName = strcat('fig3_allSourceIncrease_ROI.pdf');
    elseif ROI_i==2
        fileName = strcat('fig3_allSourceIncrease_LTL.pdf');
    end
    print(f,fileName,'-bestfit','-dpdf');

    
end


function [f,T] = boxOptImpData(plotArea, plotPerE, saveAndDispTable, ROI_i, dat, patientIDs, data_imp, data_imp_ROIAreas, sourceInd, saveTag)
f = figure(4)
clf
hold on
colorsPoints = parula(length(patientIDs)+1);
colors = parula(4);

% scatter final rec percentage for all search solutions and implanted configurations
% and prepare data for boxplot
dat_mapPerc = cell(3,1);
dat_mapPerc_imp = cell(3,1);

for t = 1:3
    dat_mapPerc{t} = zeros(length(patientIDs),1);
    dat_mapPerc_imp{t} = zeros(length(patientIDs),1);
end

patI = 1;
positions = (rand(length(patientIDs),1)-0.5)/20;

patIs = [];

for patientID = patientIDs
    
    if isempty(data_imp{patI,t})
        patI = patI + 1;
        continue
    end
    for t = 1:3
        numEs = length(data_imp{patI,t});
        if plotPerE
            dat_mapPerc{t}(patI) = (dat{patI,t}(numEs)) / numEs;
%             end
            dat_mapPerc_imp{t}(patI) = data_imp{patI,t}(end) / length(data_imp{patI,t});
        else
%             if ROI_i == 2 && ( patientID == 24 || patientID == 31  )
%                 dat_mapPerc{t}(patI) = 1-datROI{patI,t}(numEs);
%             else
            dat_mapPerc{t}(patI) = dat{patI,t}(min(numEs,length(dat{patI,t})));
%             end
            dat_mapPerc_imp{t}(patI) = data_imp{patI,t}(end);
        end
        if plotArea
            areaROI = data_imp_ROIAreas{patI,t};
            dat_mapPerc_imp{t}(patI) = dat_mapPerc_imp{t}(patI)*areaROI/10000;
            dat_mapPerc{t}(patI) = dat_mapPerc{t}(patI)*areaROI/10000;
        end
        if patientIDs(patI) == 25
            patI25 = patI;
            scatter(positions(patI)+t,dat_mapPerc{t}(patI)*100,100,colorsPoints(patI,:),'X','LineWidth',2,'HandleVisibility','off');
            if t == 1
                plot(positions(patI)+[0,0.5]+t,[dat_mapPerc{t}(patI)*100,dat_mapPerc_imp{t}(patI)*100],'.--','LineWidth',3,'color',colorsPoints(patI,:),'DisplayName',strcat("Patient ",num2str(patientID)),'LineWidth',1,'HandleVisibility','off');
            else
                plot(positions(patI)+[0,0.5]+t,[dat_mapPerc{t}(patI)*100,dat_mapPerc_imp{t}(patI)*100],'.--','LineWidth',3,'color',colorsPoints(patI,:),'DisplayName',strcat("Patient ",num2str(patientID)),'LineWidth',1,'HandleVisibility','off');
            end
            scatter(positions(patI)+0.5+t,dat_mapPerc_imp{t}(patI)*100,100,colorsPoints(patI,:),'X','LineWidth',2,'HandleVisibility','off');
        else
            scatter(positions(patI)+t,dat_mapPerc{t}(patI)*100,25,colorsPoints(patI,:),'filled','HandleVisibility','off');
            if t == 1
                plot(positions(patI)+[0,0.5]+t,[dat_mapPerc{t}(patI)*100,dat_mapPerc_imp{t}(patI)*100],'.-','color',colorsPoints(patI,:),'DisplayName',strcat("Patient ",num2str(patientID)),'LineWidth',1,'HandleVisibility','off');
            else
                plot(positions(patI)+[0,0.5]+t,[dat_mapPerc{t}(patI)*100,dat_mapPerc_imp{t}(patI)*100],'.-','color',colorsPoints(patI,:),'DisplayName',strcat("Patient ",num2str(patientID)),'LineWidth',1,'HandleVisibility','off');
            end
            scatter(positions(patI)+0.5+t,dat_mapPerc_imp{t}(patI)*100,25,colorsPoints(patI,:),'filled','HandleVisibility','off');            
        end
        
        patIs = [patIs, patI];
    end
    patI = patI + 1;
end


% check normality of opt and imp groups for all 3 thrs
% use Shapiro-Wilk Test
normBool = true(3,2); % null hypothesis of normality 
for t = 1:3
    datOpt = dat_mapPerc{t};
    datOpt(datOpt==0) = [];
    datImp = dat_mapPerc_imp{t};
    datImp(datImp==0) = [];
    % opt configs
    Results=normalitytest(datOpt');
    pVal = Results(7,2); % Shapiro-Wilk Test
    % if p value is < 0.05, we reject the null hyp of normality
    if pVal < 0.05
        normBool(t,1) = false;
        disp(strcat("Data for thrI OPT of ",num2str(t)," is NOT normal"))
    end
    % imp configs
    Results=normalitytest(datImp');
    pVal = Results(7,2); % Shapiro-Wilk Test
    % if p value is < 0.05, we reject the null hyp of normality
    if pVal < 0.05
        normBool(t,2) = false;
        disp(strcat("Data for thrI IMP of ",num2str(t)," is NOT normal"))
    end
end
% do significance testing   
% for each case, if normal, do paired t test
pVals = zeros(3,1);
for t = 1:3
    if sum(normBool(t,:)) == 2 % both opt and imp normal
        datOpt = dat_mapPerc{t};
        datOpt(datOpt==0) = [];
        datImp = dat_mapPerc_imp{t};
        datImp(datImp==0) = [];
        [h,p] = ttest(datOpt,datImp);
        pVals(t) = p;
    else % if not normal, do Wilcoxon signed rank (paired version of Wilcoxon ranked sum test)
        disp(strcat("This case (t=",num2str(t),") is NOT normal! Do Wilcoxon signed rank test!"))
        datOpt = dat_mapPerc{t};
        datOpt(datOpt==0) = [];
        datImp = dat_mapPerc_imp{t};
        datImp(datImp==0) = [];
        p = signrank(datOpt,datImp);
        pVals(t) = p;
    end
end
disp(strcat("The p values for 3 thresholds are:"))
pVals

%%
% save raw data in table
if saveAndDispTable
    varNames = ["Search_200","Imp_200","Diff_200","Search_500","Imp_500","Diff_500","Search_1000","Imp_1000","Diff_1000"];
    rowNames = string(patientIDs);
    T = table(dat_mapPerc{1}, dat_mapPerc_imp{1}, -1*(dat_mapPerc_imp{1}-dat_mapPerc{1}),dat_mapPerc{2}, dat_mapPerc_imp{2}, -1*(dat_mapPerc_imp{2}-dat_mapPerc{2}), dat_mapPerc{3},dat_mapPerc_imp{3},-1*(dat_mapPerc_imp{3}-dat_mapPerc{3}),'VariableNames',varNames,'RowNames',rowNames);
    if ROI_i == 1
        save(strcat('PlotData/MapPercTable_ROI1_source',num2str(sourceInd),"_",saveTag,".mat"),'T')
    elseif ROI_i==2
        save(strcat('PlotData/MapPercTable_ROI2_source',num2str(sourceInd),"_",saveTag,".mat"),'T')
    else
        disp("WHY COMPARE AT LH ROI?? CHECK")
    end
    disp(T)
end
% plot
f.Position = [800 350 430 300];

for t = 1:3
    noDatPatInds = find(dat_mapPerc{t}==0 & dat_mapPerc_imp{t}==0);
    dat_mapPerc{t}(noDatPatInds) = [];
    dat_mapPerc_imp{t}(noDatPatInds) = [];
    boxplot([dat_mapPerc{t},dat_mapPerc_imp{t}]*100,'Colors',[0,0,0;0.4,0.4,0.4],'Positions',[0,0.5]+t,'Widths',0.25) %'Colors',colors(3,:)) %,'PlotStyle','compact','Whisker',0,'Symbol','') %,'positions', positions)
end
patIs = unique(patIs);

plot([-1,0],[0,0],'k','LineWidth',1,'DisplayName','Optimized')
plot([-1,0],[0,0],'color',[0.4,0.4,0.4],'LineWidth',1,'DisplayName','Implanted')
plot([-1,0],[0,0],'r','LineWidth',1,'DisplayName','Difference')
plot([-1,0],[0,0],'x--','color',colorsPoints(7,:),'LineWidth',2,'MarkerSize',140,'DisplayName','Patient 25');
%legend('location','southwest')
% boxplot(dat_mapPerc_imp'*100,'Colors','k','Positions',[2]) %'Colors',colors(3,:)) %,'PlotStyle','compact','Whisker',0,'Symbol','') %,'positions', positions)

xticks([1:3]+0.5)
xlim([0.7,3.8])
xticklabels(["200 µV","500 µV", "1000 µV"])
ylim([0,110])
ax = gca; 
ax.FontSize = 20; 
grid on

if plotArea 
    if plotPerE
        ylim([0,areaROI/numEs/100+10])
    else
        ylim([0,areaROI/100+10])
    end
else
    ylim([0,100])
end
%
% diff box plot
diffs = cell(3,1);
for t = 1:3
    diffs{t} = dat_mapPerc{t} - dat_mapPerc_imp{t}; % percent that we do better
end

positions = (rand(length(patientIDs),1)-0.5)/10;
%

% f = figure(5)
% clf
%
% hold on
% colorsPoints = parula(length(patientIDs)+1);
for t = 1:3
    for p = 1:length(diffs{t})
        if patIs(p) == find(patientIDs==25)
            scatter(positions(p)+t+0.22,diffs{t}(p)*100,140,colorsPoints(patIs(p),:),'X','LineWidth',2,'HandleVisibility','off');
        else
            scatter(positions(p)+t+0.22,diffs{t}(p)*100,50,colorsPoints(patIs(p),:),'*','HandleVisibility','off');
        end
    end
    boxplot(diffs{t}*100,'Colors',['r'],'Positions',t+0.25,'Widths',0.2) %'Colors',colors(3,:)) %,'PlotStyle','compact','Whisker',0,'Symbol','') %,'positions', positions)
    % plot pat 25 special
end


xlim([0.7,3.8])
% ylim([0,110])
xticks([1:3]+0.25)
xlim([0.7,3.8])
xticklabels(["200 µV","500 µV", "1000 µV"])
% ylim([0,110])
ax = gca; 
ax.FontSize = 20; 
grid on

set(findobj(gca,'type','line'),'linew',1)

if plotArea 
    if plotPerE
        ylim([0,areaROI/numEs/100+10])
    else
        ylim([0,areaROI/100+10])
    end
else
   ylim([0,100]) 
end

%
xticklabels(["200 µV","500 µV", "1000 µV"])

% ylim([0,35])
ax = gca; 
ax.FontSize = 20; 
grid on
xlabel("Voltage threshold")

if ROI_i==1
    fileName = strcat('fig3_ROI_BoxPercRec_source',num2str(sourceInd),"_",saveTag,'.pdf');
    ylabel("Recording sensitivity of ROI")
elseif ROI_i==2
    fileName = strcat('fig3_LTL_BoxPercRec_source',num2str(sourceInd),"_",saveTag,'.pdf');
    ylabel("Recording sensitivity of TL")
else
    fileName = strcat('fig3_LH_BoxPercRec_source',num2str(sourceInd),"_",saveTag,'.pdf');
    ylabel("Recording sensitivity of LH")
end
print(f,fileName,'-bestfit','-dpdf');

end

function f = plotRawSearchData(dat,thrI,saveFigTag,tys_cA,ROIs,ROI_i,patientIDs,degMapStrs,costFnStrs)
f = figure(1)
clf
hold on
% colorz = [0 0.4470 0.7410;0.8500 0.3250 0.0980;0.9290 0.6940 0.1250;0.4940 0.1840 0.5560;0.4660 0.6740 0.1880;0.3010 0.7450 0.9330;0.6350 0.0780 0.1840];
colorz = parula(size(dat,1)+1);

for i = 1:size(dat,1)
    if ~isempty(dat{i,thrI})
        percentROI = [0;dat{i,thrI}]*100;
        plot([0:length(percentROI)-1],percentROI,'LineWidth',2.5,'color',colorz(i,:),'DisplayName',strcat("Patient ",num2str(patientIDs(i))));
    end
end
legend()
% legend(case_IDs)
% legend('optimal: 200 µV', 'optimal: 500 µV', 'optimal: 1000 µV', 'implant: 200 µV', 'implant: 500 µV', 'implant: 1000 µV')
% legend('location','northeast')
xlim([0,30])
xlabel('Electrodes')
ylabel('Percent recordable')
ss = split(tys_cA, "_");
s = split(ss(1), "cm2");
sss = split(ss(2), "cdmd");
ylim([0,100])

caseID_disp = strcat(s(1)," cm2, ",sss(1)," nAm/mm2");

yticks([0:0.25:1]*100)
xticks([0:4:20])
grid on

ROI_string = ROIs(ROI_i);
if ROI_string == "clinicianROI"
    ROI_string = "clinical";
end
title([strcat("Mapping of ",ROI_string," ROI across electrode additions"),strcat(caseID_disp, ", ",degMapStrs(thrI),", cost:",replace(costFnStrs(thrI),"_"," "))])
ax = gca; 
ax.FontSize = 20; 
set(gcf,'position',[500,500,550,250])


fileName = strcat('fig3_',ROI_string,'ROIsearchRaw',num2str(thrI),'_',saveFigTag,'.pdf');
if ~isempty(saveFigTag)
    print(gcf,fileName,'-bestfit','-dpdf');
end
end

function dat = saveOrOpenCisSearch(ROI_i,weightByArea,sourceInd, calcByDips, redoCalc, patientIDs,degMapStrs,costFnStrs,dataWorkPath,patchAreas,cdmds,ROIs,saveTag)

fileName = strcat('PlotData/Search_ROI',num2str(ROI_i),'_source',num2str(sourceInd),"_",saveTag,".mat")

if redoCalc || ~isfile(fileName)
dat = cell(length(patientIDs),length(degMapStrs)); % patients, thrs, LTL mapping percs
patIDi = 1;

if calcByDips || weightByArea
    disp("Using rec dipoles to calc perc map")
end


if contains(saveTag,"compOpt")
    useImpTLROIs = true;
else
    useImpTLROIs = false;
end

for patientID = patientIDs
    patientDir = ['Patient_' num2str(patientID)]
    
    ROINameSearch = ROIs(ROI_i);
    ROI_s = ROI_i;
    if useImpTLROIs && ROINameSearch=="LTL" && (patientID == 24 || patientID == 31)
        ROINameSearch = "clinicianROI";
        ROI_s = find(ROIs=="clinicianROI");
    end
    
    load(strcat(dataWorkPath,patientDir,'/OptSolutions/ROIs.mat'),'patches')
    load(strcat(dataWorkPath,patientDir,'/HeadModel/cortex_areas.mat'),'cortex_areas')
    tys_cA = strcat(num2str(patchAreas(sourceInd)),"cm2_",num2str(cdmds(sourceInd)),"cdmd");
    load(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataConfigs_singleMap_cis',tys_cA,'.mat'),'visDataConfigs')
    
    dipolesROI = patches.dipoles{ROI_s};
   
    
    count = 1;
    for d = 1:length(degMapStrs)
        degMapStr = degMapStrs(d);
        for i = 1:length(visDataConfigs)
            caseID = visDataConfigs{i}.full_case_id;
            if contains(caseID,costFnStrs(d)) && contains(caseID,degMapStr)
                if contains(caseID,ROINameSearch)
                    if patientID == 25
                        x = 1
                    end
                    % if were analyzing TLs and pat is 24 or 31, need to use normal clinROI (not clinROI_broad)
                    if useImpTLROIs && ROIs(ROI_i)=="LTL" && ~isempty(dat{patIDi,count}) && ( patientID == 24 || patientID == 31  )
                        % for pats whos TL ROIs are 'clinROI' surface, dont
                        % pick the broad clinROI!
                        break
                    end
                    
                    if calcByDips || weightByArea
                        numEEE = length(visDataConfigs{i}.percMapROI);
                        datT = zeros(1,numEEE);
                        recD = visDataConfigs{i}.rec_tissue_by_E.field;
                        for j = 1:numEEE
                            percRec = sum(recD(1:j,dipolesROI),1);
                            percRec(percRec>1) = 1;
                            if weightByArea
                                percRec = sum(percRec.*cortex_areas(dipolesROI)',2)/sum(cortex_areas(dipolesROI));
                            else
                                percRec = sum(percRec,2)/length(dipolesROI);
                            end
                            datT(j) = percRec;
                        end
                    else
                        datT = visDataConfigs{i}.percMapROI;
                    end
                    dat{patIDi,count} = datT';
                    caseID
                end
            end
        end
        count = count + 1;
    end
    patIDi = patIDi + 1;
    
end

fileName = strcat('PlotData/Search_ROI',num2str(ROI_i),'_source',num2str(sourceInd),"_",saveTag,".mat");
save(fileName,'dat')

else
    fileName = strcat('PlotData/Search_ROI',num2str(ROI_i),'_source',num2str(sourceInd),"_",saveTag,".mat");
    load(fileName,'dat')
end

end

function f = plotMeansSearchData(dat,saveFigTag,tys_cA,ROIs,ROI_i,patientIDs,degMapStrs,costFnStrs)
f = figure(2)
clf
hold on
% colorz = [0 0.4470 0.7410;0.8500 0.3250 0.0980;0.9290 0.6940 0.1250;0.4940 0.1840 0.5560;0.4660 0.6740 0.1880;0.3010 0.7450 0.9330;0.6350 0.0780 0.1840];
colorz = parula(4);
colorz = lines(4);
colorz = colorz(2:4,:);

numEsto75PercRS = zeros(size(dat,1),3); % 12 patients, 3 thrs

dat_all = zeros(size(dat,1),32,3); % 12 patients, 32 distance inds, 3 thrs
for i = 1:size(dat,1) % for each patient
    for thrI = 1:3
        if dat{i,thrI}(end)>=0.75
            numEsto75PercRS(i,thrI) = find(dat{i,thrI}>=0.75,1);
        end
        dat_all(i,1:length(dat{i,thrI})+1,thrI) = [0;dat{i,thrI}];
        dat_all(i,length(dat{i,thrI})+2:31,thrI) = repmat(dat{i,thrI}(end),31-(length(dat{i,thrI})+1),1);
    end
end
% get mean and std of numEs to 75% RS
for thrI = 1:3
    numEsValid = numEsto75PercRS(:,thrI);
    numEsValid(numEsValid==0) = [];
    disp(strcat("for thr ",num2str(thrI)))
    disp(strcat("there are only ",num2str(length(numEsValid))," of ",num2str(size(dat,1))," patients that got to 75% rec at this thr"))
    disp(strcat("mean numEs is ",num2str(mean(numEsValid))))
    disp(strcat("std of numEs is ",num2str(std(numEsValid))))
end

means = zeros(31,3);
rangeHigh = zeros(31,3);
rangeLow = zeros(31,3);
for i = 1:31
    nonZeroDat1 = squeeze(dat_all(:,i,1));
    nonZeroDat2 = squeeze(dat_all(:,i,2));
    nonZeroDat3 = squeeze(dat_all(:,i,3));
    nonZeroDat1(nonZeroDat1==0) = [];
    nonZeroDat2(nonZeroDat2==0) = [];
    nonZeroDat3(nonZeroDat3==0) = [];
    if ~isempty(nonZeroDat1)
        means(i,1) = mean(nonZeroDat1);
        rangeHigh(i,1) = mean(nonZeroDat1)+std(nonZeroDat1);
        rangeLow(i,1) = mean(nonZeroDat1)-std(nonZeroDat1);
    end
    if ~isempty(nonZeroDat2)
        means(i,2) = mean(nonZeroDat2);
        rangeHigh(i,2) = means(i,2)+std(nonZeroDat2);
        rangeLow(i,2) = means(i,2)-std(nonZeroDat2);
    end
    if ~isempty(nonZeroDat3)
        means(i,3) = mean(nonZeroDat3);
        rangeHigh(i,3) = means(i,3)+std(nonZeroDat3);
        rangeLow(i,3) = means(i,3)-std(nonZeroDat3);
    end
end
rangeLow = rangeLow * 100;
rangeHigh = rangeHigh * 100;
means = means * 100;

% disp summary statistics
disp("to get ≥ 75% RS, you need:")
for thrI = 1:3
    numEsMean = find(means(:,thrI)>=75,1);
    disp(strcat(num2str(numEsMean)))
end

thrs = [200, 500, 1000];
for j = 1:3
    coord_up = [[0:29,30.1]',rangeHigh(:,j)];
    coord_low = [[0:29,30.1]',rangeLow(:,j)];
    coord_combine = [coord_up;flipud(coord_low)];
    fill(coord_combine(:,1),coord_combine(:,2),colorz(j,:),'EdgeColor',colorz(j,:),'FaceAlpha',0.4,'HandleVisibility','off')

    plot([0:30],means(:,j),'LineWidth',2.5,'color',colorz(j,:),'DisplayName',strcat(num2str(thrs(j))," µV threshold"));
%     plot([0:30],rangeHigh(:,j),'LineWidth',2.5,'color',colorz(j,:),'HandleVisibility','off');
%     plot([0:30],rangeLow(:,j),'LineWidth',2.5,'color',colorz(j,:),'HandleVisibility','off');
end
% legend()
% legend(case_IDs)
% legend('optimal: 200 µV', 'optimal: 500 µV', 'optimal: 1000 µV', 'implant: 200 µV', 'implant: 500 µV', 'implant: 1000 µV')
legend('location','southeast')
xlim([0,14])
xlabel('Electrodes')
ylabel('Recording Sensitivity')
ss = split(tys_cA, "_");
s = split(ss(1), "cm2");
sss = split(ss(2), "cdmd");
ylim([0,1.01*100])
grid on
caseID_disp = strcat(s(1)," cm2, ",sss(1)," nAm/mm2");
yticks([0:0.25:1]*100)
xticks([0:4:30])
ROI_string = ROIs(ROI_i);
if ROI_string == "clinicianROI"
    ROI_string = "clinical";
end
% title([strcat("Mapping of ",ROI_string," ROI across electrode additions with std"),strcat(caseID_disp, ", ",degMapStrs(thrI),", cost:",replace(costFnStrs(thrI),"_"," "))])
ax = gca; 
ax.FontSize = 18; 
% set(gcf,'position',[500,500,550,250])
ylim([0,100])

if ROI_i ==1
    ylabel('Recording Sensitivity for ROI')
    fileName = 'fig3_ROIsearchMeanStd3Thr';
elseif ROI_i == 2 
    ylabel('Recording Sensitivity for LTL')
    fileName = 'fig3_LTLsearchMeanStd3Thr';
    xlim([0,30])
    xticks([0:4:30])
else
    ylabel('Recording Sensitivity for LH')
    fileName = 'fig3_LHsearchMeanStd3Thr';
    xlim([0,30])
    xticks([0:4:30])
end
fileName = strcat(fileName,saveFigTag,'.pdf');
if ~isempty(saveFigTag)
    print(gcf,fileName,'-bestfit','-dpdf');
end
end


function [data_imp,data_imp_ROIAreas] = saveOrOpenCisImplant(redoCalc, calcByDips, ROI_i,sourceInd,patchAreas,cdmds,dataWorkPath,weightByArea,patientIDs,degMapStrs,costFnStrs)
ROIs_full = ["clinicianROI","LTL","LH"];
ROI_string = ROIs_full(ROI_i);
data_imp = cell(length(patientIDs),3);
data_imp_ROIAreas = cell(length(patientIDs),3);

% valid imp electrodes for ROIs are already selected 
%{
numEsToIncludeImpConfigs = cell(length(patientIDs),1); % which electrodes are in TL ROI for each patient implanted configuration
if ROI_i == 2
    % patientIDs = [2,3,7,19,20,24,25,26,27,30,31,33];
    % numEsToIncludeImpConfigs = [7,0,0,0,9,6,10,4,8];
    % [] means do not include in TL analysis
    numEsToIncludeImpConfigs{1} = [1:6]; % pat 2
    numEsToIncludeImpConfigs{2} = []; % pat 3
    numEsToIncludeImpConfigs{3} = []; % pat 7
    numEsToIncludeImpConfigs{4} = []; % pat 19
    numEsToIncludeImpConfigs{5} = [1:9]; % pat 20
    numEsToIncludeImpConfigs{6} = [1:6]; % pat 24
    numEsToIncludeImpConfigs{7} = [1:7]; % pat 25
    numEsToIncludeImpConfigs{8} = [1:4]; % pat 26
    numEsToIncludeImpConfigs{9} = [1:10]; %[1:8,10,12]; % pat 27 
    numEsToIncludeImpConfigs{10} = [1:8]; % pat 30
    numEsToIncludeImpConfigs{11} = [1:6]; % pat 31 
    numEsToIncludeImpConfigs{12} = []; % pat 33
else
    % 0 means include all
    numEsToIncludeImpConfigs{1} = 0; % pat 2
    numEsToIncludeImpConfigs{2} = 0; % pat 3
    numEsToIncludeImpConfigs{3} = 0; % pat 7
    numEsToIncludeImpConfigs{4} = 0; % pat 19
    numEsToIncludeImpConfigs{5} = 0; % pat 20
    numEsToIncludeImpConfigs{6} = [1:12]; % pat 24
    numEsToIncludeImpConfigs{7} = [1:14]; % pat 25
    numEsToIncludeImpConfigs{8} = [1:13]; % pat 26
    numEsToIncludeImpConfigs{9} = [1:10]; % pat 27 LTL
    numEsToIncludeImpConfigs{10} = 0; % pat 30 
    numEsToIncludeImpConfigs{11} = 0; % pat 31 clinROI_broad for all 15
    numEsToIncludeImpConfigs{12} = 0; % pat 33 clinROI all 14
end
%}

fileName = strcat('PlotData/ImpArea_ROI',num2str(ROI_i),'_source',num2str(sourceInd),".mat");

clinBroad = true;

if redoCalc || exist(fileName,'file')==0
patIDi = 1;
for patientID = patientIDs
    patientDir = ['Patient_' num2str(patientID)]
    
    load(strcat(dataWorkPath,patientDir,'/OptSolutions/ROIs.mat'),'patches')
    load(strcat(dataWorkPath,patientDir,'/HeadModel/cortex_areas.mat'),'cortex_areas')
    
    % four patients have no implanted TL ROI
    if ROI_i == 2 && (patientID == 3 || patientID == 7 || patientID == 19 || patientID == 33)
        patIDi = patIDi + 1
        continue
    end
        
    tys_cA = strcat(num2str(patchAreas(sourceInd)),"cm2_",num2str(cdmds(sourceInd)),"cdmd");
    load(strcat(dataWorkPath,patientDir,'/OptSolutions/visDataImplantConfig_singleMap_crossAnalyze',tys_cA,'.mat'),'visDataImplantConfig')
    
    % two patients have RTL ROIs, so choose clinicianROI that corresponds
    if ROI_i == 2 && ( patientID == 24 || patientID == 31  )
        ROI_string = "clinicianROI";
        dipolesROI = patches.dipoles{1};
    else
        ROI_string = ROIs_full(ROI_i);
        dipolesROI = patches.dipoles{ROI_i};
    end
    clinBI = 0;
    for roii = 1:length(patches.name)
        if patches.name{roii} == "clinicianROI_broad"
            clinBI = roii;
            break
        end
    end
    if ROI_i == 1 && clinBroad && clinBI>0
        ROI_string = "clinicianROI_broad"
        dipolesROI = patches.dipoles{clinBI};
    end
    if patientID == 25
        x = 1
    end
    
    for i = 1:length(visDataImplantConfig)
        if isempty(visDataImplantConfig{i})
            continue
        end
        caseID = visDataImplantConfig{i}.imp_case_id;
        for thrI = 1:3
            if isempty(data_imp{patIDi,thrI}) &&  contains(caseID,tys_cA) && contains(caseID,costFnStrs(thrI)) && contains(caseID,ROI_string)
                if calcByDips || weightByArea
                    dipRecs = visDataImplantConfig{i}.rec_tissueDat_imp_allThr{5};
                    eIndsImp = 1:size(dipRecs,1);
                    data_imp{patIDi,thrI} = zeros(length(eIndsImp),1);
                    for j = 1:length(eIndsImp)
                        percRec = sum(dipRecs(eIndsImp(1:j),dipolesROI),1);
                        percRec(percRec>1) = 1;
                        if weightByArea
                            percRec = sum(percRec.*cortex_areas(dipolesROI)',2)/sum(cortex_areas(dipolesROI));
                        else
                            percRec = sum(percRec,2)/length(dipolesROI);
                        end
                        data_imp{patIDi,thrI}(j) = percRec;
                    end
                else
                    data_imp{patIDi,thrI} = visDataImplantConfig{i}.percMapROI_imp_allThr{5};
                end
                data_imp_ROIAreas{patIDi,thrI} = sum(cortex_areas(dipolesROI));
            end
        end
    end
    patIDi = patIDi + 1
end
    save(strcat('PlotData/Imp_ROI',num2str(ROI_i),'_source',num2str(sourceInd),".mat"),'data_imp')
    save(strcat('PlotData/ImpArea_ROI',num2str(ROI_i),'_source',num2str(sourceInd),".mat"),'data_imp_ROIAreas')
else
    load(strcat('PlotData/Imp_ROI',num2str(ROI_i),'_source',num2str(sourceInd),".mat"),'data_imp')
    load(strcat('PlotData/ImpArea_ROI',num2str(ROI_i),'_source',num2str(sourceInd),".mat"),'data_imp_ROIAreas')
end

end


function f = plotRawImpData(data_imp,thrI,saveFigTag,tys_cA,ROI_i,patientIDs,degMapStrs,costFnStrs)
% plot config plots raw imp data
dat = data_imp;

f = figure(3)
clf
hold on
% colorz = [0 0.4470 0.7410;0.8500 0.3250 0.0980;0.9290 0.6940 0.1250;0.4940 0.1840 0.5560;0.4660 0.6740 0.1880;0.3010 0.7450 0.9330;0.6350 0.0780 0.1840];
colorz = parula(size(dat,1)+1);

for i = 1:size(dat,1)
    if ~isempty(dat{i,thrI})
        if isempty(data_imp{i,1})
            continue
        end
%         diffs = [dat{i,thrI}(1);diff(dat{i,thrI})];
%         cumPerc = cumsum(diffs(numEsToIncludeImpConfigs{i}));
        percentROI = [0;dat{i,thrI}]*100;
%         percentROI = 1-percentROI;
        plot([0:length(percentROI)-1],percentROI,'LineWidth',2.5,'color',colorz(i,:),'DisplayName',strcat("Patient ",num2str(patientIDs(i))));
    end
end
legend()
% legend(case_IDs)
% legend('optimal: 200 µV', 'optimal: 500 µV', 'optimal: 1000 µV', 'implant: 200 µV', 'implant: 500 µV', 'implant: 1000 µV')
% legend('location','northeast')

if ROI_i == 2
    xlim([0,16])
    xticks([0:2:16])
else
    xlim([0,20])
    xticks([0:4:20])
end

xlabel('Electrodes')
ylabel('Percent recordability')
ss = split(tys_cA, "_");
s = split(ss(1), "cm2");
sss = split(ss(2), "cdmd");
ylim([0,100])
yticks([0:0.25:1]*100)
grid on
caseID_disp = strcat(s(1)," cm2, ",sss(1)," nAm/mm2");

title([strcat("Implanted configurations recording sensitivity"),strcat(caseID_disp, ", ",degMapStrs(thrI),", cost:",replace(costFnStrs(thrI),"_"," "))])

ax = gca; 
ax.FontSize = 20; 
% set(gcf,'position',[500,500,550,250])

if ROI_i == 2
    fileName = strcat('fig3_LTLimplant_thrI',num2str(thrI),'.pdf');
else
    fileName = strcat('fig3_ROIimplant_thrI',num2str(thrI),'.pdf');
end

fileName = strcat(fileName,saveFigTag);

if ~isempty(saveFigTag)
    print(gcf,fileName,'-bestfit','-dpdf');
end

end
