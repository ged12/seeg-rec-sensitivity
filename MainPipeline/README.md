# Main Pipeline
__Figure 3, Figure 4ab, Supp Table 2, and Supp Figure 2&3__

Given segmented head models, we calculated the set of valid electrode trajectories for each patient. We then simulated realisic epileptic sources at every location on the cortex using Finite Element methods, resulting in the patch lead field matrix. Using this and the set of electrode trajectories, we calculated the recordable sources for each electrode and then ran an optimization method to determine the best electrode configurations for each region of interest (ROI), source type, threshold, and patient. We quantified the recording sensitivity of implanted configurations, and compared it to that of the optimzied configurations. 

We developed demo versions of this code (running instructions below) to exhibit main functionality and data generation. Running the full version requires a SLURM-based computing cluster (Duke Computing Cluster) and previous completion of the Head Model Generation section. 


## Software and hardware requirements:
#### Demos
- Any standard computer with at least 4 GB RAM. 
- matlabR2019a
- Optional: SCIRun-5.0-beta.Y 

#### Full Version
- SLURM cluster (with ≥ 400 CPUs total, ≥ 22 CPUs/node and ≥ 200 GB / node). Data storage location with at least 3 TB availability. 
- matlabR2019a
- SCIRun-5.0-beta.Y 
- Python version 3.9.13 with packages: 
    * nilearn, nibabel, numpy, skimage
- MATLAB funtions (all included in the codebase) (see `dependencyLicenses.md`)
    * affine_fit.m, refinepatch.m, stlPlot.m, plotcube.m, normalitytest.m, intriangulation.m

## Running the demos:
The demos can be run on any standard computer, and do not require prior running of the Head Model pipeline. There are two demos for the main pipeline section: Demo 1) is for preparation of the lead field generation. This demo corresponds to step 3.2 in the full pipeline running directions. Lead field generation cannot be performed locally, so that step is not included in the demos. Demo 2) is for optimization of electrode configurations (step 5 in full pipeline). Since the full pipeline has very large memory and runtime requirements, the demos are designed to run on a small subset of data to demonstrate functionality. 

#### Demo 1: Preparation for lead field generation
This demo runs on patient 25 data saved in the `/Patient_25/` directory. All demo scripts are located in the `/DemoScripts/` directory, and all demo outputs are saved in the `/Patient_25/DemoOut/` directory. 

1. Set desired parameters for source type in the `Patient_25.config` file, keeping a single source type for runtime considerations. 
2. Set the MATLAB paths in the script `bash initOpt_sEEG_demo.sh`
3. Submit the monitoring script `bash initOpt_sEEG_demo.sh`, which performs the following steps.
    * Create parameter-specific scripts in `Patient_25/DemoScripts/` (`writeParameters_demo.m`)
    * Get contact locations for recordable radius study (`getStContactLocs_demo.m`) 
    * Perform all surface precomputations (`LeadFieldMain_demo.m`)
    * Precompute valid electrode locs, and get all valid contact locs
            * `precomputeValidElectrodeLocs_demo.m` 
    * Outputs: all located in `/DemoOut/`
        * Parameter-specific scripts in `/DemoScripts/` 
            * this includes custom matlab scripts for the demos and Python scripts to make SCIRun networks for LF gen
        * Selected contacts for recordable radius section
        * Midline plane, sulci surfaces, skin insertion locations files
        * Valid electrode set information
        * Occurrences of collisions between all pairs of electrodes
4. Visualize inputs and outputs
    * All figures were saved with visibility off. You must open them using the following syntax:
        * `openfig('filename.fig','visibile')`
    * Open `Patient_25/DemoOut/inputSurfaces.fig` on matlab to visualize input surfaces (skin, brain, skull, cortex) and implanted contact locations.
    * Open `Patient_25/DemoOut/midlinePlane.fig` on matlab to visualize calculated midline
    * Open `Patient_25/DemoOut/electrodeVis_kRand_Subset.fig` on matlab to visualize a random subset of the computed valid electrode locations
    * Open `Patient_25/DemoOut/allSelContacts.fig` on matlab to visualize the selected contact locations for the recordable radius study 
    * Open `Patient_25/DemoOut/LH_sulciPlot.fig` and `RH_sulciPlot.fig` on matlab to visualize the computed sulci surfaces

#### Demo 2: Optimization
This demo runs on patient 25 data saved in the `/Patient_25` directory. Demo 1 must be performed before this demo.
1. Submit the monitoring script `bash initSecond_demo.sh`, which performs the following steps.
    * Get ROI surfaces for optimization (est. runtime < 1 minutes)
        * `getROIs_demo.m`
    * Run optimization
        * `TreeSearch_demo.m` -> `nextBest_search_demo.m`
    * Outputs:
        * ROI surfaces
        * Optimized configurations
2. Visualize outputs
    * Run `visLoadForSCIRun_demo.m` on matlab, selecting ROI of interest for configuration visualization
    * Optional: Open `visConfigRec_demo.srn5` on SCIRun to visualize configurations and recording strength across the ROI
    * Expected Outputs:

![Demo Output Patient25 LTL](/Patient_25/DemoOut/Demo_Pat25LTL.jpg "Demo Output Patient25 LTL")
![Demo Output Patient25 LTL](/Patient_25/DemoOut/Pat25_ImpConfigLTL.png "Demo Output Patient25 LTL")
![Demo Output Patient25 LTL](/Patient_25/DemoOut/Pat25_OptConfigLTL.png "Demo Output Patient25 LTL")


## Running the full pipeline:
The pipeline is integrated using bash monitoring scripts that track job completion with the SLURM scheduler. With the exception of a few manual steps listed, two monitoring scripts should perform all of the following steps in order for one patient, though significant changes will likely need to be made to reconfigure the pipeline for other cluster systems (filepaths, data storage system, memory and CPU allocations).

0. Run full Head Model Generation section

1. Configure all scripts in `/MainPipeline`, `/ContentScripts`, and `/BashScripts` for your cluster storage system, making sure to update all paths to software, data, and scripts 
    * The system is designed for a main /home directory to store main scripts and main data, and secondary /work directory to store intermediate scripts and intermediate high-memory data. 
    * Update `Patient_{ID}.config` files with filepaths and running parameters.
    * For each patient, custom copies of all scripts in `/ContentScripts`, and `/BashScripts` are created in patient-specific folders to set parameters and allow parallel execution.

2. Manually prepare head model surfaces
    * Refine or decimate each of the following surfaces until the number of points is in the target range, and save them in the same location with the following names. Do this for each patient.
        * All input surfaces are located in the `Patient_{i}/Inputs/` directory.
        * We used and suggest the software _MeshLab_ to perform these operations. (https://www.meshlab.net/)
        * Make sure that there are no self-interesecting faces, no non-manifold edges, and no holes.
            * SkinMeshFinal.stl w #POINTS between 1500 and 2000 points -> SkinFinal.stl
            * OutSkullMesh.stl w #POINTS between 340000 and 380000 -> SkullFinal.stl
            * BrainMesh.stl w #POINTS between 2000 and 3000 -> BrainMesh.stl
            * lh_brain_final.stl with #FACES between 19950 and 20050 -> lh_white_20k.stl
            * rh_brain_final.stl with #FACES between 19950 and 20050 -> rh_white_20k.stl
    * Smooth the decimated cortex surfaces for each patient aggressively 
        * We used and suggest the software _MeshLab_ to perform these operations. (https://www.meshlab.net/)
            * To smooth agressively, we executed the `apply_coord_hc_laplacian_smoothing()` function on each surface 100 times.
        * Make sure that there are no self-interesecting faces, no non-manifold edges, and no holes.
        * Use the following file names
            * The smoothed `lh_white_20k.stl` surface should be named `lh_white_20k_SUPERSMOOTH.stl`
            * The smoothed `rh_white_20k.stl` surface should be named `rh_white_20k_SUPERSMOOTH.stl`

3. Run the main monitoring script by submitting `initOpt_sEEG.slurm` on a SLURM-based cluster system. 
    * This script initializes the main pipeline which executes all of the following steps in order. Make sure the run configurations and paths are correct.
    * `initOpt_sEEG.slurm` executes the main monitoring script, `runOpt_sEEG.slurm`, which submits each following scripts in order, waiting for completion by searching for stored JobIDs.
    1. Generate all scripts with patient specific file paths and parameters using `.config` files.
        * `initializePipelineRun.m` (est. runtime < 5 minutes)
            * `writeParameters.m`
    2. Prepare electrodes and head model
        * Get contact locs for recordable radius study (est. runtime < 1 minute)
            * `getStContactLocs.m` 
        * Perform all surface precomputations (est. runtime < 1 hour)
            * `LeadFieldMain.m` 
                * `refinepatch.m` (see `dependencyLicenses.md`)
                * `affine_fit.m` (as subfunction) (see `dependencyLicenses.md`)
                * `boundingVolumeHierarchyConstruction.m`
                * `boundingVolumeHierarchy.m`
                * `getMidline.m`
                * `getSkinInsertionLocs.m`
                * `WriteAutoPythonScripts_Singular.m`
                * `getSulciSurface.m`
        * Precompute valid electrode locs, and get all valid contact locs (est. runtime < 3 hours)
            * `getELocs.slurm` -> `precomputeValidElectrodeLocs.m` 
                * `boundingVolumeHierarchy.m`, `boundingVolumeHierarchyConstruction.m`
                * subfunctions: `stlPlot.m` (see `dependencyLicenses.md`), `plotcube.m` (see `dependencyLicenses.md`)
        * Build head tet mesh (est. runtime < 8 hours)
            * `prepareLFCompNetwork.py`
            * `makeHeadTetMesh.slurm` -> `prepareLFCompNetwork.srn5`
        * Make patch models for all elements on cortex (est. runtime < 1 hour)
            * `makePatchAllMatrices.slurm` -> `makePatchAllMatrices.m`
        * Calculate intersections between all pairs of electrodes (est. runtime < 3 hours)
            * `electrodeCollisionsCompute.slurm` -> `eCollCompute_lines.m`
    3. Calculate lead field matrix
        * `makeAndRunNets.slurm` (est. runtime < 12 hours)
            * `DipoleScript{i}.py`
            * `PatientForwardDipoleAllAutomation{i}.srn5`
            * `CompressFS.m`
        * `createLFfromFSs.slurm` -> `createLF.m` (est. runtime < 3 hours)
    4. Calculate recordable dipoles for each electrode (est. runtime < 1 hour)
        * `precompRecAreaAllElectrodes.slurm` -> `precompRecAreaAllElectrodes.m`

4. Manually define temporal lobe and clinician-defined ROI surfaces
    * `InputGeneral/getTemporalLobe.srn5` to create `Patient_{i}/Inputs/leftTemporalLobe.stl`
    * `InputGeneral/clinicalROI_generation.srn5` to create `Patient_{i}/Inputs/clinicianROI.stl`

5. Run optimization (est. runtime < 5 hours)
    * Submit secondary monitoring script `initSecond.slurm` -> `runSecond.slurm`, which submits the following scripts. Make sure the run configurations and paths are correct.
        * Optimize electrode configurations 
            * Get ROI surfaces for optimization
                * `getROIs.slurm` -> `getROIs.m`
            * Run optimization
                * `NBS.slurm` -> `TreeSearch.m` -> `nextBest_search.m`
        * Auto analyze and visualize results
            * `analyzeAndPrepVis.slurm`
                * `visConfigSolution.m`
                * `sliceCisData.m`

6. Finish analysis and plot data
    * `Figure3_plot.m`
        * `normalitytest.m` (see `dependencyLicenses.md`)
        * `InputGeneral/visConfigRec.srn5`
    * `Figure4ab_plot.m`
    * `FigureS2_plot.m`
    * `FigureS3_plot.m`
        * `InputGeneral/visConfigRec.srn5`
    * Outputs:
        * Figures/
        * SourceData/










