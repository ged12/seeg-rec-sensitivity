#!/bin/bash

# go out to main repo directory
cd ..

date
echo "****************************************************************"
echo Welcome to sEEG OPT Version 1 DEMO 2
echo Developed by Grace Dessert, dessertgrace@gmail.com  with Brandon Thio and Dr. Warren M Grill 
echo Duke University, Department of Biomedical Engineering
echo "****************************************************************"
echo We are running the pipeline for patientID:  25
echo Make sure you have run Demo 1 first!!
echo All outputs will be added to the Patient_25/DemoOut/ directory

# update scripts
/Applications/MATLAB_R2020b.app/bin/matlab -nodisplay -nosplash -r "addpath('DemoScripts/'),initializePipelineRun_demo,exit"

# get ROIs
/Applications/MATLAB_R2020b.app/bin/matlab -nodisplay -nosplash -r "addpath('Patient_25/DemoScripts/'),getROIs_demo,exit"

# run optimization
/Applications/MATLAB_R2020b.app/bin/matlab -nodisplay -nosplash -r "addpath('Patient_25/DemoScripts/'),TreeSearch_demo,exit"


exit



