#!/bin/bash

# go out to main repo directory
cd ..

date
echo "****************************************************************"
echo Welcome to sEEG OPT Version 1 DEMO 1
echo Developed by Grace Dessert, dessertgrace@gmail.com  with Brandon Thio and Dr. Warren M Grill 
echo Duke University, Department of Biomedical Engineering
echo "****************************************************************"
echo We are running the pipeline for patientID:  25
echo Make sure you have set your desired parameters in the Patient_25.config file
echo All outputs will be added to the Patient_25/DemoOut/ directory

# calculate cortex normals and face areas, make python scripts to make SCIRun networks to make LF,
#   generate all scripts with patient specific parameters and paths, make work directories, get midline, 
#   get skin insertion locations, get sulci surface, validate inputs and test collision accuracy
#nohup matlab -nodisplay -nosplash -r
#/Applications/MATLAB_R2023a.app/bin/matlab -nodisplay -nosplash -r "addpath('DemoScripts/'),initializePipelineRun_demo,exit"

# get contact locs for recordable radius study
#/Applications/MATLAB_R2023a.app/bin/matlab -nodisplay -nosplash -r "addpath('Patient_25/DemoScripts/')",getStContactLocs_demo,exit

# perform all surface precomputations
/Applications/MATLAB_R2023a.app/bin/matlab -nodisplay -nosplash -r "addpath('Patient_25/DemoScripts/')",LeadFieldMain_demo,exit

# precompute valid electrode locs, and get all valid (unique) contact locs (rec locs of LF)
/Applications/MATLAB_R2023a.app/bin/matlab -nodisplay -nosplash -r "addpath('Patient_25/DemoScripts/'),precomputeValidElectrodeLocs_demo,exit"

# make patchAll matrices (parallel with calcLF & calc intersections, and runs in parallel, so submit bash script)
/Applications/MATLAB_R2023a.app/bin/matlab -nodisplay -nosplash -r "addpath('Patient_25/DemoScripts/'),makePatchAllMatrices_demo,exit"

# calcuate intersections (parallel with calc LF, so submit bash script and get jobID)
/Applications/MATLAB_R2023a.app/bin/matlab -nodisplay -nosplash -r "addpath('Patient_25/DemoScripts/'),eCollCompute_lines_demo,exit"
