% quantify diff in recordability by cost function
% Grace Dessert
% Supplemental Figure 4efgh

%% FigureS4ef
% plot median source avg convergence plots
trialSelect = 4;
visMedCostSens(trialSelect)

%% FigureS4gh
% plot confusion matrices
for ROI_i = 2:3
    visCostSens(trialSelect,ROI_i)
end

function visMedCostSens(trialSelect)
    ROIs = ["clinicianROI","LTL","LH"]; 
    load(strcat('RawData/SensitivityAn/medSourceMeanCostSensConverg_source',num2str(trialSelect),'.mat'),'dataAllMean')
    
    % plot
    thrI = 2;
    thrs3Char = [200,500,100];
    thrPrioritys = ["200_500_1000_thrPriority","500_200_1000_thrPriority","1000_500_200_thrPriority","1000_200_500_thrPriority","500_1000_200_thrPriority","200_1000_500_thrPriority"];

    % select ROI and thr case
    for ROI_i = 2:3
        f = figure(1); %'visible'); %,'off');
        f.Position = [800 350 530 280];
        colors = lines(4);

        clf
        hold on
        % plot 6 median lines, with labels of source type for opt
        for costI = 1:6
            dat = dataAllMean{ROI_i,thrI,costI};
            colorI = find(thrs3Char==str2num(extractBetween(thrPrioritys(costI),1,3)));
            t = replace(thrPrioritys(costI),"_",", ");
            t = extractBetween(t,1,14);
            if costI <= 3
                plot([0:length(dat)],[0;dat]*100,'-','color',colors(colorI+1,:),'LineWidth',2,'DisplayName',t)
            else
                plot([0:length(dat)],[0;dat]*100,'--','color',colors(colorI+1,:),'LineWidth',2,'DisplayName',t)
            end
        end
        
        xlim([0,31])
        ylim([0,100])
        
        if ROI_i==2
            legend('location','southeast')
        end
        
        xlabel('Electrode additions')
        ylabel(strcat("Mean RS for ",ROIs(ROI_i)))
        ax = gca; 
        ax.FontSize = 26; 
        grid on
        
        if ROI_i == 2
            fileName = strcat("Figures/FigureS4e.pdf");
            savefig(f,strcat("Figures/FigureS4f.fig"))
        elseif ROI_i == 3
            fileName = strcat("Figures/FigureS4f.pdf");
            savefig(f,strcat("Figures/FigureS4f.fig"))
        end
        print(gcf,fileName,'-bestfit','-dpdf');
        
        matWrite = [0;dataAllMean{ROI_i,thrI,1}]'*100;
        for costI = 2:6
            matWrite = vertcat(matWrite,[0;dataAllMean{ROI_i,thrI,costI}]'*100);
        end
        matWrite = vertcat(string([0:length(dat)]),matWrite);
        matWrite = horzcat(["",thrPrioritys]',matWrite);
        if ROI_i == 2
            writematrix(matWrite,strcat('SourceData/FigureS4e.xls'))
        elseif ROI_i == 3
            writematrix(matWrite,strcat('SourceData/FigureS4f.xls'))
        end
        
    end
    
end


function visCostSens(trialSelect,ROI_i)
%% load and analyze and plot data
ROIs = ["clinicianROI","LTL","LH"]; 

load(strcat('RawData/SensitivityAn/diffRSCost_allPats_singleMap_ROI',ROIs(ROI_i),'_source',num2str(trialSelect),'.mat'),'diffRS_allPats')
for j = 1:12 % for each patient get perc error from best
    for thrI = 1:3
        maxThisThr = max(diffRS_allPats{j}(:,thrI));
        diffRS_allPats{j}(:,thrI) = (diffRS_allPats{j}(:,thrI)-maxThisThr)/maxThisThr*-1;
    end
end
avgDiff = zeros(6,3); % 6 costs, 3 thrs

numV = 0;
% get average perc error
for j = 1:12 % for each pat
    avgDiff = avgDiff + diffRS_allPats{j};
    numV = numV + 1;
end
avgDiff = avgDiff / numV;

% reorder 
order = [1,6,2,5,3,4];
avgDiff = avgDiff(order,:);

f = figure(1)
f.Position = [800 350 530 380];

clf
image(avgDiff*100,'CDataMapping','scaled')
colorbar
lims = caxis
caxis([0 50])

axis equal
ax = gca; 
ax.FontSize = 30; 

xlim([0.5,6.5])
ylim([0.5,6.5])
xticks([0.5:1:6.5])
yticks([0.5:1:6.5])
grid on
xlim([0.5,3.5])
xticklabels([])
yticklabels([])

%xticklabels(thrPrioritys(order))
thrPrioritys = ["200_500_1000_thrPriority","500_200_1000_thrPriority","1000_500_200_thrPriority","1000_200_500_thrPriority","500_1000_200_thrPriority","200_1000_500_thrPriority"];

labels = [];
for i = 1:6
    t = replace(thrPrioritys(i),"_",", ");
    t = extractBetween(t,1,14);
    labels = [labels,t];
end
% xlabel(labels(order))
if ROI_i == 2
    fileName = strcat("Figures/FigureS4g.pdf");
elseif ROI_i == 3
    fileName = strcat("Figures/FigureS4h.pdf");
end
print(gcf,fileName,'-bestfit','-dpdf');

matWrite = vertcat(string([200,500,1000]),avgDiff*100);
matWrite = horzcat(["",thrPrioritys]',matWrite);
if ROI_i == 2
    writematrix(matWrite,strcat('SourceData/FigureS4g.xls'))
elseif ROI_i == 3
    writematrix(matWrite,strcat('SourceData/FigureS4h.xls'))
end

end

